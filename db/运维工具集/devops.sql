-- spring cloud配置中心列表
CREATE TABLE config_server_list(
    id serial,
    name VARCHAR(128),
    configUrl VARCHAR(1024),
    appNames TEXT,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE
);

COMMENT ON TABLE config_server_list IS 'configServer配置';
COMMENT ON COLUMN config_server_list.id IS '主键';
COMMENT ON COLUMN config_server_list.name IS '名称';
COMMENT ON COLUMN config_server_list.configUrl IS '配置中心地址';
COMMENT ON COLUMN config_server_list.appNames IS '应用列表';
COMMENT ON COLUMN config_server_list.REVISION IS '乐观锁';
COMMENT ON COLUMN config_server_list.CREATED_BY IS '创建人';
COMMENT ON COLUMN config_server_list.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN config_server_list.UPDATED_BY IS '更新人';
COMMENT ON COLUMN config_server_list.UPDATED_TIME IS '更新时间';

-- 服务指标监控 配置信息表
create table tianyan_config(
    id serial,
    namespace varchar(512),
    label varchar(250),
    remark varchar(512),
    conf text,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
   PRIMARY KEY (id)
);
create unique index uidx_tianyan_config_01 on tianyan_config(namespace,label);

COMMENT ON TABLE tianyan_config IS '服务指标监控 配置信息表';
COMMENT ON COLUMN tianyan_config.id IS '主键';
COMMENT ON COLUMN tianyan_config.namespace IS '名称空间';
COMMENT ON COLUMN tianyan_config.label IS '标签：描述服务配置信息';
COMMENT ON COLUMN tianyan_config.remark IS '备注信息';
COMMENT ON COLUMN tianyan_config.conf IS '配置信息，用json描述';
COMMENT ON COLUMN tianyan_config.REVISION IS '乐观锁';
COMMENT ON COLUMN tianyan_config.CREATED_BY IS '创建人';
COMMENT ON COLUMN tianyan_config.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN tianyan_config.UPDATED_BY IS '更新人';
COMMENT ON COLUMN tianyan_config.UPDATED_TIME IS '更新时间';