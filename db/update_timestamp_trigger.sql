create or replace function upd_timestamp() returns trigger as
$$
begin
    new.UPDATED_TIME = current_timestamp;
    return new;
end
$$
language plpgsql;

-- create trigger t_name before update on ts for each row execute procedure upd_timestamp();