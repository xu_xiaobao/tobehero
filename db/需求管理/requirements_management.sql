-- 需求表
CREATE TABLE rm_demand(
    ID serial NOT NULL,
    name VARCHAR(128),
    department VARCHAR(128),
    proposer VARCHAR(32),
    describe VARCHAR(1024),
    leader VARCHAR(32),
    status VARCHAR(32),
    percent INT,
    plan_workload INT,
    plan_begin_date DATE,
    plan_test_date DATE,
    plan_online_date DATE,
    plan_finished_date DATE,
    real_begin_date DATE,
    real_test_date DATE,
    real_online_date DATE,
    real_finished_date DATE,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (ID)
);

COMMENT ON TABLE rm_demand IS '需求';
COMMENT ON COLUMN rm_demand.ID IS 'ID';
COMMENT ON COLUMN rm_demand.name IS '需求名称';
COMMENT ON COLUMN rm_demand.department IS '提出部门';
COMMENT ON COLUMN rm_demand.proposer IS '提出人';
COMMENT ON COLUMN rm_demand.describe IS '需求描述';
COMMENT ON COLUMN rm_demand.leader IS '需求负责人';
COMMENT ON COLUMN rm_demand.status IS '需求状态';
COMMENT ON COLUMN rm_demand.percent IS '需求进度百分比';
COMMENT ON COLUMN rm_demand.plan_workload IS '计划工作量';
COMMENT ON COLUMN rm_demand.plan_begin_date IS '计划开始时间';
COMMENT ON COLUMN rm_demand.plan_test_date IS '计划提测时间';
COMMENT ON COLUMN rm_demand.plan_online_date IS '计划上线时间';
COMMENT ON COLUMN rm_demand.plan_finished_date IS '计划完成时间';
COMMENT ON COLUMN rm_demand.real_begin_date IS '实际开始时间';
COMMENT ON COLUMN rm_demand.real_test_date IS '实际提测时间';
COMMENT ON COLUMN rm_demand.real_online_date IS '实际上线时间';
COMMENT ON COLUMN rm_demand.real_finished_date IS '实际完成时间';
COMMENT ON COLUMN rm_demand.REVISION IS '乐观锁';
COMMENT ON COLUMN rm_demand.CREATED_BY IS '创建人';
COMMENT ON COLUMN rm_demand.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN rm_demand.UPDATED_BY IS '更新人';
COMMENT ON COLUMN rm_demand.UPDATED_TIME IS '更新时间';

-- 需求指派人
CREATE TABLE rm_demand_designator(
    id serial NOT NULL,
    demand_id INT,
    username VARCHAR(64),
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    PRIMARY KEY (id)
);

COMMENT ON TABLE rm_demand_designator IS '需求指派人';
COMMENT ON COLUMN rm_demand_designator.id IS 'id';
COMMENT ON COLUMN rm_demand_designator.demand_id IS '需求ID';
COMMENT ON COLUMN rm_demand_designator.username IS '指派人';
COMMENT ON COLUMN rm_demand_designator.CREATED_BY IS '创建人';
COMMENT ON COLUMN rm_demand_designator.CREATED_TIME IS '创建时间';

create UNIQUE INDEX uidx_rm_demand_designator_01 on rm_demand_designator(demand_id,username);

-- 每日工作
CREATE TABLE rm_user_daily_job(
    id serial NOT NULL,
    cur_date DATE,
    emp_name VARCHAR(64),
    demand_id INT,
    stage VARCHAR(32),
    cost_hours INT,
    job_content VARCHAR(1024),
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (id)
);

COMMENT ON TABLE rm_user_daily_job IS '人员日报工作';
COMMENT ON COLUMN rm_user_daily_job.id IS 'ID';
COMMENT ON COLUMN rm_user_daily_job.cur_date IS '当天日期';
COMMENT ON COLUMN rm_user_daily_job.emp_name IS '人员姓名';
COMMENT ON COLUMN rm_user_daily_job.demand_id IS '需求ID';
COMMENT ON COLUMN rm_user_daily_job.stage IS '当前阶段';
COMMENT ON COLUMN rm_user_daily_job.cost_hours IS '花费时间';
COMMENT ON COLUMN rm_user_daily_job.job_content IS '工作内容';
COMMENT ON COLUMN rm_user_daily_job.REVISION IS '乐观锁';
COMMENT ON COLUMN rm_user_daily_job.CREATED_BY IS '创建人';
COMMENT ON COLUMN rm_user_daily_job.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN rm_user_daily_job.UPDATED_BY IS '更新人';
COMMENT ON COLUMN rm_user_daily_job.UPDATED_TIME IS '更新时间';

create unique index uidx_user_daily_job_01 on rm_user_daily_job(cur_date,emp_name,demand_id,stage);

-- 历史数据
CREATE TABLE rm_data_his(
    ID serial NOT NULL,
    table_name VARCHAR(32),
    his_data TEXT,
    VISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    PRIMARY KEY (ID)
);

COMMENT ON TABLE rm_data_his IS '数据修改记录';
COMMENT ON COLUMN rm_data_his.ID IS 'id';
COMMENT ON COLUMN rm_data_his.table_name IS '表名称';
COMMENT ON COLUMN rm_data_his.his_data IS '历史数据';
COMMENT ON COLUMN rm_data_his.VISION IS '数据版本';
COMMENT ON COLUMN rm_data_his.CREATED_BY IS '创建人';
COMMENT ON COLUMN rm_data_his.CREATED_TIME IS '创建时间';