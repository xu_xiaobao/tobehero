CREATE TABLE sys_name_config(
    sys_code VARCHAR(128) NOT NULL,
    sys_name VARCHAR(128),
    home_url VARCHAR(1024) NOT NULL,
    flag VARCHAR(1) DEFAULT '1' NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (sys_code)
);
create trigger trigger_sys_name_config_update before update on sys_name_config for each row execute procedure upd_timestamp();




COMMENT ON TABLE sys_name_config IS '系统配置表';
COMMENT ON COLUMN sys_name_config.sys_code IS '系统代码';
COMMENT ON COLUMN sys_name_config.sys_name IS '系统名称';
COMMENT ON COLUMN sys_name_config.home_url IS '首页';
COMMENT ON COLUMN sys_name_config.REVISION IS '乐观锁';
COMMENT ON COLUMN sys_name_config.CREATED_BY IS '创建人';
COMMENT ON COLUMN sys_name_config.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN sys_name_config.UPDATED_BY IS '更新人';
COMMENT ON COLUMN sys_name_config.UPDATED_TIME IS '更新时间';

create unique index uidx_sys_name  on sys_name_config(sys_name);

CREATE TABLE hero(
    username VARCHAR(64) NOT NULL,
    password VARCHAR(128),
    name VARCHAR(32),
    sex CHAR(1),
    phone VARCHAR(32),
    email VARCHAR(1024),
    flag VARCHAR(1) DEFAULT '1' NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (username)
);
create trigger trigger_hero_update before update on hero for each row execute procedure upd_timestamp();




COMMENT ON TABLE hero IS '用户表';
COMMENT ON COLUMN hero.username IS '用户名称';
COMMENT ON COLUMN hero.password IS '密码';
COMMENT ON COLUMN hero.name IS '姓名';
COMMENT ON COLUMN hero.sex IS '0:男1:女';
COMMENT ON COLUMN hero.phone IS '手机联系电话';
COMMENT ON COLUMN hero.email IS '电子邮箱';
COMMENT ON COLUMN hero.REVISION IS '乐观锁';
COMMENT ON COLUMN hero.CREATED_BY IS '创建人';
COMMENT ON COLUMN hero.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN hero.UPDATED_BY IS '更新人';
COMMENT ON COLUMN hero.UPDATED_TIME IS '更新时间';

CREATE TABLE hero_role(
    role_code VARCHAR(128) NOT NULL,
    sys_code VARCHAR(128) NOT NULL,
    role_name VARCHAR(128),
    remark VARCHAR(1024),
    flag VARCHAR(1) DEFAULT '1' NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (role_code,sys_code)
);



create trigger trigger_hero_role_update before update on hero_role for each row execute procedure upd_timestamp();

COMMENT ON TABLE hero_role IS '角色表';
COMMENT ON COLUMN hero_role.role_code IS '角色代码';
COMMENT ON COLUMN hero_role.sys_code IS '系统代码';
COMMENT ON COLUMN hero_role.role_name IS '角色名称';
COMMENT ON COLUMN hero_role.remark IS '备注';
COMMENT ON COLUMN hero_role.REVISION IS '乐观锁';
COMMENT ON COLUMN hero_role.CREATED_BY IS '创建人';
COMMENT ON COLUMN hero_role.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN hero_role.UPDATED_BY IS '更新人';
COMMENT ON COLUMN hero_role.UPDATED_TIME IS '更新时间';

CREATE TABLE hero_role_relation(
    username VARCHAR(128) NOT NULL,
    role_code VARCHAR(128) NOT NULL,
    sys_code VARCHAR(64) NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (username,role_code,sys_code)
);
create trigger trigger_hero_role_relation_update before update on hero_role_relation for each row execute procedure upd_timestamp();

COMMENT ON TABLE hero_role_relation IS '用户角色关系表';
COMMENT ON COLUMN hero_role_relation.username IS '用户名称';
COMMENT ON COLUMN hero_role_relation.role_code IS '角色代码';
COMMENT ON COLUMN hero_role_relation.sys_code IS '系统代码';
COMMENT ON COLUMN hero_role_relation.REVISION IS '乐观锁';
COMMENT ON COLUMN hero_role_relation.CREATED_BY IS '创建人';
COMMENT ON COLUMN hero_role_relation.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN hero_role_relation.UPDATED_BY IS '更新人';
COMMENT ON COLUMN hero_role_relation.UPDATED_TIME IS '更新时间';

CREATE TABLE resources(
    res_code VARCHAR(128) NOT NULL,
    sys_code VARCHAR(128) NOT NULL,
    parent_code VARCHAR(128),
    res_type VARCHAR(32),
    res_name VARCHAR(128),
    res_path VARCHAR(1024),
    menu_display_no INT,
    menu_ico_class VARCHAR(64),
    remark VARCHAR(3072),
    flag VARCHAR(1) DEFAULT '1' NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (res_code,sys_code)
);
create trigger trigger_resources_update before update on resources for each row execute procedure upd_timestamp();

COMMENT ON TABLE resources IS '资源表';
COMMENT ON COLUMN resources.res_code IS '资源代码';
COMMENT ON COLUMN resources.sys_code IS '系统代码';
COMMENT ON COLUMN resources.parent_code IS '顶级资源ID父ID为自身';
COMMENT ON COLUMN resources.res_type IS 'ext_api:外部接口
in_api:内部接口
menu:菜单
page:页面元素';
COMMENT ON COLUMN resources.res_name IS '资源名称';
COMMENT ON COLUMN resources.res_path IS '针对不同类型的数据存在不同含义
外部接口/内部接口:path为内部访问路径
菜单类型：path为菜单路径
页面元素:可以为空';
COMMENT ON COLUMN resources.menu_display_no IS '菜单展示顺序';
COMMENT ON COLUMN resources.menu_ico_class IS '菜单图标类';
COMMENT ON COLUMN resources.remark IS '备注';
COMMENT ON COLUMN resources.REVISION IS '乐观锁';
COMMENT ON COLUMN resources.CREATED_BY IS '创建人';
COMMENT ON COLUMN resources.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN resources.UPDATED_BY IS '更新人';
COMMENT ON COLUMN resources.UPDATED_TIME IS '更新时间';

CREATE TABLE role_resources_relation(
    sys_code VARCHAR(128) NOT NULL,
    role_code VARCHAR(128) NOT NULL,
    res_code VARCHAR(128) NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (sys_code,role_code,res_code)
);
create trigger trigger_role_resources_relation before update on role_resources_relation for each row execute procedure upd_timestamp();

COMMENT ON TABLE role_resources_relation IS '角色资源表';
COMMENT ON COLUMN role_resources_relation.sys_code IS '系统代码';
COMMENT ON COLUMN role_resources_relation.role_code IS '角色代码';
COMMENT ON COLUMN role_resources_relation.res_code IS '资源代码';
COMMENT ON COLUMN role_resources_relation.REVISION IS '乐观锁';
COMMENT ON COLUMN role_resources_relation.CREATED_BY IS '创建人';
COMMENT ON COLUMN role_resources_relation.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN role_resources_relation.UPDATED_BY IS '更新人';
COMMENT ON COLUMN role_resources_relation.UPDATED_TIME IS '更新时间';


INSERT INTO sys_name_config("sys_code", "sys_name", "home_url", "flag", "revision")
VALUES ('USER_MANAGE', '用户系统', '/user_sys', 1, 1);
INSERT INTO hero_role("role_code", "sys_code", "role_name", "remark", "flag", "revision")
VALUES ('SUPER_ADMIN', 'USER_MANAGE', '超级管理员', NULL, 1,1);
INSERT INTO hero("username", "password", "name", "sex", "phone", "email", "flag", "revision")
VALUES ('admin', '123456', '许小宝', '0', '18701100183', '403543653@qq.com', 1, 1);
INSERT INTO hero_role_relation("username", "role_code", "sys_code", "revision")
VALUES ('admin', 'SUPER_ADMIN', 'USER_MANAGE', 1);


/**
菜单案例
|-- 系统管理
    |-- 系统名称维护


 */

INSERT INTO "public"."resources"("res_code", "sys_code", "parent_code", "res_type", "res_name", "res_path", "menu_display_no", "menu_ico_class", "remark", "flag", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('SYS_GL_USER_MANAGE', 'USER_MANAGE', 'SYS_GL', 'menu', '用户管理', NULL, 4, NULL, '', 1, NULL, NULL, '2020-12-06 21:07:02.931145', NULL, '2020-12-06 21:10:58.017045');
INSERT INTO "public"."resources"("res_code", "sys_code", "parent_code", "res_type", "res_name", "res_path", "menu_display_no", "menu_ico_class", "remark", "flag", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('SYS_GL_ROLE_MANAGE', 'USER_MANAGE', 'SYS_GL', 'menu', '角色维护', NULL, 5, NULL, NULL, 1, NULL, NULL, '2020-12-06 21:08:40.835733', NULL, '2020-12-06 21:11:02.102797');
INSERT INTO "public"."resources"("res_code", "sys_code", "parent_code", "res_type", "res_name", "res_path", "menu_display_no", "menu_ico_class", "remark", "flag", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('SYS_GL_RESOURCE_MANAGE', 'USER_MANAGE', 'SYS_GL', 'menu', '资源维护', NULL, 6, NULL, NULL, 1, NULL, NULL, '2020-12-06 21:08:40.835733', NULL, '2020-12-06 21:11:02.102797');
INSERT INTO "public"."role_resources_relation"("sys_code", "role_code", "res_code", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('USER_MANAGE', 'SUPER_ADMIN', 'SYS_GL_USER_MANAGE', NULL, NULL, NULL, NULL, '2020-12-06 21:12:44.150973');
INSERT INTO "public"."role_resources_relation"("sys_code", "role_code", "res_code", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('USER_MANAGE', 'SUPER_ADMIN', 'SYS_GL_ROLE_MANAGE', NULL, NULL, NULL, NULL, '2020-12-06 21:12:44.150973');
INSERT INTO "public"."role_resources_relation"("sys_code", "role_code", "res_code", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('USER_MANAGE', 'SUPER_ADMIN', 'SYS_GL_RESOURCE_MANAGE', NULL, NULL, NULL, NULL, '2020-12-06 21:12:44.150973');

-- 数据字典表
CREATE TABLE sys_dic(
    id serial NOT NULL,
    key VARCHAR(1024),
    type VARCHAR(32),
    val TEXT,
    remark VARCHAR(128),
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (id)
);

COMMENT ON TABLE sys_dic IS '系统字典表';
COMMENT ON COLUMN sys_dic.id IS 'id';
COMMENT ON COLUMN sys_dic.type IS '字典类型';
COMMENT ON COLUMN sys_dic.remark IS '备注';
COMMENT ON COLUMN sys_dic.REVISION IS '乐观锁';
COMMENT ON COLUMN sys_dic.CREATED_BY IS '创建人';
COMMENT ON COLUMN sys_dic.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN sys_dic.UPDATED_BY IS '更新人';
COMMENT ON COLUMN sys_dic.UPDATED_TIME IS '更新时间';

CREATE unique index uidx_sys_dic_01 on sys_dic(key);