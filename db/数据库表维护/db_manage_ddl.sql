CREATE TABLE db_table(
    id Serial,
    sys_code VARCHAR(128) NOT NULL,
    db_type VARCHAR(128) NOT NULL,
    name VARCHAR(128),
    logic_name VARCHAR(128),
    remark VARCHAR(1024),
    flag VARCHAR(1) DEFAULT 1,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (id)
);

create trigger trigger_db_table_update before update on db_table for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_table IS '表名';
COMMENT ON COLUMN db_table.id IS '主键';
COMMENT ON COLUMN db_table.sys_code IS '系统代码';
COMMENT ON COLUMN db_table.db_type IS '数据库类型';
COMMENT ON COLUMN db_table.name IS '表名称';
COMMENT ON COLUMN db_table.logic_name IS '逻辑名称';
COMMENT ON COLUMN db_table.remark IS '备注';
COMMENT ON COLUMN db_table.flag IS '有效标识';
COMMENT ON COLUMN db_table.REVISION IS '乐观锁';
COMMENT ON COLUMN db_table.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_table.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_table.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_table.UPDATED_TIME IS '更新时间';

CREATE TABLE db_column(
    id Serial,
    tab_id int,
    name VARCHAR(128),
    logic_name VARCHAR(128),
    type_id INT,
    type_size int,
    type_decimal int,
    is_pk VARCHAR(1) DEFAULT 0,
    is_not_null VARCHAR(1) DEFAULT 0,
    is_auto_incre VARCHAR(1) DEFAULT 0,
    default_val VARCHAR(1024),
    remark VARCHAR(1024),
    flag VARCHAR(1) DEFAULT 1,
    display_no int,
    is_enum VARCHAR(1) DEFAULT 0,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (id)
);

create trigger trigger_db_column_update before update on db_column for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_column IS '字段表';
COMMENT ON COLUMN db_column.id IS '主键';
COMMENT ON COLUMN db_column.name IS '字段名称';
COMMENT ON COLUMN db_column.logic_name IS '字段逻辑名称';
COMMENT ON COLUMN db_column.type_id IS '字段类型ID';
COMMENT ON COLUMN db_column.is_pk IS '是否主键';
COMMENT ON COLUMN db_column.is_not_null IS '是否非空';
COMMENT ON COLUMN db_column.is_auto_incre IS '是否自增';
COMMENT ON COLUMN db_column.default_val IS '默认值';
COMMENT ON COLUMN db_column.remark IS '备注';
COMMENT ON COLUMN db_column.flag IS '有效标识';
COMMENT ON COLUMN db_column.is_enum IS '是否有枚举值';
COMMENT ON COLUMN db_column.REVISION IS '乐观锁';
COMMENT ON COLUMN db_column.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_column.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_column.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_column.UPDATED_TIME IS '更新时间';


CREATE TABLE db_tab_col_relation(
    id INT NOT NULL,
    tab_id INT,
    col_id INT,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (id)
);

create trigger trigger_db_tab_col_relation_update before update on db_tab_col_relation for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_tab_col_relation IS '表字段关系表';
COMMENT ON COLUMN db_tab_col_relation.id IS '主键';
COMMENT ON COLUMN db_tab_col_relation.tab_id IS '表名ID';
COMMENT ON COLUMN db_tab_col_relation.col_id IS '字段ID';
COMMENT ON COLUMN db_tab_col_relation.REVISION IS '乐观锁';
COMMENT ON COLUMN db_tab_col_relation.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_tab_col_relation.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_tab_col_relation.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_tab_col_relation.UPDATED_TIME IS '更新时间';

CREATE TABLE db_col_def(
    id SERIAL,
    db_type VARCHAR(128),
    name VARCHAR(128),
    java_type VARCHAR(128),
    needSize VARCHAR(1),
    needDecimal VARCHAR(1),
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (id)
);

create trigger trigger_db_col_def_update before update on db_col_def for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_col_def IS '数据库字段定义表';
COMMENT ON COLUMN db_col_def.id IS '主键';
COMMENT ON COLUMN db_col_def.db_type IS '数据库类型';
COMMENT ON COLUMN db_col_def.name IS '字段类型名称';
COMMENT ON COLUMN db_col_def.java_type IS 'java类型';
COMMENT ON COLUMN db_col_def.needSize IS '是否有长度';
COMMENT ON COLUMN db_col_def.needDecimal IS '是否有小数点';
COMMENT ON COLUMN db_col_def.REVISION IS '乐观锁';
COMMENT ON COLUMN db_col_def.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_col_def.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_col_def.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_col_def.UPDATED_TIME IS '更新时间';


CREATE TABLE db_enum_dic(
    id Serial,
    col_id INT,
    val VARCHAR(128),
    describe VARCHAR(128),
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (id)
);

create trigger trigger_db_enum_dic_update before update on db_enum_dic for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_enum_dic IS '数据表枚举字段可选值字典表';
COMMENT ON COLUMN db_enum_dic.id IS '主键';
COMMENT ON COLUMN db_enum_dic.col_id IS '表字段关系ID';
COMMENT ON COLUMN db_enum_dic.val IS '可选值';
COMMENT ON COLUMN db_enum_dic.describe IS '值说明';
COMMENT ON COLUMN db_enum_dic.REVISION IS '乐观锁';
COMMENT ON COLUMN db_enum_dic.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_enum_dic.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_enum_dic.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_enum_dic.UPDATED_TIME IS '更新时间';

CREATE TABLE db_table_tag_def(
    ID serial NOT NULL,
    sys_code VARCHAR(128) NOT NULL,
    tag_name VARCHAR(32) NOT NULL,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (ID)
);
create trigger trigger_db_table_tag_def_update before update on db_table_tag_def for each row execute procedure upd_timestamp();
CREATE UNIQUE INDEX uidx_db_table_tag_def_01 ON db_table_tag_def(sys_code,tag_name);
COMMENT ON TABLE db_table_tag_def IS '表标签定义';
COMMENT ON COLUMN db_table_tag_def.ID IS '主键';
COMMENT ON COLUMN db_table_tag_def.sys_code IS '系统代码';
COMMENT ON COLUMN db_table_tag_def.tag_name IS '标签名称';
COMMENT ON COLUMN db_table_tag_def.REVISION IS '乐观锁';
COMMENT ON COLUMN db_table_tag_def.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_table_tag_def.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_table_tag_def.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_table_tag_def.UPDATED_TIME IS '更新时间';

CREATE TABLE db_table_tag_relation(
    ID serial NOT NULL,
    tab_id INT,
    tag_id INT,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (ID)
);
create trigger trigger_db_table_tag_relation_update before update on db_table_tag_relation for each row execute procedure upd_timestamp();
COMMENT ON TABLE db_table_tag_relation IS '表标签';
COMMENT ON COLUMN db_table_tag_relation.ID IS '主键ID';
COMMENT ON COLUMN db_table_tag_relation.tab_id IS '表ID';
COMMENT ON COLUMN db_table_tag_relation.tag_id IS '标签ID';
COMMENT ON COLUMN db_table_tag_relation.REVISION IS '乐观锁';
COMMENT ON COLUMN db_table_tag_relation.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_table_tag_relation.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_table_tag_relation.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_table_tag_relation.UPDATED_TIME IS '更新时间';


CREATE TABLE db_datasource(
    id Serial,
    sys_code VARCHAR(128),
    name VARCHAR(128),
    jdbc_url TEXT,
    username VARCHAR(1024),
    password TEXT,
    flag VARCHAR(1) DEFAULT 1,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME TIMESTAMP,
    PRIMARY KEY (id)
);
create trigger trigger_db_datasource_update before update on db_datasource for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_datasource IS '数据源配置';
COMMENT ON COLUMN db_datasource.id IS '主键ID';
COMMENT ON COLUMN db_datasource.name IS '数据源名称';
COMMENT ON COLUMN db_datasource.sys_code IS '系统代码';
COMMENT ON COLUMN db_datasource.jdbc_url IS 'jdbcUrl';
COMMENT ON COLUMN db_datasource.username IS '用户名';
COMMENT ON COLUMN db_datasource.password IS '密码';
COMMENT ON COLUMN db_datasource.flag IS '数据有效标识';
COMMENT ON COLUMN db_datasource.REVISION IS '乐观锁';
COMMENT ON COLUMN db_datasource.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_datasource.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_datasource.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_datasource.UPDATED_TIME IS '更新时间';

/*
  数据库字段定义别名映射
  将数据库实际字段类型转化为系统内展示别名
*/
CREATE TABLE db_col_def_alias(
    alias_name VARCHAR(128) NOT NULL,
    db_type VARCHAR(128) NOT NULL,
    col_type_name VARCHAR(128),
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (alias_name,db_type)
);
create trigger trigger_db_col_def_alias_update before update on db_col_def_alias for each row execute procedure upd_timestamp();

COMMENT ON TABLE db_col_def_alias IS '数据库字段别名表';
COMMENT ON COLUMN db_col_def_alias.alias_name IS '数据库类型别名';
COMMENT ON COLUMN db_col_def_alias.db_type IS '数据库类型';
COMMENT ON COLUMN db_col_def_alias.col_type_name IS '数据库字段类型';
COMMENT ON COLUMN db_col_def_alias.REVISION IS '乐观锁';
COMMENT ON COLUMN db_col_def_alias.CREATED_BY IS '创建人';
COMMENT ON COLUMN db_col_def_alias.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN db_col_def_alias.UPDATED_BY IS '更新人';
COMMENT ON COLUMN db_col_def_alias.UPDATED_TIME IS '更新时间';

INSERT INTO "db_col_def_alias"("alias_name", "db_type", "col_type_name", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('INT', 'POSTGRESQL', 'INT4', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "db_col_def_alias"("alias_name", "db_type", "col_type_name", "revision", "created_by", "created_time", "updated_by", "updated_time") VALUES ('DECIMAL', 'POSTGRESQL', 'NUMERIC', NULL, NULL, NULL, NULL, NULL);


