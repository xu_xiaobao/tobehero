CREATE DATABASE IF NOT EXISTS hero DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- drop table if exists sys_name_config;
CREATE TABLE sys_name_config(
    sys_code VARCHAR(128) NOT NULL COMMENT '系统代码',
    sys_name VARCHAR(128) COMMENT '系统名称',
    home_url VARCHAR(1024) NOT NULL COMMENT '首页',
    flag VARCHAR(1) DEFAULT '1' NOT NULL COMMENT '',
    REVISION INT COMMENT '乐观锁',
    CREATED_BY VARCHAR(32) COMMENT '创建人',
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    UPDATED_BY VARCHAR(32) COMMENT '更新人',
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (sys_code)
) COMMENt '系统名称配置表';

create unique index uidx_sys_name  on sys_name_config(sys_name);

-- drop table if exists  hero;
CREATE TABLE hero(
    username VARCHAR(64) NOT NULL COMMENT '用户名称',
    password VARCHAR(128) COMMENT '密码',
    name VARCHAR(32) COMMENT '姓名',
    sex CHAR(1) COMMENT '0:男1:女',
    phone VARCHAR(32) COMMENT '手机联系电话',
    email VARCHAR(1024) COMMENT '电子邮箱',
    flag VARCHAR(1) DEFAULT '1' NOT NULL COMMENT '',
    REVISION INT COMMENT '乐观锁',
    CREATED_BY VARCHAR(32) COMMENT '创建人',
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    UPDATED_BY VARCHAR(32) COMMENT '更新人',
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (username)
) COMMENT '用户表';

-- drop table if exists  hero_role;
CREATE TABLE hero_role(
    role_code VARCHAR(128) NOT NULL COMMENT '角色代码',
    sys_code VARCHAR(128) NOT NULL COMMENT '系统代码',
    role_name VARCHAR(128) COMMENT '角色名称',
    remark VARCHAR(1024) COMMENT '备注',
    flag VARCHAR(1) DEFAULT '1' NOT NULL COMMENT '',
    REVISION INT COMMENT '乐观锁',
    CREATED_BY VARCHAR(32) COMMENT '创建人',
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    UPDATED_BY VARCHAR(32) COMMENT '更新人',
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (role_code,sys_code)
) COMMENT '角色表';

-- drop table if exists hero_role_relation;
CREATE TABLE hero_role_relation(
    username VARCHAR(128) NOT NULL COMMENT '用户名称',
    role_code VARCHAR(128) NOT NULL COMMENT '角色代码',
    sys_code VARCHAR(64) NOT NULL COMMENT '系统代码',
    REVISION INT COMMENT '乐观锁',
    CREATED_BY VARCHAR(32) COMMENT '创建人',
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    UPDATED_BY VARCHAR(32) COMMENT '更新人',
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (username,role_code,sys_code)
) COMMENT '用户角色关系表';

-- drop table if exists resources;
CREATE TABLE resources(
    res_code VARCHAR(128) NOT NULL COMMENT '资源代码',
    sys_code VARCHAR(128) NOT NULL COMMENT '系统代码',
    parent_code VARCHAR(128) COMMENT '顶级资源ID父ID为自身',
    res_type VARCHAR(32) COMMENT 'ext_api:外部接口,in_api:内部接口,menu:菜单,page:页面元素',
    res_name VARCHAR(128) COMMENT '资源名称',
    res_path VARCHAR(1024) COMMENT '针对不同类型的数据存在不同含义
外部接口/内部接口:path为内部访问路径
菜单类型：path为菜单路径
页面元素:可以为空',
    menu_display_no INT COMMENT '菜单展示顺序',
    menu_ico_class VARCHAR(64) COMMENT '菜单图标类',
    remark VARCHAR(3072) COMMENT '备注',
    flag VARCHAR(1) DEFAULT '1' NOT NULL COMMENT '',
    REVISION INT COMMENT '乐观锁',
    CREATED_BY VARCHAR(32) COMMENT '创建人',
    CREATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    UPDATED_BY VARCHAR(32) COMMENT '更新人',
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (res_code,sys_code)
) COMMENT '资源表';

-- drop table if exists role_resources_relation;
CREATE TABLE role_resources_relation(
    sys_code VARCHAR(128) NOT NULL COMMENT '系统代码',
    role_code VARCHAR(128) NOT NULL COMMENT '角色代码',
    res_code VARCHAR(128) NOT NULL COMMENT '资源代码',
    REVISION INT COMMENT '乐观锁',
    CREATED_BY VARCHAR(32) COMMENT '创建人',
    CREATED_TIME DATE COMMENT '创建时间',
    UPDATED_BY VARCHAR(32) COMMENT '更新人',
    UPDATED_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (sys_code,role_code,res_code)
) COMMENT '角色资源表';


-- 添加角色用户
INSERT INTO sys_name_config(sys_code, sys_name, home_url, flag, revision)
VALUES ('USER_MANAGE', '用户系统', '/user_sys', 1, 1);
INSERT INTO hero_role(role_code, sys_code, role_name, remark, flag, revision)
VALUES ('SUPER_ADMIN', 'USER_MANAGE', '超级管理员', NULL, 1,1);
INSERT INTO hero(username, password, name, sex, phone, email, flag, revision)
VALUES ('admin', '123456', '许小宝', '0', '18701100183', '403543653@qq.com', 1, 1);
INSERT INTO hero_role_relation(username, role_code, sys_code, revision)
VALUES ('admin', 'SUPER_ADMIN', 'USER_MANAGE', 1);



/**
菜单案例
|-- 系统管理
    |-- 系统名称维护


 */

INSERT INTO resources(res_code, sys_code, parent_code, res_type, res_name, res_path, menu_display_no, menu_ico_class, remark, flag, revision, created_by) VALUES ('SYS_GL_USER_MANAGE', 'USER_MANAGE', 'SYS_GL', 'menu', '用户管理', NULL, 4, NULL, '', 1, NULL, NULL);
INSERT INTO resources(res_code, sys_code, parent_code, res_type, res_name, res_path, menu_display_no, menu_ico_class, remark, flag, revision, created_by) VALUES ('SYS_GL_ROLE_MANAGE', 'USER_MANAGE', 'SYS_GL', 'menu', '角色维护', NULL, 5, NULL, NULL, 1, NULL, NULL);
INSERT INTO resources(res_code, sys_code, parent_code, res_type, res_name, res_path, menu_display_no, menu_ico_class, remark, flag, revision, created_by) VALUES ('SYS_GL_RESOURCE_MANAGE', 'USER_MANAGE', 'SYS_GL', 'menu', '资源维护', NULL, 6, NULL, NULL, 1, NULL, NULL);
INSERT INTO role_resources_relation(sys_code, role_code, res_code, revision, created_by) VALUES ('USER_MANAGE', 'SUPER_ADMIN', 'SYS_GL_USER_MANAGE', NULL, NULL);
INSERT INTO role_resources_relation(sys_code, role_code, res_code, revision, created_by) VALUES ('USER_MANAGE', 'SUPER_ADMIN', 'SYS_GL_ROLE_MANAGE', NULL, NULL);
INSERT INTO role_resources_relation(sys_code, role_code, res_code, revision, created_by) VALUES ('USER_MANAGE', 'SUPER_ADMIN', 'SYS_GL_RESOURCE_MANAGE', NULL, NULL);

-- 数据字典表
CREATE TABLE sys_dic(
    id serial NOT NULL,
    key VARCHAR(1024),
    type VARCHAR(32),
    val TEXT,
    remark VARCHAR(128),
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (id)
);

COMMENT ON TABLE sys_dic IS '系统字典表';
COMMENT ON COLUMN sys_dic.id IS 'id';
COMMENT ON COLUMN sys_dic.type IS '字典类型';
COMMENT ON COLUMN sys_dic.remark IS '备注';
COMMENT ON COLUMN sys_dic.REVISION IS '乐观锁';
COMMENT ON COLUMN sys_dic.CREATED_BY IS '创建人';
COMMENT ON COLUMN sys_dic.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN sys_dic.UPDATED_BY IS '更新人';
COMMENT ON COLUMN sys_dic.UPDATED_TIME IS '更新时间';

CREATE unique index uidx_sys_dic_01 on sys_dic(key);