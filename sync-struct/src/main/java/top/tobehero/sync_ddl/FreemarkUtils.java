package top.tobehero.sync_ddl;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class FreemarkUtils {

    private static final Configuration cfg = new Configuration(Configuration.VERSION_2_3_0);
    static {
        URL resource = FreemarkUtils.class.getResource("/freemark/templates");
        String path = resource.getPath();
        // Specify the source where the template files come from. Here I set a
        // plain directory for it, but non-file-system sources are possible too:
        try {
            path = URLDecoder.decode(path, "UTF-8");
            cfg.setDirectoryForTemplateLoading(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the preferred charset template files are stored in. UTF-8 is
        // a good choice in most applications:
        cfg.setDefaultEncoding("UTF-8");

        // Sets how errors will appear.
        // During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    public static String getContent(String template,Object modelData){
        try {
            Template temp = cfg.getTemplate(template);
            StringWriter sw = new StringWriter();
            temp.process(modelData, sw);
            return sw.toString();
        }catch (IOException | TemplateException e) {
            e.printStackTrace(System.err);
            throw new RuntimeException("freemarker模板处理失败");
        }
    }

    public static void main(String[] args) throws Exception {
        Template template = cfg.getTemplate("mysql/create_table_all.ftl");
        StringWriter sw = new StringWriter();
        Map<String, Object> dataModel = new HashMap<>();
        dataModel.put("tableName","许宝众");
        template.process(dataModel, sw);
        System.out.println(sw.toString());
    }

}
