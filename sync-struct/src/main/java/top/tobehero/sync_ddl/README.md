# 根据基础表同步目标表表结构（适用MYSQL）
## 基础知识
```sql
-- 查询Tables 通过jdbc查询
show tables;

-- 查询表信息
desc tableName;
select * from information_schema.tables where table_schema = 'hero' ;

-- 查询编码与排序规则
select * from information_schema.COLLATIONS order by CHARACTER_SET_NAME;

-- 查询表字段信息
select * from information_schema.`COLUMNS` c where TABLE_SCHEMA = 'hero' and TABLE_NAME ='test'

-- 查询索引信息
show keys from hero.test;

```
- [创建表语句](https://dev.mysql.com/doc/refman/8.0/en/create-table.html)
