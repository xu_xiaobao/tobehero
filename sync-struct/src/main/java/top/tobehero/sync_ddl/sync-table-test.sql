drop table  if exists test ;
CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `_char` char(10) CHARACTER SET utf8 DEFAULT '0' COMMENT 'char 类型测试',
  `_varchar` varchar(100) CHARACTER SET utf8 DEFAULT '' COMMENT 'varchar 类型测试',
  `_int` int(11) DEFAULT '0' COMMENT 'int 类型测试',
  `_bigint` bigint(10) DEFAULT '0' COMMENT 'bigint 类型测试',
  `_float` float DEFAULT '0' COMMENT 'float 类型测试',
  `_boolean` tinyint(1) DEFAULT '0' COMMENT 'boolean 类型测试',
  `_date` date DEFAULT NULL COMMENT 'date 类型测试',
  `_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'timestamp 类型测试',
  `_text` text CHARACTER SET utf8 COMMENT 'text 类型测试',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_test_varchar` (`_char`),
  UNIQUE KEY `idx_test_lianhe` (`_int`,`_bigint`),
  KEY `idx_test_char` (`_char`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='测试表';

-- 如果建表时主键自增，需要先去掉自增
alter table test modify id int ;
-- 删除主键
alter table test drop primary key;
-- 修改主键
alter table test add primary key(id);