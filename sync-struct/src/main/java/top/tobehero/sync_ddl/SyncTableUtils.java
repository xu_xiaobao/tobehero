package top.tobehero.sync_ddl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import top.tobehero.sync_ddl.mysql.MysqlDBInfoParser;
import top.tobehero.sync_ddl.mysql.info.IndexKeyInfo;
import top.tobehero.sync_ddl.mysql.log.*;
import top.tobehero.sync_ddl.mysql.log.parser.*;
import top.tobehero.sync_ddl.mysql.model.MysqlColumn;
import top.tobehero.sync_ddl.mysql.model.MysqlTable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 表结构同步工具类
 */
@Slf4j
public class SyncTableUtils {
    /**
     * 操作类型
     * table: 表操作
     */
    public static final String OPERATE_TYPE_TABLE = "table";
    /**
     * 操作类型
     * column: 列操作
     */
    public static final String OPERATE_TYPE_COLUMN = "column";
    /**
     * 操作类型
     * primary_key: 主键操作
     */
    public static final String OPERATE_TYPE_PRIMARY_KEY = "primary_key";
    /**
     * 操作类型
     * index: 索引操作
     */
    public static final String OPERATE_TYPE_INDEX = "index";

    public enum ErrorEnum {
        TABLE_NOT_EXISTS("source table not exist", "源表不存在", OPERATE_TYPE_TABLE),
        ;


        private String code;
        private String msg;
        private String operateType;

        ErrorEnum(String code, String msg, String operateType) {
            this.code = code;
            this.msg = msg;
            this.operateType = operateType;
        }
    }

    /**
     * 批量同步表结构
     *
     * @param dbInfoParser 解析器
     * @param srcTable     源表
     * @param desTables    目标表集合
     * @param createTable  是否自动创建表，true:当目标表不存在是自动创建，false：抛出异常
     */
    public static void batchExecuteSyncOperLogs(MysqlDBInfoParser dbInfoParser, String srcTable, String[] desTables, boolean createTable) throws SQLException {
        List<AbstractSyncOperLogParser> operLogParsers = new ArrayList<>();
        operLogParsers.add(new CreateTableSyncOperLogParser());
        operLogParsers.add(new SyncColumnOperLogParser());
        operLogParsers.add(new SyncPrimaryKeyOperLogParser());
        operLogParsers.add(new SyncIndexKeyOperLogParser());
        operLogParsers.add(new SyncColumnOrderOperLogParser());
        SyncOperLogParserDelegate syncOperLogParserDelegate = new SyncOperLogParserDelegate(operLogParsers);
        //判断表是否存在
        for (int i = 0; i < desTables.length; i++) {
            String desTable = desTables[i];
            List<SyncOperLog> syncOperLogs = diffAndGetSyncOperLogs(dbInfoParser, srcTable, desTable, createTable);
//            printOperLogs(syncOperLogs);
            //执行同步表结构操作日志
            executeSyncLogs(dbInfoParser.getConn(), syncOperLogParserDelegate, syncOperLogs);
            syncOperLogs.clear();
            //同步表字段顺序
            syncColumnsOrder(dbInfoParser, srcTable, syncOperLogParserDelegate, desTable, syncOperLogs);
        }
    }

    /**
     * 同步表字段顺序
     * @param dbInfoParser
     * @param srcTable
     * @param syncOperLogParserDelegate
     * @param desTable
     * @param syncOperLogs
     * @throws SQLException
     */
    private static void syncColumnsOrder(MysqlDBInfoParser dbInfoParser, String srcTable, SyncOperLogParserDelegate syncOperLogParserDelegate, String desTable, List<SyncOperLog> syncOperLogs) throws SQLException {
        MysqlTable srcTableInfo = dbInfoParser.getTableInfo(srcTable);
        MysqlTable desTableInfo = dbInfoParser.getTableInfo(desTable);
        List<MysqlColumn> srcCols = srcTableInfo.getCols();
        List<MysqlColumn> desCols = desTableInfo.getCols();
        boolean isSame = srcCols.size() == desCols.size();
        if (!isSame){
            log.info("[源表({})和目标表({})列不一致，无法进行排序]",srcTableInfo.getTableName(),desTableInfo.getTableName());
            return;
        }
        for (int i = 0; i < srcCols.size(); i++) {
            MysqlColumn srcCol = srcCols.get(i);
            MysqlColumn desCol = desCols.get(i);
            if (!srcCol.getColumnName().equalsIgnoreCase(desCol.getColumnName())){
                isSame = false;
            }
        }
        if (!isSame){
            List<MysqlColumn> orderCols = new ArrayList<>();
            srcCols.forEach(srcCol -> {
                MysqlColumn MysqlColumn = desCols.stream().filter((desCol) -> srcCol.getColumnName().equalsIgnoreCase(desCol.getColumnName())).findFirst().get();
                orderCols.add(MysqlColumn);
            });
            desTableInfo.setCols(orderCols);
            SyncColsOrderOperLog syncColOrderOperLog = new SyncColsOrderOperLog();
            syncColOrderOperLog.setTableInfo(desTableInfo);
            syncOperLogs.add(syncColOrderOperLog);
            executeSyncLogs(dbInfoParser.getConn(), syncOperLogParserDelegate, syncOperLogs);
        }
    }

    /**
     * 执行同步日志
     *
     * @param conn
     * @param syncOperLogParserDelegate
     * @param syncOperLogs
     */
    private static void executeSyncLogs(Connection conn, SyncOperLogParserDelegate syncOperLogParserDelegate, List<SyncOperLog> syncOperLogs) {
        List<SyncOperLog> undoLogs = new ArrayList<>();
        boolean undo = false;
        try {
            for (SyncOperLog syncOperLog : syncOperLogs) {
                String sql = syncOperLogParserDelegate.getSql(syncOperLog);
                log.info("[执行同步日志]-[{}]\nSQL:\n{}", syncOperLog, sql);
                //执行SQL
                executeSql(conn, sql);
                //加入回滚日志
                undoLogs.add(syncOperLog);
            }
        } catch (Exception e) {
            undo = undoLogs.size() != 0;
            log.error("表同步发生异常", e);
        } finally {
            if (undo) {
                //执行回滚操作
                for (SyncOperLog undoLog : undoLogs) {
                    String undoSql = syncOperLogParserDelegate.getUndoSql(undoLog);
                    //执行SQL
                    try {
                        executeSql(conn, undoSql);
                    } catch (SQLException e) {
                        log.error("【回滚失败】", e);
                    }
                }
            }
        }
    }

    /*
     * 执行SQL
     */
    private static void executeSql(Connection conn, String sqlScript) throws SQLException {
        String[] sqlArr = sqlScript.replaceAll("--\\s+.*", "").split(";");
        List<String> sqlList = Arrays.stream(sqlArr).filter(str -> StringUtils.isNotBlank(str)).collect(Collectors.toList());
        for (String sql : sqlList) {
            sql = sql.trim();
            boolean result = false;
            ResultSet resultSet = null;
            try {
                Statement statement = conn.createStatement();
                statement.execute(sql);
                result = true;
            } finally {
                if (resultSet != null) {
                    resultSet.close();
                }
            }
        }
    }

    /**
     * 单次处理同步表结构
     *
     * @param dbInfoParser
     * @param srcTable
     * @param desTable
     * @param createTable
     */
    @SneakyThrows
    private static List<SyncOperLog> diffAndGetSyncOperLogs(MysqlDBInfoParser dbInfoParser, String srcTable, String desTable, boolean createTable) throws SQLException {
        List<SyncOperLog> undoLogs = new ArrayList<>();
        MysqlTable srcTableInfo;
        if (!dbInfoParser.existsTable(srcTable)) {
            throw new IllegalArgumentException("源表【" + srcTable + "】不存在");
        }
        //检查目标表是否存在
        if (!dbInfoParser.existsTable(desTable)) {
            if (!createTable) {
                throw new IllegalArgumentException("目标表【" + desTable + "】不存在");
            } else {
                srcTableInfo = dbInfoParser.getTableInfo(srcTable);
                //创建新建表的操作日志
                SyncCreateTableOperLog logInfo = new SyncCreateTableOperLog();
                MysqlTable createTableInfo = srcTableInfo.clone();
                createTableInfo.setTableName(desTable);
                IndexKeyInfo primaryKeyInfo = createTableInfo.getPrimaryKeyInfo();
                primaryKeyInfo.setTableName(desTable);
                List<IndexKeyInfo> indexKeyInfos = createTableInfo.getIndexKeyInfos();
                if (indexKeyInfos != null) {
                    Iterator<IndexKeyInfo> it = indexKeyInfos.iterator();
                    while (it.hasNext()) {
                        IndexKeyInfo keyInfo = it.next();
                        String keyName = keyInfo.getName();
                        keyInfo.setTableName(desTable);
                        keyInfo.setName(keyName);
                    }
                }
                logInfo.setTableInfo(createTableInfo);
                undoLogs.add(logInfo);
            }
        } else {
            srcTableInfo = dbInfoParser.getTableInfo(srcTable);
            MysqlTable desTableInfo = dbInfoParser.getTableInfo(desTable);
            //对比两张表的不同
            undoLogs.addAll(differentTableInfo(srcTableInfo.clone(), desTableInfo.clone()));
        }
//        printOperLogs(undoLogs);
        return undoLogs;
    }

    /**
     * 对比两张表异同生成操作日志
     *
     * @param srcTable
     * @param desTable
     * @return
     */
    private static List<SyncOperLog> differentTableInfo(MysqlTable srcTable, MysqlTable desTable) {
        List<SyncOperLog> list = new ArrayList<>();
        //比对列
        differentColInfo(srcTable, desTable, list);

        //比对主键
        differentPrimaryKeyInfo(srcTable, desTable, list);

        //比对索引
        differentIndexKeyInfo(srcTable, desTable, list);
        return list;
    }

    /**
     * 比对索引<br>
     * 备注：
     * 索引信息全部以源表为准
     * 目标表索引名称命名规范：((idx|uidx)_[tableName]_[srcIndexName])<br>
     * 例如：
     * 源表名称：test，索引名称：idx_test_name，目标表名称：test_01
     * 目标表索引名称为：idx_test_01_name
     *  @param srcTable
     * @param desTable
     * @param list
     */
    private static void differentIndexKeyInfo(MysqlTable srcTable, MysqlTable desTable, List<SyncOperLog> list) {
        List<IndexKeyInfo> srcIndexKeyInfos = srcTable.getIndexKeyInfos();
        List<IndexKeyInfo> desIndexKeyInfos = desTable.getIndexKeyInfos();
        String srcTableName = srcTable.getTableName();
        String desTableName = desTable.getTableName();
        for (int i = 0; i < srcIndexKeyInfos.size(); i++) {
            IndexKeyInfo srcIndexKeyInfo = srcIndexKeyInfos.get(i);
            srcIndexKeyInfo.setTableName(desTableName);
            String srcKeyName = srcIndexKeyInfo.getName();
            String desIndexName = srcKeyName;
            IndexKeyInfo desIndexKeyInfo = null;
            if (desIndexKeyInfos != null) {
                Iterator<IndexKeyInfo> desIt = desIndexKeyInfos.iterator();
                while (desIt.hasNext()) {
                    IndexKeyInfo item = desIt.next();
                    if (item.getName().equalsIgnoreCase(desIndexName)) {
                        desIndexKeyInfo = item;
                        desIt.remove();
                        break;
                    }
                }
            }
            if (desIndexKeyInfo == null) { //新增
                srcIndexKeyInfo.setTableName(desTableName);
                srcIndexKeyInfo.setName(desIndexName);
                SyncIndexKeyOperLog operLog = createIndexKeyInfoOperLog(SyncIndexKeyOperLog.OperateTypeEnum.ADD, null, srcIndexKeyInfo);
                list.add(operLog);
            } else {
                srcIndexKeyInfo.setTableName(desTableName);
                srcIndexKeyInfo.setName(desIndexName);
                if (srcIndexKeyInfo.isDiff(desIndexKeyInfo)) { //修改
                    srcIndexKeyInfo.setTableName(desTableName);
                    srcIndexKeyInfo.setName(desIndexName);
                    SyncIndexKeyOperLog operLog = createIndexKeyInfoOperLog(SyncIndexKeyOperLog.OperateTypeEnum.MODIFY, desIndexKeyInfo, srcIndexKeyInfo);
                    list.add(operLog);
                }
            }
        }
        if (desIndexKeyInfos != null && desIndexKeyInfos.size() != 0) {//删除
            desIndexKeyInfos.forEach(info -> {
                SyncIndexKeyOperLog operLog = createIndexKeyInfoOperLog(SyncIndexKeyOperLog.OperateTypeEnum.DROP, info, null);
                list.add(operLog);
            });
        }
    }

    /**
     * 创建索引 操作日志
     *
     * @param operateType
     * @param srcIndexKeyInfo
     * @param desIndexKeyInfo
     * @return
     */
    private static SyncIndexKeyOperLog createIndexKeyInfoOperLog(SyncIndexKeyOperLog.OperateTypeEnum operateType, IndexKeyInfo srcIndexKeyInfo, IndexKeyInfo desIndexKeyInfo) {
        SyncIndexKeyOperLog operLog = new SyncIndexKeyOperLog(operateType);
        //设置原始索引信息
        operLog.setSrcKeyInfo(srcIndexKeyInfo);
        //设置目标索引信息
        operLog.setDesKeyInfo(desIndexKeyInfo);
        return operLog;
    }

    /**
     * 比对主键
     *  @param srcTable
     * @param desTable
     * @param list
     */
    private static void differentPrimaryKeyInfo(MysqlTable srcTable, MysqlTable desTable, List<SyncOperLog> list) {
        IndexKeyInfo srcPriKey = srcTable.getPrimaryKeyInfo();
        IndexKeyInfo desPriKey = desTable.getPrimaryKeyInfo();
        SyncPrimaryKeyOperLog operLog = null;
        if (srcPriKey != null) {
            srcPriKey.setTableName(desTable.getTableName());
        }
        if (srcPriKey == null) {
            if (desPriKey != null) {//删除
                operLog = new SyncPrimaryKeyOperLog(SyncPrimaryKeyOperLog.OperateTypeEnum.DROP);
                operLog.setSrcKeyInfo(desPriKey);
            }
        } else {
            if (desPriKey == null) {//添加
                operLog = new SyncPrimaryKeyOperLog(SyncPrimaryKeyOperLog.OperateTypeEnum.ADD);
                operLog.setDesKeyInfo(srcPriKey);
            } else if (srcPriKey.isDiff(desPriKey)) {//修改
                operLog = new SyncPrimaryKeyOperLog(SyncPrimaryKeyOperLog.OperateTypeEnum.MODIFY);
                operLog.setSrcKeyInfo(desPriKey);
                operLog.setDesKeyInfo(srcPriKey);
            }
        }
        if (operLog != null) {
            list.add(operLog);
        }
    }


    /**
     * 比对列信息
     *  @param srcTable
     * @param desTable
     * @param list
     */
    private static void differentColInfo(MysqlTable srcTable, MysqlTable desTable, List<SyncOperLog> list) {
        List<MysqlColumn> srcCols = srcTable.getCols();
        List<MysqlColumn> desCols = desTable.getCols().stream().collect(Collectors.toList());
        String srcTableName = srcTable.getTableName();
        String desTableName = desTable.getTableName();
        //比对字段
        for (int i = 0; i < srcCols.size(); i++) {
            MysqlColumn srcCol = srcCols.get(i);
            String srcColName = srcCol.getColumnName();
            MysqlColumn desCol = null;
            Iterator<MysqlColumn> desIt = desCols.iterator();
            while (desIt.hasNext()) {
                MysqlColumn item = desIt.next();
                if (item.getColumnName().equalsIgnoreCase(srcColName)) {
                    desCol = item;
                    desIt.remove();
                }
            }
            if (desCol == null) {//新增
                SyncColumnOperLog operLog = createColOperateLog(SyncColumnOperLog.OperateTypeEnum.ADD, srcTableName, desTableName, null, srcCol);
                list.add(operLog);
            } else if (srcCol.isDiff(desCol)) {//修改
                SyncColumnOperLog operLog = createColOperateLog(SyncColumnOperLog.OperateTypeEnum.MODIFY, srcTableName, desTableName, desCol, srcCol);
                list.add(operLog);
            }
        }
        //删除
        if (desCols.size() != 0) {
            for (MysqlColumn desCol : desCols) {
                SyncColumnOperLog operLog = createColOperateLog(SyncColumnOperLog.OperateTypeEnum.DROP, srcTableName, desTableName, desCol, null);
                list.add(operLog);
            }
        }
    }

    private static SyncColumnOperLog createColOperateLog(SyncColumnOperLog.OperateTypeEnum operType, String srcTableName, String desTableName, MysqlColumn srcCol, MysqlColumn desCol) {
        SyncColumnOperLog operLog = new SyncColumnOperLog(operType);
        operLog.setSrcTableName(srcTableName);
        operLog.setDesTableName(desTableName);
        operLog.setSrcColInfo(srcCol);
        operLog.setDesColInfo(desCol);
        return operLog;
    }

    //打印一下操作日志
    private static void printOperLogs(List<SyncOperLog> undoLogs) {
        System.out.println(JSON.toJSONString(undoLogs, SerializerFeature.SortField, SerializerFeature.PrettyFormat));
    }

    public static void main(String[] args) throws Exception {
        String jdbcUrl = "jdbc:mysql://192.168.100.101:3306/hero?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC";
        String user = "root";
        String password = "root123";
        Class.forName("com.mysql.jdbc.Driver");
        String srcTable = "test";
        String desTable = "test_110100";
        try (Connection conn = DriverManager.getConnection(jdbcUrl, user, password);
             MysqlDBInfoParser dbInfoParser = (MysqlDBInfoParser) DBInfoParserFactory.getInfoParser(conn)) {
            batchExecuteSyncOperLogs(dbInfoParser, srcTable, new String[]{desTable}, true);
        }
    }
}
