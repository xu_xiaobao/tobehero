package top.tobehero.sync_ddl;


import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import top.tobehero.sync_ddl.mysql.model.MysqlTable;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 数据库信息解析器，负责解析数据库表信息
 */
@Slf4j
public abstract class DBInfoParser implements Closeable {
    @Getter
    private Connection conn;

    @SneakyThrows
    public DBInfoParser(Connection conn) {
        this.conn = conn;
        this.init();
    }

    /**
     * 查询表是否存在
     *
     * @param tableName
     * @return
     * @throws SQLException
     */
    public boolean existsTable(String tableName) throws SQLException {
        ResultSet rs = null;
        try {
            rs = conn.getMetaData().getTables("", "", tableName, null);
            return rs.next();
        } finally {
            rs.close();
        }
    }

    /**
     * 获取表信息
     *
     * @param tableName
     * @return
     */
    public abstract Table getTableInfo(String tableName) throws SQLException;

    /**
     * 生成建表语句
     *
     * @param tableInfo
     * @return
     * @throws SQLException
     */
    public abstract String createTableSql(MysqlTable tableInfo) throws SQLException;

    /**
     * 初始化扩展方法
     */
    protected void init() throws Exception {
    }

    /**
     * SQL执行模板方法
     *
     * @param sql
     * @param params
     * @param callback
     * @param <T>
     * @return
     * @throws SQLException
     */
    protected <T> T doExecuteSql(String sql, Object[] params, Function<ResultSet, T> callback) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(sql);
        if (params != null && params.length > 0) {
            for (int i = 1; i <= params.length; i++) {
                ps.setObject(i, params[i - 1]);
            }
        }
        boolean execute = ps.execute();
        if (execute) {
            ResultSet resultSet = ps.getResultSet();
            try {
                return callback.apply(resultSet);
            } finally {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
                if (ps != null && !ps.isClosed()) {
                    ps.close();
                }
            }
        } else {
            throw new RuntimeException("SQL执行错误【" + sql + "】");
        }
    }

    /**
     * SQL列表查询
     *
     * @param sql
     * @param params
     * @param handleItem
     * @param <T>
     * @return
     * @throws SQLException
     */
    protected <T> List<T> qryList(String sql, Object[] params, Function<ResultSet, T> handleItem) throws SQLException {
        return doExecuteSql(sql, params, (resultSet) -> {
            List<T> list = new ArrayList<>();
            try {
                while (resultSet.next()) {
                    T e = handleItem.apply(resultSet);
                    list.add(e);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            return list;
        });
    }

    /**
     * 列表查询
     *
     * @param sql
     * @param params
     * @return
     * @throws SQLException
     */
    protected List<Map<String, Object>> qryListMap(String sql, Object... params) throws SQLException {
        List<String> labelNames = new ArrayList<>();
        return qryList(sql, params, rs -> {
            try {
                HashMap<String, Object> row = new HashMap<>();
                if (labelNames.size() == 0) {
                    ResultSetMetaData metaData = rs.getMetaData();
                    int columnCount = metaData.getColumnCount();
                    for (int i = 1; i <= columnCount; i++) {
                        labelNames.add(metaData.getColumnLabel(i));
                    }
                }
                for (String labelName : labelNames) {
                    row.put(underline2Camel(labelName), rs.getObject(labelName));
                }
                return row;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * 列表查询，返回指定bean的列表集合
     *
     * @param sql
     * @param params
     * @param tClass
     * @param <T>
     * @return
     */
    @SneakyThrows
    protected <T> List<T> qryList(String sql, Object[] params, Class<T> tClass) {
        List<String> labelNames = new ArrayList<>();
        List<Field> allField = null;
        Field[] declaredFields = tClass.getDeclaredFields();
        if (declaredFields == null) {
            return null;
        }
        allField = Arrays.stream(declaredFields).filter(field -> {
            int modifiers = field.getModifiers();
            boolean aFinal = Modifier.isFinal(modifiers);
            boolean aStatic = Modifier.isStatic(modifiers);
            return !aFinal || !aStatic;
        }).map(field -> {
            field.setAccessible(true);
            return field;
        }).collect(Collectors.toList());
        List<Field> finalAllField = allField;
        return qryListMap(sql, params).stream().map(rowMap -> {
            try {
                T t = null;
                for (Map.Entry<String, Object> entry : rowMap.entrySet()) {
                    String colName = entry.getKey();
                    Object val = entry.getValue();
                    Field field1 = finalAllField.stream().filter(field -> field.getName().equals(colName)).findFirst().orElseGet(null);
                    if (field1 != null) {
                        t = (t != null) ? t : tClass.newInstance();
                        field1.set(t ,val);
                    }
                }
                return t;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }

    /**
     * 下划线转驼峰
     *
     * @param labelName
     * @return
     */
    private static String underline2Camel(String labelName) {
        if (labelName == null) {
            return null;
        }
        labelName = labelName.toLowerCase().replaceAll("_+", "_").replaceAll("_$", "");
        int indexOf = -1;
        while ((indexOf = labelName.indexOf("_")) != -1) {
            labelName = labelName.substring(0, indexOf) + labelName.substring(indexOf + 1, indexOf + 2).toUpperCase() + labelName.substring(indexOf + 2);
        }
        return labelName;
    }

    public static void main(String[] args) {
        String a = "CONSTRAINT_CATALOG\tCONSTRAINT_SCHEMA\tCONSTRAINT_NAME\tTABLE_CATALOG\tTABLE_SCHEMA\tTABLE_NAME\tCOLUMN_NAME\tORDINAL_POSITION\tPOSITION_IN_UNIQUE_CONSTRAINT\tREFERENCED_TABLE_SCHEMA\tREFERENCED_TABLE_NAME\tREFERENCED_COLUMN_NAME";

        String[] split = a.split("\t");
        for (String colName : split) {
            System.out.println("private String " + underline2Camel(colName).replaceAll("`","") + ";");
        }
    }

    @Override
    public void close() throws IOException {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (Exception e) {
            log.error("数据库连接关闭失败", e);
        }
    }
}
