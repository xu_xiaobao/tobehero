package top.tobehero.sync_ddl;

import java.util.List;

public class SyncOperLogParserDelegate extends  AbstractSyncOperLogParser<SyncOperLog>{
    private List<AbstractSyncOperLogParser> parsers;

    public SyncOperLogParserDelegate(List<AbstractSyncOperLogParser> parsers) {
        this.parsers = parsers;
    }
    private AbstractSyncOperLogParser getParser(SyncOperLog operLog){
        AbstractSyncOperLogParser findParser = parsers.stream().filter(parser -> parser.isMatch(operLog)).findFirst().orElse(null);
        if (findParser != null){
            return findParser;
        }
        throw new RuntimeException("未找到处理【"+operLog.getClass().getName()+"】的解析器");
    }

    @Override
    public String getSql(SyncOperLog operLog) {
        return this.getParser(operLog).getSql(operLog);
    }

    @Override
    public String getUndoSql(SyncOperLog operLog) {
        return this.getParser(operLog).getUndoSql(operLog);
    }
}
