package top.tobehero.sync_ddl;


import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.function.Consumer;

public class Util {
    public static Consumer VOID_CONSUMER = (i)->{};
    @SneakyThrows
    public static Object deepClone(Object obj){
        if (obj == null) {
            return null;
        }
        ByteArrayOutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;
        ByteArrayInputStream inputStream = null;
        ObjectInputStream objectInputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(obj);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            objectInputStream = new ObjectInputStream(inputStream);
            return objectInputStream.readObject();
        } finally {
            closeQuietly(objectInputStream);
            closeQuietly(inputStream);
            closeQuietly(objectOutputStream);
            closeQuietly(outputStream);
        }
    }

    public static void closeQuietly(Closeable closeable){
        IOUtils.closeQuietly(closeable,VOID_CONSUMER);
    }
}
