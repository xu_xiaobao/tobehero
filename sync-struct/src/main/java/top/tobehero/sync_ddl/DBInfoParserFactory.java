package top.tobehero.sync_ddl;


import top.tobehero.sync_ddl.mysql.MysqlDBInfoParser;

import java.lang.ref.WeakReference;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DBInfoParserFactory {
    private DBInfoParserFactory(){}

    private static Map<Connection, WeakReference<DBInfoParser>> parserMap = new ConcurrentHashMap<>();
    public static DBInfoParser getInfoParser(Connection conn) throws SQLException {
        if(conn.isClosed()){
            throw new IllegalStateException("连接不可用，获取失败");
        }
        return parserMap.computeIfAbsent(conn,(k)->{
            DatabaseMetaData metaData;
            String productName = null;
            try {
                metaData = k.getMetaData();
                productName = metaData.getDatabaseProductName().toLowerCase();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            switch (productName){
                case "mysql":
                    MysqlDBInfoParser parser = new MysqlDBInfoParser(conn);
                    return new WeakReference<DBInfoParser>(parser);
                default:
                    throw new IllegalStateException("未实现的数据库类型["+productName+"]");
            }
        }).get();
    }
}
