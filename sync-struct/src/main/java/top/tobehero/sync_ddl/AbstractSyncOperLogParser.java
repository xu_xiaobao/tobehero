package top.tobehero.sync_ddl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class AbstractSyncOperLogParser<T extends SyncOperLog> {

    private Class<T> operLogClazz;
    public AbstractSyncOperLogParser(){
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if(actualTypeArguments.length == 0){
            throw new RuntimeException("泛型参数未指定");
        }
        operLogClazz = (Class<T>) actualTypeArguments[0];
    }
    /**
     * 得到未执行的SQL
     *
     * @param operLog
     * @return
     */
    public abstract String getSql(T operLog);

    /**
     * 得到回滚的SQL
     *
     * @param operLog
     * @return
     */
    public abstract String getUndoSql(T operLog);

    protected boolean isMatch(SyncOperLog syncOperLog) {
        return syncOperLog != null && syncOperLog.getClass().equals(operLogClazz);
    }
}
