package top.tobehero.sync_ddl.mysql.info;

import lombok.Data;
import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import top.tobehero.sync_ddl.mysql.model.MysqlColumn;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 索引信息
 */
@Data
public class IndexKeyInfo implements Serializable {
    private String tableName;
    private String name;
    private String comment;
    private boolean canNull;
    private boolean unique;
    private List<String> colNames;
    private List<MysqlColumn> columns;
    private String indexType;

    /**
     * 是否相同
     *
     * @param other
     * @return
     */
    public boolean isDiff(IndexKeyInfo other) {
        boolean result =
//                StringUtils.equals(tableName, other.getTableName()) &&
//                 StringUtils.equals(name, other.getName()) &&
                 StringUtils.equals(comment, other.getComment())  &&
                canNull == other.isCanNull() && unique == other.isUnique();
        if(!result) {
            return true;
        }
        if(colNames.size() == other.getColNames().size()){
            for (int i = 0; i < colNames.size(); i++) {
                if (!colNames.get(i).equalsIgnoreCase(other.getColNames().get(i))){
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @SneakyThrows
    @Override
    public IndexKeyInfo clone() {
        if (null == this){
            return null;
        }
        IndexKeyInfo target = (IndexKeyInfo) BeanUtils.cloneBean(this);
        target.setColNames(this.getColNames().stream().collect(Collectors.toList()));
        return target;
    }
}
