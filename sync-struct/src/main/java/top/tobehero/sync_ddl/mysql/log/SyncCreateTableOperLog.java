package top.tobehero.sync_ddl.mysql.log;


import lombok.Data;
import top.tobehero.sync_ddl.MysqlSyncTableOperLog;
import top.tobehero.sync_ddl.mysql.model.MysqlTable;

/**
 * 同步表 创建表操作日志
 */
@Data
public class SyncCreateTableOperLog implements MysqlSyncTableOperLog {
    private MysqlTable tableInfo;

}
