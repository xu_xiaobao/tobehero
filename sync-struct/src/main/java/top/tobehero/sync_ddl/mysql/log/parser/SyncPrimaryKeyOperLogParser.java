package top.tobehero.sync_ddl.mysql.log.parser;


import top.tobehero.sync_ddl.AbstractSyncOperLogParser;
import top.tobehero.sync_ddl.FreemarkUtils;
import top.tobehero.sync_ddl.mysql.log.SyncPrimaryKeyOperLog;

public class SyncPrimaryKeyOperLogParser extends AbstractSyncOperLogParser<SyncPrimaryKeyOperLog> {
    @Override
    public String getSql(SyncPrimaryKeyOperLog operLog) {
        SyncPrimaryKeyOperLog.OperateTypeEnum operateType = operLog.getOperateType();
        String operateTypeName = operateType.name().toLowerCase();
        return FreemarkUtils.getContent("mysql/primary_key_" + operateTypeName + ".ftl", operLog);
    }

    @Override
    public String getUndoSql(SyncPrimaryKeyOperLog operLog) {
        SyncPrimaryKeyOperLog.OperateTypeEnum operateType = operLog.getOperateType();
        String operateTypeName = operateType.name().toLowerCase();
        return FreemarkUtils.getContent("mysql/undo_primary_key_" + operateTypeName + ".ftl", operLog);
    }
}
