package top.tobehero.sync_ddl.mysql.log.parser;


import top.tobehero.sync_ddl.AbstractSyncOperLogParser;
import top.tobehero.sync_ddl.FreemarkUtils;
import top.tobehero.sync_ddl.mysql.log.SyncColsOrderOperLog;

public class SyncColumnOrderOperLogParser extends AbstractSyncOperLogParser<SyncColsOrderOperLog> {
    @Override
    public String getSql(SyncColsOrderOperLog operLog) {
        return FreemarkUtils.getContent("mysql/sync_columns_order.ftl", operLog);
    }

    @Override
    public String getUndoSql(SyncColsOrderOperLog operLog) {
        return null;
    }
}
