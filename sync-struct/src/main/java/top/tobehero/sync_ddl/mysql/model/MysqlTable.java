package top.tobehero.sync_ddl.mysql.model;


import lombok.Data;
import lombok.SneakyThrows;
import top.tobehero.sync_ddl.Table;
import top.tobehero.sync_ddl.Util;
import top.tobehero.sync_ddl.mysql.info.IndexKeyInfo;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class MysqlTable extends Table {
    //TABLE_CATALOG	TABLE_SCHEMA	TABLE_NAME	TABLE_TYPE	ENGINE	VERSION	ROW_FORMAT	TABLE_ROWS	AVG_ROW_LENGTH	DATA_LENGTH	MAX_DATA_LENGTH	INDEX_LENGTH	DATA_FREE	`AUTO_INCREMENT`	CREATE_TIME	UPDATE_TIME	CHECK_TIME	TABLE_COLLATION	CHECKSUM	CREATE_OPTIONS	TABLE_COMMENT;
    private String tableCatalog;
    private String tableSchema;
    private String tableName;
    private String tableType;
    private String engine;
    private BigInteger version;
    private String rowFormat;
    private BigInteger tableRows;
    private BigInteger avgRowLength;
    private BigInteger dataLength;
    private BigInteger maxDataLength;
    private BigInteger indexLength;
    private BigInteger dataFree;
    private BigInteger autoIncrement;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Timestamp checkTime;
    private String tableCollation;
    private String checksum;
    private String createOptions;
    private String tableComment;

    /*--------------其他字段-----------------------------*/
    private String tableCharset;
    private List<MysqlColumn> cols;
    private IndexKeyInfo primaryKeyInfo;
    private List<IndexKeyInfo> indexKeyInfos;

    @SneakyThrows
    @Override
    public MysqlTable clone() throws CloneNotSupportedException {
        return (MysqlTable) Util.deepClone(this);
    }

    public static void main(String[] args) throws Exception {
        MysqlTable mysqlTable = new MysqlTable();
        mysqlTable.setTableCharset("tableCharset");
        mysqlTable.setCheckTime(new Timestamp(new Date().getTime()));
        mysqlTable.setAvgRowLength(BigInteger.valueOf(1));
        ArrayList<MysqlColumn> cols = new ArrayList<>();
        MysqlColumn col1 = new MysqlColumn();
        cols.add(col1);
        mysqlTable.setCols(cols);
        System.out.println(mysqlTable);
        MysqlTable clone = mysqlTable.clone();
        MysqlColumn mysqlColumn = clone.getCols().get(0);
        System.out.println(mysqlColumn == col1);
    }
}

