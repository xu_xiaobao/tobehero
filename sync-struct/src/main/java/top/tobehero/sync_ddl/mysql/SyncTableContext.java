package top.tobehero.sync_ddl.mysql;

import lombok.Data;

import java.util.List;

@Data
public class SyncTableContext {
    /**
     * 数据库默认字符集
     */
    private String defaultCharset;
    /**
     * 数据库默认的排序规则
     */
    private String defaultCollationName;
    /**
     * 排序规则列表
     */
    private List<Collation> collationsList;

    public String findCharsetByCollation(String collationName){
        if (collationsList == null) {
            return null;
        }
        return collationsList.stream().filter(collation -> collation.getCollationName().equalsIgnoreCase(collationName)).findFirst().get().getCharacterSetName();
    }
}
