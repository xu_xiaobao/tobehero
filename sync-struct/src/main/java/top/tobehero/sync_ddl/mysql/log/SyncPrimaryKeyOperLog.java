package top.tobehero.sync_ddl.mysql.log;


import lombok.Data;
import top.tobehero.sync_ddl.MysqlSyncTableOperLog;
import top.tobehero.sync_ddl.mysql.info.IndexKeyInfo;

@Data
public class SyncPrimaryKeyOperLog implements MysqlSyncTableOperLog {
    public SyncPrimaryKeyOperLog(OperateTypeEnum operateType) {
        this.operateType = operateType;
    }
    public enum OperateTypeEnum {
        ADD,MODIFY,DROP
    }
    private OperateTypeEnum operateType;


    private IndexKeyInfo srcKeyInfo;
    private IndexKeyInfo desKeyInfo;

}
