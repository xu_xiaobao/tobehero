package top.tobehero.sync_ddl.mysql.log.parser;


import top.tobehero.sync_ddl.AbstractSyncOperLogParser;
import top.tobehero.sync_ddl.FreemarkUtils;
import top.tobehero.sync_ddl.mysql.log.SyncCreateTableOperLog;
import top.tobehero.sync_ddl.mysql.model.MysqlTable;

public class  CreateTableSyncOperLogParser extends AbstractSyncOperLogParser<SyncCreateTableOperLog> {

    @Override
    public String getSql(SyncCreateTableOperLog operLog) {
        MysqlTable tableInfo = operLog.getTableInfo();
        return FreemarkUtils.getContent("mysql/create_table_all.ftl", tableInfo);
    }

    @Override
    public String getUndoSql(SyncCreateTableOperLog operLog) {
        MysqlTable tableInfo = operLog.getTableInfo();
        return FreemarkUtils.getContent("mysql/drop_table_all.ftl", tableInfo);
    }
}
