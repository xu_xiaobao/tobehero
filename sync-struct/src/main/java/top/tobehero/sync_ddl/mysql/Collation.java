package top.tobehero.sync_ddl.mysql;

import lombok.Data;

import java.math.BigInteger;

@Data
public class Collation {
//    COLLATION_NAME	CHARACTER_SET_NAME	ID	IS_DEFAULT	IS_COMPILED	SORTLEN
    private BigInteger id;
    /**
     * 排序规则
     */
    private String collationName;
    /**
     * 字符编码
     */
    private String characterSetName;
    /**
     * 是否是默认排序规则
     */
    private String isDefault;
    /**
     *
     */
    private String isCompiled;
    /**
     * 排序长度
     */
    private BigInteger sortlen;

    public boolean isDefault(){
        return "YES".equalsIgnoreCase(isDefault);
    }
}
