package top.tobehero.sync_ddl.mysql.log.parser;


import top.tobehero.sync_ddl.AbstractSyncOperLogParser;
import top.tobehero.sync_ddl.FreemarkUtils;
import top.tobehero.sync_ddl.mysql.log.SyncColumnOperLog;

public class SyncColumnOperLogParser extends AbstractSyncOperLogParser<SyncColumnOperLog> {
    @Override
    public String getSql(SyncColumnOperLog operLog) {
        SyncColumnOperLog.OperateTypeEnum operateType = operLog.getOperateType();
        switch (operateType) {
            case ADD:
            case DROP:
            case MODIFY:
                return FreemarkUtils.getContent("mysql/column_" + operateType.name().toLowerCase() + ".ftl", operLog);
            default:
                throw new RuntimeException("未知错误类型");
        }
    }

    @Override
    public String getUndoSql(SyncColumnOperLog operLog) {
        SyncColumnOperLog.OperateTypeEnum operateType = operLog.getOperateType();
        switch (operateType) {
            case ADD:
            case DROP:
            case MODIFY:
                return FreemarkUtils.getContent("mysql/undo_column_" + operateType.name().toLowerCase() + ".ftl", operLog);
            default:
                throw new RuntimeException("未知错误类型");
        }
    }
}
