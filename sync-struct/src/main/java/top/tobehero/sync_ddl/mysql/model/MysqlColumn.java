package top.tobehero.sync_ddl.mysql.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import top.tobehero.sync_ddl.Column;
import top.tobehero.sync_ddl.Util;

import java.math.BigInteger;

@Data
@EqualsAndHashCode(of = {"columnName", "columnDefault", "isNullable", "dataType", "characterSetName", "collationName", "columnType", "columnKey", "extra", "columnComment"})
public class MysqlColumn extends Column {
    //    TABLE_CATALOG	TABLE_SCHEMA	TABLE_NAME	COLUMN_NAME	ORDINAL_POSITION	COLUMN_DEFAULT	IS_NULLABLE	DATA_TYPE	CHARACTER_MAXIMUM_LENGTH	CHARACTER_OCTET_LENGTH	NUMERIC_PRECISION	NUMERIC_SCALE	DATETIME_PRECISION	CHARACTER_SET_NAME	COLLATION_NAME	COLUMN_TYPE	COLUMN_KEY	EXTRA	`PRIVILEGES`	COLUMN_COMMENT	GENERATION_EXPRESSION
    private String tableCatalog;
    private String tableSchema;
    private String tableName;
    private String columnName;
    private BigInteger ordinalPosition;
    private String columnDefault;
    private String isNullable;
    private String dataType;
    private BigInteger characterMaximumLength;
    private BigInteger characterOctetLength;
    private BigInteger numericPrecision;
    private BigInteger numericScale;
    private BigInteger datetimePrecision;
    private String characterSetName;
    private String collationName;
    private String columnType;
    private String columnKey;
    private String extra;
    private String privileges;
    private String columnComment;
    private String generationExpression;

    /*--------------其他字段-----------------------------*/

    @Override
    protected MysqlColumn clone() throws CloneNotSupportedException {
        return (MysqlColumn) Util.deepClone(this);
    }

    public boolean isDiff(MysqlColumn other) {
        return !this.equals(other);
    }

    public String getColType(){
        if (StringUtils.containsAny(columnType,"char","text")){
            return "text";
        }
        return "";
    }
}
