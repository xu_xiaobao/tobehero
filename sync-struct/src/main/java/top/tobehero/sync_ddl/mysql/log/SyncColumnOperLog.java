package top.tobehero.sync_ddl.mysql.log;


import lombok.Data;
import top.tobehero.sync_ddl.MysqlSyncTableOperLog;
import top.tobehero.sync_ddl.mysql.model.MysqlColumn;

/**
 * 同步表  列操作日志
 */
@Data
public class SyncColumnOperLog implements MysqlSyncTableOperLog {
    public enum OperateTypeEnum {
        ADD,MODIFY,DROP
    }
    private OperateTypeEnum operateType;
    private String srcTableName;
    private String desTableName;
    private MysqlColumn srcColInfo;
    private MysqlColumn desColInfo;
    public SyncColumnOperLog(OperateTypeEnum operateType){
        this.operateType = operateType;
    }
}

