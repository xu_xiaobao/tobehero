package top.tobehero.sync_ddl.mysql.log.parser;


import top.tobehero.sync_ddl.AbstractSyncOperLogParser;
import top.tobehero.sync_ddl.FreemarkUtils;
import top.tobehero.sync_ddl.mysql.log.SyncIndexKeyOperLog;

public class SyncIndexKeyOperLogParser extends AbstractSyncOperLogParser<SyncIndexKeyOperLog> {
    @Override
    public String getSql(SyncIndexKeyOperLog operLog) {
        SyncIndexKeyOperLog.OperateTypeEnum operateType = operLog.getOperateType();
        String operateTypeName = operateType.name().toLowerCase();
        return FreemarkUtils.getContent("mysql/index_key_" + operateTypeName + ".ftl", operLog);
    }

    @Override
    public String getUndoSql(SyncIndexKeyOperLog operLog) {
        SyncIndexKeyOperLog.OperateTypeEnum operateType = operLog.getOperateType();
        String operateTypeName = operateType.name().toLowerCase();
        return FreemarkUtils.getContent("mysql/undo_index_key_" + operateTypeName + ".ftl", operLog);
    }
}
