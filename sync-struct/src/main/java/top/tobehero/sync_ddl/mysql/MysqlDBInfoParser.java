package top.tobehero.sync_ddl.mysql;

import org.apache.commons.collections.CollectionUtils;
import top.tobehero.sync_ddl.DBInfoParser;
import top.tobehero.sync_ddl.FreemarkUtils;
import top.tobehero.sync_ddl.mysql.info.IndexKeyInfo;
import top.tobehero.sync_ddl.mysql.model.MysqlColumn;
import top.tobehero.sync_ddl.mysql.model.MysqlTable;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.regex.Pattern.MULTILINE;

public class MysqlDBInfoParser extends DBInfoParser {
    private static final String SQL_SHOW_DATABASE_CHARSET = "show variables like 'character_set_database'";
    private static final String SQL_QUERY_COLLATION = "select * from information_schema.COLLATIONS order by id;";
//    private static final String SQL_SHOW_COLUMNS = "show full columns from ${tableName};";
    private static final String SQL_SHOW_COLUMNS = "select * from information_schema.`COLUMNS` c where TABLE_SCHEMA = '${dbName}' and TABLE_NAME =  '${tableName}';";
    private static final String SQL_SHOW_KEYS = "show keys from ${tableName};";
    private static final String SQL_INFORMATION_SCHEMA_TABLES = "select * from information_schema.tables where table_name = '${tableName}' and table_schema = '${dbName}'";
    private static final String KEY_NAME_PRIMARY_KEY = "PRIMARY";
    private static final Pattern STR_CONCAT_REGEX = Pattern.compile("\\$\\{\\S+?}", MULTILINE);

    private static ThreadLocal<SyncTableContext> CONTEXT = new ThreadLocal<>();
    public MysqlDBInfoParser(Connection conn) {
        super(conn);
    }

    @Override
    protected void init() throws Exception {
        //读取字符集信息
        loadContextInfo();
    }
    public SyncTableContext getSyncTableContext(){
        return CONTEXT.get();
    }

    private void loadContextInfo() throws SQLException {
        Connection conn = getConn();
        String schema = conn.getCatalog();
        String dbCharset = qryList(SQL_SHOW_DATABASE_CHARSET,null,resultSet -> {
            try {
                return resultSet.getString("value");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }).get(0);
        List<Collation> collations = qryList(SQL_QUERY_COLLATION,null,(resultSet -> {
            try {
                BigInteger id = resultSet.getBigDecimal("id").toBigInteger();
                String collationName = resultSet.getString("COLLATION_NAME");
                String characterSetName = resultSet.getString("CHARACTER_SET_NAME");
                String isDefault = resultSet.getString("IS_DEFAULT");
                String isCompiled = resultSet.getString("IS_COMPILED");
                BigInteger sortlen = resultSet.getBigDecimal("SORTLEN").toBigInteger();
                Collation collation = new Collation();
                collation.setId(id);
                collation.setCharacterSetName(characterSetName);
                collation.setCollationName(collationName);
                collation.setIsDefault(isDefault);
                collation.setIsCompiled(isCompiled);
                collation.setSortlen(sortlen);
                return collation;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }));
        Map<String, List<Collation>> listMap = collations.stream().collect(Collectors.groupingBy(Collation::getCharacterSetName));
        List<Collation> defaultCharsetCollations = listMap.get(dbCharset);
        Collation defaultCollation = defaultCharsetCollations.stream().filter(collation -> collation.isDefault()).findFirst().get();
        String collationName = defaultCollation.getCollationName();
        SyncTableContext context = new SyncTableContext();
        context.setCollationsList(collations);
        context.setDefaultCharset(dbCharset);
        context.setDefaultCollationName(collationName);
        CONTEXT.set(context);
    }

    @Override
    public MysqlTable getTableInfo(String tableName) throws SQLException {
        MysqlTable info = getBaseTableInfo(tableName);
        List<MysqlColumn> columnInfos = getColumnInfos(tableName);
        List<IndexKeyInfo> indexKeyInfos = getIndexKeyInfos(tableName);
        info.setCols(columnInfos);

        if (indexKeyInfos != null && indexKeyInfos.size() != 0) {
            // key info填充 columnInfo
            Map<String, List<IndexKeyInfo>> listMap = indexKeyInfos.stream().map(keyInfo->{
                List<MysqlColumn> columns =  keyInfo.getColNames().stream().map(colName -> columnInfos.stream().filter(columnInfo -> columnInfo.getColumnName().equalsIgnoreCase(colName)).findFirst().get()).collect(Collectors.toList());
                keyInfo.setColumns(columns);
                return keyInfo;
            }).collect(Collectors.groupingBy(i -> {
                String keyName = i.getName();
                if (KEY_NAME_PRIMARY_KEY.equals(keyName)) {
                    return KEY_NAME_PRIMARY_KEY;
//                } else if (i.isUnique()) {
//                    return "unique";
                } else {
                    return "index";
                }
            }));
            List<IndexKeyInfo> pkIndexs = listMap.get(KEY_NAME_PRIMARY_KEY);
            info.setPrimaryKeyInfo(pkIndexs != null && pkIndexs.size() != 0 ? pkIndexs.get(0) : null);
            info.setIndexKeyInfos(listMap.get("index"));
//            info.setUniqueIndexKeyInfos(listMap.get("unique"));
        }
        return info;
    }

    @Override
    public String createTableSql(MysqlTable tableInfo) throws SQLException {
        return getTemplateContent("mysql/create_table_all.ftl", tableInfo);
    }

    /**
     * 获取模板内容
     *
     * @param template
     * @param modelData
     * @return
     */
    private String getTemplateContent(String template, Object modelData) {
        try {
            return FreemarkUtils.getContent(template, modelData);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 得到表注释
     *
     * @param tableName
     * @return
     */
    private MysqlTable getBaseTableInfo(String tableName) throws SQLException {
        SyncTableContext syncTableContext = getSyncTableContext();
        MysqlTable tableInfo = new MysqlTable();
        String sql = formatString(SQL_INFORMATION_SCHEMA_TABLES, tableName, getConn().getCatalog());
        List<MysqlTable> tables = qryList(sql, null, MysqlTable.class);
        if (CollectionUtils.isNotEmpty(tables)){
            MysqlTable table = tables.get(0);
            String tableComment =  table.getTableComment();
            String tableCollation = table.getTableCollation();
            String engine = table.getEngine();
            String charset = syncTableContext.findCharsetByCollation(tableCollation);
            tableInfo.setTableName(tableName);
            tableInfo.setTableComment(tableComment);
            tableInfo.setTableCollation(tableCollation);
            tableInfo.setEngine(engine);
            tableInfo.setTableCharset(charset);
            return tableInfo;
        }
        return null;
    }

    /**
     * 获取索引信息
     *
     * @param tableName
     * @return
     */
    private List<IndexKeyInfo> getIndexKeyInfos(String tableName) throws SQLException {
        String sql = formatString(SQL_SHOW_KEYS, tableName);
        List<IndexKeyInfo> list = new ArrayList<>();
        qryList(sql, null, (resultSet) -> {
            try {
                String keyName = resultSet.getString("Key_name");
                String indexType = resultSet.getString("Index_type");
                String colName = resultSet.getString("Column_name");
                String comment = resultSet.getString("comment");
                boolean isUnique = "0".equalsIgnoreCase(resultSet.getString("Non_unique"));
                int seqInIndex = resultSet.getInt("Seq_in_index");
                boolean canNull = "YES".equalsIgnoreCase(resultSet.getString("Null"));
                IndexKeyInfo info = list.stream().filter(i -> keyName.equalsIgnoreCase(i.getName())).findFirst().orElseGet(() -> {
                    IndexKeyInfo e = new IndexKeyInfo();
                    e.setTableName(tableName);
                    e.setName(keyName);
                    e.setIndexType(indexType);
                    e.setUnique(isUnique);
                    e.setCanNull(canNull);
                    e.setComment(comment);
                    e.setColNames(new ArrayList<>());
                    list.add(e);
                    return e;
                });
                List<String> colNames = info.getColNames();
                colNames.add(seqInIndex - 1, colName);
                return null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
        return list;
    }

    /**
     * 根据模板和参数拼接字符串
     *
     * @param pattern
     * @param values
     * @return
     */
    public static String formatString(String pattern, String... values) {
        if (values == null || values.length == 0) {
            return pattern;
        }
        Matcher matcher = STR_CONCAT_REGEX.matcher(pattern);
        StringBuilder sb = new StringBuilder();
        int idx = 0;
        int start;
        int end = 0;

        while (matcher.find() && idx < values.length) {
            start = matcher.start();
            sb.append(pattern, end, start);
            end = matcher.end();
            sb.append(values[idx++]);
        }
        if (end != pattern.length()) {
            sb.append(pattern, end, pattern.length());
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(formatString("show full columns from ${tableName} ;\nhello ${name}!!!", "tableName", "许宝众", "SAdasd"));
    }

    /**
     * 收集字段元数据
     *
     * @return
     */
    private List<MysqlColumn> getColumnInfos(String tableName) throws SQLException {
        String dbName = getConn().getCatalog();
        String selectTable = formatString(SQL_SHOW_COLUMNS,dbName,tableName);
        return qryList(selectTable, null, MysqlColumn.class).stream().map(column -> {
            return column;
        }).collect(Collectors.toList());
    }


}
