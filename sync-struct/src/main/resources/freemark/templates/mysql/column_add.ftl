<#assign col=desColInfo/>
<#--ALTER TABLE ${desTableName}
ADD COLUMN ${desColInfo.name} ${desColInfo.type}
<#if !desColInfo.canNull> NOT NULL</#if>
<#if desColInfo.defaultVal?exists> DEFAULT <#if desColInfo.colType?exists && desColInfo.colType == 'text'>'</#if>${desColInfo.defaultVal}<#if desColInfo.colType?exists && desColInfo.colType == 'text'>'</#if></#if>
<#if !desColInfo.priKey && desColInfo.extra?exists  && desColInfo.extra?length != 0 ></#if> ${desColInfo.extra}
<#if desColInfo.comment?exists && desColInfo.comment != ''> COMMENT '${desColInfo.comment}'</#if>;-->
<#lt>ALTER TABLE ${desTableName} ADD COLUMN ${desColInfo.columnName}<#rt>
<#if col.generationExpression?exists && col.generationExpression?length == 0>
<#-- mysql 5.7
    data_type [NOT NULL | NULL] [DEFAULT default_value]
    [AUTO_INCREMENT] [UNIQUE [KEY]] [[PRIMARY] KEY]
    [COMMENT 'string']
    [COLLATE collation_name]
    [COLUMN_FORMAT {FIXED | DYNAMIC | DEFAULT}]
    [STORAGE {DISK | MEMORY}]
    [reference_definition]
-->
    <#lt> ${col.columnType}<#rt>
    <#lt><#if col.isNullable != 'YES'> NOT NULL</#if><#rt>
    <#lt><#if col.columnDefault??> DEFAULT <#if col.colType == "text">'</#if>${col.columnDefault}<#if col.colType == "text">'</#if></#if><#rt>
    <#lt><#if col.extra?exists  && col.extra?length != 0 > ${col.extra}</#if><#rt>
    <#lt><#if col.columnComment?exists && col.columnComment?length != 0 > COMMENT '${col.columnComment}'</#if><#rt>
    <#lt><#if col.collationName?exists && col.collationName?length != 0 > COLLATE ${col.collationName}</#if><#rt>
<#else>
<#-- mysql 5.7
    data_type
    [COLLATE collation_name]
    [GENERATED ALWAYS] AS (expr)
    [VIRTUAL | STORED] [NOT NULL | NULL]
    [UNIQUE [KEY]] [[PRIMARY] KEY]
    [COMMENT 'string']
    [reference_definition]
-->
    <#lt> ${col.columnType}<#rt>
    <#lt><#if col.collationName?exists && col.collationName?length != 0 > COLLATE ${col.collationName}</#if><#rt>
    <#lt> GENERATED ALWAYS AS ${col.generationExpression}<#rt>
    <#lt><#if col.extra?exists  && col.extra?length != 0 > ${col.extra}</#if><#rt>
    <#lt><#if col.columnComment?exists && col.columnComment?length != 0 > COMMENT '${col.columnComment}'</#if><#rt>
</#if>
<#lt>;<#rt>
