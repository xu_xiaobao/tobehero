ALTER TABLE ${desKeyInfo.tableName} DROP PRIMARY KEY;
ALTER TABLE ${desKeyInfo.tableName} ADD PRIMARY KEY(<#list desKeyInfo.colNames as colName>${colName}<#sep>,</#list>);