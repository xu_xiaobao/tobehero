<#if tableComment?? && tableComment != "">-- 表注释：${tableComment!""}</#if>
<#rt>
create table ${tableName} (
<#-- -->
 <#list cols as col>
     `${col.columnName}`<#rt>
     <#if col.generationExpression?exists && col.generationExpression?length == 0>
         <#-- mysql 5.7
             data_type [NOT NULL | NULL] [DEFAULT default_value]
             [AUTO_INCREMENT] [UNIQUE [KEY]] [[PRIMARY] KEY]
             [COMMENT 'string']
             [COLLATE collation_name]
             [COLUMN_FORMAT {FIXED | DYNAMIC | DEFAULT}]
             [STORAGE {DISK | MEMORY}]
             [reference_definition]
         -->
         <#lt> ${col.columnType}<#rt>
         <#lt><#if col.isNullable != 'YES'> NOT NULL</#if><#rt>
         <#lt><#if col.columnDefault??> DEFAULT <#if col.colType == "text">'</#if>${col.columnDefault}<#if col.colType == "text">'</#if></#if><#rt>
         <#lt><#if col.extra?exists  && col.extra?length != 0 > ${col.extra}</#if><#rt>
         <#lt><#if col.columnComment?exists && col.columnComment?length != 0 > COMMENT '${col.columnComment}'</#if><#rt>
         <#lt><#if col.collationName?exists && col.collationName?length != 0 > COLLATE ${col.collationName}</#if><#rt>
     <#else>
         <#-- mysql 5.7
             data_type
             [COLLATE collation_name]
             [GENERATED ALWAYS] AS (expr)
             [VIRTUAL | STORED] [NOT NULL | NULL]
             [UNIQUE [KEY]] [[PRIMARY] KEY]
             [COMMENT 'string']
             [reference_definition]
         -->
         <#lt> ${col.columnType}<#rt>
         <#lt><#if col.collationName?exists && col.collationName?length != 0 > COLLATE ${col.collationName}</#if><#rt>
         <#lt> GENERATED ALWAYS AS ${col.generationExpression}<#rt>
         <#lt><#if col.extra?exists  && col.extra?length != 0 > ${col.extra}</#if><#rt>
         <#lt><#if col.columnComment?exists && col.columnComment?length != 0 > COMMENT '${col.columnComment}'</#if><#rt>
     </#if>
     <#lt><#sep>,
 </#list>
<#if primaryKeyInfo?exists>
    <#lt>,
    PRIMARY KEY (<#rt>
    <#list primaryKeyInfo.colNames as col>
        <#lt>`${col}`<#sep>,
    </#list>
    <#lt>)<#if indexKeyInfos?exists>,</#if>
</#if>
<#if indexKeyInfos?exists>
    <#list indexKeyInfos as indexKeyInfo >
        <#lt>    <#rt>
        <#if indexKeyInfo.unique?exists && !indexKeyInfo.unique && 'FULLTEXT | SPATIAL'?indexOf(indexKeyInfo.indexType) == -1>
            <#lt>KEY<#rt>
            <#lt> ${indexKeyInfo.name} USING ${indexKeyInfo.indexType}<#rt>
            <#lt> (<#list indexKeyInfo.colNames as col><#rt><#lt>`${col}`<#sep>,</#list>)<#sep>,<#rt>
        </#if>
        <#if indexKeyInfo.unique?exists && !indexKeyInfo.unique && 'FULLTEXT | SPATIAL'?indexOf(indexKeyInfo.indexType) != -1>
            <#lt>${indexKeyInfo.indexType} KEY ${indexKeyInfo.name}<#rt>
            <#lt> (<#list indexKeyInfo.colNames as col><#rt><#lt>`${col}`<#sep>,</#list>)<#sep>,<#rt>
        </#if>
        <#if indexKeyInfo.unique?exists && indexKeyInfo.unique>
            <#lt>UNIQUE KEY<#rt>
            <#lt> ${indexKeyInfo.name} USING ${indexKeyInfo.indexType}<#rt>
            <#lt> (<#list indexKeyInfo.colNames as col><#rt><#lt>`${col}`<#sep>,</#list>)<#sep>,<#rt>
        </#if>
        <#--<#lt><#if indexKeyInfo.unique?exists && indexKeyInfo.unique>UNIQUE KEY</#if><#rt>
        <#lt><#if !indexKeyInfo.unique?exists || !indexKeyInfo.unique>KEY</#if><#rt>
        <#lt> ${indexKeyInfo.name} USING ${indexKeyInfo.indexType} <#rt>-->

    </#list>
</#if>

)<#rt>
<#lt><#if engine?exists && engine != ''> ENGINE=${engine}</#if><#rt>
<#lt><#if tableCollation?exists && tableCollation != ''> COLLATE=${tableCollation}</#if><#rt>
<#lt><#if tableComment?exists && tableComment != ''> COMMENT='${tableComment}';</#if>
