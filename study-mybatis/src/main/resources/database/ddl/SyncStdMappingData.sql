DROP TABLE IF EXISTS mapping_data_xy;
CREATE TABLE mapping_data_xy(
    id varchar(100),
    init_pk varchar(100),
    knowledge_type varchar(100),
    drug_name varchar(100),
    dos_name varchar(100),
    std_code varchar(100),
    std_name varchar(100),
    create_time date,
    create_id varchar(100),
    update_time date,
    update_id varchar(100),
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS mapping_data_jb;
CREATE TABLE mapping_data_jb(
    id varchar(100),
    init_pk varchar(100),
    knowledge_type varchar(100),
    dise_name varchar(100),
    std_code varchar(100),
    std_name varchar(100),
    create_time date,
    create_id varchar(100),
    update_time date,
    update_id varchar(100),
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS drug_part_info_01;
CREATE TABLE drug_part_info_01(
    id varchar(100),
    init_pk varchar(100),
    drug_a_name varchar(100),
    drug_b_name varchar(100),
    create_time date,
    create_id varchar(100),
    update_time date,
    update_id varchar(100),
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS drug_part_info_02;
CREATE TABLE drug_part_info_02(
    id varchar(100),
    init_pk varchar(100),
    drug_name varchar(100),
    create_time date,
    create_id varchar(100),
    update_time date,
    update_id varchar(100),
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS drug_part_info_03;
CREATE TABLE drug_part_info_02(
    id varchar(100),
    init_pk varchar(100),
    dise_name varchar(100),
    create_time date,
    create_id varchar(100),
    update_time date,
    update_id varchar(100),
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS drug_part_info_04;
CREATE TABLE drug_part_info_04(
    id varchar(100),
    init_pk varchar(100),
    dos_name varchar(100),
    create_time date,
    create_id varchar(100),
    update_time date,
    update_id varchar(100),
    PRIMARY KEY (`id`)
);