
-- "t1": "and name LIKE '%' || #{keyword_0_name} || '%'\nand name LIKE '%' || #{keyword_1_name} || '%'\nand name LIKE '%' || #{keyword_2_name} || '%'\nand sex = #{match_sex}\nand age = #{match_age}\nand remark LIKE '%' || #{text_remark} || '%'\nand id IN ( #{match_0_id}, #{match_1_id} )",

create table dynamic_query_tb(
    id int,
    name varchar(100),
    age int,
    sex varchar(2),
    remark varchar(200),
    `CREATED_BY` varchar(32) DEFAULT NULL COMMENT '创建人',
	`CREATED_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`UPDATED_BY` varchar(32) DEFAULT NULL COMMENT '更新人',
	`UPDATED_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
	PRIMARY KEY (`id`)
);


