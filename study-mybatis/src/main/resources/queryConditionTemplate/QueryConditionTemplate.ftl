<#compress >
<#list items as item>
    <#if item.searchType == 'MATCH'>
        <#assign inputClass>${item.inputVal.getClass().name.toLowerCase()}</#assign>
        <#if inputClass?contains("list")>
            and ${item.columnName} IN (<#rt>
            <#list item.inputVal as val>
                <#assign paramKey>${'match_' + val?index + '_' + item.columnName }</#assign>
                <#noparse>#{</#noparse>${paramKey}}<#sep >,<#rt>
            </#list> )
            <#elseif inputClass?contains('string')>
                and ${item.columnName} = <#rt>
                <#assign paramKey>${'match_' + item.columnName }</#assign>
                <#noparse>#{</#noparse>${paramKey}}
        </#if>
    </#if>
    <#if item.searchType == 'TEXT'>
        <#assign inputClass>${item.inputVal.getClass().name.toLowerCase()}</#assign>
        <#if inputClass?contains('string')>
            and ${item.columnName} LIKE '%' || <#rt>
            <#assign paramKey>${'text_' + item.columnName }</#assign>
            <#noparse>#{</#noparse>${paramKey}} || '%'
        </#if>
    </#if>
    <#if item.searchType == 'KEYWORD'>
        <#assign inputClass>${item.inputVal.getClass().name.toLowerCase()}</#assign>
        <#if inputClass?contains('list')>
            <#list item.inputVal as val>
                <#assign paramKey>${'keyword_' + val?index + '_' + item.columnName }</#assign>
                and ${item.columnName} LIKE '%' || <#rt>
                <#noparse>#{</#noparse>${paramKey}} || '%'
            </#list>
        </#if>
    </#if>
</#list>
</#compress>
