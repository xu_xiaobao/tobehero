package top.tobehero.study.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.study.mybatis.service.SyncTableService;

/**
 * @description 同步表结构
 * @author xbz
 * @since 2022-07-02
 */
@RestController
public class SyncTableController {

    @Autowired
    private SyncTableService syncTableService;
    /**
     * 同步源表结构至目标表
     * @param srcTable
     * @param desTable
     * @return
     */
    public String syncTable(String srcTable,String desTable){
        syncTableService.syncTables(srcTable,desTable);
        return "success";
    }
}
