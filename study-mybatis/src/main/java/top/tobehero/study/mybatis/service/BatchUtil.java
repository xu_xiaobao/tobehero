package top.tobehero.study.mybatis.service;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BatchUtil {
    public static void runTasks(List<AbstractTwoPCTask> twoPCTaskList){
        int taskCount = twoPCTaskList.size();
        ExecutorService executorService = Executors.newFixedThreadPool(taskCount);
        // 任务启动前准备工作
        CountDownLatch taskDownLatch = new CountDownLatch(taskCount);
        CyclicBarrier oneStepBarrier = new CyclicBarrier(taskCount);
        AtomicBoolean success = new AtomicBoolean(true);
        for (int i = 0; i < taskCount; i++) {
            AbstractTwoPCTask task = twoPCTaskList.get(i);
            Runnable taskRunnable = () -> {
                try {
                    try {
                        // 开启事务
                        // 处理业务逻辑
                        task.doTask();
                    } catch (Exception e) {
                        e.printStackTrace();
                        success.set(false);
                    }
                    // 等待全部执行完成
                    while (true) {
                        try {
                            oneStepBarrier.await();
                            break;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (BrokenBarrierException e) {
                            e.printStackTrace();
                        }
                    }
                    // 根据结果执行提交或者回滚
                    if (success.get()) {
                        // 提交
                        task.commit();
                    }else {
                        // 回滚
                        task.rollback();
                    }
                } finally {
                    taskDownLatch.countDown();
                }
            };
            executorService.submit(taskRunnable);
        }
        executorService.shutdown();
        while(!executorService.isTerminated()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
