package top.tobehero.study.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.tobehero.study.mybatis.config.SyncStdMappingDataConfigProperties;
import top.tobehero.study.mybatis.dto.generic.SyncStdMappingDeleteDTO;
import top.tobehero.study.mybatis.dto.generic.SyncStdMappingInsertDTO;

import java.util.List;
import java.util.Map;

@Mapper
public interface SyncStdMappingDataMapper {

    /**
     * 查询源表数据
     * @param tableName
     * @param columnInfoList
     * @param knowledgeType
     * @param initPk
     * @return
     */
    List<Map<String, Object>> selectSourceDataList(String tableName, List<SyncStdMappingDataConfigProperties.SourceTableColumnInfo> columnInfoList, String knowledgeType, String initPk);

    /**
     * 查询标准映射数据
     * @param tableName
     * @param columnInfoList
     * @param knowledgeType
     * @param initPk
     * @return
     */
    List<Map<String, Object>> selectMappingDataList(String tableName, List<SyncStdMappingDataConfigProperties.ColumnInfo> columnInfoList, String knowledgeType, String initPk);

    /**
     * 批量新增
     * @param insertDTO
     * @return
     */
    int batchInsertMappingData(SyncStdMappingInsertDTO insertDTO);

    /**
     * 批量删除
     * @param deleteDTO
     */
    int batchDeleteMappingData(SyncStdMappingDeleteDTO deleteDTO);
}
