package top.tobehero.study.mybatis.utils;

import lombok.Data;

import java.util.List;

@Data
public class QueryConditionItem {
    private String columnName;
    private String searchType;
    private Object inputVal;
    private List<WhereSegment> whereSegments;
}
