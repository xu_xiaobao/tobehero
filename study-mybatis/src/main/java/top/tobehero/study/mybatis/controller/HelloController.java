package top.tobehero.study.mybatis.controller;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.study.mybatis.entity.Hero;
import top.tobehero.study.mybatis.mapper.HeroMapper;
import top.tobehero.study.mybatis.mapper.DeceptiveMapper;

@RestController
@RequestMapping("/hello")
public class HelloController {
    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    private HeroMapper heroMapper;

//    @Autowired
    private DeceptiveMapper deceptiveMapper;

    @RequestMapping("/**")
    public String all(){
        return appName;
    }

    @RequestMapping("/sqlSessionFactory")
    public String sqlSessionFactory(){
        return sqlSessionFactory.toString();
    }

    @RequestMapping("/getUser")
    public String getUser(String username){
        Hero user = heroMapper.getUser(username);
        return user.toString();
    }

    @RequestMapping("/getDeceptiveMapper")
    public String getDeceptiveMapper(){
        return this.deceptiveMapper.sayHello();
    }
}
