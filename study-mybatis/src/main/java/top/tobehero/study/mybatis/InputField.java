package top.tobehero.study.mybatis;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InputField {
    /**
     * 是否必填<br>
     *     String: not blank
     *     List/Array: not empty
     *     other: not null
     * @return
     */
    boolean required() default false;
    boolean hidden() default false;
    SearchType searchType() default SearchType.NONE;
    String columnName() default "";
    String desc() default "";

    enum SearchType{
        /**
         * 普通文本，模糊搜索
         */
        TEXT,
        /**
         * 全匹配检索
         */
        MATCH,
        /**
         * 关键字，复合搜索
         */
        KEYWORD,
        /**
         * 不支持检索
         */
        NONE
    }
}
