package top.tobehero.study.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface DynamicQuerySqlMapper {
    /**
     * 动态条件查询
     * @param qryMap
     * @return
     */
    List<Map<String,Object>> queryByDynamicCondition(Map<String,Object> qryMap);
}
