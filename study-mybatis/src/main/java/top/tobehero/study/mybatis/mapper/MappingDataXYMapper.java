package top.tobehero.study.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.tobehero.study.mybatis.entity.MappingDataXY;

@Mapper
public interface MappingDataXYMapper extends BaseMapper<MappingDataXY> {
}
