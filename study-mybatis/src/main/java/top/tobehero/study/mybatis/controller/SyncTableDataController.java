package top.tobehero.study.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.study.mybatis.service.SyncStdMappingDataService;

@RestController
@RequestMapping(value = "/sync-data")
public class SyncTableDataController {
    @Autowired
    private SyncStdMappingDataService syncStdMappingDataService;
    @RequestMapping("/execute")
    public void syncTableData(){
        syncStdMappingDataService.syncMappingData("Drug", "1");
    }
}
