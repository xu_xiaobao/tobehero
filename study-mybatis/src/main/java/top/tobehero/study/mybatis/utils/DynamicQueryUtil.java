package top.tobehero.study.mybatis.utils;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.thoughtworks.xstream.XStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class DynamicQueryUtil {
    private static final XStream X_STREAM = new XStream();
    private static Map<String,DynamicQueryConfig> ALL_CONFIG_MAP = new HashMap<>();
    private static boolean startup = false;

    public static synchronized void loadConfiguration() throws IOException {
        if (startup) {
            return;
        }
        X_STREAM.setClassLoader(DynamicQueryUtil.class.getClassLoader());
        X_STREAM.processAnnotations(DynamicQueryConfigRoot.class);
        X_STREAM.allowTypes(new Class[]{DynamicQueryConfigRoot.class, DynamicQueryConfig.class, DynamicQueryColumn.class, DynamicQueryConfigProperty.class});
        // 读取查询配置信息
        ClassPathResource querysFileResource = new ClassPathResource("/querys");
        File querysFileDir = querysFileResource.getFile();
        File[] files = querysFileDir.listFiles();
        if (files != null) {
            for (File file : files) {
                parseQueryConfig(file);
            }
        }
        startup = true;
    }

    private static void parseQueryConfig(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File childFile : files) {
                parseQueryConfig(childFile);
            }
        } else if (file.isFile()) {
            String fileName = file.getName();
            String suffix = FileUtil.getSuffix(fileName);
            if (suffix.equals("xml")) {
                DynamicQueryConfigRoot root = (DynamicQueryConfigRoot) X_STREAM.fromXML(file);
                List<DynamicQueryConfig> queryList = root.getQueryList();
                if (queryList != null) {
                    String namespace = root.getNamespace();
                    for (DynamicQueryConfig config : queryList) {
                        config.init();
                        String queryId = config.getId();
                        String key = namespace + "::"+queryId;
                        DynamicQueryConfig oldVal = ALL_CONFIG_MAP.putIfAbsent(key, config);
                        if (oldVal != null && oldVal != config) {
                            log.warn("存在重复配置项：namespace:{},ids:[{},{}]", namespace, queryId, config.getId());
                        }
                    }
                }
            }
        }
    }

    /**
     * 获取配置信息
     * @param namespace
     * @param queryId
     * @return
     */
    public static DynamicQueryConfig getQueryConfig(String namespace, String queryId) {
        DynamicQueryConfig config = ALL_CONFIG_MAP.get(namespace + "::" + queryId);
        if (config == null) {
            throw new IllegalStateException("未找到查询参数配置");
        }
        return config;
    }

    public static void main(String[] args) throws IOException {
        DynamicQueryConfig config = getQueryConfig("hello", "queryDynamicDetailList");
        System.out.println(JSON.toJSONString(config, SerializerFeature.PrettyFormat));
    }

}
