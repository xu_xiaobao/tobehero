package top.tobehero.study.mybatis.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Hero {
    /**
     * 用户名称
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    private String name;

    /**
     * 0:男1:女
     */
    private String sex;

    /**
     * 手机联系电话
     */
    private String phone;

    /**
     * 电子邮箱
     */
    private String email;

    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;
}
