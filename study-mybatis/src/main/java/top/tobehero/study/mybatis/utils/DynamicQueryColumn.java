package top.tobehero.study.mybatis.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@XStreamAlias("column")
public class DynamicQueryColumn {
    private String name;
    private String alias;
    private String zhName;
    private boolean hidden;
    private boolean supportOrder;

    public String getFieldName(){
        if (StringUtils.isNotBlank(alias)) {
            return com.baomidou.mybatisplus.core.toolkit.StringUtils.underlineToCamel(alias);
        }
        return com.baomidou.mybatisplus.core.toolkit.StringUtils.underlineToCamel(name);
    }
}
