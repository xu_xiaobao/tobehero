package top.tobehero.study.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.study.mybatis.service.ReadBigDataService;

import java.util.Map;

@RestController
@RequestMapping(value = "/readBigData")
public class ReadBigDataController {

    @Autowired
    private ReadBigDataService readBigDataService;
    /**
     * 插入数据
     * @param count
     */
    @RequestMapping(value = "/insert")
    public void insert(@RequestParam(defaultValue = "100") Integer count) {
        readBigDataService.insert(count);
    }

    @RequestMapping(value = "/memoryCost")
    public Map<String,Object> memoryCost(@RequestParam(defaultValue = "100") Integer count) {
        return readBigDataService.memoryCost(count);
    }
    @RequestMapping(value = "/reloadCache")
    public Map<String,Object> reloadCache() {
        return readBigDataService.reloadCache();
    }


}
