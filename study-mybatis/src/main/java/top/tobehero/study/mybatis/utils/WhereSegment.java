package top.tobehero.study.mybatis.utils;

import lombok.Data;

import java.util.List;

@Data
public class WhereSegment {
    private String columnName;
    private WhereOperateType operateType;
    private List<String> paramKeys;
    private List<Object> paramVals;
    enum WhereOperateType {
        IN,
        EQ,
        LIKE
    }
}
