package top.tobehero.study.mybatis.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import top.tobehero.study.mybatis.entity.Hero;

@Mapper
public interface HeroMapper {
    @Select("select * from hero where username = #{username}")
    Hero getUser(@Param("username") String username);
}
