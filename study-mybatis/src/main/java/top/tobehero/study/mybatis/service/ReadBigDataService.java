package top.tobehero.study.mybatis.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.lucene.util.RamUsageEstimator;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import top.tobehero.study.mybatis.entity.TestDO;
import top.tobehero.study.mybatis.mapper.TestMapper;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ReadBigDataService {
    @Autowired
    private TestMapper testMapper;
    @Autowired
    private DatabaseFuzzyCache databaseFuzzyCache;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insert(Integer insertRows) {
        long startTime = System.currentTimeMillis();
        int taskCount = 20;
        int a = insertRows / taskCount;
        int b = insertRows % taskCount;
        if (b != 0) {
            a = a + 1;
        }
        int splitSize = a;
        List<AbstractTwoPCTask> twoPCTasks = new ArrayList<>(taskCount);
        for (int i = 0; i < taskCount; i++) {
            int taskI = i;
            twoPCTasks.add(new AbstractTwoPCTask() {
                TransactionStatus transaction;

                @Override
                public void doTask() {
                    log.info("开启事务");
                    DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
                    transactionDefinition.setPropagationBehavior(Propagation.REQUIRES_NEW.value());
                    transaction = transactionManager.getTransaction(transactionDefinition);
                    TestDO testDO = new TestDO();
                    int start = taskI * splitSize;
                    int end = start + splitSize;
                    if (end > insertRows) {
                        end = insertRows;
                    }
                    for (int i = start; i < end; i++) {
                        testDO.setId(null);
                        String name = "name_" + i;
                        String groupName = "groupName_" + taskI;
                        testDO.setName(name);
                        testDO.setGroupName(groupName);
                        testMapper.insert(testDO);
                    }
                }

                @Override
                public void commit() {
                    log.info("提交事务");
                    transactionManager.commit(transaction);
                }

                @Override
                public void rollback() {
                    log.info("回滚事务");
                    transactionManager.rollback(transaction);
                }
            });
        }
        BatchUtil.runTasks(twoPCTasks);
        log.info("多线程批量插入耗时:{}ms", System.currentTimeMillis() - startTime);
    }

    /**
     * @param rows
     * @return
     */
    public Map<String, Object> memoryCost(Integer rows) {
        List<TestDO> testDOS = testMapper.selectList(new LambdaQueryWrapper<TestDO>()
        );
        Map<String, List<TestDO>> listMap = testDOS.stream().collect(Collectors.groupingBy(TestDO::getGroupName));
        listMap.forEach((k, v) -> {
            v.stream().forEach((item) -> {
                databaseFuzzyCache.addWord(k, item.getName());
            });
        });
        String humanSizeOf = RamUsageEstimator.humanSizeOf(databaseFuzzyCache);
        HashMap<String, Object> res = new HashMap<>();
        res.put("size", databaseFuzzyCache.totalSize());
        res.put("sizeOf", humanSizeOf);
        res.put("version", databaseFuzzyCache.getTimestamp());
        return res;
    }


    @SneakyThrows
    public Map<String, Object> reloadCache() {
        System.out.println("preCleanupSize:" + databaseFuzzyCache.totalSize());
        databaseFuzzyCache.updateTimestamp(System.currentTimeMillis());
        // 分批读取
//        try (SqlSession sqlSession = sqlSessionFactory.openSession();
//             Cursor<TestDO> listCursor = sqlSession.selectCursor("top.tobehero.study.mybatis.mapper.TestMapper.selectByCursor", null, new RowBounds())) {
//            for (TestDO testDO : listCursor) {
//                databaseFuzzyCache.addWord(testDO.getGroupName(), testDO.getName());
//            }
//        }
        // 游标 fetchSize不生效
//        testMapper.selectTestList(new ResultHandler<TestDO>() {
//            @Override
//            public void handleResult(ResultContext<? extends TestDO> resultContext) {
//                TestDO obj = resultContext.getResultObject();
//                databaseFuzzyCache.addWord(obj.getGroupName(), obj.getName());
//            }
//        });
        // jdbc 分批获取
//        int fetchSize = jdbcTemplate.getFetchSize();
//        System.out.println("fetchSize:" + fetchSize);
//        jdbcTemplate.query(con -> {
//            PreparedStatement statement = con.prepareStatement("select * from test", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
//            statement.setFetchSize(1000);
//            return statement;
//        }, rs -> {
//            String group_name = rs.getString("group_name");
//            String name = rs.getString("name");
//            databaseFuzzyCache.addWord(group_name, name);
//        });
        // jdbc 原生分批获取
        List<TestDO> allList = getTestDOS(100);
//        List<TestDO> allList = getTestDOS(50000);

//        allList = testMapper.selectList(new LambdaQueryWrapper<>());
        String x = RamUsageEstimator.humanSizeOf(allList);
        System.out.println("size:"+x);


        // 分页读取
//        long pageIndex = 1;
//        long pageSize = 10000;
//        List<TestDO> records;
//        while (true) {
//            Page<TestDO> page = testMapper.selectPage(new Page<>(pageIndex++, pageSize), new LambdaQueryWrapper<>());
//            records = page.getRecords();
//            if (CollectionUtils.isEmpty(records)) {
//                break;
//            }
//            records.stream().collect(Collectors.groupingBy(TestDO::getGroupName)).forEach((k, v) -> {
//                v.stream().forEach((item) -> databaseFuzzyCache.addWord(k, item.getName()));
//            });
//        }

        // 普通读取
//        List<TestDO> testDOS = testMapper.selectList(new LambdaQueryWrapper<>());
//        testDOS.stream().collect(Collectors.groupingBy(TestDO::getGroupName)).forEach((k,v) -> {
//            v.stream().forEach((item) ->databaseFuzzyCache.addWord(k,item.getName()));
//        });
        databaseFuzzyCache.evictInvalid();
        System.out.println("postCleanupSize:" + databaseFuzzyCache.totalSize());
        String humanSizeOf = RamUsageEstimator.humanSizeOf(databaseFuzzyCache);
        HashMap<String, Object> res = new HashMap<>();
        res.put("size", databaseFuzzyCache.totalSize());
        res.put("sizeOf", humanSizeOf);
        res.put("version", databaseFuzzyCache.getTimestamp());
        return res;
    }

    private List<TestDO> getTestDOS(int fetchSize) throws SQLException {
        long startIime = System.currentTimeMillis();
        List<TestDO> allList;
        try {
            allList = new ArrayList<>();
            DataSource dataSource = jdbcTemplate.getDataSource();
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement("select * from test", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
                statement.setFetchSize(fetchSize);
                boolean execute = statement.execute();
                ResultSet rs = statement.getResultSet();
                while (rs.next()) {
                    long id = rs.getLong("id");
                    String group_name = rs.getString("group_name");
                    String name = rs.getString("name");
    //                databaseFuzzyCache.addWord(group_name, name);
                    TestDO testDO = new TestDO();
                    testDO.setId(BigInteger.valueOf(id));
                    testDO.setGroupName(group_name);
                    testDO.setName(name);
                    allList.add(testDO);
                }
            }
        } finally {
            System.out.println("fetchSize:" + fetchSize + ", costTime:" + (System.currentTimeMillis() - startIime));
        }
        return allList;
    }
}
