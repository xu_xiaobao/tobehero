package top.tobehero.study.mybatis.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

@Data
@XStreamAlias("property")
public class DynamicQueryConfigProperty {
    @XStreamAsAttribute
    private String key;
    @XStreamAsAttribute
    private String value;
}
