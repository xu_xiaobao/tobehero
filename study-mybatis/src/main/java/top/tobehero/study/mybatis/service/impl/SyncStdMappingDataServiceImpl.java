package top.tobehero.study.mybatis.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.tobehero.study.mybatis.service.SyncStdMappingDataExecutor;
import top.tobehero.study.mybatis.service.SyncStdMappingDataService;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class SyncStdMappingDataServiceImpl implements SyncStdMappingDataService {
    @Autowired
    private List<SyncStdMappingDataExecutor> executorServices;

    @Override
    public void syncMappingData(String knowledgeType, String initPk) {
        CompletableFuture[] completableFutures = executorServices.stream().filter(i->i.getKnowledgeType().equals(knowledgeType)).map(executeService -> CompletableFuture.supplyAsync(() -> {
            executeService.execute(initPk);
            return Void.class;
        })).toArray(CompletableFuture[]::new);
        // 多类型并行执行
        CompletableFuture.allOf(completableFutures).exceptionally(throwable -> {
            log.error("同步映射数据错误", throwable);
            throw new RuntimeException(throwable);
        });
    }

}
