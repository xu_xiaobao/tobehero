package top.tobehero.study.mybatis.utils;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.lang.reflect.Field;

@Data
public class FieldErrInfo {
    private String fieldName;
    @JsonIgnore
    @JSONField(serialize = false, deserialize = false)
    private Field field;
    private String errMsg;
}
