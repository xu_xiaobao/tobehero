package top.tobehero.study.mybatis.dto.generic.res;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class QueryMultiTableDataResDTO {
    private Integer pkName;
    private String name;
    private Integer age;
    private String sex;
    private String remark;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
}
