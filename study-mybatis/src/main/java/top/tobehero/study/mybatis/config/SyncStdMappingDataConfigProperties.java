package top.tobehero.study.mybatis.config;

import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "sync-std-mapping-data")
@Data
public class SyncStdMappingDataConfigProperties {
    /**
     * 配置项
     */
    private Map<String, List<SyncStdMappingDataConfig>> config;


    public SyncStdMappingDataConfig getConfigBy(String knowledgeType, String mappingType) {
        return config.getOrDefault(knowledgeType, Lists.newArrayList()).stream().filter(i->i.getMappingType().equals(mappingType)).findFirst().orElseThrow(()->new RuntimeException("未找到配置项！"));
    }
    @Data
    public static class SyncStdMappingDataConfig {
        private String mappingType;
        private String tableName;
        private List<ColumnInfo> columnInfoList;
    }

    @Data
    public static class ColumnInfo {

        /**
         * 列名称
         */
        private String columnName;

        /**
         * 数据来源表
         */
        private List<SourceTableColumnInfo> sourceTableList;
    }

    @Data
    public static class SourceTableColumnInfo {
        /**
         * 表名称
         */
        private String tableName;
        /**
         * 列名称
         */
        private String columnName;
    }
}
