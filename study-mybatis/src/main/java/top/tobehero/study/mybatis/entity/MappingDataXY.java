package top.tobehero.study.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "mapping_data_xy")
public class MappingDataXY {
    @TableId(value = "id")
    private String id;
    private String initPk;
    private String knowledgeType;
    private String drugName;
    private String dosName;
    private String stdCode;
    private String stdName;
    private Date createTime;
    private String createId;
    private Date updateTime;
    private String updateId;
}
