package top.tobehero.study.mybatis.service;

/**
 * 同步映射数据执行器
 */
public interface SyncStdMappingDataExecutor {
    /**
     * 得到知识类型
     *
     * @return
     */
    String getKnowledgeType();

    /**
     * 得到映射类型
     *
     * @return
     */
    String getMappingType();

    /**
     * 执行同步数据
     */
    void execute(String initPk);
}
