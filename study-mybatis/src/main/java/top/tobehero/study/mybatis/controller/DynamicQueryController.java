package top.tobehero.study.mybatis.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.util.function.Tuple2;
import top.tobehero.study.mybatis.dto.generic.req.QueryMultiTableDataReq;
import top.tobehero.study.mybatis.service.GenericDynamicService;
import top.tobehero.study.mybatis.utils.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 通用接口设计案例<br>
 * <ul>
 * <li>自定义参数验证</li>
 * </ul>
 */
@RestController
@RequestMapping(value = "/generic")
public class DynamicQueryController {
    @Autowired
    private GenericDynamicService genericDynamicService;

    @RequestMapping("/testValidateParams")
    public List<FieldErrInfo> testValidateParams(@RequestBody String reqBody) {
        QueryMultiTableDataReq reqDTO = JSON.parseObject(reqBody, QueryMultiTableDataReq.class);
        List<FieldErrInfo> fieldErrInfos = InputFieldUtil.validateParams(reqDTO);
        return fieldErrInfos;
    }

    @RequestMapping("/parseQueryCondition")
    public ResponseEntity parseQueryCondition(@RequestBody String reqBody) {
        QueryMultiTableDataReq reqDTO = JSON.parseObject(reqBody, QueryMultiTableDataReq.class);
        List<FieldErrInfo> fieldErrInfos = InputFieldUtil.validateParams(reqDTO);
        if (CollectionUtil.isNotEmpty(fieldErrInfos)) {
            String errMsg = fieldErrInfos.stream().map((fieldErrInfo -> fieldErrInfo.getFieldName() + ":" + fieldErrInfo.getErrMsg())).collect(Collectors.joining(","));
            return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
        }
        QueryCondition queryCondition = InputFieldUtil.parseQueryCondition(reqDTO);
        return new ResponseEntity(queryCondition, HttpStatus.OK);
    }

    @RequestMapping("/dynamicSqlQuery")
    public ResponseEntity dynamicSqlQuery(@RequestBody String reqBody) {
        QueryMultiTableDataReq reqDTO = JSON.parseObject(reqBody, QueryMultiTableDataReq.class);
        List<FieldErrInfo> fieldErrInfos = InputFieldUtil.validateParams(reqDTO);
        if (CollectionUtil.isNotEmpty(fieldErrInfos)) {
            String errMsg = fieldErrInfos.stream().map((fieldErrInfo -> fieldErrInfo.getFieldName() + ":" + fieldErrInfo.getErrMsg())).collect(Collectors.joining(","));
            return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
        }
        Tuple2<String, Map<String, Object>> sqlAndQryParamMap = InputFieldUtil.parseConditionAndParamMap(reqDTO);
        return new ResponseEntity(sqlAndQryParamMap, HttpStatus.OK);
    }

    @RequestMapping("/queryDetailList")
    public ResponseEntity queryDetailList(@RequestBody String reqBody) {
        DynamicQueryConfig queryConfig = DynamicQueryUtil.getQueryConfig("hello", "queryDynamicDetailList");
        Class<?> reqDTOClass = queryConfig.getWhereConditionClass();
        Object reqDTO = JSON.parseObject(reqBody, reqDTOClass);
        List<FieldErrInfo> fieldErrInfos = InputFieldUtil.validateParams(reqDTO);
        if (CollectionUtil.isNotEmpty(fieldErrInfos)) {
            String errMsg = fieldErrInfos.stream().map((fieldErrInfo -> fieldErrInfo.getFieldName() + ":" + fieldErrInfo.getErrMsg())).collect(Collectors.joining(","));
            return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
        }
        List<?> resMap = genericDynamicService.queryList(reqDTO, queryConfig ,queryConfig.getResultTypeClass());
        return new ResponseEntity(resMap, HttpStatus.OK);
    }

    @RequestMapping(value = "/queryFormInfo")
    public ResponseEntity queryFormInfo(String namespace, String id) {
        DynamicQueryConfig queryConfig = DynamicQueryUtil.getQueryConfig(namespace, id);
        List<DynamicQueryColumn> selectColumns = queryConfig.getSelectColumns();
        return new ResponseEntity(selectColumns, HttpStatus.OK);
    }

}
