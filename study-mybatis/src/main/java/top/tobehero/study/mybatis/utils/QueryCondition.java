package top.tobehero.study.mybatis.utils;

import lombok.Data;

import java.util.*;

@Data
public class QueryCondition {
    private List<QueryConditionItem> items;

    public List<WhereSegment> getAllWhereSegments() {
        ArrayList<WhereSegment> allSegment = new ArrayList<>();
        if (items != null) {
            for (QueryConditionItem item : items) {
                allSegment.addAll(item.getWhereSegments());
            }
        }
        return Collections.unmodifiableList(allSegment);
    }

    public Map<String,Object> getQryParamMap(){
        LinkedHashMap<String, Object> qryMap = new LinkedHashMap<>();
        if (items != null) {
            for (QueryConditionItem item : items) {
                List<WhereSegment> whereSegments = item.getWhereSegments();
                if (whereSegments != null) {
                    for (WhereSegment whereSegment : whereSegments) {
                        List<String> paramKeys = whereSegment.getParamKeys();
                        List<Object> paramVals = whereSegment.getParamVals();
                        for (int i = 0; i < paramKeys.size(); i++) {
                            String paramKey = paramKeys.get(i);
                            Object paramVal = paramVals.get(i);
                            qryMap.put(paramKey, paramVal);
                        }
                    }
                }
            }
        }
        return qryMap;
    }
}
