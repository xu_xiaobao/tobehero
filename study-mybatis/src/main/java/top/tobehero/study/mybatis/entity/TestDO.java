package top.tobehero.study.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigInteger;

@TableName("test")
@Data
public class TestDO {
    @TableId(type = IdType.AUTO)
    private BigInteger id;
    private String name;
    private String groupName;
}
