package top.tobehero.study.mybatis.dto.generic.req;

import lombok.Data;
import top.tobehero.study.mybatis.InputField;

import java.util.List;

@Data
public class QueryMultiTableDataReq {
    @InputField(required = true, desc = "姓名", searchType = InputField.SearchType.KEYWORD)
    private String name;

    @InputField(desc = "性别", searchType = InputField.SearchType.MATCH)
    private String sex;

    @InputField(required = true, desc = "年龄", searchType = InputField.SearchType.MATCH)
    private Integer age;

    @InputField(desc = "备注", searchType = InputField.SearchType.TEXT)
    private String remark;

    @InputField(desc = "主键ID列表", searchType = InputField.SearchType.MATCH , columnName = "id")
    private List<Integer> ids;

    @InputField(required = true, desc = "关联信息")
    private RelationInfo relationInfo;

    private List<OrderByColumn> orderByColumns;

    @Data
    public static class RelationInfo {
        @InputField(required = true, desc = "姓名")
        private String name;
        @InputField(required = true, desc = "关系")
        private String relation;

        @InputField(desc = "年龄")
        private Integer age;

        @InputField(required = true,desc = "标签列表")
        private List<String> tags;
    }

    @Data
    public static class OrderByColumn {
        private String name;
        private String order;
    }

}
