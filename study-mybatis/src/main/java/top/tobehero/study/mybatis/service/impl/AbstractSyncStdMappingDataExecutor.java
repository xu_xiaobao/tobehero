package top.tobehero.study.mybatis.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;
import top.tobehero.study.mybatis.config.SyncStdMappingDataConfigProperties;
import top.tobehero.study.mybatis.dto.generic.SyncStdMappingDeleteDTO;
import top.tobehero.study.mybatis.dto.generic.SyncStdMappingInsertDTO;
import top.tobehero.study.mybatis.mapper.SyncStdMappingDataMapper;
import top.tobehero.study.mybatis.service.SyncStdMappingDataExecutor;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractSyncStdMappingDataExecutor implements SyncStdMappingDataExecutor {
    /**
     * 知识类型
     */
    @Getter
    protected String knowledgeType;
    /**
     * 映射类型
     */
    @Getter
    private String mappingType;

    public AbstractSyncStdMappingDataExecutor(String knowledgeType, String mappingType) {
        this.knowledgeType = knowledgeType;
        this.mappingType = mappingType;
    }

    @Autowired
    private SyncStdMappingDataConfigProperties syncStdMappingDataConfigProperties;
    @Autowired
    private SyncStdMappingDataMapper syncStdMappingDataMapper;

    @Override
    public void execute(String initPk) {
        // 查询已存在的标准映射数据
        List<Map<String, Object>> existsMappingDataList = queryExistsMappingDataList(initPk);
        // 查询标准映射来源数据
        Map<String, List<Map<String, Object>>> sourceDataMap = querySourceDataMap(initPk);
        // 对比数据
        Map<Tuple2<String, String>, List<Object>> removeDataMap = new HashMap<>();
        Map<Tuple2<String, String>, List<Object>> addDataMap = new HashMap<>();
        diffData(existsMappingDataList, sourceDataMap, addDataMap, removeDataMap);
        // 数据入库
        insertDataList(initPk,addDataMap);
        deleteDataList(initPk,removeDataMap);
    }

    /**
     * 数据数据列表
     * @param initPk
     * @param removeDataMap
     */
    protected void deleteDataList(String initPk, Map<Tuple2<String, String>, List<Object>> removeDataMap) {
        if (!removeDataMap.isEmpty()) {
            removeDataMap.forEach((columnInfo, dataList)->{
                String tableName = columnInfo.getT1();
                String columnName = columnInfo.getT2();
                SyncStdMappingDeleteDTO deleteDTO = new SyncStdMappingDeleteDTO();
                deleteDTO.setTableName(tableName);
                deleteDTO.setColumnName(columnName);
                deleteDTO.setKnowledgeType(knowledgeType);
                deleteDTO.setInitPk(initPk);
                Lists.partition(dataList, 500).forEach(listPar->{
                    deleteDTO.setRowDataList(listPar);
                    syncStdMappingDataMapper.batchDeleteMappingData(deleteDTO);
                });
            });
        }
    }

    /**
     * 添加数据列表
     * @param addDataMap
     */
    protected void insertDataList(String initPk, Map<Tuple2<String, String>, List<Object>> addDataMap) {
        if (!addDataMap.isEmpty()) {
            Snowflake snowflake = new Snowflake();
            addDataMap.forEach((columnInfo, dataList)->{
                String tableName = columnInfo.getT1();
                String columnName = columnInfo.getT2();
                SyncStdMappingInsertDTO insertDTO = new SyncStdMappingInsertDTO();
                insertDTO.setColumnName(columnName);
                insertDTO.setTableName(tableName);
                insertDTO.setNowTime(new Date());
                insertDTO.setKnowledgeType(knowledgeType);
                insertDTO.setMappingType(mappingType);
                insertDTO.setInitPk(initPk);
                // 获取主键名称
                TableInfo tableInfo = TableInfoHelper.getTableInfo(tableName);
                String pkName = tableInfo.getKeyColumn();
                insertDTO.setPkName(pkName);
                Lists.partition(dataList, 500).forEach(listPar->{
                    List<TreeMap<String, Object>> rowDataList = listPar.stream().map(listItem -> {
                        TreeMap<String, Object> rowData = new TreeMap<>();
                        rowData.put(columnName, listItem);
                        rowData.put(pkName, snowflake.nextIdStr());
                        return rowData;
                    }).collect(Collectors.toList());
                    insertDTO.setRowDataList(rowDataList);
                    syncStdMappingDataMapper.batchInsertMappingData(insertDTO);
                });
            });
        }
    }

    /**
     * 对比数据
     *
     * @param existsMappingDataList
     * @param sourceDataMap
     * @param addDataMap            新增数据
     * @param removeDataMap         删除数据
     */
    protected void diffData(List<Map<String, Object>> existsMappingDataList, Map<String, List<Map<String, Object>>> sourceDataMap, Map<Tuple2<String, String>, List<Object>> addDataMap, Map<Tuple2<String, String>, List<Object>> removeDataMap) {
        // 单字段对比
        SyncStdMappingDataConfigProperties.SyncStdMappingDataConfig config = syncStdMappingDataConfigProperties.getConfigBy(knowledgeType, mappingType);
        List<SyncStdMappingDataConfigProperties.ColumnInfo> columnInfoList = config.getColumnInfoList();
        for (SyncStdMappingDataConfigProperties.ColumnInfo columnInfo : columnInfoList) {
            String columnName = columnInfo.getColumnName();
            Set<Object> mappingDataSet = existsMappingDataList.stream().map(rowData -> rowData.get(columnName)).filter(Objects::nonNull).collect(Collectors.toSet());
            Set<Object> sourceDataSet = columnInfo.getSourceTableList()
                    .stream()
                    .map(sourceTableColumnInfo -> {
                        List<Map<String, Object>> rowList = sourceDataMap.get(sourceTableColumnInfo.getTableName());
                        if (CollectionUtils.isEmpty(rowList)) {
                            return null;
                        }
                        return rowList.stream().map(rowData -> rowData.get(sourceTableColumnInfo.getColumnName())).distinct().collect(Collectors.toList());
                    }).filter(Objects::nonNull).flatMap(Collection::stream).filter(Objects::nonNull).collect(Collectors.toSet());
            List<Object> removeList = Lists.newArrayList();
            removeList.addAll(mappingDataSet);
            removeList.removeAll(sourceDataSet);
            List<Object> addList = Lists.newArrayList();
            addList.addAll(sourceDataSet);
            addList.removeAll(mappingDataSet);

            if (CollectionUtils.isNotEmpty(addList)) {
                addDataMap.put(Tuples.of(config.getTableName(), columnName), addList);
            }
            if (CollectionUtils.isNotEmpty(removeList)) {
                removeDataMap.put(Tuples.of(config.getTableName(), columnName), removeList);
            }
        }
    }

    public static void main(String[] args) {
        ArrayList<String> aList = new ArrayList<>();
        aList.add("Hello");
        aList.add("World");
        aList.add("no word");

        ArrayList<String> bList = new ArrayList<>();
        bList.add("Hello");
        bList.add("World1");

        ArrayList aListCopy = (ArrayList) aList.clone();
        aListCopy.retainAll(bList);
        System.out.println("交集（保留）:" + aListCopy);

        ArrayList<String> removeB = new ArrayList(aList);
        removeB.removeAll(bList);
        System.out.println("A-B（移除）:" + removeB);

        ArrayList<String> removeA = new ArrayList(bList);
        removeA.removeAll(aList);
        System.out.println("B-A（新增）:" + removeA);


    }

    /**
     * 查询标准映射来源数据
     *
     * @param initPk
     * @return
     */
    protected Map<String, List<Map<String, Object>>> querySourceDataMap(String initPk) {
        SyncStdMappingDataConfigProperties.SyncStdMappingDataConfig config = syncStdMappingDataConfigProperties.getConfigBy(knowledgeType, mappingType);
        List<SyncStdMappingDataConfigProperties.ColumnInfo> columnInfoList = config.getColumnInfoList();
        Map<String, List<SyncStdMappingDataConfigProperties.SourceTableColumnInfo>> sourceTableGroup = columnInfoList.stream().map(SyncStdMappingDataConfigProperties.ColumnInfo::getSourceTableList).flatMap(Collection::stream).collect(Collectors.groupingBy(SyncStdMappingDataConfigProperties.SourceTableColumnInfo::getTableName));
        HashMap<String, List<Map<String, Object>>> resMap = new HashMap<>();
        sourceTableGroup.forEach((tableName, columnList) -> {
            List<Map<String, Object>> sourceDataList = syncStdMappingDataMapper.selectSourceDataList(tableName, columnList, knowledgeType, initPk);
            if (sourceDataList == null) {
                sourceDataList = Lists.newArrayList();
            }
            resMap.put(tableName, sourceDataList);
        });
        return resMap;
    }

    /**
     * 查询已存在的标准映射数据
     *
     * @param initPk
     * @return
     */
    protected List<Map<String, Object>> queryExistsMappingDataList(String initPk) {
        SyncStdMappingDataConfigProperties.SyncStdMappingDataConfig config = syncStdMappingDataConfigProperties.getConfigBy(knowledgeType, mappingType);
        String tableName = config.getTableName();
        List<SyncStdMappingDataConfigProperties.ColumnInfo> columnInfoList = config.getColumnInfoList();
        List<Map<String, Object>> resList = syncStdMappingDataMapper.selectMappingDataList(tableName, columnInfoList, knowledgeType, initPk);
        if (resList == null) {
            resList = Lists.newArrayList();
        }
        return resList;
    }
}
