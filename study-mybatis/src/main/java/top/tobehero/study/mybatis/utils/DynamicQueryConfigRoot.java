package top.tobehero.study.mybatis.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

@Data
@XStreamAlias("Querys")
public class DynamicQueryConfigRoot {
    @XStreamAsAttribute
    private String namespace = "DEFAULT";
    @XStreamImplicit
    private List<DynamicQueryConfig> queryList;
}
