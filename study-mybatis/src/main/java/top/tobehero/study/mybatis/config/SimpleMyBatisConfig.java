package top.tobehero.study.mybatis.config;

//
///**
// * 最简化Spring集成MyBatis配置
// */
//@Configuration
//@MapperScan(basePackageClasses = {HeroMapper.class},annotationClass = Mapper.class)
//public class SimpleMyBatisConfig {
//    @Bean
//    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
//        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
//        factoryBean.setDataSource(dataSource);
//        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
//        //使用下划线"_"转驼峰
//        configuration.setMapUnderscoreToCamelCase(true);
////        //添加mapper包路径
////        configuration.addMappers("top.tobehero.study.mybatis.mapper");
//        factoryBean.setConfiguration(configuration);
//        return factoryBean.getObject();
//    }
//
////    @Bean
////    public HeroMapper userMapper(SqlSessionFactory sqlSessionFactory) throws Exception {
////        SqlSessionTemplate sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
////        return sqlSessionTemplate.getMapper(HeroMapper.class);
////    }
//
//    @Bean
//    public SqlSessionTemplate sqlSessionTemplate(@Autowired  SqlSessionFactory sqlSessionFactory){
//        return new SqlSessionTemplate(sqlSessionFactory);
//    }
////    @Bean
////    public MapperFactoryBean userMapper(@Autowired  SqlSessionFactory sqlSessionFactory) throws Exception {
////        MapperFactoryBean mfb = new MapperFactoryBean();
////        mfb.setMapperInterface(HeroMapper.class);
////        mfb.setSqlSessionFactory(sqlSessionFactory);
////        return mfb;
////    }
////    @Bean
////    public MapperFactoryBean userMapper(@Autowired  SqlSessionTemplate sqlSessionTemplate) throws Exception {
////        MapperFactoryBean mfb = new MapperFactoryBean();
////        mfb.setMapperInterface(HeroMapper.class);
////        mfb.setSqlSessionTemplate(sqlSessionTemplate);
////        return mfb;
////    }
////    @Bean
////    public MapperScannerConfigurer mapperScannerConfigurer(){
////        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
////        configurer.setProcessPropertyPlaceHolders(true);
////        configurer.setAnnotationClass(Mapper.class);
////        configurer.setMapperFactoryBeanClass(MapperFactoryBean.class);
////        configurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
////        configurer.setBasePackage("top.tobehero.study.mybatis.mapper");
////        System.out.println(configurer.hashCode());
////        return configurer;
////    }
//
//
//}
