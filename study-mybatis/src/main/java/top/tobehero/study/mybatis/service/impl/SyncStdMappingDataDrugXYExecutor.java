package top.tobehero.study.mybatis.service.impl;

import org.springframework.stereotype.Component;

@Component
public class SyncStdMappingDataDrugXYExecutor extends AbstractSyncStdMappingDataExecutor {
    public SyncStdMappingDataDrugXYExecutor() {
        super("Drug", "XY");
    }
}
