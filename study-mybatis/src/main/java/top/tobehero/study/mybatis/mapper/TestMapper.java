package top.tobehero.study.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.session.ResultHandler;
import top.tobehero.study.mybatis.entity.TestDO;


@Mapper
public interface TestMapper extends BaseMapper<TestDO> {

    Cursor<TestDO> selectByCursor(ResultHandler<TestDO> handler);

    @Options(fetchSize = 1000, resultSetType = ResultSetType.FORWARD_ONLY)
    @Select("select * from test")
    @ResultType(TestDO.class)
    void selectTestList(ResultHandler<TestDO> handler);
}
