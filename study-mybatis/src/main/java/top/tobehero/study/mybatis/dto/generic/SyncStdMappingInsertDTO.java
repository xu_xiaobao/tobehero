package top.tobehero.study.mybatis.dto.generic;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.TreeMap;

@Data
public class SyncStdMappingInsertDTO {
    private String tableName;
    private String columnName;
    private String pkName;
    private List<TreeMap<String,Object>> rowDataList;
    private Date nowTime;
    private String userCode;
    private String knowledgeType;
    private String mappingType;
    private String initPk;
}
