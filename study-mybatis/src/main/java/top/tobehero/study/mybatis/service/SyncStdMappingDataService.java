package top.tobehero.study.mybatis.service;

/**
 * 同步标准映射数据服务
 */
public interface SyncStdMappingDataService {

    /**
     * 同步标准映射数据
     * @param initPk
     */
    void syncMappingData(String knowledgeType,String initPk);
}
