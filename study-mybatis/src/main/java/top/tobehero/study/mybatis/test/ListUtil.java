package top.tobehero.study.mybatis.test;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class ListUtil {
    /**
     * List分区操作汇总返回数据
     *
     * @param inList
     * @param partition
     * @param mappingFunction
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> List<R> listPartition(List<T> inList, int partition, Function<List<T>, List<R>> mappingFunction) {
        return Lists.partition(inList, partition).stream().parallel().map(mappingFunction::apply).flatMap(Collection::stream).collect(Collectors.toList());
    }

    @FunctionalInterface
    public interface PageQueryFunction<T, R> {
        List<R> doQuery(int pageNum, int pageSize, T queryParam);
    }

    @FunctionalInterface
    public interface PageQueryBeforeFunction<T> {
        void accept(int pageNum, int pageSize, T queryParam);
    }

    /**
     * 分页查询所有数据
     *
     * @param queryParam        查询参数
     * @param pageSize          分页查询，页大小
     * @param pageQueryFunction 分页查询函数
     * @param beforeQuery
     * @param <T>               入参类型
     * @param <R>               出参类型
     * @return
     */
    public static <T, R> List<R> pageQueryAll(T queryParam, int pageSize, PageQueryFunction<T, R> pageQueryFunction, PageQueryBeforeFunction<T> beforeQuery) {
        List<R> allList = new ArrayList<>();
        int pageNum = 1;
        while (true) {
            if (beforeQuery != null) {
                beforeQuery.accept(pageNum, pageSize, queryParam);
            }
            List<R> pageList = pageQueryFunction.doQuery(pageSize, pageNum, queryParam);
            if (CollectionUtils.isEmpty(pageList)) {
                break;
            }
            allList.addAll(pageList);
            pageNum++;
        }
        return allList;
    }

    /**
     * 分页查询所有数据
     *
     * @param queryParam        查询参数
     * @param pageSize          分页查询，页大小
     * @param pageQueryFunction 分页查询函数
     * @param <T>               入参类型
     * @param <R>               出参类型
     * @return
     */
    public static <T, R> List<R> pageQueryAll(T queryParam, int pageSize, PageQueryFunction<T, R> pageQueryFunction) {
        return pageQueryAll(queryParam, pageSize, pageQueryFunction,
                ((pageNum, pageSize1, qryParam) -> {

                }));
    }

    @Test
    public void testListAddAll() {
        ArrayList<String> list = new ArrayList<>();
        System.out.println("before:");
        System.out.println(list);
        List<String> list1 = new ArrayList<>();
        list1.add("hello");
        list.addAll(list1);
        System.out.println("after:");
        System.out.println(list);

    }

    @Test
    public void testPartitionOperate() {
        List<String> inList = new ArrayList<>();
        for (int i = 0; i < 20000; i++) {
            inList.add("in_" + i);
        }
        List<Integer> resList = listPartition(inList, 500, pList -> {
            Random random = new Random();
            int sleep = random.nextInt(3000);
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName);
            List<Integer> collect = pList.stream().map(item -> Integer.valueOf(item.split("_")[1])).collect(Collectors.toList());
            return collect;
        });
        List<Integer> newList = resList.stream().collect(Collectors.toList());
        System.out.println("new->" + newList);
        newList.sort(Integer::compareTo);
        System.out.println("old->" + resList);
        boolean equals = true;
        for (int i = 0; i < resList.size(); i++) {
            Integer a = newList.get(i);
            Integer b = resList.get(i);
            if (!a.equals(b)) {
                equals = false;
                break;
            }
        }
        System.out.println("equals-->" + equals);
    }

    @Test
    public void testPageQueryAll() {
        prepareDataList();
        List<ExportBean> exportBeans = pageQueryAll(null, 10, (pageSize, pageNum, queryParam) -> getPageData(pageNum, pageSize));
        System.out.println(exportBeans.size());
        System.out.println(exportBeans);
    }

    public static List<ExportBean> dataList = new ArrayList<>();

    public static void prepareDataList() {
        for (int i = 0; i < 2000; i++) {
            ExportBean bean = new ExportBean();
            bean.setTitle("title-" + i);
            bean.setContent("content-" + i);
            dataList.add(bean);
        }
    }


    /**
     * @param pageNum  1开始
     * @param pageSize
     * @return
     */
    public static List<ExportBean> getPageData(long pageNum, long pageSize) {
        long start = (pageNum - 1) * pageSize;
        long end = start + pageSize;
        if (end >= dataList.size()) {
            end = dataList.size();
        }
        if (start > end) {
            return Collections.emptyList();
        }
        return dataList.subList((int) start, (int) end).stream().collect(Collectors.toList());
    }
}
