package top.tobehero.study.mybatis.service;

import cn.hutool.core.collection.CollectionUtil;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.ExpiryPolicy;
import org.ehcache.impl.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class DatabaseFuzzyCache {
    public static final HashSet<String> EMPTY_SET = new HashSet<>();
    private CacheManager CACHE_MANAGER;

    public final String CACHE_NAME = "DatabaseFuzzyCache";
    /**
     * 当前缓存版本
     */
    private AtomicReference<Long> timestampRef = new AtomicReference<>(System.currentTimeMillis());

    private Cache<String, ConcurrentHashMap> cache;

    {
        CACHE_MANAGER = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache(CACHE_NAME,
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(
                                String.class,
                                ConcurrentHashMap.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder()
                                        .heap(500, MemoryUnit.MB)
                                        .disk(10, MemoryUnit.GB)


                        )
                                .withExpiry(ExpiryPolicy.NO_EXPIRY)
                                .withKeySerializer(StringSerializer.class)
                                .build())
                .with(CacheManagerBuilder.persistence("d:/ReadBigDataCache"))
                .build(true);
        cache = CACHE_MANAGER.getCache(CACHE_NAME, String.class, ConcurrentHashMap.class);
    }

    @PreDestroy
    public void destroy() {
        if (CACHE_MANAGER != null) {
            CACHE_MANAGER.close();
        }
    }

    public String getTimestamp() {
        Long timestamp = timestampRef.get();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(timestamp));
    }

    /**
     * 更新缓存版本，并清除无效缓存
     *
     * @param newTimestamp
     */
    public void updateTimestamp(Long newTimestamp) {
        while (true) {
            if (timestampRef.compareAndSet(timestampRef.get(), newTimestamp)) break;
        }
    }

    /**
     * 向缓存中添加词
     *
     * @param key
     * @param word
     */
    public void addWord(String key, String word) {
        if (word == null) {
            return;
        }
        ConcurrentHashMap<String, Long> words = getValue(key);
        words.put(word, timestampRef.get());
    }

    /**
     * 获取某类型的所有词
     *
     * @param key
     * @return
     */
    public Set<String> getWords(String key) {
        ConcurrentHashMap<String, Long> words = getValue(key);
        if (CollectionUtil.isEmpty(words)) {
            return EMPTY_SET;
        }
        return new HashSet<>(words.keySet());
    }

    /**
     * 清理所有失效词
     */
    public void evictInvalid() {
        Iterator<Cache.Entry<String, ConcurrentHashMap>> iterator = cache.iterator();
        while (iterator.hasNext()) {
            Cache.Entry<String, ConcurrentHashMap> entry = iterator.next();
            String key = entry.getKey();
            evictInvalid(key);
        }
    }

    /**
     * 清理失效词
     */
    public void evictInvalid(String key) {
        ConcurrentHashMap<String, Long> words = getValue(key);
        if (CollectionUtil.isEmpty(words)) {
            return;
        }
        Long currentTimestamp = timestampRef.get();
        for (Map.Entry<String, Long> item : words.entrySet()) {
            if (!item.getValue().equals(currentTimestamp)) {
                words.remove(item.getKey());
            }
        }
    }

    /**
     * 总缓存数量
     *
     * @return
     */
    public long totalSize() {
        Iterator<Cache.Entry<String, ConcurrentHashMap>> iterator = cache.iterator();
        long totalSize = 0;
        while (iterator.hasNext()) {
            Cache.Entry<String, ConcurrentHashMap> entry = iterator.next();
            int size = entry.getValue().size();
            totalSize += size;
        }
        return totalSize;
    }

    /**
     * 单类型缓存数量
     *
     * @param key
     * @return
     */
    public long totalSize(String key) {
        return cache.get(key).size();
    }

    private ConcurrentHashMap<String, Long> getValue(String key) {
        ConcurrentHashMap<String, Long> words = cache.get(key);
        if (words == null) {
            synchronized (DatabaseFuzzyCache.class) {
                if (words == null) {
                    words = new ConcurrentHashMap<>();
                    cache.put(key, words);
                    // 放入缓存后需要重新获取缓存对象，否则并非同一个对象
                    words = cache.get(key);
                }
            }
        }
        return words;
    }

}
