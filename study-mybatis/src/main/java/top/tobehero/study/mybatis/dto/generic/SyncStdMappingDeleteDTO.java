package top.tobehero.study.mybatis.dto.generic;

import lombok.Data;

import java.util.List;

@Data
public class SyncStdMappingDeleteDTO {
    private String tableName;
    private String columnName;
    private List<Object> rowDataList;
    private String knowledgeType;
    private String initPk;
}
