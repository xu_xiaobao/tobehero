package top.tobehero.study.mybatis.service;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TwoPCTaskDemo extends AbstractTwoPCTask {
    @Override
    public void doTask() {
        long threadId = Thread.currentThread().getId();
        System.out.println(threadId + ":" + "任务开始");
        try {
            TimeUnit.SECONDS.sleep(new Random().nextInt(5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(threadId + ":" + "等待提交");
    }

    @Override
    public void commit() {
        long threadId = Thread.currentThread().getId();
        System.out.println(threadId + ":" + "任务提交");
    }

    @Override
    public void rollback() {
        long threadId = Thread.currentThread().getId();
        System.out.println(threadId + ":" + "任务回滚");
    }
}
