package top.tobehero.study.mybatis.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import com.google.common.collect.Lists;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateModel;
import freemarker.template.Version;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;
import top.tobehero.study.mybatis.InputField;

import java.io.StringWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InputFieldUtil {
    private static Configuration TEMPLATE_CONFIG = null;
    private static final Version FREEMARKER_VERSION = Configuration.VERSION_2_3_31;
    static {
        TEMPLATE_CONFIG = new Configuration(FREEMARKER_VERSION);
        TEMPLATE_CONFIG.setDefaultEncoding("UTF-8");
        TEMPLATE_CONFIG.setTemplateLoader(new ClassTemplateLoader(InputFieldUtil.class,"/queryConditionTemplate"));

    }
    /**
     * 校验参数
     *
     * @param obj
     * @return
     */
    @SneakyThrows
    public static List<FieldErrInfo> validateParams(Object obj) {
        Class<?> objClass = obj.getClass();
        Field[] fields = ReflectUtil.getFields(objClass);
        List<FieldErrInfo> errInfos = new ArrayList<>();
        for (Field field : fields) {
            validateRequiredParams(null, field, obj, errInfos);
        }
        return errInfos;
    }

    @SneakyThrows
    private static void validateRequiredParams(Field parentField, Field field, Object obj, List<FieldErrInfo> errInfos) {
        InputField inputField = field.getAnnotation(InputField.class);
        if (inputField == null) {
            return;
        }
        boolean required = inputField.required();
        if (!required) {
            return;
        }
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        String parentFieldName = parentField == null ? null : parentField.getName();
        Object fieldVal = field.get(obj);
        Class<?> fieldType = field.getType();
        String fieldName = (parentFieldName == null ? "" : parentFieldName + ".") + field.getName();
        if (fieldVal == null) {
            FieldErrInfo errInfo = new FieldErrInfo();
            errInfo.setErrMsg("不能为空");
            errInfo.setField(field);
            errInfo.setFieldName(fieldName);
            errInfos.add(errInfo);
        } else if (fieldVal instanceof String && StringUtils.isBlank((String) fieldVal)) {
            FieldErrInfo errInfo = new FieldErrInfo();
            errInfo.setErrMsg("不能为空");
            errInfo.setField(field);
            errInfo.setFieldName(fieldName);
            errInfos.add(errInfo);
        } else if (List.class.isAssignableFrom(fieldType) && ((List) fieldVal).size() == 0) {
            FieldErrInfo errInfo = new FieldErrInfo();
            errInfo.setErrMsg("不能为空列表");
            errInfo.setField(field);
            errInfo.setFieldName(fieldName);
            errInfos.add(errInfo);
        } else if (fieldType.isArray() && Array.getLength(fieldVal) == 0) {
            FieldErrInfo errInfo = new FieldErrInfo();
            errInfo.setErrMsg("不能为空数组");
            errInfo.setField(field);
            errInfo.setFieldName(fieldName);
            errInfos.add(errInfo);
        } else if (fieldType.getClassLoader() != null) {
            Class<?> childClass = fieldVal.getClass();
            Field[] fields = ReflectUtil.getFields(childClass);
            for (Field childField : fields) {
                validateRequiredParams(field, childField, fieldVal, errInfos);
            }
        }
    }

    /**
     * 解析输入参数的查询条件
     * @param inObj
     * @return
     */
    public static QueryCondition parseQueryCondition(Object inObj) {
        ArrayList<QueryConditionItem> items = null;
        try {
            Field[] fields = ReflectUtil.getFields(inObj.getClass());
            items = new ArrayList<>();
            for (Field field : fields) {
                InputField inputField = field.getAnnotation(InputField.class);
                if (inputField == null) {
                    continue;
                }
                InputField.SearchType searchType = inputField.searchType();
                if (searchType == null || searchType == InputField.SearchType.NONE) {
                    continue;
                }
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }
                Object fieldVal = field.get(inObj);
                switch (searchType) {
                    case KEYWORD:
                        parseQueryConditionForKeyWord(inputField, field, fieldVal, items);
                        break;
                    case TEXT:
                        parseQueryConditionForText(inputField, field, fieldVal, items);
                        break;
                    case MATCH:
                        parseQueryConditionForMatch(inputField, field, fieldVal, items);
                        break;
                    default:
                        throw new RuntimeException("暂不支持此搜索类型：" + searchType.name());
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        QueryCondition queryCondition = new QueryCondition();
        queryCondition.setItems(items);
        return queryCondition;
    }

    /**
     * 解析查询条件转换为查询参数Map
     * @param obj
     * @return
     */
    @SneakyThrows
    public static Tuple2<String,Map<String,Object>> parseConditionAndParamMap(Object obj){
        QueryCondition queryCondition = parseQueryCondition(obj);
        Template template = TEMPLATE_CONFIG.getTemplate("WhereSegmentTemplate-mysql.ftl");
        StringWriter out = new StringWriter();
        BeansWrapper beansWrapper = new BeansWrapper(FREEMARKER_VERSION);
        TemplateModel templateMode = beansWrapper.wrap(queryCondition);
        template.process(templateMode, out);
        Map<String, Object> qryParamMap = queryCondition.getQryParamMap();
        Map<String, Object> otherParams = BeanUtil.beanToMap(obj, Arrays.stream(ReflectUtil.getFields(obj.getClass())).filter(field -> {
            InputField inputField = field.getAnnotation(InputField.class);
            return inputField == null || inputField.searchType() == InputField.SearchType.NONE;
        }).map(field -> field.getName()).collect(Collectors.toList()).toArray(new String[0]));
        qryParamMap.putAll(otherParams);
        return Tuples.of(out.toString(), qryParamMap);

    }
    private static void parseQueryConditionForMatch(InputField inputField, Field field, Object fieldVal, ArrayList<QueryConditionItem> items) {
        if (fieldVal == null) {
            return;
        } else {
            QueryConditionItem item = new QueryConditionItem();
            String columnName = getColumnName(inputField, field);
            item.setColumnName(columnName);
            item.setInputVal(fieldVal);
            item.setSearchType(InputField.SearchType.MATCH.name());
            ArrayList<WhereSegment> whereSegments = new ArrayList<>();
            item.setWhereSegments(whereSegments);
            items.add(item);
            // 转换WhereSegment
            if (List.class.isAssignableFrom(field.getType())) {
                List<Object> paramList = (List<Object>) fieldVal;
                WhereSegment whereSegment = new WhereSegment();
                whereSegment.setColumnName(columnName);
                whereSegment.setOperateType(WhereSegment.WhereOperateType.IN);
                ArrayList<String> paramKeys = new ArrayList<>();
                ArrayList<Object> paramVals = new ArrayList<>();
                whereSegment.setParamKeys(paramKeys);
                whereSegment.setParamVals(paramVals);
                for (int i = 0; i < paramList.size(); i++) {
                    Object paramVal = paramList.get(i);
                    String paramKey = InputField.SearchType.MATCH.name().toLowerCase() + "_" + i +"_" + columnName;
                    paramKeys.add(paramKey);
                    paramVals.add(paramVal);
                }
                whereSegments.add(whereSegment);
            } else{
                String paramKey = InputField.SearchType.MATCH.name().toLowerCase() + "_" + columnName;
                WhereSegment whereSegment = new WhereSegment();
                whereSegment.setColumnName(columnName);
                whereSegment.setOperateType(WhereSegment.WhereOperateType.EQ);
                whereSegment.setParamKeys(Lists.newArrayList(paramKey));
                whereSegment.setParamVals(Lists.newArrayList(fieldVal));
                whereSegments.add(whereSegment);
            }
        }
    }

    private static void parseQueryConditionForText(InputField inputField, Field field, Object fieldVal, ArrayList<QueryConditionItem> items) {
        if (fieldVal == null) {
            return;
        } else {
            if (fieldVal instanceof String) {
                String strVal = (String) fieldVal;
                QueryConditionItem item = new QueryConditionItem();
                String columnName = getColumnName(inputField, field);
                item.setColumnName(columnName);
                item.setInputVal(strVal);
                item.setSearchType(InputField.SearchType.TEXT.name());
                ArrayList<WhereSegment> whereSegments = new ArrayList<>();
                item.setWhereSegments(whereSegments);
                items.add(item);

                // 转换WhereSegment
                String paramKey = InputField.SearchType.TEXT.name().toLowerCase() + "_" + columnName;
                WhereSegment whereSegment = new WhereSegment();
                whereSegment.setColumnName(columnName);
                whereSegment.setOperateType(WhereSegment.WhereOperateType.LIKE);
                whereSegment.setParamKeys(Lists.newArrayList(paramKey));
                whereSegment.setParamVals(Lists.newArrayList(fieldVal));
                whereSegments.add(whereSegment);
            }
        }
    }

    private static void parseQueryConditionForKeyWord(InputField inputField, Field field, Object fieldVal, List<QueryConditionItem> items) {
        Class<?> fieldType = field.getType();
        List<String> paramList = null;
        if (fieldVal == null) {
            return;
        } else {
            if (fieldVal instanceof String) {
                String strVal = (String) fieldVal;
                String[] splitVal = strVal.split("\\s+");
                if (splitVal != null) {
                    paramList = Arrays.stream(splitVal).collect(Collectors.toList());
                }
            } else if (List.class.isAssignableFrom(fieldType)) {
                paramList = (List) fieldVal;
            }
        }
        if (CollectionUtil.isNotEmpty(paramList)) {
            String columnName = getColumnName(inputField, field);
            QueryConditionItem item = new QueryConditionItem();
            item.setColumnName(columnName);
            item.setInputVal(paramList);
            item.setSearchType(InputField.SearchType.KEYWORD.name());
            ArrayList<WhereSegment> whereSegments = new ArrayList<>();
            item.setWhereSegments(whereSegments);
            items.add(item);

            for (int i = 0; i < paramList.size(); i++) {
                Object paramVal = paramList.get(i);
                String paramKey = InputField.SearchType.KEYWORD.name().toLowerCase() + "_" + i +"_" + columnName;
                WhereSegment whereSegment = new WhereSegment();
                whereSegment.setColumnName(columnName);
                whereSegment.setOperateType(WhereSegment.WhereOperateType.LIKE);
                whereSegment.setParamKeys(Lists.newArrayList(paramKey));
                whereSegment.setParamVals(Lists.newArrayList(paramVal));
                whereSegments.add(whereSegment);
            }
        }
    }

    private static String getColumnName(InputField inputField, Field field) {
        String s = inputField.columnName();
        if (StringUtils.isBlank(s)) {
            s = com.baomidou.mybatisplus.core.toolkit.StringUtils.camelToUnderline(field.getName());
        }
        return s;
    }

}
