package top.tobehero.study.mybatis.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@XStreamAlias("Query")
public class DynamicQueryConfig {
    private String id;
    private List<DynamicQueryColumn> selectColumns;
    private String tableName;
    private String fixedCondition;
    private Class<?> whereConditionClass;
    private Class<?> resultTypeClass;
    private List<DynamicQueryConfigProperty> properties;
    private Map<String, Object> props;
    
    public void init(){
        initProps();
    }

    private void initProps() {
        if (properties != null) {
            props = new HashMap<>();
            for (DynamicQueryConfigProperty property : properties) {
                String key = property.getKey();
                String value = property.getValue();
                props.put(key, value);
            }
        }
    }

    public String getSqlColumns() {
        if (selectColumns == null || selectColumns.size() == 0) {
            return "";
        }
        return selectColumns.stream().map(this::getLabelName).collect(Collectors.joining(","));
    }

    public String getLabelName(DynamicQueryColumn column) {
        String name = column.getName();
        String alias = column.getAlias();
        if (StringUtils.isNotBlank(alias)) {
            return name + " as " + alias;
        }
        return name;
    }
}
