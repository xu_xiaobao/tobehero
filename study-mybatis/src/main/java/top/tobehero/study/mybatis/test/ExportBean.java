package top.tobehero.study.mybatis.test;

import lombok.Data;

@Data
public class ExportBean {
    private String title;
    private String content;
}
