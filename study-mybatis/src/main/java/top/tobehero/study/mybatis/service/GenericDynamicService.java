package top.tobehero.study.mybatis.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.util.function.Tuple2;
import top.tobehero.study.mybatis.mapper.DynamicQuerySqlMapper;
import top.tobehero.study.mybatis.utils.DynamicQueryConfig;
import top.tobehero.study.mybatis.utils.InputFieldUtil;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class GenericDynamicService {

    @Autowired
    private DynamicQuerySqlMapper dynamicQuerySqlMapper;

    /**
     * 通用查询
     *
     * @param reqDTO
     * @param queryConfig
     * @param selectMapper
     * @return
     */
    public List<Map<String, Object>> queryMap(Object reqDTO, DynamicQueryConfig queryConfig, Function<Map<String, Object>, List<Map<String, Object>>> selectMapper) {
        Tuple2<String, Map<String, Object>> sqlAndQryParamMap = InputFieldUtil.parseConditionAndParamMap(reqDTO);
        String whereCondition = sqlAndQryParamMap.getT1();
        Map<String, Object> qryParamMap = sqlAndQryParamMap.getT2();
        qryParamMap.put("WhereCondition", whereCondition);
        qryParamMap.put("QueryConfig", queryConfig);
        return selectMapper.apply(qryParamMap);
    }

    public <T> List<T> queryAndConvert(Object reqDTO, DynamicQueryConfig queryConfig, Function<Map<String, Object>, List<Map<String, Object>>> selectMapper, Function<Map<String, Object>, T> convertMap) {
        return this.queryMap(reqDTO, queryConfig, (qryParamMap) -> {
            List<Map<String, Object>> maps = selectMapper.apply(qryParamMap);
            return maps;
        }).stream().map(rowMap -> convertMap.apply(rowMap)).collect(Collectors.toList());
    }

    public <T> List<T> queryList(Object reqDTO, DynamicQueryConfig queryConfig, Function<Map<String, Object>, List<Map<String, Object>>> selectMapper, Class<T> tClass) {
        Class<?> resultTypeClass = queryConfig.getResultTypeClass();
        Assert.isTrue(tClass.equals(resultTypeClass), "必须使用与配置相同的class进行转换");
        List<T> resList = this.queryAndConvert(reqDTO, queryConfig, selectMapper, mapToBeanFunction(tClass));
        return resList;
    }

    public List<Map<String, Object>> queryMap(Object reqDTO, DynamicQueryConfig queryConfig) {
        Tuple2<String, Map<String, Object>> sqlAndQryParamMap = InputFieldUtil.parseConditionAndParamMap(reqDTO);
        String whereCondition = sqlAndQryParamMap.getT1();
        Map<String, Object> qryParamMap = sqlAndQryParamMap.getT2();
        qryParamMap.put("WhereCondition", whereCondition);
        qryParamMap.put("QueryConfig", queryConfig);
        return dynamicQuerySqlMapper.queryByDynamicCondition(qryParamMap);
    }

    public <T> List<T> queryAndConvert(Object reqDTO, DynamicQueryConfig queryConfig, Function<Map<String, Object>, T> convertMap) {
        return this.queryMap(reqDTO, queryConfig).stream().map(rowMap -> convertMap.apply(rowMap)).collect(Collectors.toList());
    }

    public <T> List<T> queryList(Object reqDTO, DynamicQueryConfig queryConfig, Class<T> tClass) {
        Class<?> resultTypeClass = queryConfig.getResultTypeClass();
        Assert.isTrue(tClass.equals(resultTypeClass), "必须使用与配置相同的class进行转换");
        List<T> resList = this.queryAndConvert(reqDTO, queryConfig, mapToBeanFunction(tClass));
        return resList;
    }

    private <T> Function<Map<String, Object>, T> mapToBeanFunction(Class<T> tClass) {
        return (rowMap) -> BeanUtil.mapToBean(rowMap, tClass, true, CopyOptions.create().ignoreNullValue());
    }
}
