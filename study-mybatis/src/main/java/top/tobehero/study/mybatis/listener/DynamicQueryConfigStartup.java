package top.tobehero.study.mybatis.listener;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import top.tobehero.study.mybatis.utils.DynamicQueryUtil;

@Component
public class DynamicQueryConfigStartup implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        DynamicQueryUtil.loadConfiguration();
    }
}
