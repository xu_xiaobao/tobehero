package top.tobehero.study.mybatis.test;

import org.junit.Test;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapStudy {

    @Test
    public void testComputeIfAbsent() {
        List<Tuple2<String, String>> list = new ArrayList<>();
        list.add(Tuples.of("一班", "张三"));
        list.add(Tuples.of("一班", "李四"));
        list.add(Tuples.of("二班", "王五"));
        list.add(Tuples.of("二班", "赵六"));
        Map<String, List<String>> map = new HashMap<>();
        list.forEach(item -> {
            map.computeIfAbsent(item.getT1(), k -> new ArrayList<>()).add(item.getT2());
        });
        System.out.println(map);
    }
}
