package top.tobehero.study.mybatis.utils;

import com.alibaba.fastjson.JSON;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateModel;
import freemarker.template.Version;
import lombok.SneakyThrows;
import org.junit.Test;
import top.tobehero.study.mybatis.dto.generic.req.QueryMultiTableDataReq;

import java.io.StringWriter;

public class InputFieldUtilTest {

    @SneakyThrows
    @Test
    public void parseQueryCondition() {
        String json = "{\n" +
                "    \"name\": \"张三 王五 赵六\",\n" +
                "    \"age\": 28,\n" +
                "    \"sex\":\"男\",\n" +
                "    \"remark\":\"备注\",\n" +
                "    \"ids\": [\n" +
                "        \"1\",\n" +
                "        \"2\"\n" +
                "    ],\n" +
                "    \"relationInfo\": {\n" +
                "        \"name\": \"李四\",\n" +
                "        \"relation\":\"配偶\",\n" +
                "        \"tags\" : [\"1\"]\n" +
                "    }\n" +
                "}";
        QueryMultiTableDataReq reqDTO = JSON.parseObject(json, QueryMultiTableDataReq.class);
        QueryCondition queryCondition = InputFieldUtil.parseQueryCondition(reqDTO);
        Version version = Configuration.VERSION_2_3_31;
        Configuration configuration = new Configuration(version);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateLoader(new ClassTemplateLoader(this.getClass(),"/queryConditionTemplate"));
//        Template template = configuration.getTemplate("QueryConditionTemplate.ftl");
        Template template = configuration.getTemplate("WhereSegmentTemplate.ftl");
        StringWriter out = new StringWriter();
        BeansWrapper beansWrapper = new BeansWrapper(version);
        TemplateModel templateMode = beansWrapper.wrap(queryCondition);
        template.process(templateMode, out);
        System.out.println(out.toString());
        System.out.println("-------------------");
        System.out.println(queryCondition.getQryParamMap());
    }
}