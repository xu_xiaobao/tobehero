package top.tobehero.usermanage.sysname.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import top.tobehero.usermanage.sysname.entity.SysNameConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
public interface ISysNameConfigService extends IService<SysNameConfig> {

    Page<SysNameConfig> findPageBySysNameConfig(long current,long size, SysNameConfig data);

    SysNameConfig findBySysCode(String sysCode);
}
