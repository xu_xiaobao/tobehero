package top.tobehero.usermanage.sysname.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import top.tobehero.usermanage.sysname.entity.SysNameConfig;
import top.tobehero.usermanage.sysname.mapper.SysNameConfigMapper;
import top.tobehero.usermanage.sysname.service.ISysNameConfigService;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
public class SysNameConfigServiceImpl extends ServiceImpl<SysNameConfigMapper, SysNameConfig> implements ISysNameConfigService {

    @Override
    public Page<SysNameConfig> findPageBySysNameConfig(long current,long size,SysNameConfig data) {
        Page page = new Page(current,size);
        LambdaQueryWrapper<SysNameConfig> queryWrapper = new LambdaQueryWrapper<>();
        String sysName = data.getSysName();
        String flag = data.getFlag();
        if(StringUtils.isNotBlank(flag)){
            queryWrapper.eq(SysNameConfig::getFlag, flag);
        }
        if(StringUtils.isNotBlank(sysName)){
            queryWrapper.like(SysNameConfig::getSysName,"%"+sysName+"%");
        }
        return this.page(page,queryWrapper);
    }

    @Override
    public SysNameConfig findBySysCode(String sysCode) {
        return this.lambdaQuery().eq(SysNameConfig::getSysCode, sysCode).one();
    }
}
