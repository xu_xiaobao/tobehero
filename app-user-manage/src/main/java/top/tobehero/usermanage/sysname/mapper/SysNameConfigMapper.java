package top.tobehero.usermanage.sysname.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.tobehero.usermanage.sysname.entity.SysNameConfig;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface SysNameConfigMapper extends BaseMapper<SysNameConfig> {

}
