package top.tobehero.usermanage.sysname.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.sysname.entity.SysNameConfig;
import top.tobehero.usermanage.sysname.service.ISysNameConfigService;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 系统配置表 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@RestController
@RequestMapping("/sysname/sys-name-config")
@Slf4j
public class SysNameConfigController {
    @Resource
    private ISysNameConfigService iSysNameConfigService;

    @RequestMapping("/save")
    public BaseRes save(@RequestBody SysNameConfig sysNameConfig){
        try{
            iSysNameConfigService.save(sysNameConfig);
        }catch (Exception ex){
            log.error("[添加系统名称配置]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp();
    }

    @RequestMapping("/update")
    public BaseRes update(@RequestBody SysNameConfig sysNameConfig){
        try{
            iSysNameConfigService.updateById(sysNameConfig);
        }catch (Exception ex){
            log.error("[更新系统名称配置]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp();
    }

    @RequestMapping("/page")
    public BaseRes<Page<SysNameConfig>> page(@RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "10") long size, @RequestBody SysNameConfig data){
        Page<SysNameConfig> page = iSysNameConfigService.findPageBySysNameConfig(current, size, data);
        return ResponseUtils.outSuccessResp(page);
    }
    @RequestMapping("/queryBySysCode")
    public BaseRes<SysNameConfig> queryBySysCode(@RequestBody SysNameConfig sysNameConfig){
        SysNameConfig data = iSysNameConfigService.findBySysCode(sysNameConfig.getSysCode());
        if(data!=null){
            return ResponseUtils.outSuccessResp(data);
        }else{
            return ResponseUtils.outErrorResp("-1","未查到数据");
        }
    }
    @RequestMapping("/getSysNameList")
    public BaseRes<List<SysNameConfig>> getSysNameList(){
        List<SysNameConfig> list = iSysNameConfigService.lambdaQuery().eq(SysNameConfig::getFlag, "1").orderByAsc(SysNameConfig::getSysName).list();
        return ResponseUtils.outSuccessResp(list);
    }
}
