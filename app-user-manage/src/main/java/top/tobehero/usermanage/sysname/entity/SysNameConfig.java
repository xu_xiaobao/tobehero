package top.tobehero.usermanage.sysname.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统配置表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysNameConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 系统代码
     */
    @TableId
    private String sysCode;

    /**
     * 系统名称
     */
    private String sysName;

    /**
     * 首页
     */
    private String homeUrl;

    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
