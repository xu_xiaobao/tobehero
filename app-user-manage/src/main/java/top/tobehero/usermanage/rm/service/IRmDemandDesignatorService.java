package top.tobehero.usermanage.rm.service;

import top.tobehero.usermanage.rm.entity.RmDemandDesignator;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 需求指派人 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface IRmDemandDesignatorService extends IService<RmDemandDesignator> {

}
