package top.tobehero.usermanage.rm.service;

import top.tobehero.usermanage.rm.entity.RmUserDailyJob;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 人员日报工作 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface IRmUserDailyJobService extends IService<RmUserDailyJob> {

}
