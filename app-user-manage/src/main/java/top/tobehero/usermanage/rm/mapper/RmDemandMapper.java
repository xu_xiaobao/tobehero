package top.tobehero.usermanage.rm.mapper;

import top.tobehero.usermanage.rm.entity.RmDemand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 需求 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface RmDemandMapper extends BaseMapper<RmDemand> {

}
