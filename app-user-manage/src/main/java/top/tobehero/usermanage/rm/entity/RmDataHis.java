package top.tobehero.usermanage.rm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RmDataHis implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 历史数据
     */
    private String hisData;

    /**
     * 数据版本
     */
    private Integer vision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;


}
