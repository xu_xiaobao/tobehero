package top.tobehero.usermanage.rm.mapper;

import top.tobehero.usermanage.rm.entity.RmUserDailyJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 人员日报工作 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface RmUserDailyJobMapper extends BaseMapper<RmUserDailyJob> {

}
