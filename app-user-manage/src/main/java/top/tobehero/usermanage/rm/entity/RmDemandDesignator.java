package top.tobehero.usermanage.rm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 需求指派人
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RmDemandDesignator implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 需求ID
     */
    private Integer demandId;

    /**
     * 指派人
     */
    private String username;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;


}
