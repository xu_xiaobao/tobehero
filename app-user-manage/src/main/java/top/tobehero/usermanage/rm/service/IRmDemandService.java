package top.tobehero.usermanage.rm.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.rm.entity.RmDemand;
import top.tobehero.usermanage.rm.vo.RmDemandQueryVo;

/**
 * <p>
 * 需求 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface IRmDemandService extends IService<RmDemand> {
    BaseRes saveDemand(RmDemand demand);
    BaseRes updateDemand(RmDemand demand);
    Page<RmDemand> findPage(long current, long size, RmDemandQueryVo queryVo);
    RmDemand findById(Integer id);
}
