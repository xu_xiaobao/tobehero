package top.tobehero.usermanage.rm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 需求
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RmDemand implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 需求名称
     */
    private String name;

    /**
     * 提出部门
     */
    private String department;

    /**
     * 提出人
     */
    private String proposer;

    /**
     * 需求描述
     */
    private String describe;

    /**
     * 需求负责人
     */
    private String leader;

    /**
     * 需求状态
     */
    private String status;

    /**
     * 需求进度百分比
     */
    private Integer percent;

    /**
     * 计划工作量
     */
    private Integer planWorkload;

    /**
     * 计划开始时间
     */
    private LocalDate planBeginDate;

    /**
     * 计划提测时间
     */
    private LocalDate planTestDate;

    /**
     * 计划上线时间
     */
    private LocalDate planOnlineDate;

    /**
     * 计划完成时间
     */
    private LocalDate planFinishedDate;

    /**
     * 实际开始时间
     */
    private LocalDate realBeginDate;

    /**
     * 实际提测时间
     */
    private LocalDate realTestDate;

    /**
     * 实际上线时间
     */
    private LocalDate realOnlineDate;

    /**
     * 实际完成时间
     */
    private LocalDate realFinishedDate;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
