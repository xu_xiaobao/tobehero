package top.tobehero.usermanage.rm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 人员日报工作
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RmUserDailyJob implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 当天日期
     */
    private LocalDate curDate;

    /**
     * 人员姓名
     */
    private String empName;

    /**
     * 需求ID
     */
    private Integer demandId;

    /**
     * 当前阶段
     */
    private String stage;

    /**
     * 花费时间
     */
    private Integer costHours;

    /**
     * 工作内容
     */
    private String jobContent;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
