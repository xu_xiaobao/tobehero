package top.tobehero.usermanage.rm.service.impl;

import top.tobehero.usermanage.rm.entity.RmDataHis;
import top.tobehero.usermanage.rm.mapper.RmDataHisMapper;
import top.tobehero.usermanage.rm.service.IRmDataHisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Service
public class RmDataHisServiceImpl extends ServiceImpl<RmDataHisMapper, RmDataHis> implements IRmDataHisService {

}
