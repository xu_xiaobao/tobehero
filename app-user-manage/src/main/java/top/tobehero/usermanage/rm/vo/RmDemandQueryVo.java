package top.tobehero.usermanage.rm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode
public class RmDemandQueryVo {
    private String name;
    private String status;
    private String leader;
    private String proposer;
    private String department;
    private String createdBy;
    private LocalDate createdTimeBegin;
    private LocalDate createdTimeEnd;
}
