package top.tobehero.usermanage.rm.mapper;

import top.tobehero.usermanage.rm.entity.RmDataHis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface RmDataHisMapper extends BaseMapper<RmDataHis> {

}
