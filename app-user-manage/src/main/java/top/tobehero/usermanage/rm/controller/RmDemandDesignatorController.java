package top.tobehero.usermanage.rm.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 需求指派人 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Controller
@RequestMapping("/rm/rm-demand-designator")
public class RmDemandDesignatorController {

}
