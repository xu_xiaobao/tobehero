package top.tobehero.usermanage.rm.service;

import top.tobehero.usermanage.rm.entity.RmDataHis;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface IRmDataHisService extends IService<RmDataHis> {

}
