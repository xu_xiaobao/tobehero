package top.tobehero.usermanage.rm.service.impl;

import top.tobehero.usermanage.rm.entity.RmUserDailyJob;
import top.tobehero.usermanage.rm.mapper.RmUserDailyJobMapper;
import top.tobehero.usermanage.rm.service.IRmUserDailyJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 人员日报工作 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Service
public class RmUserDailyJobServiceImpl extends ServiceImpl<RmUserDailyJobMapper, RmUserDailyJob> implements IRmUserDailyJobService {

}
