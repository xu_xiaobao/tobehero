package top.tobehero.usermanage.rm.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.constants.AuthConstants;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.core.utils.RedisLockUtils;
import top.tobehero.usermanage.rm.entity.RmDataHis;
import top.tobehero.usermanage.rm.entity.RmDemand;
import top.tobehero.usermanage.rm.mapper.RmDemandMapper;
import top.tobehero.usermanage.rm.service.IRmDataHisService;
import top.tobehero.usermanage.rm.service.IRmDemandService;
import top.tobehero.usermanage.rm.vo.RmDemandQueryVo;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;

/**
 * <p>
 * 需求 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Service
@Slf4j
public class RmDemandServiceImpl extends ServiceImpl<RmDemandMapper, RmDemand> implements IRmDemandService {
    @Resource
    private HttpSession httpSession;
    @Resource
    private RedisLockUtils redisLockUtils;
    @Resource
    private IRmDataHisService iRmDataHisService;
    @Override
    public BaseRes saveDemand(RmDemand demand) {
        final String lockKey = "top.tobehero.usermanage.rm.provider.impl.RmDemandServiceImpl.saveDemand";
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        boolean lock = false;
        try{
            lock = redisLockUtils.lock(lockKey,60);
            if(!lock){
                return ResponseUtils.outErrorResp("-1","数据锁定中，请稍后重试！");
            }
            Integer existsCount = this.lambdaQuery().eq(RmDemand::getName, demand.getName()).count();
            if(existsCount>0){
                return ResponseUtils.outErrorResp("-2","需求名称重复，请检查！");
            }
            demand.setRevision(1);
            demand.setCreatedBy(username);
            demand.setCreatedTime(LocalDate.now());
            boolean save = this.save(demand);
            if(save){
                return ResponseUtils.outSuccessResp(demand.getId());
            }else{
                return ResponseUtils.outErrorResp("-99","保存失败");
            }
        }catch (Exception ex){
            log.error("[需求维护]-[保存失败]",ex);
            return ResponseUtils.outErrorResp("-99","保存失败");
        }finally {
            if(lock){
                redisLockUtils.unlock(lockKey);
            }
        }
    }

    @Override
    @Transactional
    public BaseRes updateDemand(RmDemand demand) {
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        LocalDate now = LocalDate.now();
        Integer id = demand.getId();
        if(id==null){
            return ResponseUtils.outErrorResp("-1","主键不能为空");
        }
        RmDemand existsDemand = this.findById(demand.getId());
        if(existsDemand==null){
            return ResponseUtils.outErrorResp("-2","数据不存在");
        }
        final String lockKey = "top.tobehero.usermanage.rm.provider.impl.RmDemandServiceImpl.updateDemand_"+demand.getId();
        boolean lock = false;
        try{
            lock = redisLockUtils.lock(lockKey,60);
            if(!lock){
                return ResponseUtils.outErrorResp("-3","数据锁定中，请稍后重试！");
            }
            Integer existsCount = this.lambdaQuery().eq(RmDemand::getName, demand.getName()).ne(RmDemand::getId,id).count();
            if(existsCount>0){
                return ResponseUtils.outErrorResp("-4","需求名称重复，请检查！");
            }
            Integer revision = existsDemand.getRevision();
            //更新数据
            BeanUtils.copyProperties(demand,existsDemand,new String[]{"createdBy","createdTime","name"});
            demand.setUpdatedBy(username);
            demand.setUpdatedTime(now);
            demand.setRevision(revision +1);
            LambdaUpdateWrapper<RmDemand> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(RmDemand::getId,id).eq(RmDemand::getRevision,revision);
            boolean update = this.update(demand, updateWrapper);
            if(!update){
                return ResponseUtils.outErrorResp("-2","数据版本不一致，请重新操作！");
            }
            //保存历史副本
            String oldDataJson = JSON.toJSONString(existsDemand);
            RmDataHis dataHis = new RmDataHis();
            dataHis.setTableName("rm_demand");
            dataHis.setHisData(oldDataJson);
            dataHis.setCreatedBy(username);
            dataHis.setVision(revision);
            dataHis.setCreatedTime(now);
            iRmDataHisService.save(dataHis);
            return ResponseUtils.outSuccessResp(demand);
        }catch (Exception ex){
            log.error("[需求维护]-[更新失败]",ex);
            return ResponseUtils.outErrorResp("-99","保存失败");
        }finally {
            if(lock){
                redisLockUtils.unlock(lockKey);
            }
        }
    }

    @Override
    public Page<RmDemand> findPage(long current, long size, RmDemandQueryVo qryVo) {
        String name = qryVo.getName();
        String leader = qryVo.getLeader();
        String proposer = qryVo.getProposer();
        String department = qryVo.getDepartment();
        String createdBy = qryVo.getCreatedBy();
        LocalDate createdTimeBegin = qryVo.getCreatedTimeBegin();
        LocalDate createdTimeEnd = qryVo.getCreatedTimeEnd();
        String status = qryVo.getStatus();
        LambdaQueryChainWrapper<RmDemand> lambdaQuery = lambdaQuery();
        if(StringUtils.isNotBlank(name)){
            lambdaQuery.like(RmDemand::getName,"%"+name+"%");
        }
        if(StringUtils.isNotBlank(leader)){
            lambdaQuery.like(RmDemand::getLeader,"%"+leader+"%");
        }
        if(StringUtils.isNotBlank(proposer)){
            lambdaQuery.like(RmDemand::getProposer,"%"+proposer+"%");
        }
        if(StringUtils.isNotBlank(department)){
            lambdaQuery.eq(RmDemand::getDepartment,department);
        }
        if(StringUtils.isNotBlank(status)){
            lambdaQuery.eq(RmDemand::getStatus,status);
        }
        if(StringUtils.isNotBlank(createdBy)){
            lambdaQuery.like(RmDemand::getCreatedBy,"%"+createdBy+"%");
        }
        if(createdTimeBegin!=null){
            lambdaQuery.ge(RmDemand::getCreatedTime,createdTimeBegin);
        }
        if(createdTimeEnd!=null){
            lambdaQuery.le(RmDemand::getCreatedTime,createdTimeEnd);
        }
        return lambdaQuery.page(new Page<>(current,size));
    }

    @Override
    public RmDemand findById(Integer id) {
        return this.lambdaQuery().eq(RmDemand::getId,id).one();
    }
}
