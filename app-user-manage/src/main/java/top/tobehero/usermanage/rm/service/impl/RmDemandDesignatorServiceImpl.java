package top.tobehero.usermanage.rm.service.impl;

import top.tobehero.usermanage.rm.entity.RmDemandDesignator;
import top.tobehero.usermanage.rm.mapper.RmDemandDesignatorMapper;
import top.tobehero.usermanage.rm.service.IRmDemandDesignatorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 需求指派人 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Service
public class RmDemandDesignatorServiceImpl extends ServiceImpl<RmDemandDesignatorMapper, RmDemandDesignator> implements IRmDemandDesignatorService {

}
