package top.tobehero.usermanage.rm.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.rm.entity.RmDemand;
import top.tobehero.usermanage.rm.service.IRmDemandService;
import top.tobehero.usermanage.rm.vo.RmDemandQueryVo;

import javax.annotation.Resource;

/**
 * <p>
 * 需求 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@RestController
@RequestMapping("/rm/demand")
public class RmDemandController {
    @Resource
    private IRmDemandService iRmDemandService;
    @RequestMapping("/save")
    public BaseRes save(@RequestBody RmDemand demand){
        return iRmDemandService.saveDemand(demand);
    }
    @RequestMapping("/findById")
    public BaseRes findById(Integer id){
        RmDemand  data = iRmDemandService.findById(id);
        if(data==null){
            return ResponseUtils.outErrorResp("-1","未查到表定义");
        }
        return ResponseUtils.outSuccessResp(data);
    }

    @RequestMapping("/page")
    public BaseRes<Page<RmDemand>> page(@RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "10") long size, @RequestBody RmDemandQueryVo queryVo){
        Page<RmDemand> page = iRmDemandService.findPage(current, size, queryVo);
        return ResponseUtils.outSuccessResp(page);
    }

    @RequestMapping("/update")
    public BaseRes update(@RequestBody RmDemand demand){
        return iRmDemandService.updateDemand(demand);
    }
}
