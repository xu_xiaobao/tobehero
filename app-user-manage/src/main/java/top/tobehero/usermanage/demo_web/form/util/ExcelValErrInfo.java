package top.tobehero.usermanage.demo_web.form.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ExcelValErrInfo {
    private Integer rowIndex;
    private Integer colIndex;
    private Exception exception;
}
