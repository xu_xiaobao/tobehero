package top.tobehero.usermanage.demo_web.form.service;

import top.tobehero.usermanage.demo_web.form.dto.output.ImportExcelResDTO;

import java.io.File;

public interface IDemoFormService {
    /**
     * 倒入excel数据
     * @param file
     * @return
     */
    ImportExcelResDTO importExcel(File file);
}
