package top.tobehero.usermanage.demo_web.form.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 表单案例
 * </p>
 *
 * @author xuxiaobao
 * @since 2022-08-07
 */
@Getter
@Setter
@TableName("demo_form")
@ApiModel(value = "DemoForm对象", description = "表单案例")
public class DemoForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("姓名，唯一不可重复")
    private String name;

    @ApiModelProperty("性别，1-男，2-女，3-未知")
    private String sex;

    private Integer age;

    private Integer seqNo;

    @ApiModelProperty("创建人")
    private String createdBy;

    @ApiModelProperty("创建时间")
    private Date createdTime;

    @ApiModelProperty("更新人")
    private String updatedBy;

    @ApiModelProperty("更新时间")
    private Date updatedTime;


}
