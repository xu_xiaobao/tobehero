package top.tobehero.usermanage.demo_web.form.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.metadata.data.CellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.tobehero.framework.common.exception.BusinessException;
import top.tobehero.usermanage.demo_web.form.convert.DemoFormConvert;
import top.tobehero.usermanage.demo_web.form.dto.input.DemoFormExcel;
import top.tobehero.usermanage.demo_web.form.dto.output.ImportExcelResDTO;
import top.tobehero.usermanage.demo_web.form.entity.DemoForm;
import top.tobehero.usermanage.demo_web.form.mapper.DemoFormMapper;
import top.tobehero.usermanage.demo_web.form.service.IDemoFormService;
import top.tobehero.usermanage.demo_web.form.util.ExcelAnalysisHeaderException;
import top.tobehero.usermanage.demo_web.form.util.ExcelInputBase;
import top.tobehero.usermanage.demo_web.form.util.ExcelValErrInfo;
import top.tobehero.usermanage.demo_web.form.util.SimpleModelExcelReadListener;
import top.tobehero.usermanage.demo_web.form.util.validator.ExcelDataValidator;
import top.tobehero.usermanage.demo_web.form.util.validator.impl.ExcelDataSingleColumnRepeatValidator;

import java.io.File;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DemoFormServiceImpl implements IDemoFormService {
    @Autowired
    private DemoFormMapper demoFormMapper;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public ImportExcelResDTO importExcel(File file) {
        // 读取数据
        SimpleModelExcelReadListener<DemoFormExcel> excelReadListener = new SimpleModelExcelReadListener<>();
        try {
            EasyExcel.read(file, excelReadListener).head(DemoFormExcel.class).headRowNumber(1).sheet(0).doRead();
        } catch (ExcelAnalysisHeaderException e) {
            throw new BusinessException("-1", "excel表头校验失败");
        } catch (Exception e) {
            throw new BusinessException("excel解析失败");
        }
        List<ExcelValErrInfo> errInfoList = excelReadListener.getErrInfoList();
        if (CollectionUtil.isNotEmpty(errInfoList)) {
            List<String> errMsgList = errInfoList.stream().map(errInfo -> {
                Integer rowIndex = errInfo.getRowIndex();
                Integer colIndex = errInfo.getColIndex();
                Exception exception = errInfo.getException();
                String message = exception.getMessage();
                if (exception instanceof ExcelDataConvertException) {
                    ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException) exception;
                    CellData<?> cellData = excelDataConvertException.getCellData();
                    String cellValString = cellData.getStringValue();
                    ExcelContentProperty excelContentProperty = excelDataConvertException.getExcelContentProperty();
                    Field field = excelContentProperty.getField();
                    message = " Convert data [" + cellValString + "] " + "to class " + field.getType().getName();
                }
                return String.format("第%s行，第%s列，解析发生错误,原因：%s", rowIndex, colIndex, message);
            }).collect(Collectors.toList());
            ImportExcelResDTO resDTO = new ImportExcelResDTO();
            resDTO.setErrMsgList(errMsgList);
            return resDTO;
        }
        List<DemoFormExcel> excelDataList = excelReadListener.getDataList();
        // excel data validate
        excelDataValidate(excelDataList);
        List<DemoFormExcel> repeatDataList = checkDbRepeat(excelDataList);
        if (repeatDataList.size() > 0) {
            List<String> errMsgList = repeatDataList.stream().map(demoFormExcel -> {
                Integer rowIndex = demoFormExcel.getRowIndex();
                String name = demoFormExcel.getName();
                return String.format("第%s行,名称[%s]数据库已存在", rowIndex, name);
            }).collect(Collectors.toList());
            ImportExcelResDTO resDTO = new ImportExcelResDTO();
            resDTO.setErrMsgList(errMsgList);
            return resDTO;
        }
        // 插入数据
        batchImportDataList(excelDataList);
        ImportExcelResDTO resDTO = new ImportExcelResDTO();
        return resDTO;
    }

    /**
     * excel数据校验
     *
     * @param dataList
     */
    private void excelDataValidate(List<DemoFormExcel> dataList) {
        ExcelDataSingleColumnRepeatValidator nameRepeatColumnValidator = new ExcelDataSingleColumnRepeatValidator<String, DemoFormExcel>(DemoFormExcel.class, "name");
        ExcelDataValidator<DemoFormExcel> validator = new ExcelDataValidator.Builder<>(DemoFormExcel.class)
                .addColumnValidators(nameRepeatColumnValidator)
                .build();
        validator.validate(dataList);
        List<String> errMsgList = nameRepeatColumnValidator.getErrMsgList();
        if (errMsgList != null && errMsgList.size() > 0) {
            throw BusinessException.of("-1", "excel存在数据重复", errMsgList);
        }
    }

    private void batchImportDataList(List<DemoFormExcel> excelDataList) {
        Date now = new Date();
        for (List<DemoFormExcel> listPart : Lists.partition(excelDataList, 1000)) {
            List<DemoForm> demoFormList = listPart.stream().map(demoFormExcel -> {
                DemoForm demoForm = DemoFormConvert.INSTANCE.toDemoForm(demoFormExcel);
                demoForm.setId(IdUtil.fastSimpleUUID());
                demoForm.setCreatedBy("createBy");
                demoForm.setCreatedTime(now);
                demoForm.setUpdatedBy("updatedBy");
                demoForm.setUpdatedTime(now);
                return demoForm;
            }).collect(Collectors.toList());
            demoFormMapper.batchInsert(demoFormList);
        }
    }

    private List<DemoFormExcel> checkExcelRepeat(List<DemoFormExcel> dataList) {
        // excel 重复判断
        Map<String, List<DemoFormExcel>> groupByMap = dataList.stream().collect(Collectors.groupingBy(DemoFormExcel::getName));
        List<DemoFormExcel> norepeatList = groupByMap.values()
                .stream()
                .filter((list) -> list.size() > 1)
                .flatMap(List::stream)
                .sorted((Comparator.comparingInt(ExcelInputBase::getRowIndex)))
                .collect(Collectors.toList());
        return norepeatList;
    }

    private List<DemoFormExcel> checkDbRepeat(List<DemoFormExcel> dataList) {
        // 数据库重复判断
        List<DemoFormExcel> dbRepeatList = new ArrayList<>();
        for (List<DemoFormExcel> listPart : Lists.partition(dataList, 2)) {
            List<String> nameList = listPart.stream().map(DemoFormExcel::getName).collect(Collectors.toList());
            List<DemoForm> list = demoFormMapper.selectList(
                    new LambdaQueryWrapper<DemoForm>()
                            .select(DemoForm::getName)
                            .in(DemoForm::getName, nameList)
            );
            if (list.size() > 0) {
                list.forEach((dbDemoForm -> {
                    String dbDemoFormName = dbDemoForm.getName();
                    DemoFormExcel demoFormExcel = listPart.stream().filter(it -> dbDemoFormName.equals(it.getName())).findFirst().get();
                    dbRepeatList.add(demoFormExcel);
                }));
            }
        }
        return dbRepeatList;
    }
}
