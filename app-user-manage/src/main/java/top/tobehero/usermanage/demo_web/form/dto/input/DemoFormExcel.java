package top.tobehero.usermanage.demo_web.form.dto.input;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import top.tobehero.usermanage.demo_web.form.util.ExcelInputBase;

@Data
public class DemoFormExcel extends ExcelInputBase {

    @ExcelProperty(index = 0, value = "姓名")
    private String name;
    @ExcelProperty(index = 1, value = "年龄")
    private Integer age;

    private String ext;

}
