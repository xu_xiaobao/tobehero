package top.tobehero.usermanage.demo_web.form.convert;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import top.tobehero.usermanage.demo_web.form.dto.input.DemoFormExcel;
import top.tobehero.usermanage.demo_web.form.entity.DemoForm;

import java.util.List;

@Mapper
public interface DemoFormConvert {
    DemoFormConvert INSTANCE = Mappers.getMapper(DemoFormConvert.class);

    DemoForm toDemoForm(DemoFormExcel data);

    List<DemoForm> toDemoForm(List<DemoFormExcel> data);


}
