drop table if exists demo_form;
create table demo_form(
	id varchar(100) not null comment '主键ID',
	name varchar(50) comment '姓名，唯一不可重复',
	age int,
	seqNo int,
	`CREATED_BY` varchar(32) DEFAULT NULL COMMENT '创建人',
	`CREATED_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`UPDATED_BY` varchar(32) DEFAULT NULL COMMENT '更新人',
	`UPDATED_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单案例';