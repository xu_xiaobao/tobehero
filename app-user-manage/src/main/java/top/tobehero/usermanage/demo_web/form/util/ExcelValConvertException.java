package top.tobehero.usermanage.demo_web.form.util;

import lombok.Data;

import java.lang.reflect.Field;
import java.util.List;

public class ExcelValConvertException extends Exception {

    private List<ErrInfo> errInfos;
    public ExcelValConvertException(List<ErrInfo> errInfo){
        this.errInfos = errInfos;
    }

    @Data
    public static class ErrInfo{
        private Integer colIndex;
        private Field field;
        private String srcVal;
        private String errMsg;
        private Exception ex;
    }
}
