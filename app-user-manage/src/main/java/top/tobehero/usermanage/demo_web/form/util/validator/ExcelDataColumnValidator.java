package top.tobehero.usermanage.demo_web.form.util.validator;

import java.lang.reflect.Field;

/**
 * excel列校验器
 */
public interface ExcelDataColumnValidator<T,D> {
    /**
     * 校验
     *
     * @param rowIndex 行号
     * @param colIndex 列号
     * @param cellVal  单元值
     * @param rowData 行数据
     */
    void doValidate(int rowIndex, int colIndex, T cellVal, Field field, D rowData);

    /**
     * 数据校验完成
     */
    default void  validateFinished(){}

    /**
     * 是否支持
     * @param field
     * @return
     */
    boolean isSupport(Field field);

}
