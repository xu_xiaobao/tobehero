package top.tobehero.usermanage.demo_web.form.util;

import com.alibaba.excel.exception.ExcelAnalysisException;

/**
 * excel头部校验异常
 */
public class ExcelAnalysisHeaderException extends ExcelAnalysisException {
}
