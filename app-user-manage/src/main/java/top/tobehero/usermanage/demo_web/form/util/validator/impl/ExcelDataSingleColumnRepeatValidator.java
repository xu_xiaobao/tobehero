package top.tobehero.usermanage.demo_web.form.util.validator.impl;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.SneakyThrows;
import top.tobehero.usermanage.demo_web.form.util.ExcelInputBase;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 数据单列重复校验
 */
public class ExcelDataSingleColumnRepeatValidator<C, D extends ExcelInputBase> extends AbstractExcelDataColumnRepeatValidator<C, C, D> {
    private String fieldName;
    private Field field;
    private ExcelProperty excelProperty;
    private String colName;

    @SneakyThrows
    public ExcelDataSingleColumnRepeatValidator(Class<D> rowClass, String fieldName) {
        super(rowClass);
        this.field = rowClass.getDeclaredField(fieldName);
        this.fieldName = fieldName;
        this.excelProperty = field.getAnnotation(ExcelProperty.class);
        String[] value = excelProperty.value();
        this.colName = value.length != 0 ? value[0] : fieldName;
    }

    @Override
    public boolean isSupport(Field field) {
        return this.fieldName.equals(field.getName());
    }

    @Override
    protected C getKey(C cellVal, Field field, D rowData) {
        return cellVal;
    }

    @Override
    public List<String> getErrMsgList() {
        return getRepeatMap().entrySet().stream().map((entry)->{
            C key = entry.getKey();
            List<D> value = entry.getValue();
            String repeatNumbers = value.stream().map(item->{
                Integer rowIndex = item.getRowIndex();
                return rowIndex + "";
            }).collect(Collectors.joining(","));
            return String.format("第[%s]行,列[%s].值[%s],存在重复。",repeatNumbers,colName,key);
        }).collect(Collectors.toList());
    }
}
