package top.tobehero.usermanage.demo_web.form.util;

import lombok.Data;

@Data
public class ExcelInputBase {
    private Integer rowIndex;
}
