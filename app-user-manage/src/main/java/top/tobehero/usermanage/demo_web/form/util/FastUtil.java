package top.tobehero.usermanage.demo_web.form.util;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class FastUtil {
    /**
     * List分区操作汇总返回数据
     *
     * @param inList
     * @param partition
     * @param mappingFunction
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> List<R> listPartition(List<T> inList, int partition, Function<List<T>, List<R>> mappingFunction) {
        return Lists.partition(inList, partition).stream().parallel().map(mappingFunction::apply).flatMap(Collection::stream).collect(Collectors.toList());
    }

    @FunctionalInterface
    public interface PageQueryFunction<T, R> {
        List<R> doQuery(int pageNum, int pageSize, T queryParam);
    }

    @FunctionalInterface
    public interface PageQueryBeforeFunction<T> {
        void accept(int pageNum, int pageSize, T queryParam);
    }

    /**
     * 分页查询所有数据
     *
     * @param queryParam        查询参数
     * @param pageSize          分页查询，页大小
     * @param pageQueryFunction 分页查询函数
     * @param beforeQuery
     * @param <T>               入参类型
     * @param <R>               出参类型
     * @return
     */
    public static <T, R> List<R> pageQueryAll(T queryParam, int pageSize, PageQueryFunction<T, R> pageQueryFunction, PageQueryBeforeFunction<T> beforeQuery) {
        List<R> allList = new ArrayList<>();
        int pageNum = 1;
        while (true) {
            if (beforeQuery != null) {
                beforeQuery.accept(pageNum, pageSize, queryParam);
            }
            List<R> pageList = pageQueryFunction.doQuery(pageSize, pageNum, queryParam);
            if (CollectionUtils.isEmpty(pageList)) {
                break;
            }
            allList.addAll(pageList);
            pageNum++;
        }
        return allList;
    }

    /**
     * 分页查询所有数据
     *
     * @param queryParam        查询参数
     * @param pageSize          分页查询，页大小
     * @param pageQueryFunction 分页查询函数
     * @param <T>               入参类型
     * @param <R>               出参类型
     * @return
     */
    public static <T, R> List<R> pageQueryAll(T queryParam, int pageSize, PageQueryFunction<T, R> pageQueryFunction) {
        return pageQueryAll(queryParam, pageSize, pageQueryFunction,
                ((pageNum, pageSize1, qryParam) -> {

                }));
    }
}
