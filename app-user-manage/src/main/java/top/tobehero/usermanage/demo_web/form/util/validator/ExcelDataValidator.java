package top.tobehero.usermanage.demo_web.form.util.validator;

import com.alibaba.excel.annotation.ExcelProperty;
import top.tobehero.usermanage.demo_web.form.util.ExcelInputBase;
import top.tobehero.usermanage.demo_web.form.util.ExcelUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExcelDataValidator<D extends ExcelInputBase> {
    private List<ExcelDataColumnValidator> columnValidators;
    private List<ExcelDataRowValidator<D>> rowValidators;
    private Class<?> rowClass;
    private List<Field> fieldList;

    private ExcelDataValidator(Class<D> rowClass) {
        this.rowClass = rowClass;
        this.fieldList = ExcelUtil.getFieldList(rowClass);
    }
    public void validate(List<D> dataList) {
        if (dataList == null || dataList.size() == 0) {
            return;
        }
        // 校验
        for (int i = 0; i < dataList.size(); i++) {
            D rowData = dataList.get(i);
            Integer rowIndex = rowData.getRowIndex();
            // 行校验
            invokeRowValidate(rowData, rowIndex);
            // 列校验
            invokeColumnValidate(rowData, rowIndex);
        }
        // 列校验完成回调
        if (columnValidators != null && columnValidators.size() > 0) {
            for (int k = 0; k < columnValidators.size(); k++) {
                ExcelDataColumnValidator columnValidator = columnValidators.get(k);
                columnValidator.validateFinished();
            }
        }
        // 行校验完成回调
        if (rowValidators != null && rowValidators.size() > 0) {
            for (int i = 0; i < rowValidators.size(); i++) {
                ExcelDataRowValidator<D> rowValidator = rowValidators.get(i);
                rowValidator.validateFinished();
            }
        }
    }

    /**
     * 行校验
     *
     * @param rowData
     */
    protected void invokeRowValidate(D rowData, int rowIndex) {
        if (rowValidators != null && columnValidators.size() > 0) {
            for (int i = 0; i < rowValidators.size(); i++) {
                ExcelDataRowValidator<D> rowValidator = rowValidators.get(i);
                rowValidator.doValidate(rowIndex, rowData);
            }
        }
    }

    /**
     * 列校验
     *
     * @param rowData
     */
    protected void invokeColumnValidate(D rowData, int rowIndex) {
        for (int j = 0; j < fieldList.size(); j++) {
            Field field = fieldList.get(j);
            Object colVal;
            try {
                colVal = field.get(rowData);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            for (int k = 0; k < columnValidators.size(); k++) {
                ExcelDataColumnValidator columnValidator = columnValidators.get(k);
                if (columnValidator.isSupport(field)) {
                    ExcelProperty excelProperty = field.getAnnotation(ExcelProperty.class);
                    columnValidator.doValidate(rowIndex, excelProperty.index(), colVal, field, rowData);
                }
            }
        }
    }

    public static class Builder< D extends ExcelInputBase> {
        private ExcelDataValidator<D> validator;
        private List<ExcelDataRowValidator<D>> rowValidators;
        private List<ExcelDataColumnValidator> columnValidators;
        public Builder(Class<D> dataClass){
            this.validator = new ExcelDataValidator(dataClass);
        }

        public Builder<D> addRowValidators(ExcelDataRowValidator<D>... rowValidators){
            if (this.rowValidators == null) {
                this.rowValidators = new ArrayList<>();
            }
            List<ExcelDataRowValidator<D>> excelDataRowValidators = Arrays.asList(rowValidators);
            this.rowValidators.addAll(excelDataRowValidators);
            return this;
        }

        public Builder<D> addColumnValidators(ExcelDataColumnValidator<?,D>... columnValidators){
            if (this.columnValidators == null) {
                this.columnValidators = new ArrayList<>();
            }
            this.columnValidators.addAll(Arrays.asList(columnValidators));
            return this;
        }

        public ExcelDataValidator<D> build() {
            ExcelDataValidator<D> validator = this.validator;
            validator.rowValidators = rowValidators;
            validator.columnValidators = columnValidators;
            return validator;
        }
    }
}
