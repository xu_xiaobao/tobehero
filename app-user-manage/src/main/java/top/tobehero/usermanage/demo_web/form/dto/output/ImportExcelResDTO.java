package top.tobehero.usermanage.demo_web.form.dto.output;

import lombok.Data;

import java.util.List;

@Data
public class ImportExcelResDTO {
    private List<String> errMsgList;

}
