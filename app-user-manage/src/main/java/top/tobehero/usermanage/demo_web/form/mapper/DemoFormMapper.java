package top.tobehero.usermanage.demo_web.form.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import top.tobehero.usermanage.demo_web.form.entity.DemoForm;

import java.util.List;

/**
 * <p>
 * 表单案例 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2022-08-07
 */
public interface DemoFormMapper extends BaseMapper<DemoForm> {

    int batchInsert(@Param("list") List<DemoForm> list);
}
