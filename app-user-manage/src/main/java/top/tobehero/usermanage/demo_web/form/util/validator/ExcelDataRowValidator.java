package top.tobehero.usermanage.demo_web.form.util.validator;

/**
 * excel 数据校验器
 */
public interface ExcelDataRowValidator<T> {
    /**
     * 校验数据
     * @param rowIndex
     * @param data
     */
    void doValidate(int rowIndex,T data);

    /**
     * 数据校验完成
     */
    default void  validateFinished(){}
}
