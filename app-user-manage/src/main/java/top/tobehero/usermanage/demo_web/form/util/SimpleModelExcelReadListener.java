package top.tobehero.usermanage.demo_web.form.util;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.read.metadata.holder.ReadRowHolder;
import lombok.Data;
import lombok.SneakyThrows;
import top.tobehero.usermanage.demo_web.form.dto.input.DemoFormExcel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class SimpleModelExcelReadListener<T> extends AnalysisEventListener<T> {
    private List<ExcelValErrInfo> errInfoList = new ArrayList<>();
    private List<T> dataList = new ArrayList<>();

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        if (!ExcelUtil.validateExcelHeader(headMap, DemoFormExcel.class)) {
            throw new ExcelAnalysisHeaderException();
        }
    }

    @SneakyThrows
    @Override
    public void invoke(T data, AnalysisContext context) {
        ReadRowHolder readRowHolder = context.readRowHolder();
        if (data instanceof ExcelInputBase) {
            ExcelInputBase excelInputBase = (ExcelInputBase) data;
            Integer rowIndex = readRowHolder.getRowIndex();
            excelInputBase.setRowIndex(rowIndex);
        }
        dataList.add(data);
    }

    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {
        exception.printStackTrace();
        if (exception instanceof ExcelAnalysisHeaderException) {
            throw exception;
        } else if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException) exception;
            Integer columnIndex = excelDataConvertException.getColumnIndex();
            Integer rowIndex = excelDataConvertException.getRowIndex();
            ExcelValErrInfo excelValErrInfo = new ExcelValErrInfo(rowIndex, columnIndex, exception);
            this.errInfoList.add(excelValErrInfo);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
    }

}