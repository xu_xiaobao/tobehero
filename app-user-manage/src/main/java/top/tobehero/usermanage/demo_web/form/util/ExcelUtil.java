package top.tobehero.usermanage.demo_web.form.util;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.LFUCache;
import cn.hutool.core.util.ArrayUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import top.tobehero.usermanage.demo_web.form.dto.input.DemoFormExcel;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExcelUtil {
    private static LFUCache<Class<?>, List<Field>> EXCEL_BEAN_CLASS_CACHE = CacheUtil.newLFUCache(1000);

    /**
     * 校验表头
     * @param headerMap
     * @param tClass
     * @return
     */
    public static boolean validateExcelHeader(Map<Integer, String> headerMap, Class<?> tClass){
        List<Field> fieldList = getFieldList(tClass);
        if (fieldList.size() != headerMap.size()) {
            return false;
        }
        for (int i = 0; i < fieldList.size(); i++) {
            Field field = fieldList.get(i);
            ExcelProperty excelProperty = field.getAnnotation(ExcelProperty.class);
            String[] value = excelProperty.value();
            String header = headerMap.get(i);
            if (!StringUtils.containsAny(header,value)){
                return false;
            }
        }
        return true;
    }

    public static <T> T mapToBean(Map<Integer, String> rowMap, Class<T> tClass) throws ExcelValConvertException {
        List<Field> fieldList = getFieldList(tClass);
        T obj = null;
        try {
            obj = tClass.newInstance();
        } catch (Exception e) {
            ExcelValConvertException.ErrInfo errInfo = new ExcelValConvertException.ErrInfo();
            errInfo.setErrMsg("无法创建[" + tClass.getName() + "]对象实例");
            errInfo.setEx(e);
            throw new ExcelValConvertException(Lists.newArrayList(errInfo));
        }
        List<ExcelValConvertException.ErrInfo> errInfos = Lists.newArrayList();
        for (Map.Entry<Integer, String> entry : rowMap.entrySet()) {
            Integer colIndex = entry.getKey();
            String colVal = entry.getValue();
            if (fieldList.size() > colIndex) {
                Field field = fieldList.get(colIndex);
                try {
                    Object fieldVal = convertVal(colVal, field);
                    field.set(obj, fieldVal);
                } catch (Exception e) {
                    ExcelValConvertException.ErrInfo errInfo = new ExcelValConvertException.ErrInfo();
                    errInfo.setErrMsg("无法转换属性[" + field + "]");
                    errInfo.setEx(e);
                    errInfo.setColIndex(colIndex);
                    errInfo.setField(field);
                    errInfos.add(errInfo);
                }
            }
        }
        if (errInfos.size() > 0) {
            throw new ExcelValConvertException(errInfos);
        }
        return obj;
    }

    private static Object convertVal(String colVal, Field field) {
        if (colVal == null) {
            return null;
        }
        Class<?> fieldType = field.getType();
        if (fieldType.equals(String.class)) {
            return colVal;
        } else if (fieldType.equals(Long.class)) {
            return Long.valueOf(colVal);
        } else if (fieldType.equals(Integer.class)) {
            return Integer.valueOf(colVal);
        } else if (fieldType.equals(BigInteger.class)) {
            return new BigInteger(colVal);
        } else if (fieldType.equals(BigDecimal.class)) {
            return new BigDecimal(colVal);
        } else if (fieldType.equals(Integer.class)) {
            return Integer.valueOf(colVal);
        } else {
            throw new IllegalArgumentException("不支持的转换类型:" + fieldType);
        }
    }

    public static List<Field> getFieldList(Class<?> excelBeanClass) {
        List<Field> fields = EXCEL_BEAN_CLASS_CACHE.get(excelBeanClass);
        if (fields != null) {
            return fields;
        }
        fields = getExcelBeanFieldsDirectly(excelBeanClass);
        EXCEL_BEAN_CLASS_CACHE.put(excelBeanClass, fields);
        return fields;
    }

    /**
     * 获取excel bean class 的字段列表
     *
     * @param excelBeanClass
     * @return
     */
    private static List<Field> getExcelBeanFieldsDirectly(Class<?> excelBeanClass) {
        Field[] fields = excelBeanClass.getDeclaredFields();
        if (ArrayUtil.isEmpty(fields)) {
            throw new RuntimeException("类字段不能为空");
        }
        return Arrays.stream(fields).filter(field -> {
            int modifiers = field.getModifiers();
            ExcelProperty excelProperty = field.getAnnotation(ExcelProperty.class);
            return excelProperty != null
                    && !Modifier.isStatic(modifiers)
                    && !Modifier.isFinal(modifiers)
                    && !Modifier.isNative(modifiers);
        }).map(field -> {
            field.setAccessible(true);
            return field;
        }).sorted((o1, o2) -> {
            ExcelProperty o1ExcelProperty = o1.getAnnotation(ExcelProperty.class);
            ExcelProperty o2ExcelProperty = o2.getAnnotation(ExcelProperty.class);
            if (o2ExcelProperty == null) {
                return 1;
            } else if (o1ExcelProperty == null) {
                return -1;
            } else {
                return Integer.compare(o1ExcelProperty.index(), o2ExcelProperty.index());
            }
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Field> fieldList = getFieldList(DemoFormExcel.class);
        fieldList.forEach(System.out::println);
    }
}
