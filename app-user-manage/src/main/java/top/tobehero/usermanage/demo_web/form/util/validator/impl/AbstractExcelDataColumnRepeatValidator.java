package top.tobehero.usermanage.demo_web.form.util.validator.impl;

import top.tobehero.usermanage.demo_web.form.util.ExcelInputBase;
import top.tobehero.usermanage.demo_web.form.util.validator.ExcelDataColumnValidator;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractExcelDataColumnRepeatValidator<K, C, D extends ExcelInputBase> implements ExcelDataColumnValidator<C, D> {
    private Class<?> rowClass;
    private Map<K, List<D>> repeatMap = new HashMap<>();

    public AbstractExcelDataColumnRepeatValidator(Class<D> rowClass) {
        this.rowClass = rowClass;
    }

    @Override
    public void doValidate(int rowIndex, int colIndex, C cellVal, Field field, D rowData) {
        repeatMap.computeIfAbsent(getKey(cellVal, field, rowData), (k) -> new ArrayList<>()).add(rowData);
    }

    /**
     * 获取key
     *
     * @param cellVal
     * @param field
     * @param rowData
     * @return
     */
    protected abstract K getKey(C cellVal, Field field, D rowData);

    @Override
    public void validateFinished() {
        Iterator<Map.Entry<K, List<D>>> iterator = repeatMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, List<D>> entry = iterator.next();
            List<D> valList = entry.getValue();
            if (valList.size() == 1) {
                iterator.remove();
            }
        }
    }

    public Map<K, List<D>> getRepeatMap() {
        return repeatMap;
    }

    public List<String> getErrMsgList() {
        Collection<List<D>> values = repeatMap.values();
        return values.stream().map(demoFormExcels -> demoFormExcels.stream().map(demoFormExcel -> {
            Integer rowIndex = demoFormExcel.getRowIndex();
            String repeatRowIndex = demoFormExcels.stream()
                    .filter(mapDemoFormExcel -> !mapDemoFormExcel.getRowIndex().equals(rowIndex))
                    .map(mapDemoFormExcel -> mapDemoFormExcel.getRowIndex() + "")
                    .collect(Collectors.joining(","));
            return String.format("第%s行与第%s行的值,存在重复。", rowIndex, repeatRowIndex);
        }).collect(Collectors.toList())).flatMap(List::stream).collect(Collectors.toList());
    }
}