package top.tobehero.usermanage.demo_web.form.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.demo_web.form.dto.output.ImportExcelResDTO;
import top.tobehero.usermanage.demo_web.form.service.IDemoFormService;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/demo-form")
public class DemoFormController {
    @Autowired
    private IDemoFormService demoFormService;

    @GetMapping(value = "/hello")
    public String hello() {
        return this.getClass().getName();
    }

    @PostMapping(value = "/importExcel")
    public BaseRes<ImportExcelResDTO> importExcel(MultipartFile file) {
        ImportExcelResDTO resDTO = null;
        File descFile = new File("E:\\工作\\北分福袋\\tobehero\\app-user-manage\\src\\main\\java\\top\\tobehero\\usermanage\\demo_web\\form", IdUtil.fastSimpleUUID()+ "." + FileUtil.getSuffix(file.getOriginalFilename()));
        try {
            file.transferTo(descFile);
            resDTO = demoFormService.importExcel(descFile);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            FileUtils.deleteQuietly(descFile);
        }
        return ResponseUtils.outSuccessResp(resDTO);
    }
}
