package top.tobehero.usermanage.core.mapper;

import top.tobehero.usermanage.core.entity.SysDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统字典表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface SysDicMapper extends BaseMapper<SysDic> {

}
