package top.tobehero.usermanage.core.entity;

import java.time.LocalDate;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统字典表
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysDic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Integer id;

    private String key;

    /**
     * 字典类型
     */
    private String type;

    private String val;

    /**
     * 备注
     */
    private String remark;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
