package top.tobehero.usermanage.core.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.core.service.ISysDicService;

import javax.annotation.Resource;

@RequestMapping("/sys/dic")
@RestController
public class DicController {
    @Resource
    private ISysDicService iSysDicService;
    @RequestMapping("getDicValByKey")
    public BaseRes getDicValByKey(String key){
        return iSysDicService.getDicValByKey(key);
    }
}
