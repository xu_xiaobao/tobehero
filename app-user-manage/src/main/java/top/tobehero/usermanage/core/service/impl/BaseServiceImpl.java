package top.tobehero.usermanage.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.core.toolkit.support.SerializedLambda;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import top.tobehero.usermanage.core.service.IBaseService;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BaseServiceImpl<M extends BaseMapper<T>,T> extends ServiceImpl<M,T> implements IBaseService<T> {

    /**
     * 单表集合根据主键合并更新
     * @param sources
     * @param targets
     * @param idCloumn
     */
    public void mergeSingleTableBean(List<T> sources, List<T> targets, SFunction<T, ?> idCloumn){
        try{
            //找到所有新增对象
            List<T> insertObjs = new ArrayList<>();
            //找到所有本次更新对象
            List<T> updateObjs = new ArrayList<>();
            //找到所有本次删除对象
            List<T> delObjs = new ArrayList<>();
            Iterator<T> targetsIt = targets.iterator();
            //        Iterator<T> sourcesIt = sources.iterator();
            while(targetsIt.hasNext()){
                T target = targetsIt.next();
                if(target==null){
                    targetsIt.remove();
                }else{
                    // 决策当前目标更新对象和源数据对象集合属于什么操作关系：新增|更新
                    Iterator<T> sourcesIt = sources.iterator();
                    boolean hasOld = false;
                    while(sourcesIt.hasNext()){
                        T source = sourcesIt.next();
                        hasOld = this.isPkEquals(source,target,idCloumn);
                        if(hasOld){
                            //复制目标数据的非空字段
                            PropertyDescriptor[] pds = BeanUtils.getPropertyDescriptors(target.getClass());
                            for (PropertyDescriptor pd: pds) {
                                Method writeMethod = pd.getWriteMethod();
                                Method readMethod = pd.getReadMethod();
                                if(readMethod!=null&&writeMethod!=null){
                                    Object targetVal = readMethod.invoke(target);
                                    if(targetVal==null){
                                        Object sourceVal = readMethod.invoke(source);
                                        writeMethod.invoke(target,sourceVal);
                                    }
                                }
                            }
                            sourcesIt.remove();
                            break;
                        }
                    }
                    if(!hasOld){
                        insertObjs.add(target);
                    }else{
                        updateObjs.add(target);
                    }
                }
            }
            Iterator<T> sourcesIt = sources.iterator();
            while(sourcesIt.hasNext()){
                T source = sourcesIt.next();
                delObjs.add(source);
            }
            if(insertObjs.size()>0){
                for(T i:insertObjs){
                    boolean save = this.save(i);
                    Assert.isTrue(save,"操作失败");
                }
            }
            if(updateObjs.size()>0){
                for(T i:updateObjs){
                    boolean update = this.updateById(i);
                    Assert.isTrue(update,"操作失败");
                }
            }
            if(delObjs.size()>0){
                for(T i:delObjs){
                    boolean del = this.remove(new LambdaQueryWrapper<T>().eq(idCloumn,getPkVal(i,idCloumn)));
                    Assert.isTrue(del,"操作失败");
                }
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    private Serializable getPkVal(T t,SFunction<T,?> idCloumn) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SerializedLambda resolve = LambdaUtils.resolve(idCloumn);
        Method method = resolve.getImplClass().getMethod(resolve.getImplMethodName());
        return (Serializable) method.invoke(t);
    }
    private <T> boolean isPkEquals(T source, T target, SFunction<T,?> idCloumn) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SerializedLambda resolve = LambdaUtils.resolve(idCloumn);
        Method method = resolve.getImplClass().getMethod(resolve.getImplMethodName());
        Object sourceIdVal = method.invoke(source);
        Object targetIdVal = method.invoke(source);
        if((source!=null&&!source.equals(target))||(source==null&&target!=null)){
            return false;
        }
        return true;
    }
}
