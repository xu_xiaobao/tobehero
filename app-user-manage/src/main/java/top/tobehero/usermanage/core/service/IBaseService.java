package top.tobehero.usermanage.core.service;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IBaseService<T> extends IService<T> {
    /**
     * 单表集合根据主键合并更新
     * @param sources
     * @param targets
     * @param idCloumn
     */
    void mergeSingleTableBean(List<T> sources, List<T> targets, SFunction<T, ?> idCloumn);
}
