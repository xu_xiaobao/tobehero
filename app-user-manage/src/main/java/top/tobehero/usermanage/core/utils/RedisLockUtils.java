package top.tobehero.usermanage.core.utils;

import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component("redisLockUtils")
public class RedisLockUtils {
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 默认锁定30分钟
     * @param key
     * @return
     */
    public boolean lock(String key){
        return this.lock(key,30*60);
    }
    /**
     * 锁定指定key
     * @param key
     * @param timeout
     *          可以为空，此时不设置超时时间，单位：秒。设置正整数时，锁超时自动释放
     * @return
     */
    public boolean lock(String key,Integer timeout){
        BoundValueOperations valueOperations = redisTemplate.boundValueOps(key);
        Boolean ifAbsent = valueOperations.setIfAbsent("1");
        if(ifAbsent){
            if(timeout!=null){
                valueOperations.expire(timeout, TimeUnit.SECONDS);
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 解锁指定key
     * @param key
     * @return
     */
    public boolean unlock(String key){
        Boolean delete = redisTemplate.delete(key);
        return delete;
    }
}
