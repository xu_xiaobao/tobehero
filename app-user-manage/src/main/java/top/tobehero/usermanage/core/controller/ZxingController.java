package top.tobehero.usermanage.core.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;


/**
 *

 * 描述:
 *	用于生成二维码和条形码
 * @author 许宝众
 * @version 1.0
 * @since 2017年7月12日 上午9:45:55
 */
@Controller
@RequestMapping("/static/zxing")
public class ZxingController {
    /**二维码默认宽度，单位（像素）**/
    public static final int QRCODE_DEFAULT_WIDTH=200;
    /**二维码默认高度，单位（像素）**/
    public static final int QRCODE_DEFAULT_HEIGHT=200;
    /**条形码默认宽度，单位（像素）**/
    public static final int BARCODE_DEFAULT_WIDTH=290;
    /**条形码默认宽度，单位（像素）**/
    public static final int BARCODE_DEFAULT_HEIGHT=100;

    /**
     * 生成指定宽、高、内容的二维码
     * 描述:
     * @param width
     * @param height
     * @param content
     * @author 许宝众 2017年7月12日 上午9:50:33
     * @throws Exception
     */
    @RequestMapping("qrcode")
    public void qrcode(Integer width,Integer height,String content,HttpServletRequest request,HttpServletResponse response) throws Exception{
        Assert.isTrue(StringUtils.isNotBlank(content),"二维码内容不能为空");
        if(width==null||width<1){
            width=QRCODE_DEFAULT_WIDTH;
        }
        if(height==null||height<1){
            height=QRCODE_DEFAULT_HEIGHT;
        }
        final String format = "png";// 图像类型
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        BitMatrix bitMatrix;
        // 生成矩阵
        bitMatrix = new MultiFormatWriter().encode(content,BarcodeFormat.QR_CODE, width, height, hints);
        ServletOutputStream os = response.getOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, format, os);
        closeOutStream(os);
    }
//    /**
//     * 生成指定宽、高、内容的二维码,带人保logo
//     * 描述:
//     * @param width
//     * @param height
//     * @param content
//     * @author 许宝众 2017年7月12日 上午9:50:33
//     * @throws Exception
//     */
//    @RequestMapping("qrcode2")
//    public void qrcode2(Integer width,Integer height,String content,HttpServletRequest request,HttpServletResponse response) throws Exception{
//        File logoFile = new File("D:/cxb_jgj_hd.jpg");
//        if(!logoFile.exists()){
//            qrcode(width, height, content, request, response);
//            return ;
//        }else{
//            Assert.isTrue(StringUtils.isNotBlank(content),"二维码内容不能为空");
//            if(width==null||width<1){
//                width=QRCODE_DEFAULT_WIDTH;
//            }
//            if(height==null||height<1){
//                height=QRCODE_DEFAULT_HEIGHT;
//            }
//            final String format = "png";// 图像类型
//            int logoX = width/20*7;
//            int logoY = height/20*7;
//            int logoW = width/10*3;
//            int logoH = width/10*3;
//
//            Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
//            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
//            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
//            hints.put(EncodeHintType.MARGIN, 0);
//            BitMatrix bitMatrix;
//            // 生成矩阵
//            bitMatrix = new MultiFormatWriter().encode(content,BarcodeFormat.QR_CODE, width, height, hints);
//            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
//            //开始礼品二维码数据穿件bitmap图片，分别设置为黑色和白色
//            for (int i = 0; i < width; i++) {
//                for (int j = 0; j < height; j++) {
//                    image.setRGB(i, j, bitMatrix.get(i, j)?0xFF000000:0xFFFFFFFF);
//                }
//            }
//            //画logo
//            Graphics2D g= image.createGraphics();
//            BufferedImage logo = ImageIO.read(logoFile);
//            g.drawImage(logo, logoX, logoY, logoW, logoH, null);
//            BasicStroke basicStroke = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
//            RoundRectangle2D.Float round = new RoundRectangle2D.Float(logoX, logoY, logoW, logoH, 20, 20);
//            g.setStroke(basicStroke);
//            g.setColor(Color.WHITE);
//            g.draw(round);
//            //画边框
//            BasicStroke basicStroke2 = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
//            g.setStroke(basicStroke2);
//            RoundRectangle2D.Float round2 = new RoundRectangle2D.Float(logoX+2, logoY+2, logoW-4, logoH-4, 20, 20);
//            g.setColor(new Color(128, 128,128));
//            g.draw(round2);
//            g.dispose();
//            logo.flush();
//            ServletOutputStream os = response.getOutputStream();
//            ImageIO.write(image, format, os);
//            closeOutStream(os);
//        }
//    }
    @RequestMapping("barcode")
    public void barcode(Integer width,Integer height,String content,HttpServletRequest request,HttpServletResponse response) throws Exception{
        Assert.isTrue(StringUtils.isNotBlank(content),"条形码内容不能为空");
        if(width==null||width<1){
            width=BARCODE_DEFAULT_WIDTH;
        }
        if(height==null||height<1){
            height=BARCODE_DEFAULT_HEIGHT;
        }
        final String format = "png";// 图像类型
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
                BarcodeFormat.CODE_128, width, height, hints);// 生成矩阵
        ServletOutputStream os = response.getOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, format, os);
        closeOutStream(os);
    }
    public void closeOutStream(OutputStream os){
        if(os!=null){
            try{
                os.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
