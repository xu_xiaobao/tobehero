package top.tobehero.usermanage.core.service;

import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.core.entity.SysDic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统字典表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
public interface ISysDicService extends IService<SysDic> {
    BaseRes getDicValByKey(String key);
}
