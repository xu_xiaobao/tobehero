package top.tobehero.usermanage.core.service.impl;

import com.alibaba.fastjson.JSON;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.core.entity.SysDic;
import top.tobehero.usermanage.core.mapper.SysDicMapper;
import top.tobehero.usermanage.core.service.ISysDicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统字典表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2021-01-09
 */
@Service
public class SysDicServiceImpl extends ServiceImpl<SysDicMapper, SysDic> implements ISysDicService {

    @Override
    public BaseRes getDicValByKey(String key) {
        SysDic sysDic = this.lambdaQuery().eq(SysDic::getKey, key).one();
        if(sysDic==null){
            return ResponseUtils.outErrorResp("-1","数据字段未配置");
        }
        String type = sysDic.getType();
        String val = sysDic.getVal();
        Object parse = JSON.parse(val);
        return ResponseUtils.outSuccessResp(parse);
    }
}
