package top.tobehero.usermanage.hero.entity;

import java.time.LocalDate;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 角色资源表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleResourcesRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色代码
     */
    private String roleCode;

    /**
     * 资源代码
     */
    private String resCode;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
