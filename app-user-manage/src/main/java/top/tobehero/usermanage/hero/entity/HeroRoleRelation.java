package top.tobehero.usermanage.hero.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户角色关系表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class HeroRoleRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 角色代码
     */
    private String roleCode;

    /**
     * 系统代码
     */
    private String sysCode;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
