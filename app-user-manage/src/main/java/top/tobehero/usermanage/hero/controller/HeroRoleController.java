package top.tobehero.usermanage.hero.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.constants.AuthConstants;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.hero.entity.HeroRole;
import top.tobehero.usermanage.hero.service.IHeroRoleService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@RestController
@RequestMapping("/heroRole")
@Slf4j
public class HeroRoleController {
    @Resource
    private IHeroRoleService iHeroRoleService;
    @Resource
    private HttpSession httpSession;
    @RequestMapping("/save")
    public BaseRes save(@RequestBody HeroRole heroRole){
        try{
            if(heroRole==null){
                return ResponseUtils.outErrorResp("-1","实体不能为空");
            }
            String roleCode = heroRole.getRoleCode();
            if(StringUtils.isBlank(roleCode)){
                return ResponseUtils.outErrorResp("-1","角色代码不能为空");
            }
            String roleName = heroRole.getRoleName();
            if(StringUtils.isBlank(roleName)){
                return ResponseUtils.outErrorResp("-1","角色名称不能为空");
            }
            String sysCode = heroRole.getSysCode();
            if(StringUtils.isBlank(sysCode)){
                return ResponseUtils.outErrorResp("-1","系统代码不能为空");
            }
            heroRole.setCreatedTime(LocalDateTime.now());
            String currentUser = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
            heroRole.setCreatedBy(currentUser);
            iHeroRoleService.save(heroRole);
        }catch (Exception ex){
            log.error("[添加角色]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp(heroRole);
    }

    @RequestMapping("/update")
    public BaseRes update(@RequestBody HeroRole heroRole){
        try{
            if(heroRole==null||StringUtils.isBlank(heroRole.getSysCode())||StringUtils.isBlank(heroRole.getRoleCode())){
                return ResponseUtils.outErrorResp("-1","用户名不能为空");
            }
            heroRole.setUpdatedTime(LocalDateTime.now());
            String roleCode = heroRole.getRoleCode();
            if(StringUtils.isBlank(roleCode)){
                return ResponseUtils.outErrorResp("-1","角色代码不能为空");
            }
            String roleName = heroRole.getRoleName();
            if(StringUtils.isBlank(roleName)){
                return ResponseUtils.outErrorResp("-1","角色名称不能为空");
            }
            String sysCode = heroRole.getSysCode();
            if(StringUtils.isBlank(sysCode)){
                return ResponseUtils.outErrorResp("-1","系统代码不能为空");
            }
            String currentUser = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
            heroRole.setUpdatedBy(currentUser);
            iHeroRoleService.updateById(heroRole);
        }catch (Exception ex){
            log.error("[更新角色]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp();
    }

    @RequestMapping("/page")
    public BaseRes<Page<HeroRole>> page(@RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "10") long size, @RequestBody HeroRole data){
        Page<HeroRole> page = iHeroRoleService.findPageByHeroRole(current, size, data);
        return ResponseUtils.outSuccessResp(page);
    }

    @RequestMapping("/queryByRoleNameAndSysCode")
    public BaseRes<HeroRole> queryByRoleNameAndSysCode(@RequestBody HeroRole heroRole){
        HeroRole data = iHeroRoleService.queryByRoleNameAndSysCode(heroRole.getSysCode(),heroRole.getRoleCode());
        if(data!=null){
            return ResponseUtils.outSuccessResp(data);
        }else{
            return ResponseUtils.outErrorResp("-1","未查到数据");
        }
    }
}
