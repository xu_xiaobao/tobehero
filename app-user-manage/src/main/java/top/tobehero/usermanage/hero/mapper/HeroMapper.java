package top.tobehero.usermanage.hero.mapper;

import top.tobehero.usermanage.hero.entity.Hero;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface HeroMapper extends BaseMapper<Hero> {

}
