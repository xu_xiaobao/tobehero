package top.tobehero.usermanage.hero.mapper;

import top.tobehero.usermanage.hero.entity.HeroRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface HeroRoleMapper extends BaseMapper<HeroRole> {

}
