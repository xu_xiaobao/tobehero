package top.tobehero.usermanage.hero.mapper;

import top.tobehero.usermanage.hero.entity.RoleResourcesRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色资源表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface RoleResourcesRelationMapper extends BaseMapper<RoleResourcesRelation> {

}
