package top.tobehero.usermanage.hero.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import top.tobehero.usermanage.hero.entity.HeroRole;
import top.tobehero.usermanage.hero.mapper.HeroRoleMapper;
import top.tobehero.usermanage.hero.service.IHeroRoleService;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
public class HeroRoleServiceImpl extends ServiceImpl<HeroRoleMapper, HeroRole> implements IHeroRoleService {
    @Override
    public boolean updateById(HeroRole heroRole){
        Assert.notNull(heroRole,"实体不能为空");
        Assert.notNull(heroRole.getSysCode(),"系统代码不能为空");
        Assert.notNull(heroRole.getRoleCode(),"角色代码不能为空");
        return this.update(heroRole,new LambdaQueryWrapper<HeroRole>().eq(HeroRole::getRoleCode,heroRole.getRoleCode()).eq(HeroRole::getSysCode,heroRole.getSysCode()));
    }
    @Override
    public Page<HeroRole> findPageByHeroRole(long current, long size, HeroRole data) {
        String sysCode = data.getSysCode();
        String roleName = data.getRoleName();
        String flag = data.getFlag();
        LambdaQueryChainWrapper<HeroRole> lambdaQuery = this.lambdaQuery();
        if(StringUtils.isNotBlank(sysCode)){
            lambdaQuery.eq(HeroRole::getSysCode,sysCode);
        }
        if(StringUtils.isNotBlank(roleName)){
            lambdaQuery.like(HeroRole::getRoleName,"%"+roleName+"%");
        }
        if(StringUtils.isNotBlank(flag)){
            lambdaQuery.eq(HeroRole::getFlag,flag);
        }
        return lambdaQuery.page(new Page(current,size));
    }

    @Override
    public HeroRole queryByRoleNameAndSysCode(String sysCode, String roleCode) {
        return this.lambdaQuery().eq(HeroRole::getSysCode,sysCode).eq(HeroRole::getRoleCode,roleCode).one();
    }
}
