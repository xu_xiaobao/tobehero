package top.tobehero.usermanage.hero.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 角色资源表 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Controller
@RequestMapping("/hero/role-resources-relation")
public class RoleResourcesRelationController {

}
