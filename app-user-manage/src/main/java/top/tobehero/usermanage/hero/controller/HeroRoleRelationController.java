package top.tobehero.usermanage.hero.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户角色关系表 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Controller
@RequestMapping("/hero/hero-role-relation")
public class HeroRoleRelationController {

}
