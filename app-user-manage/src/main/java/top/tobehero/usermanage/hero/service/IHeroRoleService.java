package top.tobehero.usermanage.hero.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.tobehero.usermanage.hero.entity.HeroRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface IHeroRoleService extends IService<HeroRole> {

    Page<HeroRole> findPageByHeroRole(long current, long size, HeroRole data);

    HeroRole queryByRoleNameAndSysCode(String sysCode, String roleCode);
}
