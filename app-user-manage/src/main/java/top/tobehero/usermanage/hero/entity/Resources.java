package top.tobehero.usermanage.hero.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 资源表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Resources implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 资源代码
     */
    private String resCode;

    /**
     * 系统代码
     */
    private String sysCode;

    /**
     * 顶级资源ID父ID为自身
     */
    private String parentCode;

    /**
     * ext_api:外部接口	in_api:内部接口	menu:菜单	page:页面元素
     */
    private String resType;

    /**
     * 资源名称
     */
    private String resName;

    /**
     * 针对不同类型的数据存在不同含义	外部接口/内部接口:path为内部访问路径	菜单类型：path为菜单路径	页面元素:可以为空
     */
    private String resPath;

    /**
     * 菜单展示顺序
     */
    private Integer menuDisplayNo;

    /**
     * 菜单图标类
     */
    private String menuIcoClass;

    /**
     * 备注
     */
    private String remark;

    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
