package top.tobehero.usermanage.hero.service.impl;

import top.tobehero.usermanage.hero.entity.Resources;
import top.tobehero.usermanage.hero.mapper.ResourcesMapper;
import top.tobehero.usermanage.hero.service.IResourcesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
public class ResourcesServiceImpl extends ServiceImpl<ResourcesMapper, Resources> implements IResourcesService {

}
