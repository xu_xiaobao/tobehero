package top.tobehero.usermanage.hero.service.impl;

import top.tobehero.usermanage.hero.entity.RoleResourcesRelation;
import top.tobehero.usermanage.hero.mapper.RoleResourcesRelationMapper;
import top.tobehero.usermanage.hero.service.IRoleResourcesRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色资源表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
public class RoleResourcesRelationServiceImpl extends ServiceImpl<RoleResourcesRelationMapper, RoleResourcesRelation> implements IRoleResourcesRelationService {

}
