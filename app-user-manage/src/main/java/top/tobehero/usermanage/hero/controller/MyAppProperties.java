package top.tobehero.usermanage.hero.controller;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "myapp.nacos")
@RefreshScope
@Data
@Component
public class MyAppProperties {
    private String val;
}
