package top.tobehero.usermanage.hero.service;

import top.tobehero.usermanage.hero.entity.HeroRoleRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface IHeroRoleRelationService extends IService<HeroRoleRelation> {

}
