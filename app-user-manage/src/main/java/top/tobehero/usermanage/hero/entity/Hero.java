package top.tobehero.usermanage.hero.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Hero implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名称
     */
    @TableId(type=IdType.NONE)
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    private String name;

    /**
     * 0:男1:女
     */
    private String sex;

    /**
     * 手机联系电话
     */
    private String phone;

    /**
     * 电子邮箱
     */
    private String email;

    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
