package top.tobehero.usermanage.hero.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.constants.AuthConstants;
import top.tobehero.framework.common.feign.user.HeroServiceFeign;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.framework.common.vo.HeroVo;
import top.tobehero.framework.common.vo.MenuItemVo;
import top.tobehero.usermanage.hero.entity.Hero;
import top.tobehero.usermanage.hero.service.IHeroService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@RestController
@Slf4j
public class HeroController implements HeroServiceFeign {
    @Resource
    private HttpSession httpSession;
    @Resource
    HttpServletRequest request;
    @Resource
    private FindByIndexNameSessionRepository sessionRepository;
    @Resource
    private IHeroService iHeroService;
    @Override
    public String getToken() {
        return httpSession.getId();
    }

    @Override
    public BaseRes online(Map<String, String> params){
        log.info("[用户登录请求]-[请求参数{}]",params);
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        if(StringUtils.isNotBlank(username)){
            return ResponseUtils.outSuccessResp();
        }
        username = params.get("username");
        String password = params.get("password");
        if(StringUtils.isBlank(username)||StringUtils.isBlank(password)){
            return ResponseUtils.outErrorResp("-1","参数错误");
        }
        BaseRes<Hero> res = iHeroService.qryHeroByUsernameAndPassword("USER_MANAGE", username, password);
        if(res.getErrcode()!=ResponseUtils.ResponseEnum.SUCCESS.getErrcode()){
            return ResponseUtils.outErrorResp(res.getErrcode(),res.getErrmsg());
        }
        //保存当前用户登录名
        httpSession.setAttribute(AuthConstants.SESSION_USERNAME_KEY, username);
        //设置当前用户登录索引，后续可以查询到该用户所有登录回话信息
        httpSession.setAttribute(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME, username);
        Map<String,String> dataJson = new HashMap<>();
        dataJson.put("token",httpSession.getId());
        log.info("[用户登录成功]-[username:{}]",username);
        return ResponseUtils.outSuccessResp(dataJson);
    }

    @Override
    public BaseRes<HeroVo> info(String token) {
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        HeroVo hero = new HeroVo();
        hero.setName(username);
        hero.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        hero.setIntroduction("I am a super administrator");
        return iHeroService.findUserInfoByUsername(username);
    }
    @Override
    public BaseRes<List<MenuItemVo>> menuInfo(String sysCode) {
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        return iHeroService.getMenuInfo(sysCode,username);
    }
    @Override
    public BaseRes offline(){
        this.httpSession.invalidate();
        return ResponseUtils.outSuccessResp();
    }

    @Override
    public boolean isAuthed(String token) {
        Session session = this.sessionRepository.findById(token);
        return session!=null&&session.getAttribute(AuthConstants.SESSION_USERNAME_KEY)!=null;
    }

    @Override
    public boolean isAuthorized(String token,String sysName, String path) {
        return false;
    }

    @RequestMapping("/save")
    public BaseRes save(@RequestBody Hero hero){
        try{
            if(hero==null){
                return ResponseUtils.outErrorResp("-1","实体不能为空");
            }
            String username = hero.getUsername();
            if(StringUtils.isBlank(username)){
                return ResponseUtils.outErrorResp("-1","用户名不能为空");
            }
            String name = hero.getName();
            if(StringUtils.isBlank(name)){
                return ResponseUtils.outErrorResp("-1","昵称不能为空");
            }
            String sex = hero.getSex();
            if(StringUtils.isBlank(sex)){
                return ResponseUtils.outErrorResp("-1","性别不能为空");
            }
            String password = hero.getPassword();
            if(StringUtils.isBlank(password)){
                return ResponseUtils.outErrorResp("-1","密码不能为空");
            }
            String phone = hero.getPhone();
            if(StringUtils.isBlank(phone)){
                return ResponseUtils.outErrorResp("-1","手机号不能为空");
            }
            String email = hero.getEmail();
            if(StringUtils.isBlank(email)){
                return ResponseUtils.outErrorResp("-1","邮箱不能为空");
            }
            hero.setCreatedTime(LocalDateTime.now());
            String currentUser = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
            hero.setCreatedBy(currentUser);
            iHeroService.save(hero);
        }catch (Exception ex){
            log.error("[添加用户]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp(hero);
    }

    @RequestMapping("/update")
    public BaseRes update(@RequestBody Hero hero){
        try{
            if(hero==null||StringUtils.isBlank(hero.getUsername())){
                return ResponseUtils.outErrorResp("-1","用户名不能为空");
            }
            if(hero==null){
                return ResponseUtils.outErrorResp("-1","实体不能为空");
            }
            String username = hero.getUsername();
            if(StringUtils.isBlank(username)){
                return ResponseUtils.outErrorResp("-1","用户名不能为空");
            }
            String name = hero.getName();
            if(StringUtils.isBlank(name)){
                return ResponseUtils.outErrorResp("-1","昵称不能为空");
            }
            String sex = hero.getSex();
            if(StringUtils.isBlank(sex)){
                return ResponseUtils.outErrorResp("-1","性别不能为空");
            }
            String password = hero.getPassword();
            if(StringUtils.isBlank(password)){
                return ResponseUtils.outErrorResp("-1","密码不能为空");
            }
            String phone = hero.getPhone();
            if(StringUtils.isBlank(phone)){
                return ResponseUtils.outErrorResp("-1","手机号不能为空");
            }
            String email = hero.getEmail();
            if(StringUtils.isBlank(email)){
                return ResponseUtils.outErrorResp("-1","邮箱不能为空");
            }
            hero.setUpdatedTime(LocalDateTime.now());
            String currentUser = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
            hero.setUpdatedBy(currentUser);
            iHeroService.updateById(hero);
        }catch (Exception ex){
            log.error("[更新用户]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp();
    }

    @RequestMapping("/page")
    public BaseRes<Page<Hero>> page(@RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "10") long size, @RequestBody Hero data){
        Page<Hero> page = iHeroService.findPageByHero(current, size, data);
        return ResponseUtils.outSuccessResp(page);
    }

    @RequestMapping("/queryByUsername")
    public BaseRes<Hero> queryBySysCode(@RequestBody Hero hero){
        Hero data = iHeroService.findByUsername(hero.getUsername());
        if(data!=null){
            return ResponseUtils.outSuccessResp(data);
        }else{
            return ResponseUtils.outErrorResp("-1","未查到数据");
        }
    }

    @Autowired
    private MyAppProperties myAppProperties;
    @RequestMapping("/echoVal")
    public BaseRes<String> echoVal(){
        return ResponseUtils.outSuccessResp(myAppProperties.getVal());
    }
}
