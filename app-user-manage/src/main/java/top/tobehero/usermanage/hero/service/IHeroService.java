package top.tobehero.usermanage.hero.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.vo.HeroVo;
import top.tobehero.framework.common.vo.MenuItemVo;
import top.tobehero.usermanage.hero.entity.Hero;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface IHeroService extends IService<Hero> {
   BaseRes<Hero> qryHeroByUsernameAndPassword(String sysCode, String username, String password);

   BaseRes<HeroVo> findUserInfoByUsername(String username);

   BaseRes<List<MenuItemVo>> getMenuInfo(String sysCode, String username);

   Page<Hero> findPageByHero(long current, long size, Hero data);

    Hero findByUsername(String username);
}
