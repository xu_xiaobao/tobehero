package top.tobehero.usermanage.hero.service;

import top.tobehero.usermanage.hero.entity.Resources;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface IResourcesService extends IService<Resources> {

}
