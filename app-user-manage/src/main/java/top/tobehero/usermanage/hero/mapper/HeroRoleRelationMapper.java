package top.tobehero.usermanage.hero.mapper;

import top.tobehero.usermanage.hero.entity.HeroRoleRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
public interface HeroRoleRelationMapper extends BaseMapper<HeroRoleRelation> {

}
