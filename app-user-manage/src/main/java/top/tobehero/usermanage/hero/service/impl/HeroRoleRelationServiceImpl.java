package top.tobehero.usermanage.hero.service.impl;

import top.tobehero.usermanage.hero.entity.HeroRoleRelation;
import top.tobehero.usermanage.hero.mapper.HeroRoleRelationMapper;
import top.tobehero.usermanage.hero.service.IHeroRoleRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
public class HeroRoleRelationServiceImpl extends ServiceImpl<HeroRoleRelationMapper, HeroRoleRelation> implements IHeroRoleRelationService {

}
