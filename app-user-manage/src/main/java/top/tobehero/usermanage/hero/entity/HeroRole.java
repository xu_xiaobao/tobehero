package top.tobehero.usermanage.hero.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class HeroRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色代码
     */
    @TableId
    private String roleCode;

    /**
     * 系统代码
     */
    private String sysCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 备注
     */
    private String remark;

    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
