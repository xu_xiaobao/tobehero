package top.tobehero.usermanage.hero.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.framework.common.vo.HeroVo;
import top.tobehero.framework.common.vo.MenuItemVo;
import top.tobehero.usermanage.hero.entity.*;
import top.tobehero.usermanage.hero.mapper.*;
import top.tobehero.usermanage.hero.service.IHeroService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-10-31
 */
@Service
@Slf4j
public class HeroServiceImpl extends ServiceImpl<HeroMapper, Hero> implements IHeroService {
    @Resource
    private HeroMapper heroMapper;
    @Resource
    private HeroRoleMapper heroRoleMapper;
    @Resource
    private HeroRoleRelationMapper heroRoleRelationMapper;
    @Resource
    private RoleResourcesRelationMapper roleResourcesRelationMapper;
    @Resource
    private ResourcesMapper resourcesMapper;

    @Override
    public BaseRes<Hero> qryHeroByUsernameAndPassword(String sysCode,String username, String password) {
        log.info("[用户登录]-[Service接收到请求]-[sysCode:{},username:{}]",sysCode,username);
        Hero hero = this.lambdaQuery().eq(Hero::getUsername, username).eq(Hero::getPassword, password).eq(Hero::getFlag, "1").one();
        if(hero==null){
            return ResponseUtils.outErrorResp("-2","用户名或密码错误");
        }
        //查询当前用户是是否有指定系统的角色
        List<HeroRoleRelation> heroRoleRelations = heroRoleRelationMapper.selectList(new QueryWrapper<HeroRoleRelation>().eq("sys_code", sysCode).eq("username", username));
        if(heroRoleRelations==null||heroRoleRelations.size()==0){
            return ResponseUtils.outErrorResp("-3","未授予登录权限");
        }
        ArrayList<String> rolesCodes = new ArrayList<>();
        heroRoleRelations.forEach((s)->{
            rolesCodes.add(s.getRoleCode());
        });
        Integer count = heroRoleMapper.selectCount(new LambdaQueryWrapper<HeroRole>().eq(HeroRole::getSysCode, sysCode).eq(HeroRole::getFlag,"1").in(HeroRole::getRoleCode,rolesCodes));
        if(count>0){
            return ResponseUtils.outSuccessResp(hero);
        }
        log.info("[用户登录]-[Service接收处理完成]-[sysCode:{},username:{}]",sysCode,username);
        MDC.clear();
        return ResponseUtils.outErrorResp("-3","未授予登录权限");
    }

    @Override
    public BaseRes<HeroVo> findUserInfoByUsername(String username) {
        Hero hero = this.query().eq("username", username).eq("flag", "1").one();
        if(hero!=null){
            HeroVo heroVo = new HeroVo();
            heroVo.setName(hero.getName());
            return ResponseUtils.outSuccessResp(heroVo);
        }
        return ResponseUtils.outErrorResp("-1","未查到用户信息");
    }

    @Override
    public BaseRes<List<MenuItemVo>> getMenuInfo(String sysCode, String username) {
        List<MenuItemVo> menuList = new ArrayList<>();
        // 查询指定系统角色
        List<HeroRoleRelation> heroRoleRelations = heroRoleRelationMapper.selectList(new QueryWrapper<HeroRoleRelation>().eq("sys_code", sysCode).eq("username", username));
        if(heroRoleRelations!=null&&heroRoleRelations.size()>0){
            ArrayList<String> rolesCodes = new ArrayList<>();
            heroRoleRelations.forEach((s)->{
                rolesCodes.add(s.getRoleCode());
            });
            //查询系统角色的菜单资源
            List<RoleResourcesRelation> roleResourcesRelations = roleResourcesRelationMapper.selectList(new QueryWrapper<RoleResourcesRelation>().eq("sys_code", sysCode).in("role_code", rolesCodes));
            if(roleResourcesRelations!=null&&roleResourcesRelations.size()>0){
                List<String> ressourceCodes = new ArrayList<>();
                roleResourcesRelations.forEach((s)->{
                    ressourceCodes.add(s.getResCode());
                });
                List<Resources> resources = resourcesMapper.selectList(
                        new QueryWrapper<Resources>()
                                .eq("flag", "1")
                                .eq("sys_code", sysCode)
                                .eq("res_type","menu")
                                .in("res_code", ressourceCodes)
                                .orderByAsc("menu_display_no")

                );
                //遍历资源数据，组装树形结构
                if(resources!=null&&resources.size()>0){
                    int preSize = 0;
                    int postSize = -1;
                    for(;preSize!=postSize;){
                        preSize = resources.size();
                        Iterator<Resources> resIt = resources.iterator();
                        while(resIt.hasNext()){
                            Resources resource = resIt.next();
                            String parentCode = resource.getParentCode();
                            String resCode = resource.getResCode();
                            Integer menuDisplayNo = resource.getMenuDisplayNo();
                            String resPath = resource.getResPath();
                            String menuIcoClass = resource.getMenuIcoClass();
                            String resName = resource.getResName();

                            //将资源转换为菜单
                            boolean isTop = resCode.equals(parentCode);
                            if(isTop){
                                MenuItemVo menuItemVo = getMenuItemVo(parentCode, resCode,resName, menuDisplayNo, resPath, menuIcoClass);
                                menuList.add(menuItemVo);
                                resIt.remove();
                            }else{
                                MenuItemVo parentMenuItemVo = findParentMenuItemVo(parentCode,menuList);
                                if(parentMenuItemVo!=null){
                                    List<MenuItemVo> children = parentMenuItemVo.getChildren();
                                    if(children==null){
                                        children = new ArrayList<>();
                                        parentMenuItemVo.setChildren(children);
                                    }
                                    MenuItemVo menuItemVo = getMenuItemVo(parentCode, resCode,resName, menuDisplayNo, resPath, menuIcoClass);
                                    children.add(menuItemVo);
                                    resIt.remove();
                                }
                            }
                        }
                        postSize = resources.size();
                    }
                }
            }
        }
        return ResponseUtils.outSuccessResp(menuList);
    }

    @Override
    public Page<Hero> findPageByHero(long current, long size, Hero data) {
        String username = data.getUsername();
        String name = data.getName();
        String flag = data.getFlag();
        String phone = data.getPhone();

        LambdaQueryChainWrapper<Hero> queryChainWrapper = this.lambdaQuery();
        if(StringUtils.isNotBlank(username)){
            queryChainWrapper.like(Hero::getUsername,"%"+username+"%");
        }
        if(StringUtils.isNotBlank(username)){
            queryChainWrapper.like(Hero::getName,"%"+name+"%");
        }
        if(StringUtils.isNotBlank(flag)){
            queryChainWrapper.eq(Hero::getFlag,flag);
        }
        if(StringUtils.isNotBlank(phone)){
            queryChainWrapper.eq(Hero::getPhone,phone);
        }
        return queryChainWrapper.page(new Page<>(current,size));
    }

    private MenuItemVo findParentMenuItemVo(String parentCode, List<MenuItemVo> menuList) {
        if(menuList!=null){
            for (MenuItemVo item:menuList) {
                if(parentCode.equals(item.getMenuCode())){
                    return item;
                }
                MenuItemVo res = findParentMenuItemVo(parentCode, item.getChildren());
                if(res!=null){
                    return res;
                }
            }
        }
        return null;
    }

    private MenuItemVo getMenuItemVo(String parentCode, String resCode,String resName, Integer menuDisplayNo, String resPath, String menuIcoClass) {
        MenuItemVo menuItemVo = new MenuItemVo();
        menuItemVo.setPath(resPath);
        menuItemVo.setDisplayNo(menuDisplayNo);
        menuItemVo.setIcon(menuIcoClass);
        menuItemVo.setMenuCode(resCode);
        menuItemVo.setMenuName(resName);
        menuItemVo.setParentCode(parentCode);
        return menuItemVo;
    }

    @Override
    public Hero findByUsername(String username) {
        return this.lambdaQuery().eq(Hero::getUsername,username).one();
    }
}
