package top.tobehero.usermanage.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import top.tobehero.framework.common.log.filter.TraceLogFilter;

@Configuration
public class TraceLogConfig {
    @Bean
    public FilterRegistrationBean addTraceLogFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new TraceLogFilter());
        registration.addUrlPatterns("/*");
        registration.setName("traceLogFilter");
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}
