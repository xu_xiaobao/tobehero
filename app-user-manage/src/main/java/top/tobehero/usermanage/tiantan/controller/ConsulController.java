package top.tobehero.usermanage.tiantan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.tiantan.consul.CheckServiceReq;
import top.tobehero.usermanage.tiantan.service.ConsulWatcherService;

import java.util.List;

@RestController
@RequestMapping("/tianyan/consul")
public class ConsulController {
    @Autowired
    private ConsulWatcherService consulWatcherService;

    @RequestMapping(value = "/checkService",consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseRes checkService(@RequestBody CheckServiceReq req){
        return consulWatcherService.checkService(req);
    }

    @RequestMapping(value = "/checkServices",consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseRes checkServices(@RequestBody List<CheckServiceReq> req){
        return consulWatcherService.checkService(req);
    }

}
