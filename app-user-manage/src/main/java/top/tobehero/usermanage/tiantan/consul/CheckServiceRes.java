package top.tobehero.usermanage.tiantan.consul;

import lombok.Data;

@Data
public class CheckServiceRes {
    private String serviceName;
    private Integer expectedNumber;
    private Integer realNumber;
    private String errcode;
    private String errmsg;
}
