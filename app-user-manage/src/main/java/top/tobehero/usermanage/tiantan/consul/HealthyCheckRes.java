package top.tobehero.usermanage.tiantan.consul;

import lombok.Data;

import java.util.List;

@Data
public class HealthyCheckRes {
    private String Node;
    private String CheckID;
    private String Name;
    private String Status;
    private String Notes;
    private String Output;
    private String ServiceID;
    private String ServiceName;
    private List<String> ServiceTags;
    private String Type;
//    "Definition": {},
    private Integer CreateIndex;
    private Integer ModifyIndex;
}
