package top.tobehero.usermanage.tiantan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.tobehero.usermanage.tiantan.entity.TianyanConfig;
import top.tobehero.usermanage.tiantan.mapper.TianyanConfigMapper;
import top.tobehero.usermanage.tiantan.service.ITianyanConfigService;

@Service
public class TianyanConfigServiceImpl extends ServiceImpl<TianyanConfigMapper,TianyanConfig> implements ITianyanConfigService {
    @Override
    public TianyanConfig getConf(String namespace,String label){
        return this.lambdaQuery().eq(TianyanConfig::getNamespace, namespace).eq(TianyanConfig::getLabel, label).one();
    }
}
