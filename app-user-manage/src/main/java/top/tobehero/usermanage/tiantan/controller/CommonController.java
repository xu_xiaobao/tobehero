package top.tobehero.usermanage.tiantan.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.tiantan.service.ConsulWatcherService;

import javax.annotation.Resource;

@RestController
@RequestMapping("/tianyan/common")
public class CommonController {
    @Resource
    private ConsulWatcherService consulWatcherService;
    /**
     * 获取指定namespace下的监控配置
     * @param namespace
     * @return
     */
    @RequestMapping(value = "/getConf")
    public BaseRes getConf(String namespace, String label){
        return consulWatcherService.getConf(namespace,label);
    }
}
