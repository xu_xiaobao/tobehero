package top.tobehero.usermanage.tiantan.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.tiantan.consul.CheckServiceReq;
import top.tobehero.usermanage.tiantan.consul.CheckServiceRes;
import top.tobehero.usermanage.tiantan.consul.HealthyCheckRes;
import top.tobehero.usermanage.tiantan.entity.TianyanConfig;

import javax.annotation.Resource;
import java.util.*;

@Service
@Slf4j
public class ConsulWatcherService {
    /***consul服务监控*/
    public static final String CHECK_SERVICE_API="/v1/health/checks/";
    @Resource
    private ITianyanConfigService iTianyanConfigService;

    private static int orderCheckList(CheckServiceRes a, CheckServiceRes b) {
        String aErrcode = a.getErrcode();
        String bErrcode = b.getErrcode();

        if(!aErrcode.equals(bErrcode)){
            if (ResponseUtils.ResponseEnum.SUCCESS.getErrcode().equals(aErrcode)) {
                return 1;
            }else{
                return -1;
            }
        }else{
            String aServiceName = a.getServiceName();
            String bServiceName = b.getServiceName();
            return aServiceName.compareTo(bServiceName);
        }
    }

    /**
     * 检查consul服务
     * @return
     */
    public BaseRes checkService(CheckServiceReq req){
        try{
            RestTemplate restTemplate = new RestTemplate();
            String httpUrl = req.getConsulApiAddress()+CHECK_SERVICE_API + req.getServiceName();
            ResponseEntity<String> response = restTemplate.getForEntity(httpUrl,String.class);
            HttpStatus statusCode = response.getStatusCode();
            if(statusCode.value()==200){
                String resBody = response.getBody();
                List<HealthyCheckRes> healthyCheckResList = JSON.parseArray(resBody, HealthyCheckRes.class);
                if(healthyCheckResList!=null&&healthyCheckResList.size()>0){
                    int sucCount = 0;
                    for (HealthyCheckRes healthyCheckRes:healthyCheckResList){
                        String status = healthyCheckRes.getStatus();
                        if("passing".equals(status)){
                            sucCount++;
                        }
                    }
                    HashMap<String,Object> data = new HashMap<>();
                    data.put("realNumber",sucCount);
                    if(sucCount>=req.getExpectedNumber()){
                        return ResponseUtils.outSuccessResp(data);
                    }else{
                        return ResponseUtils.outErrorResp("-3","实际服务【"+sucCount+"】个",data);
                    }
                }else{
                    return ResponseUtils.outErrorResp("-2","服务个数为【0】");
                }
            }else{
                return ResponseUtils.outErrorResp("-1","检查consul check api 异常");
            }
        }catch (Exception ex){
            log.error("[检查consul服务异常]",ex);
            return ResponseUtils.outErrorResp("9999","检查consul service异常");
        }
    }

    public BaseRes checkService(List<CheckServiceReq> req) {
        List<CheckServiceRes> data = new ArrayList<>();
        req.stream().forEach((r)->{
            CheckServiceRes result = new CheckServiceRes();
            BaseRes res = this.checkService(r);
            result.setErrcode(res.getErrcode());
            result.setErrmsg(res.getErrmsg());
            result.setServiceName(r.getServiceName());
            result.setExpectedNumber(r.getExpectedNumber());
            Map<String,Object> resData = (Map<String, Object>) res.getData();
            if(resData!=null){
                Integer realNumber = (Integer) resData.get("realNumber");
                result.setRealNumber(realNumber);
            }
            data.add(result);
        });
        Collections.sort(data, ConsulWatcherService::orderCheckList);
        return ResponseUtils.outSuccessResp(data);
    }

    /**
     * 读取指定名称空间和标签下的配置信息
     * @param namespace
     * @return
     */
    public BaseRes getConf(String namespace,String label) {
        TianyanConfig conf = iTianyanConfigService.getConf(namespace, label);
        if(conf==null){
            return ResponseUtils.outErrorResp("-1","配置信息未找到");
        }
        String confText = conf.getConf();
        JSONObject confJson = null;
        try {
            confJson = JSON.parseObject(confText);
        }catch (Exception ex){
            ex.printStackTrace();
            return ResponseUtils.outErrorResp("-2","配置信息格式错误");
        }
        return ResponseUtils.outSuccessResp(confJson);
    }
}
