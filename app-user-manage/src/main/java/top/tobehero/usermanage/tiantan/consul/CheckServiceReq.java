package top.tobehero.usermanage.tiantan.consul;

import lombok.Data;

@Data
public class CheckServiceReq {
    private String serviceName;
    private String consulApiAddress;
    /**期望服务数量**/
    private int expectedNumber=1;
}
