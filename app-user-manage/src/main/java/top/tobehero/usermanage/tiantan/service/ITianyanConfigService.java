package top.tobehero.usermanage.tiantan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.tobehero.usermanage.tiantan.entity.TianyanConfig;

public interface ITianyanConfigService extends IService<TianyanConfig> {
    /**
     * 查询指定名称空间和标签下的配置
     * @param namespace
     * @param label
     * @return
     */
    TianyanConfig getConf(String namespace,String label);
}
