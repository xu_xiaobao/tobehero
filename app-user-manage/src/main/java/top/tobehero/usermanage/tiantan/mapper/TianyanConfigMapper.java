package top.tobehero.usermanage.tiantan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.tobehero.usermanage.hero.entity.Hero;
import top.tobehero.usermanage.tiantan.entity.TianyanConfig;

public interface TianyanConfigMapper extends BaseMapper<TianyanConfig> {

}
