package top.tobehero.usermanage.yaml.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import top.tobehero.usermanage.yaml.entity.ConfigServerList;
import top.tobehero.usermanage.yaml.service.IConfigServerListService;
import top.tobehero.usermanage.yaml.service.IConfigServerService;
import top.tobehero.usermanage.yaml.utils.YamlUtils;
import top.tobehero.usermanage.yaml.vo.ConfigServerItem;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static top.tobehero.usermanage.yaml.utils.YamlUtils.unicodeToString;

@Service
public class ConfigServerServiceImpl implements IConfigServerService {
    @Resource
    private IConfigServerListService iConfigServerListService;
    @Override
    public List<ConfigServerItem> getConfigServerList() {
        List<ConfigServerList> list = iConfigServerListService.lambdaQuery().orderByAsc(ConfigServerList::getName).list();
        return list.stream().map((m) -> {
            Integer id = m.getId();
            String configurl = m.getConfigurl();
            String name = m.getName();
            String appnames = m.getAppnames();
            List<String> appNameList = new ArrayList<>();
            if (StringUtils.isNotBlank(appnames)) {
                appNameList = JSON.parseArray(appnames, String.class);
            }
            ConfigServerItem item = new ConfigServerItem();
            item.setId(id);
            item.setAppNames(appNameList);
            item.setConfigServerMame(name);
            item.setConfigUrl(configurl);
            return item;
        }).collect(Collectors.toList());
    }

    @Override
    public JSONObject queryAppConfig(Integer configId, String appName, String profiles, String label) throws Exception {
        ConfigServerList configServerItem = iConfigServerListService.lambdaQuery().eq(ConfigServerList::getId, configId).one();
        Assert.notNull(configServerItem,"请检查配置中心配置信息");
        String configurl = configServerItem.getConfigurl();
        String url = configurl + appName + "/" + profiles + "/" + label;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<JSONObject> responseEntity = restTemplate.getForEntity(url, JSONObject.class, new HashMap<>());
        HttpStatus statusCode = responseEntity.getStatusCode();
        Properties properties = new Properties();
        if(statusCode==HttpStatus.OK){
            JSONObject bodyJson = responseEntity.getBody();
            JSONArray propertySources = bodyJson.getJSONArray("propertySources");
            for (int i = 0;i<propertySources.size();i++) {
                JSONObject jsonItem = propertySources.getJSONObject(i);
                JSONObject source = jsonItem.getJSONObject("source");
                properties.putAll(source);
            }
        }
        String jsonStr = YamlUtils.propertyToJson(properties);
        String yaml = YamlUtils.jsonToYaml(jsonStr);
        JSONObject data = new JSONObject();
        data.put("json",jsonStr);
        data.put("yaml",yaml);
        List<String> propList = new ArrayList<>();
        Enumeration<?> enumeration = properties.propertyNames();
        while(enumeration.hasMoreElements()){
            Object key = enumeration.nextElement();
            String value = properties.get(key)+"";
            value = unicodeToString(value);
            propList.add(key+"="+value);
        }
        propList.sort((o1, o2) -> o1.compareTo(o2));
        StringBuffer propSb = new StringBuffer();
        propList.forEach((i)->{
            propSb.append(i+"\n");
        });
        data.put("properties",propSb.toString());
        return data;
    }

}
