package top.tobehero.usermanage.yaml.vo;

import lombok.Data;

import java.util.List;

@Data
public class ConfigServerItem {
    private Integer id;
    private String configServerMame;
    private List<String> appNames;
    private String configUrl;

}
