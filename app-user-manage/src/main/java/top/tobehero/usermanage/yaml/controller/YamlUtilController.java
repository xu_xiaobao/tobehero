package top.tobehero.usermanage.yaml.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.yaml.utils.YamlUtils;

import java.io.StringReader;
import java.util.Map;
import java.util.Properties;

@RestController
@RequestMapping("/yamlTools")
public class YamlUtilController {
    @RequestMapping("/propToJson")
    public BaseRes<String> propToJson(@RequestParam(name="text") String props){
        Properties properties = new Properties();
        try {
            properties.load(new StringReader(props));
            return ResponseUtils.outSuccessResp(YamlUtils.propertyToJson(properties));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.outErrorResp("-1","解析失败");
        }
    }

    @RequestMapping("/jsonToYaml")
    public BaseRes<String> jsonToYaml(@RequestParam(name="text") String json){
        try {
            return ResponseUtils.outSuccessResp(YamlUtils.jsonToYaml(json));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.outErrorResp("-1","解析失败");
        }
    }

    @RequestMapping("/propToYaml")
    public BaseRes<String> propToYaml(@RequestBody Map<String,String> map){
        try {
            String text = map.get("text");
            Properties properties = new Properties();
            properties.load(new StringReader(text));
            String json = YamlUtils.propertyToJson(properties);
            return ResponseUtils.outSuccessResp(YamlUtils.jsonToYaml(json));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.outErrorResp("-1","解析失败");
        }
    }
    @RequestMapping(value="/yamlToProp",consumes = {MediaType.APPLICATION_JSON_VALUE})
    public BaseRes<String> yamlToProp(@RequestBody Map<String,String> map){
        try {
            String text = map.get("text");
            return ResponseUtils.outSuccessResp(YamlUtils.yamlToProp(text));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.outErrorResp("-1","解析失败");
        }
    }
}
