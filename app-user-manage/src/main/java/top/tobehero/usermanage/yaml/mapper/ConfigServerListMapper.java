package top.tobehero.usermanage.yaml.mapper;

import top.tobehero.usermanage.yaml.entity.ConfigServerList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * configServer配置 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-12-26
 */
public interface ConfigServerListMapper extends BaseMapper<ConfigServerList> {

}
