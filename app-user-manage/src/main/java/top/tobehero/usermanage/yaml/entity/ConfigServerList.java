package top.tobehero.usermanage.yaml.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * configServer配置
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-12-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ConfigServerList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 配置中心地址
     */
    private String configurl;

    /**
     * 应用列表
     */
    private String appnames;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
