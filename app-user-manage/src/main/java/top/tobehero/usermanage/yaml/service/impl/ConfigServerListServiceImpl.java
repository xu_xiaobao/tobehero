package top.tobehero.usermanage.yaml.service.impl;

import top.tobehero.usermanage.yaml.entity.ConfigServerList;
import top.tobehero.usermanage.yaml.mapper.ConfigServerListMapper;
import top.tobehero.usermanage.yaml.service.IConfigServerListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * configServer配置 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-12-26
 */
@Service
public class ConfigServerListServiceImpl extends ServiceImpl<ConfigServerListMapper, ConfigServerList> implements IConfigServerListService {

}
