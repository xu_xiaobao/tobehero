package top.tobehero.usermanage.yaml.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.yaml.service.IConfigServerService;
import top.tobehero.usermanage.yaml.vo.ConfigServerItem;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/configServer")
public class ConfigServerController {
    @Resource
    private IConfigServerService iConfigServerService;
    @RequestMapping("getConfigServerList")
    public BaseRes getConfigServerList(){
        List<ConfigServerItem> configServerList = iConfigServerService.getConfigServerList();
        return ResponseUtils.outSuccessResp(configServerList);
    }

    /**
     * 查询配置中心应用配置
     * @return
     */
    @RequestMapping("queryAppConfig")
    public BaseRes queryAppConfig(Integer configId,String appName,String profiles,String label){
        JSONObject data = null;
        try {
            data = iConfigServerService.queryAppConfig(configId,appName,profiles,label);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtils.outErrorResp("-1","读取配置中心数据失败");
        }
        return ResponseUtils.outSuccessResp(data);
    }
}
