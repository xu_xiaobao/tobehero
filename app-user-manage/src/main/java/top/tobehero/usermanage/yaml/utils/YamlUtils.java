package top.tobehero.usermanage.yaml.utils;

import com.alibaba.fastjson.JSON;
import org.yaml.snakeyaml.Yaml;
import pl.jalokim.propertiestojson.util.PropertiesToJsonConverter;
import pl.jalokim.propertiestojson.util.PropertiesToJsonConverterBuilder;

import java.io.*;
import java.util.*;

import static com.alibaba.fastjson.serializer.SerializerFeature.PrettyFormat;
import static com.alibaba.fastjson.serializer.SerializerFeature.SortField;

public class YamlUtils {
    /**
     * 属性->JSON
     * @param properties
     * @return
     */
    public static String propertyToJson(Properties properties){
        PropertiesToJsonConverter propsToJsonConverter = PropertiesToJsonConverterBuilder
                .builder()
                .build();
        String s = propsToJsonConverter.convertToJson(properties);
        String s1 = JSON.parseObject(s).toString(PrettyFormat, SortField);
        StringReader sr = new StringReader(s1);
        BufferedReader bufferedReader = new BufferedReader(sr);
        String oneLine ;
        StringBuffer resSb = new StringBuffer();
        try {
            while((oneLine = bufferedReader.readLine())!=null){
                if(oneLine!=null){
                    String replaceLine = replaceStartTabByTwoSpace(oneLine);
                    resSb.append(replaceLine+"\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resSb.toString();
    }

    /**
     * 将每行开头出现的tab换成两个空格
     * @param oneLine
     * @return
     */
    private static String replaceStartTabByTwoSpace(String oneLine) {
        char[] chars = oneLine.toCharArray();
        StringBuffer sb = new StringBuffer();
        boolean hasEnd = false;
        for(int i=0;i<chars.length;i++){
            if(!hasEnd&&chars[i]=='\t'){
                sb.append("  ");
            }
        }
        sb.append(oneLine.replaceAll("^\t+",""));
        return sb.toString();
    }

    public static String jsonToYaml(String json){
        Map<String, Object> map = JSON.parseObject(json,Map.class);
        Yaml yaml = new Yaml();
        return yaml.dumpAsMap(map);
    }

    public static String yamlToJson(String yaml){
        Yaml yml = new Yaml();
        Map map = yml.loadAs(yaml, Map.class);
        PropertiesToJsonConverter propsToJsonConverter = PropertiesToJsonConverterBuilder
                .builder()
                .build();
        String json = propsToJsonConverter.convertFromValuesAsObjectMap(map);
        return json;
    }

    public static String yamlToProp(String yaml){
        String jsonStr = yamlToJson(yaml);
        Properties properties = JSONParserUtils2.jsonToProperties(jsonStr);
        List<String> propList = new ArrayList<>();
        Enumeration<?> enumeration = properties.propertyNames();
        while(enumeration.hasMoreElements()){
            Object key = enumeration.nextElement();
            String value = properties.get(key)+"";
            value = unicodeToString(value);
            propList.add(key+"="+value);
        }
        propList.sort((o1, o2) -> o1.compareTo(o2));
        StringBuffer propSb = new StringBuffer();
        propList.forEach((i)->{
            propSb.append(i+"\n");
        });
        return propSb.toString();
    }
    public static String unicodeToString(String s) {
        String[] split = s.split("\\\\");

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < split.length ; i++) {

            if (split[i].startsWith("u")){
                builder.append((char) Integer.parseInt(split[i].substring(1,5), 16));
                if(split[i].length()>5){
                    builder.append(split[i].substring(5));
                }
            }else {
                builder.append(split[i]);
            }

        }
        return builder.toString();
    }
    public static void main(String[] args) throws Exception {
        InputStream in = YamlUtils.class.getResourceAsStream("/test-properties.properties");
        Properties properties = new Properties();
        properties.load(new InputStreamReader(in,"UTF-8"));
        System.out.println(properties);
        System.out.println("===================================");
        String json = propertyToJson(properties);
        System.out.println(json);
        System.out.println("===================================");
        String yamlStr = jsonToYaml(json);
        System.out.println("===================================");
        System.out.println(yamlToProp(yamlStr));
    }
}
