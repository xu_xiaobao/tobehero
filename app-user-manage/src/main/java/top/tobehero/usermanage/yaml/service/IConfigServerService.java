package top.tobehero.usermanage.yaml.service;

import com.alibaba.fastjson.JSONObject;
import top.tobehero.usermanage.yaml.vo.ConfigServerItem;

import java.util.List;

public interface IConfigServerService {
    /**
     * 获取配置中心列表
     * @return
     */
    List<ConfigServerItem> getConfigServerList();

    /**
     * 查询指定配置中心内应用配置
     * @param configId
     * @param appName
     * @param profiles
     * @param label
     * @return
     */
    JSONObject queryAppConfig(Integer configId, String appName,String profiles, String label) throws Exception;
}
