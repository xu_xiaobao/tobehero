package top.tobehero.usermanage.yaml.service;

import top.tobehero.usermanage.yaml.entity.ConfigServerList;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * configServer配置 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-12-26
 */
public interface IConfigServerListService extends IService<ConfigServerList> {

}
