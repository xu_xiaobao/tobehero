package top.tobehero.usermanage.db_manage.tab.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 表字段关系表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbTabColRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 表名ID
     */
    private Integer tabId;

    /**
     * 字段ID
     */
    private Integer colId;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
