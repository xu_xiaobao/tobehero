package top.tobehero.usermanage.db_manage.tab.service.impl;

import org.springframework.stereotype.Service;
import top.tobehero.usermanage.core.service.impl.BaseServiceImpl;
import top.tobehero.usermanage.db_manage.tab.entity.DbEnumDic;
import top.tobehero.usermanage.db_manage.tab.mapper.DbEnumDicMapper;
import top.tobehero.usermanage.db_manage.tab.service.IDbEnumDicService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Service
public class DbEnumDicServiceImpl extends BaseServiceImpl<DbEnumDicMapper, DbEnumDic> implements IDbEnumDicService {

}
