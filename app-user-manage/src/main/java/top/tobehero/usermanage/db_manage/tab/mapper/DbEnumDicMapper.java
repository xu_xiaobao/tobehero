package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbEnumDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface DbEnumDicMapper extends BaseMapper<DbEnumDic> {

}
