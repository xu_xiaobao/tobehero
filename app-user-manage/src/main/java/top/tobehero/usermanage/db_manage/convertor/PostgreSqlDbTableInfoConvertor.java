package top.tobehero.usermanage.db_manage.convertor;

import top.tobehero.usermanage.db_manage.enums.DataBaseEnum;

public class PostgreSqlDbTableInfoConvertor extends SimpleJdbcDbTableInfoConvertor {
    @Override
    public boolean isMatch(DataBaseEnum dataBaseEnum) {
        return DataBaseEnum.POSTGRESQL==dataBaseEnum;
    }
    @Override
    protected String getTableQuerydSql(String tableName) {
        return "select * from " + tableName +" limit 0";
    }
}
