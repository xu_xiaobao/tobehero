package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表标签 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-21
 */
public interface DbTableTagRelationMapper extends BaseMapper<DbTableTagRelation> {

}
