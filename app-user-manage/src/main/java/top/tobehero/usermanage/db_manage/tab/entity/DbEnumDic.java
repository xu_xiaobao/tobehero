package top.tobehero.usermanage.db_manage.tab.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbEnumDic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 表字段关系ID
     */
    private Integer colId;

    /**
     * 可选值
     */
    private String val;

    /**
     * 值说明
     */
    private String describe;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
