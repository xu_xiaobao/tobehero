package top.tobehero.usermanage.db_manage.tab.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.constants.AuthConstants;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.core.service.impl.BaseServiceImpl;
import top.tobehero.usermanage.db_manage.tab.entity.*;
import top.tobehero.usermanage.db_manage.tab.mapper.*;
import top.tobehero.usermanage.db_manage.tab.service.*;
import top.tobehero.usermanage.db_manage.tab.vo.*;
import top.tobehero.usermanage.sysname.entity.SysNameConfig;
import top.tobehero.usermanage.sysname.mapper.SysNameConfigMapper;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * 表名 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Service
@Transactional
public class DbTableServiceImpl extends BaseServiceImpl<DbTableMapper, DbTable> implements IDbTableService {
    @Resource
    private DbTableMapper dbTableMapper;
    @Resource
    private DbColumnMapper dbColumnMapper;
    @Resource
    private DbColDefMapper dbColDefMapper;
    @Resource
    private SysNameConfigMapper sysNameConfigMapper;
    @Resource
    private HttpSession httpSession;
    @Resource
    private DbTableTagDefMapper dbTableTagDefMapper;
    @Resource
    private DbTableTagRelationMapper dbTableTagRelationMapper;
    @Resource
    private DbEnumDicMapper dbEnumDicMapper;
    @Resource
    private IDbTableTagRelationService iDbTableTagRelationService;
    @Resource
    private IDbEnumDicService iDbEnumDicService;
    @Override
    public BaseRes save(EditTable tab) {
        if(tab.getTabId()!=null){
            return this.update(tab);
        }
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        String tableName = tab.getTableName();
        String sysCode = tab.getSysCode();
        String dbType = tab.getDbType();

        if(StringUtils.isBlank(tableName)){
            return ResponseUtils.outErrorResp("-1","表名称不能为空！");
        }
        if(StringUtils.isBlank(dbType)){
            return ResponseUtils.outErrorResp("-1","数据库类型不能为空！");
        }
        if(StringUtils.isBlank(sysCode)){
            return ResponseUtils.outErrorResp("-1","系统名称不能为空！");
        }
        //检查输入字段类型是否合法
        List<EditTableColumn> cloumns = tab.getColumns();
        if(cloumns==null||cloumns.size()==0){
            return ResponseUtils.outErrorResp("-1","表字段不能为空！");
        }
        StringBuffer errMsgSb = new StringBuffer();
        for (EditTableColumn item:cloumns) {
            String colName = item.getColName();
            String logicName = item.getLogicName();
            Integer typeId = item.getTypeId();
            Integer typeSize = item.getTypeSize();
            Integer typeDecimal = item.getTypeDecimal();
            String isNull = item.getIsNotNull();
            String isPk = item.getIsPk();
            String isAutoIncre = item.getIsAutoIncre();
            String defaultVal = item.getDefaultVal();
            String isEnum = item.getIsEnum();
            String remark = item.getRemark();


            //校验数据库字段类型是否存在
            if(typeId==null){
                errMsgSb.append("字段逻辑名【"+logicName+"】，字段类型为不能空\n");
                continue;
            }
            DbColDef dbColDef = dbColDefMapper.selectById(typeId);
            if(dbColDef==null){
                errMsgSb.append("字段逻辑名【"+logicName+"】，字段类型不存在\n");
                continue;
            }
            String needSize = dbColDef.getNeedSize();
            if("1".equals(needSize)){
                if(typeSize==null){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，字段长度必填\n");
                    continue;
                }
                if(typeSize<=0){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，字段长度必需为正整数\n");
                    continue;
                }
            }
            String needDecimal = dbColDef.getNeedDecimal();
            if("1".equals(needDecimal)){
                if(typeDecimal==null){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，小数点必填\n");
                    continue;
                }
                if(typeDecimal < 0){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，小数点必需为非负数\n");
                    continue;
                }
            }
        }
        String errMsg = errMsgSb.toString();
        if(StringUtils.isNotBlank(errMsg)){
            return ResponseUtils.outErrorResp("-1",errMsg);
        }
        //查询系统内是否有重复表结构
        LambdaQueryWrapper<DbTable> qryWrapper = new LambdaQueryWrapper<DbTable>().eq(DbTable::getName, tableName).eq(DbTable::getSysCode, sysCode);
        List<DbTable> dbTables = dbTableMapper.selectList(qryWrapper);
        if(dbTables!=null&&dbTables.size()>0){
            return ResponseUtils.outErrorResp("-1","当前表已存在，请检查！");
        }
        //保存表结构
        //保存表定义
        DbTable dbTable = new DbTable();
        dbTable.setSysCode(tab.getSysCode());
        dbTable.setDbType(dbType);
        dbTable.setName(tab.getTableName());
        dbTable.setLogicName(tab.getLogicName());
        dbTable.setRemark(tab.getRemark());
        dbTable.setFlag("1");
        dbTable.setRevision(1);
        LocalDateTime now = LocalDateTime.now();
        dbTable.setCreatedBy(username);
        dbTable.setCreatedTime(now);
        int insert = dbTableMapper.insert(dbTable);
        if(insert==0){
            throw new RuntimeException("保存失败");
        }
        //保存字段定义
        Integer displayNo = 0;
        for (EditTableColumn item:cloumns) {
            String colName = item.getColName();
            String logicName = item.getLogicName();
            Integer typeId = item.getTypeId();
            Integer typeSize = item.getTypeSize();
            Integer typeDecimal = item.getTypeDecimal();
            String isNull = item.getIsNotNull();
            String isPk = item.getIsPk();
            String isAutoIncre = item.getIsAutoIncre();
            String defaultVal = item.getDefaultVal();
            String isEnum = item.getIsEnum();
            String remark = item.getRemark();

            DbColumn dbColumn = new DbColumn();
            dbColumn.setName(colName);
            dbColumn.setTabId(dbTable.getId());
            dbColumn.setLogicName(logicName);
            dbColumn.setTypeId(typeId);
            dbColumn.setTypeSize(typeSize);
            dbColumn.setTypeDecimal(typeDecimal);
            dbColumn.setIsNotNull(isNull);
            dbColumn.setIsPk(isPk);
            dbColumn.setIsAutoIncre(isAutoIncre);
            dbColumn.setDefaultVal(defaultVal);
            dbColumn.setIsEnum(isEnum);
            dbColumn.setRemark(remark);
            dbColumn.setCreatedTime(now);
            dbColumn.setCreatedBy(username);
            dbColumn.setRevision(1);
            dbColumn.setFlag("1");
            dbColumn.setDisplayNo(++displayNo);
            int insert1 = dbColumnMapper.insert(dbColumn);
            if(insert1==0){
                throw new RuntimeException("保存失败");
            }
        }
        //保存数据表与标签
        tab.setTabId(dbTable.getId());
        this.saveOrUpdateTableTags(tab);

        return ResponseUtils.outSuccessResp(dbTable.getId());
    }

    /**
     * 保存或更新数据表标签
     * @param tab
     */
    private void saveOrUpdateTableTags(EditTable tab) {
        LocalDateTime now = LocalDateTime.now();
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        Integer tabId = tab.getTabId();
        if(tabId!=null){
            List<String> tags = tab.getTags();
            List<DbTableTagDef> allTags = new ArrayList<DbTableTagDef>();
            if(tags!=null&&tags.size()>0){
                String sysCode = tab.getSysCode();
                //查询当前系统下是否存在此标签,没有则保存
                for (String tagName:tags) {
                    DbTableTagDef dbTableTagDef = dbTableTagDefMapper.selectOne(new LambdaQueryWrapper<DbTableTagDef>().eq(DbTableTagDef::getSysCode, sysCode).eq(DbTableTagDef::getTagName, tagName));
                    if(dbTableTagDef==null) {
                        dbTableTagDef = new DbTableTagDef();
                        dbTableTagDef.setSysCode(sysCode);
                        dbTableTagDef.setTagName(tagName);
                        dbTableTagDef.setCreatedBy(username);
                        dbTableTagDef.setCreatedTime(now);
                        dbTableTagDefMapper.insert(dbTableTagDef);
                    }
                    allTags.add(dbTableTagDef);
                }
            }
            //维护表与标签关系
            List<DbTableTagRelation> dbTableTagRelations = dbTableTagRelationMapper.selectList(new LambdaQueryWrapper<DbTableTagRelation>().eq(DbTableTagRelation::getTabId, tabId));
            List<DbTableTagRelation> targetRelations = new ArrayList<>();
            for(DbTableTagDef tag:allTags){
                Integer tagId = tag.getId();
                DbTableTagRelation newRelation = new DbTableTagRelation();
                newRelation.setTabId(tabId);
                newRelation.setTagId(tagId);
                targetRelations.add(newRelation);
            }
            iDbTableTagRelationService.mergeSingleTableBean(dbTableTagRelations,targetRelations,DbTableTagRelation::getId);
        }
    }

    @Override
    public EditTable findEditTableById(Integer id) {
        DbTable dbTable = dbTableMapper.selectById(id);
        if(dbTable==null){
            return null;
        }
        Integer tabId = dbTable.getId();
        List<DbColumn> dbColumns = dbColumnMapper.selectList(new LambdaQueryWrapper<DbColumn>()
                .eq(DbColumn::getTabId, tabId)
                .orderByDesc(DbColumn::getFlag)
                .orderByAsc(DbColumn::getDisplayNo)
        );
        List<EditTableColumn> columns = new ArrayList<>();
        if(dbColumns!=null){
            for (DbColumn item:dbColumns) {
                EditTableColumn column = new EditTableColumn();
                column.setDbColumnId(item.getId());
                column.setLogicName(item.getLogicName());
                column.setColName(item.getName());
                column.setTypeId(item.getTypeId());
                column.setTypeSize(item.getTypeSize());
                column.setTypeDecimal(item.getTypeDecimal());
                column.setIsNotNull(item.getIsNotNull());
                column.setIsPk(item.getIsPk());
                column.setIsAutoIncre(item.getIsAutoIncre());
                column.setDefaultVal(item.getDefaultVal());
                column.setIsEnum(item.getIsEnum());
                column.setRemark(item.getRemark());
                column.setDbColumnId(item.getId());
                columns.add(column);
            }
        }
        List<DbColDef> dbColDefs = dbColDefMapper.selectList(new LambdaQueryWrapper<DbColDef>().eq(DbColDef::getDbType, dbTable.getDbType()));
        columns.stream().forEach((r)->{
            for (DbColDef dbColDef:dbColDefs) {
                Integer typeId = dbColDef.getId();
                if(typeId.equals(r.getTypeId())){
                    r.setTypeName(dbColDef.getName());
                    break;
                }
            }
        });
        EditTable table = new EditTable();
        table.setTabId(dbTable.getId());
        table.setLogicName(dbTable.getLogicName());
        table.setTableName(dbTable.getName());
        table.setDbType(dbTable.getDbType());
        table.setSysCode(dbTable.getSysCode());
        table.setRemark(dbTable.getRemark());
        table.setColumns(columns);

        List<DbTableTagRelation> dbTableTagRelations = dbTableTagRelationMapper.selectList(new LambdaQueryWrapper<DbTableTagRelation>().select(DbTableTagRelation::getTagId).eq(DbTableTagRelation::getTabId, tabId));
        ArrayList<Integer> tagIds = new ArrayList<>();
        if(dbTableTagRelations!=null&&dbTableTagRelations.size()>0){
            dbTableTagRelations.stream().forEach((i)->{
                tagIds.add(i.getTagId());
            });
            List<DbTableTagDef> dbTableTagDefs = dbTableTagDefMapper.selectList(new LambdaQueryWrapper<DbTableTagDef>().in(DbTableTagDef::getId,tagIds));
            if(dbTableTagDefs!=null){
                List<String> tags = new ArrayList<>();
                for (DbTableTagDef dbTableTagDef:dbTableTagDefs) {
                    String tagName = dbTableTagDef.getTagName();
                    tags.add(tagName);
                }
                table.setTags(tags);
            }
        }
        return table;
    }

    @Override
    public Page<DbTable> findPage(long current, long size, DbTable data) {
        String dbType = data.getDbType();
        String flag = data.getFlag();
        String name = data.getName();
        String logicName = data.getLogicName();
        logicName = logicName!=null?logicName.toLowerCase():null;
        String sysCode = data.getSysCode();
        LambdaQueryChainWrapper<DbTable> lambdaQuery = this.lambdaQuery();
        if(StringUtils.isNotBlank(dbType)){
            lambdaQuery.eq(DbTable::getDbType,dbType);
        }
        if(StringUtils.isNotBlank(flag)){
            lambdaQuery.eq(DbTable::getFlag,flag);
        }
        if(StringUtils.isNotBlank(name)){
            lambdaQuery.like(DbTable::getName,"%"+name+"%");
        }
        if(StringUtils.isNotBlank(logicName)){
            lambdaQuery.like(DbTable::getLogicName,"%"+logicName+"%");
        }
        if(StringUtils.isNotBlank(sysCode)){
            lambdaQuery.eq(DbTable::getSysCode,sysCode);
        }
        lambdaQuery.orderByAsc(DbTable::getSysCode,DbTable::getDbType,DbTable::getName);
        Page<DbTable> page = lambdaQuery.page(new Page<>(current, size));
        List<SysNameConfig> sysNameConfigs = sysNameConfigMapper.selectList(new LambdaQueryWrapper<SysNameConfig>().eq(SysNameConfig::getFlag, "1"));
        page.getRecords().stream().forEach((r->{
            for (SysNameConfig item:sysNameConfigs) {
                if(item.getSysCode().equals(r.getSysCode())){
                    r.setSysName(item.getSysName());
                    break;
                }
            }
        }));
        return page;
    }

    @Override
    public BaseRes update(EditTable tab) {
        String username = (String) httpSession.getAttribute(AuthConstants.SESSION_USERNAME_KEY);
        Integer tabId = tab.getTabId();
        Assert.isTrue(tabId !=null,"tabId不能为空");

        String tableName = tab.getTableName();
        String sysCode = tab.getSysCode();
        String dbType = tab.getDbType();

//        if(StringUtils.isBlank(tableName)){
//            return ResponseUtils.outErrorResp("-1","表名称不能为空！");
//        }
//        if(StringUtils.isBlank(dbType)){
//            return ResponseUtils.outErrorResp("-1","数据库类型不能为空！");
//        }
//        if(StringUtils.isBlank(sysCode)){
//            return ResponseUtils.outErrorResp("-1","系统名称不能为空！");
//        }
        //检查输入字段类型是否合法
        List<EditTableColumn> cloumns = tab.getColumns();
        if(cloumns==null||cloumns.size()==0){
            return ResponseUtils.outErrorResp("-1","表字段不能为空！");
        }
        StringBuffer errMsgSb = new StringBuffer();
        for (EditTableColumn item:cloumns) {
            String colName = item.getColName();
            String logicName = item.getLogicName();
            Integer typeId = item.getTypeId();
            Integer typeSize = item.getTypeSize();
            Integer typeDecimal = item.getTypeDecimal();
            String isNull = item.getIsNotNull();
            String isPk = item.getIsPk();
            String isAutoIncre = item.getIsAutoIncre();
            String defaultVal = item.getDefaultVal();
            String isEnum = item.getIsEnum();
            String remark = item.getRemark();


            //校验数据库字段类型是否存在
            if(typeId==null){
                errMsgSb.append("字段逻辑名【"+logicName+"】，字段类型为不能空\n");
                continue;
            }
            DbColDef dbColDef = dbColDefMapper.selectById(typeId);
            if(dbColDef==null){
                errMsgSb.append("字段逻辑名【"+logicName+"】，字段类型不存在\n");
                continue;
            }
            String needSize = dbColDef.getNeedSize();
            if("1".equals(needSize)){
                if(typeSize==null){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，字段长度必填\n");
                    continue;
                }
                if(typeSize<=0){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，字段长度必需为正整数\n");
                    continue;
                }
            }
            String needDecimal = dbColDef.getNeedDecimal();
            if("1".equals(needDecimal)){
                if(typeDecimal==null){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，小数点必填\n");
                    continue;
                }
                if(typeDecimal<0){
                    errMsgSb.append("字段逻辑名【"+logicName+"】，小数点必需为非负数\n");
                    continue;
                }
            }
        }
        String errMsg = errMsgSb.toString();
        if(StringUtils.isNotBlank(errMsg)){
            return ResponseUtils.outErrorResp("-1",errMsg);
        }
        //查询系统内是否有重复表结构
        LambdaQueryWrapper<DbTable> qryWrapper = new LambdaQueryWrapper<DbTable>()
                .eq(DbTable::getName, tableName)
                .eq(DbTable::getSysCode, sysCode)
                .ne(DbTable::getId,tabId);
        List<DbTable> dbTables = dbTableMapper.selectList(qryWrapper);
        if(dbTables!=null&&dbTables.size()>0){
            return ResponseUtils.outErrorResp("-1","当前表已存在，请检查！");
        }
        //更新表结构
        //更新表定义：只更新表的详细描述
        DbTable existsDbTable = dbTableMapper.selectById(tabId);
        if(existsDbTable==null){
            return ResponseUtils.outErrorResp("-1","当前表不存在无法更新！");
        }
        LocalDateTime now = LocalDateTime.now();
        DbTable updateDbTable = new DbTable();
        updateDbTable.setRemark(tab.getRemark());
        Integer revision = existsDbTable.getRevision();
        updateDbTable.setUpdatedTime(now);
        updateDbTable.setCreatedBy(username);
        updateDbTable.setRevision(revision!=null?revision+1:1);
        int updateRes = dbTableMapper.update(updateDbTable, new LambdaQueryWrapper<DbTable>().eq(DbTable::getId, tabId).eq(DbTable::getRevision, revision));
        if(updateRes==0){
            throw new RuntimeException("数据版本不一致，更新失败");
        }
        List<DbColumn> dbColumns = dbColumnMapper.selectList(new LambdaQueryWrapper<DbColumn>().eq(DbColumn::getTabId, tabId));
        //保存字段定义
        Integer displayNo = 0;
        Iterator<EditTableColumn> it = cloumns.iterator();
        while(it.hasNext()){
            EditTableColumn item = it.next();
            String colName = item.getColName();
            String logicName = item.getLogicName();
            Integer typeId = item.getTypeId();
            Integer typeSize = item.getTypeSize();
            Integer typeDecimal = item.getTypeDecimal();
            String isNull = item.getIsNotNull();
            String isPk = item.getIsPk();
            String isAutoIncre = item.getIsAutoIncre();
            String defaultVal = item.getDefaultVal();
            String isEnum = item.getIsEnum();
            String remark = item.getRemark();
            Integer dbColumnId = item.getDbColumnId();
//            Optional.ofNullable(dbColumnId)..ifPresent(((columnId) -> {
//                Optional<DbColumn> first = dbColumns.stream().filter((i) -> i.getId() == columnId).findFirst();
//            }));
            DbColumn existsDbColumn = null;
            if(dbColumnId!=null){
                //更新数据
                existsDbColumn = dbColumns.stream().filter((i) -> dbColumnId.equals(i.getId())).findFirst().get();
                Assert.notNull(existsDbColumn,"字段定义不存在");
                DbColumn updateDbColumn = new DbColumn();
                updateDbColumn.setName(colName);
                updateDbColumn.setTabId(tabId);
                updateDbColumn.setLogicName(logicName);
                updateDbColumn.setTypeId(typeId);
                updateDbColumn.setTypeSize(typeSize);
                updateDbColumn.setTypeDecimal(typeDecimal);
                updateDbColumn.setIsNotNull(isNull);
                updateDbColumn.setIsPk(isPk);
                updateDbColumn.setIsAutoIncre(isAutoIncre);
                updateDbColumn.setDefaultVal(defaultVal);
                updateDbColumn.setIsEnum(isEnum);
                updateDbColumn.setRemark(remark);
                updateDbColumn.setUpdatedTime(now);
                updateDbColumn.setUpdatedBy(username);
                Integer revision1 = existsDbColumn.getRevision();
                updateDbColumn.setRevision(revision1!=null?revision1+1:1);
                updateDbColumn.setFlag("1");
                updateDbColumn.setDisplayNo(++displayNo);

                int updateDbColumn1 = dbColumnMapper.update(updateDbColumn, new LambdaQueryWrapper<DbColumn>().eq(DbColumn::getId, dbColumnId).eq(DbColumn::getRevision,revision1));
                if(updateDbColumn1==0){
                    throw new RuntimeException("数据版本不一致，更新失败");
                }
                //移除已存在
                Iterator<DbColumn> it1 = dbColumns.iterator();
                while(it1.hasNext()){
                    if(it1.next()==existsDbColumn){
                        it1.remove();
                    }
                }
            }else{
                //保存新数据
                DbColumn dbColumn = new DbColumn();
                dbColumn.setName(colName);
                dbColumn.setTabId(tabId);
                dbColumn.setLogicName(logicName);
                dbColumn.setTypeId(typeId);
                dbColumn.setTypeSize(typeSize);
                dbColumn.setTypeDecimal(typeDecimal);
                dbColumn.setIsNotNull(isNull);
                dbColumn.setIsPk(isPk);
                dbColumn.setIsAutoIncre(isAutoIncre);
                dbColumn.setDefaultVal(defaultVal);
                dbColumn.setIsEnum(isEnum);
                dbColumn.setRemark(remark);
                dbColumn.setCreatedTime(now);
                dbColumn.setRevision(1);
                dbColumn.setFlag("1");
                dbColumn.setDisplayNo(++displayNo);
                int insert1 = dbColumnMapper.insert(dbColumn);
                if(insert1==0){
                    throw new RuntimeException("保存失败");
                }
            }
        }
        if(dbColumns.size()>0){
            ArrayList<Integer> ids = new ArrayList<>();
            dbColumns.forEach((i)->{
                ids.add(i.getId());
            });
            dbColumnMapper.delete(new LambdaQueryWrapper<DbColumn>().in(DbColumn::getId,ids));
        }
        //保存数据表与标签
        tab.setTabId(tab.getTabId());
        this.saveOrUpdateTableTags(tab);
        return ResponseUtils.outSuccessResp(tab.getTabId());
    }

    @Override
    public List<DbTableTagDef> findTags(String sysCode, String tagName) {
        return dbTableTagDefMapper.selectList(new LambdaQueryWrapper<DbTableTagDef>().eq(DbTableTagDef::getSysCode,sysCode).like(DbTableTagDef::getTagName,tagName));
    }

    @Override
    public EditColEnums qryEnumsByColId(Integer colId) {
        DbColumn dbColumn = dbColumnMapper.selectById(colId);
        if(dbColumn!=null){
            Integer tabId = dbColumn.getTabId();
            DbTable dbTable = dbTableMapper.selectById(tabId);
            if(dbTable!=null){
                String tabName = dbTable.getName();
                EditColEnums data = new EditColEnums();
                data.setColId(colId);
                data.setTabId(tabId);
                data.setColName(dbColumn.getName());
                data.setTabName(dbTable.getName());
                List<DbEnumDic> dbEnumDics = dbEnumDicMapper.selectList(new LambdaQueryWrapper<DbEnumDic>().eq(DbEnumDic::getColId, colId).orderByAsc(DbEnumDic::getVal));
                data.setEnums(dbEnumDics);
                return data;
            }
        }
        return null;
    }

    @Override
    public void saveColEnums(EditColEnums colEnums) {
        List<DbEnumDic> targetList = colEnums.getEnums();
        Integer colId = colEnums.getColId();
        List<DbEnumDic> dbEnumDics = dbEnumDicMapper.selectList(new LambdaQueryWrapper<DbEnumDic>().eq(DbEnumDic::getColId, colId));
        iDbEnumDicService.mergeSingleTableBean(dbEnumDics,targetList,DbEnumDic::getId);
    }

    @Override
    public BaseRes getTableDefTree() {
        List<DbTable> list = this.lambdaQuery().eq(DbTable::getFlag, "1").list();
        LambdaQueryWrapper<SysNameConfig> sysNameConfigLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysNameConfigLambdaQueryWrapper.eq(SysNameConfig::getFlag,"1");
        List<SysNameConfig> sysNameConfigs = sysNameConfigMapper.selectList(sysNameConfigLambdaQueryWrapper);
        List<SysNameTreeItem> treeList = new ArrayList<>();
        for(DbTable dbTable:list){
            String sysCode = dbTable.getSysCode();
            SysNameConfig sysNameConfig = sysNameConfigs.stream()
                    .filter((c)->c.getSysCode().equals(sysCode))
                    .findFirst()
                    .get();
            SysNameTreeItem sysNameTreeItem = null;
            for(SysNameTreeItem treeItem:treeList){
                String id = treeItem.getId();
                if(id.equals(sysCode)){
                    sysNameTreeItem = treeItem;
                    break;
                }
            }
            if(sysNameTreeItem==null){
                sysNameTreeItem = new SysNameTreeItem();
                sysNameTreeItem.setId(dbTable.getSysCode());
                sysNameTreeItem.setLabel(sysNameConfig.getSysName());
                sysNameTreeItem.setData(sysNameConfig);
                sysNameTreeItem.setChildren(new ArrayList<>());
                treeList.add(sysNameTreeItem);
            }
            List<TabDefTreeItem> children = sysNameTreeItem.getChildren();
            TabDefTreeItem tabDefTreeItem = new TabDefTreeItem();
            tabDefTreeItem.setId(dbTable.getId()+"");
            tabDefTreeItem.setLabel(dbTable.getLogicName());
            tabDefTreeItem.setData(dbTable);
            children.add(tabDefTreeItem);
        }
        for (SysNameTreeItem item:treeList) {
            List<TabDefTreeItem> children = item.getChildren();
            children.sort((o1,o2)->o1.getLabel().compareTo(o2.getLabel()));
        }
        treeList.sort((o1,o2)->o1.getLabel().compareTo(o2.getId()));
        return ResponseUtils.outSuccessResp(treeList);
    }
    @Resource
    private IDbColumnService iDbColumnService;
    @Resource
    private IDbColDefService iDbColDefService;
    @Override
    public BaseRes getTableSummaryInfo(Integer tableId) {
        DbTable dbTable = this.lambdaQuery().eq(DbTable::getId, tableId).eq(DbTable::getFlag, "1").one();
        SummaryTableDef tableDef = new SummaryTableDef();
        tableDef.setTableId(tableId);
        tableDef.setLogicName(dbTable.getLogicName());
        tableDef.setTableName(dbTable.getName());
        List<SummaryTabColDef> columns = new ArrayList<>();
        tableDef.setColumns(columns);
        List<DbColumn> list = iDbColumnService.lambdaQuery().eq(DbColumn::getTabId, tableId).eq(DbColumn::getFlag, "1").orderByAsc(DbColumn::getDisplayNo).list();
        List<DbColDef> dbColTypes = iDbColDefService.lambdaQuery().eq(DbColDef::getDbType, dbTable.getDbType()).list();
        for(DbColumn dbColumn : list){
            SummaryTabColDef colDef = new SummaryTabColDef();
            Integer colId = dbColumn.getId();
            colDef.setColId(colId);
            colDef.setName(dbColumn.getName());
            colDef.setLogicName(dbColumn.getLogicName());
            Integer typeId = dbColumn.getTypeId();
            DbColDef dbColDef = dbColTypes.stream().filter((i) ->
                i.getId() == typeId
            ).findFirst().get();
            colDef.setTypeName(dbColDef.getName());
            colDef.setTypeSize(dbColumn.getTypeSize());
            colDef.setTypeDecimal(dbColumn.getTypeDecimal());
            colDef.setIsPk(dbColumn.getIsPk());
            colDef.setIsNotNull(dbColumn.getIsNotNull());
            colDef.setIsAutoIncre(dbColumn.getIsAutoIncre());
            colDef.setDefaultVal(dbColumn.getDefaultVal());
            colDef.setRemark(dbColumn.getRemark());
            colDef.setDisplayNo(dbColumn.getDisplayNo());
            String isEnum = dbColumn.getIsEnum();
            colDef.setIsEnum(isEnum);
            if("1".equals(isEnum)){
                List<DbEnumDic> dbEnumDics = iDbEnumDicService.lambdaQuery().eq(DbEnumDic::getColId, colId).orderByAsc(DbEnumDic::getVal).list();
                colDef.setEnums(dbEnumDics);
            }
            columns.add(colDef);
        }
        return ResponseUtils.outSuccessResp(tableDef);
    }
}
