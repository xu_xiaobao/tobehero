package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;
import top.tobehero.usermanage.db_manage.tab.entity.DbEnumDic;

import java.util.List;

@Data
public class EditColEnums {
    private String tabName;
    private String colName;
    private Integer tabId;
    private Integer colId;
    private List<DbEnumDic> enums;
}
