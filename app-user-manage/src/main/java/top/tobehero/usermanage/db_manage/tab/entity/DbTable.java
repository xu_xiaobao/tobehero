package top.tobehero.usermanage.db_manage.tab.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 表名
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 系统代码
     */
    private String sysCode;

    /***
     *
     */
    @TableField(exist = false)
    private String sysName;

    /**
     * 数据库类型
     */
    private String dbType;

    /**
     * 表名称
     */
    private String name;

    /**
     * 逻辑名称
     */
    private String logicName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 有效标识
     */
    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
