package top.tobehero.usermanage.db_manage.convertor;

import lombok.Data;

@Data
public class DbColInfo {
    /**是否自增**/
    private boolean autoIncrement;
    /**是否大小写敏感**/
    private boolean caseSensitive;
    /**是否可查，被用于wehere条件后**/
    private boolean searchable;
    /**是否货币类型**/
    private boolean currency;
    /***
     * 是否可以为空
     * 0 :不允许为空
     * 1 :允许为空
     * 2 :未知是否可以为空
     */
    private int nullable;
    /**是否为带符号数字**/
    private boolean signed;
    /**指示指定列的正常最大宽度（以字符为单位）。**/
    private int columnDisplaySize;
    /**查询语句中as后的别名**/
    private String columnLabel;
    /**字段名**/
    private String columnName;
    /**table的schema**/
    private String schemaName;
    /**
     * 对于数值数据，为最大精度
     * 对于字符串数据，为字符串长度
     * 对于datetime数据类型，这是字符串表示的字符长度（假设分数秒组件允许的最大精度）
     * 对于二进制数据，为字节长度
     * 对于ROWID数据类型，为字节长度
     * 对于数据类型不支持则返回0
     */
    private int precision;
    /**小数点长度**/
    private int scale;
    /**得到表名**/
    private String tableName;
    /**catalogName**/
    private String catalogName;
    /**获取字段SQL类型**/
    private int columnType;
    /**获取字段SQL数据库指定类型**/
    private String columnTypeName;
    /**只读**/
    private boolean readOnly;
    /**可写**/
    private boolean writable;
    /**指示对指定列的写入是否一定成功**/
    private boolean definitelyWritable;
    /**获取java类型的全类名**/
    private String columnClassName;
    /***注释*/
    private String remarks;
}
