package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;
import top.tobehero.usermanage.sysname.entity.SysNameConfig;

@Data
public class SysNameTreeItem extends BaseTreeItem<SysNameConfig,TabDefTreeItem>{
    public SysNameTreeItem(){
        super("sysName");
    }
}
