package top.tobehero.usermanage.db_manage.tab.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 数据库字段别名表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbColDefAlias implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库类型别名
     */
    private String aliasName;

    /**
     * 数据库类型
     */
    private String dbType;

    /**
     * 数据库字段类型
     */
    private String colTypeName;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDate updatedTime;


}
