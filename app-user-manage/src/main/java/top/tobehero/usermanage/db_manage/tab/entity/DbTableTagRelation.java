package top.tobehero.usermanage.db_manage.tab.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 表标签
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbTableTagRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /**
     * 表ID
     */
    private Integer tabId;

    /**
     * 标签ID
     */
    private Integer tagId;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
