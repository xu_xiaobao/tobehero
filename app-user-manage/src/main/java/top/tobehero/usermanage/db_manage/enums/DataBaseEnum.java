package top.tobehero.usermanage.db_manage.enums;

/**
 * 数据库类型枚举
 */
public enum DataBaseEnum {
    MYSQL("MYSQL","com.mysql.jdbc.Driver"),
    POSTGRESQL("POSTGRESQL","org.postgresql.Driver"),
    INFORMIX("INFORMIX","com.informix.jdbc.IfxDriver");
    public String code;
    DataBaseEnum(String code,String driverClass){
        this.code = code;
        this.driverClass = driverClass;
    }
    private String driverClass;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }}
