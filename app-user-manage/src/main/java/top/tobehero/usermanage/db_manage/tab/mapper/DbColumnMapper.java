package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbColumn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字段表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface DbColumnMapper extends BaseMapper<DbColumn> {

}
