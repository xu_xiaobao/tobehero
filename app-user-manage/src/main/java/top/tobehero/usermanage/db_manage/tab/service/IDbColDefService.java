package top.tobehero.usermanage.db_manage.tab.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.tobehero.usermanage.core.service.IBaseService;
import top.tobehero.usermanage.db_manage.tab.entity.DbColDef;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface IDbColDefService extends IBaseService<DbColDef> {
    Page<DbColDef> findPage(long current, long size, DbColDef data);
}
