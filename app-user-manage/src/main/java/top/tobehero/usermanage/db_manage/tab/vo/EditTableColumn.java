package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;

@Data
public class EditTableColumn {
    private Integer dbColumnId;
    private String logicName;
    private String colName;
    private Integer typeId;
    /**字段类型名称**/
    private String typeName;
    private Integer typeSize;
    private Integer typeDecimal;
    private String isNotNull = "0";
    private String isPk = "0";
    private String isAutoIncre = "0";
    private String defaultVal;
    private String isEnum;
    private String remark;
    /**未知数据库字段类型名称**/
    private String unknownTypeName;
}
