package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbColDef;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface DbColDefMapper extends BaseMapper<DbColDef> {

}
