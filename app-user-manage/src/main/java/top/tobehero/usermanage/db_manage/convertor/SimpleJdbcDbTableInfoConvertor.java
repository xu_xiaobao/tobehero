package top.tobehero.usermanage.db_manage.convertor;

import top.tobehero.usermanage.db_manage.enums.DataBaseEnum;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class SimpleJdbcDbTableInfoConvertor implements IDbTableInfoConvertor {
    @Override
    public DbTableInfo parseTableInfo(Connection connection, String catalog, String schema, String tableName) throws Exception{
        if(schema==null){
            schema = connection.getSchema();
        }
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet tablesRs = metaData.getTables(catalog, schema, tableName, new String[]{"TABLE"});
//        List<String> tables = getQueryTableName(connection,dataBaseEnum,schema);
        DbTableInfo table = null;
        if(tablesRs.next()){
            table = new DbTableInfo();
            String remarks = tablesRs.getString("remarks");
            table.setTableName(tableName);
            table.setRemarks(remarks);
            table.setDataBaseEnum(DataBaseEnum.POSTGRESQL);
        }
        if(table!=null){
            parseTableInfo(table,connection,tableName);
        }
        return table;
    }

    @Override
    public List<String> getTables(Connection connection, String catalog, String schema) throws SQLException {
        DatabaseMetaData metaData = connection.getMetaData();
        if(schema==null){
            schema=connection.getSchema();
        }
        ResultSet tablesRs = metaData.getTables(catalog, schema, null, new String[]{"TABLE"});
        List<String> tables = new ArrayList<>();
        while(tablesRs.next()){
            String tableName = tablesRs.getString("table_name");
//            String remarks = tablesRs.getString("remarks");
            tables.add(tableName);
        }
        return tables;
    }

    private void parseTableInfo(DbTableInfo tableInfo, Connection connection, String tableName) throws Exception{
        String tableQuerydSql = getTableQuerydSql(tableName);
        PreparedStatement ps = connection.prepareStatement(tableQuerydSql);
        ps.execute();
        ResultSet resultSet = ps.getResultSet();
        ResultSetMetaData metaData = resultSet.getMetaData();
        //查询结果列长度
        int columnCount = metaData.getColumnCount();
        tableInfo.setTableName(tableName);
        tableInfo.setColumns(new ArrayList<>());
        for(int i=1;i<=columnCount;i++){
            //是否自增
//            boolean isAutoIncrement(int column) throws SQLException;
            boolean autoIncrement = metaData.isAutoIncrement(i);
            //是否大小写敏感
//            boolean isCaseSensitive(int column) throws SQLException;
            boolean caseSensitive = metaData.isCaseSensitive(i);
            //字段是否允许被用于wehere条件后
//            boolean isSearchable(int column) throws SQLException;
            boolean searchable = metaData.isSearchable(i);
            //是否为货币类型
//            boolean isCurrency(int column) throws SQLException;
            boolean currency = metaData.isCurrency(i);
            //是否可以为空
//            int isNullable(int column) throws SQLException;
            int nullable = metaData.isNullable(i);

            //是否为带符号数字
//            boolean isSigned(int column) throws SQLException;
            boolean signed = metaData.isSigned(i);
            //指示指定列的正常最大宽度（以字符为单位）。
//            int getColumnDisplaySize(int column) throws SQLException;
            int columnDisplaySize = metaData.getColumnDisplaySize(i);
            //查询语句中as后的别名
//            String getColumnLabel(int column) throws SQLException;
            String columnLabel = metaData.getColumnLabel(i);
            //字段名
//            String getColumnName(int column) throws SQLException;
            String columnName = metaData.getColumnName(i);
            //table的schema
//            String getSchemaName(int column) throws SQLException;
            String schemaName = metaData.getSchemaName(i);
            /*
             * 对于数值数据，为最大精度
             * 对于字符串数据，为字符串长度
             * 对于datetime数据类型，这是字符串表示的字符长度（假设分数秒组件允许的最大精度）
             * 对于二进制数据，为字节长度
             * 对于ROWID数据类型，为字节长度
             * 对于数据类型不支持则返回0
             */
//            int getPrecision(int column) throws SQLException;
            int precision = metaData.getPrecision(i);

            /*
             * 小数点长度
             */
//            int getScale(int column) throws SQLException;
            int scale = metaData.getScale(i);
            //得到表名
//            String getTableName(int column) throws SQLException;
//            String tableName = metaData.getTableName(i);
            //Catalog name
//            String getCatalogName(int column) throws SQLException;
            String catalogName = metaData.getCatalogName(i);
            //获取字段SQL类型
//            int getColumnType(int column) throws SQLException;
            int columnType = metaData.getColumnType(i);
            //获取字段SQL数据库指定类型
//            String getColumnTypeName(int column) throws SQLException;
            String columnTypeName = metaData.getColumnTypeName(i);
            //只读
//            boolean isReadOnly(int column) throws SQLException;
            boolean readOnly = metaData.isReadOnly(i);
            //可写
//            boolean isWritable(int column) throws SQLException;
            boolean writable = metaData.isWritable(i);
            //指示对指定列的写入是否一定成功
//            boolean isDefinitelyWritable(int column) throws SQLException;
            boolean definitelyWritable = metaData.isDefinitelyWritable(i);
            String remarks = null;
            DatabaseMetaData conMetaData = connection.getMetaData();
            ResultSet columnsRs = conMetaData.getColumns(null, connection.getSchema(), tableName, columnName);
            if(columnsRs.next()){
                remarks = columnsRs.getString("remarks");
            }
            //--------------------------JDBC 2.0-----------------------------------
            //获取java类型的全类名
//            String getColumnClassName(int column) throws SQLException;
            String columnClassName = metaData.getColumnClassName(i);
//            System.out.println("=======================================================");
            DbColInfo dbcolInfo = new DbColInfo();
//            System.out.println("autoIncrement"+"\t\t"+autoIncrement);
            dbcolInfo.setAutoIncrement(autoIncrement);
//            System.out.println("caseSensitive"+"\t\t"+caseSensitive);
            dbcolInfo.setCaseSensitive(caseSensitive);
//            System.out.println("searchable"+"\t\t"+searchable);
            dbcolInfo.setSearchable(searchable);
//            System.out.println("currency"+"\t\t"+currency);
            dbcolInfo.setCurrency(currency);
//            System.out.println("nullable"+"\t\t"+nullable);
            dbcolInfo.setNullable(nullable);
//            System.out.println("signed"+"\t\t"+signed);
            dbcolInfo.setSigned(signed);
//            System.out.println("columnDisplaySize"+"\t\t"+columnDisplaySize);
            dbcolInfo.setColumnDisplaySize(columnDisplaySize);
//            System.out.println("columnLabel"+"\t\t"+columnLabel);
            dbcolInfo.setColumnLabel(columnLabel);
//            System.out.println("columnName"+"\t\t"+columnName);
            dbcolInfo.setColumnName(columnName);
//            System.out.println("schemaName"+"\t\t"+schemaName);
            dbcolInfo.setSchemaName(schemaName);
//            System.out.println("precision"+"\t\t"+precision);
            dbcolInfo.setPrecision(precision);
//            System.out.println("scale"+"\t\t"+scale);
            dbcolInfo.setScale(scale);
//            System.out.println("tableName"+"\t\t"+tableName);
            dbcolInfo.setTableName(tableName);
//            System.out.println("catalogName"+"\t\t"+catalogName);
            dbcolInfo.setCatalogName(catalogName);
//            System.out.println("columnType"+"\t\t"+columnType);
            dbcolInfo.setColumnType(columnType);
//            System.out.println("columnTypeName"+"\t\t"+columnTypeName);
            dbcolInfo.setColumnTypeName(columnTypeName);
//            System.out.println("readOnly"+"\t\t"+readOnly);
            dbcolInfo.setReadOnly(readOnly);
//            System.out.println("writable"+"\t\t"+writable);
            dbcolInfo.setWritable(writable);
//            System.out.println("definitelyWritable"+"\t\t"+definitelyWritable);
            dbcolInfo.setDefinitelyWritable(definitelyWritable);
//            System.out.println("columnClassName"+"\t\t"+columnClassName);
            dbcolInfo.setColumnClassName(columnClassName);
            dbcolInfo.setRemarks(remarks);
//            System.out.println("=======================================================");
            tableInfo.getColumns().add(dbcolInfo);
        }
        DbColInfo dbColInfo = tableInfo.getColumns().get(0);
        String catalogName = dbColInfo.getCatalogName();
        String schemaName = dbColInfo.getSchemaName();
        DatabaseMetaData metaData1 = connection.getMetaData();
        ResultSet primaryKeys = metaData1.getPrimaryKeys(catalogName, schemaName, tableName);
        boolean hasPk = false;
        List<String> pkList = new ArrayList<>();
        String pkName = null;
        while(primaryKeys.next()){
            hasPk = true;
            ResultSetMetaData metaData2 = primaryKeys.getMetaData();
            int columnCount1 = metaData2.getColumnCount();
            String column_name = primaryKeys.getString("column_name");
            pkList.add(column_name);
            pkName = primaryKeys.getString("pk_name");
        }
        tableInfo.setHasPk(hasPk);
        if(hasPk){
            tableInfo.setPkList(pkList);
            tableInfo.setPkName(pkName);
        }
    }

    protected abstract String getTableQuerydSql(String tableName);
}
