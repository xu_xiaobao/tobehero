package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;
import top.tobehero.usermanage.db_manage.tab.entity.DbEnumDic;

import java.util.List;

@Data
public class SummaryTabColDef {
    private Integer colId;
    private String name;
    private String logicName;
    private String typeName;
    private Integer typeSize;
    private Integer typeDecimal;
    /**
     * 展示顺序
     */
    private Integer displayNo;
    /**
     * 是否主键
     */
    private String isPk;

    /**
     * 是否非空
     */
    private String isNotNull;

    /**
     * 是否自增
     */
    private String isAutoIncre;

    /**
     * 默认值
     */
    private String defaultVal;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否有枚举值
     */
    private String isEnum;

    /**
     * 枚举值列表
     */
    private List<DbEnumDic> enums;
}
