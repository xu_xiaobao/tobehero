package top.tobehero.usermanage.db_manage.convertor;

import top.tobehero.usermanage.db_manage.enums.DataBaseEnum;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IDbTableInfoConvertor {
    /**
     * 解析表信息
     * @param connection
     * @param catalog
     * @param schema
     * @param tableName
     * @return
     */
    DbTableInfo parseTableInfo(Connection connection, String catalog, String schema, String tableName) throws Exception;

    /**
     *  获取数据库库集合
     * @param connection
     * @return
     * @throws SQLException
     */
    List<String> getTables(Connection connection, String catalog, String schema) throws SQLException;

    /**
     * 是否匹配此转换器
     * @return
     */
    default boolean isMatch(DataBaseEnum dataBaseEnum){
        return false;
    };
}
