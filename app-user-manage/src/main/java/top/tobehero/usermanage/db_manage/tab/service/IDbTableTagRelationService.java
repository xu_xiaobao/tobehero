package top.tobehero.usermanage.db_manage.tab.service;

import top.tobehero.usermanage.core.service.IBaseService;
import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagRelation;

/**
 * <p>
 * 表标签 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-21
 */
public interface IDbTableTagRelationService extends IBaseService<DbTableTagRelation> {

}
