package top.tobehero.usermanage.db_manage.tab.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.db_manage.tab.entity.DbDatasource;

/**
 * <p>
 * 数据源配置 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-23
 */
public interface IDbDatasourceService extends IService<DbDatasource> {

    BaseRes parseDbTable(String dsName, String tableName);

    BaseRes loadDsNames(String sysCode);
}
