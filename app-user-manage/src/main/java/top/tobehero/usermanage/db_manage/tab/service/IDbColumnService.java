package top.tobehero.usermanage.db_manage.tab.service;

import top.tobehero.usermanage.core.service.IBaseService;
import top.tobehero.usermanage.db_manage.tab.entity.DbColumn;

/**
 * <p>
 * 字段表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface IDbColumnService extends IBaseService<DbColumn> {

}
