package top.tobehero.usermanage.db_manage.tab.service;

import top.tobehero.usermanage.core.service.IBaseService;
import top.tobehero.usermanage.db_manage.tab.entity.DbEnumDic;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface IDbEnumDicService extends IBaseService<DbEnumDic> {

}
