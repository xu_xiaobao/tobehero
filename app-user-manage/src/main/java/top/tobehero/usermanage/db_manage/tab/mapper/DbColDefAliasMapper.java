package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbColDefAlias;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 数据库字段别名表 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-29
 */
public interface DbColDefAliasMapper extends BaseMapper<DbColDefAlias> {

}
