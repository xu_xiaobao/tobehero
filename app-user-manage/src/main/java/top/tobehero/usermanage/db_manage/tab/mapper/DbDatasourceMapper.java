package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbDatasource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 数据源配置 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-23
 */
public interface DbDatasourceMapper extends BaseMapper<DbDatasource> {

}
