package top.tobehero.usermanage.db_manage.tab.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 数据源配置
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbDatasource implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 系统代码
     */
    private String sysCode;
    /**
     * 数据源名称
     */
    private String name;

    /**
     * jdbcUrl
     */
    private String jdbcUrl;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 数据有效标识
     */
    private String flag;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
