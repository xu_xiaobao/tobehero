package top.tobehero.usermanage.db_manage.tab.service.impl;

import org.springframework.stereotype.Service;
import top.tobehero.usermanage.core.service.impl.BaseServiceImpl;
import top.tobehero.usermanage.db_manage.tab.entity.DbColumn;
import top.tobehero.usermanage.db_manage.tab.mapper.DbColumnMapper;
import top.tobehero.usermanage.db_manage.tab.service.IDbColumnService;

/**
 * <p>
 * 字段表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Service
public class DbColumnServiceImpl extends BaseServiceImpl<DbColumnMapper, DbColumn> implements IDbColumnService {

}
