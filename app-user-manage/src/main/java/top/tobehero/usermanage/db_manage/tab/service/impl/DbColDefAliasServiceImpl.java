package top.tobehero.usermanage.db_manage.tab.service.impl;

import top.tobehero.usermanage.db_manage.tab.entity.DbColDefAlias;
import top.tobehero.usermanage.db_manage.tab.mapper.DbColDefAliasMapper;
import top.tobehero.usermanage.db_manage.tab.service.IDbColDefAliasService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据库字段别名表 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-29
 */
@Service
public class DbColDefAliasServiceImpl extends ServiceImpl<DbColDefAliasMapper, DbColDefAlias> implements IDbColDefAliasService {

}
