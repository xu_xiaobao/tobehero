package top.tobehero.usermanage.db_manage.tab.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.db_manage.convertor.DbColInfo;
import top.tobehero.usermanage.db_manage.convertor.DbTableInfo;
import top.tobehero.usermanage.db_manage.convertor.DbTableInfoConvertorDelegate;
import top.tobehero.usermanage.db_manage.enums.DataBaseEnum;
import top.tobehero.usermanage.db_manage.tab.entity.*;
import top.tobehero.usermanage.db_manage.tab.mapper.DbDatasourceMapper;
import top.tobehero.usermanage.db_manage.tab.mapper.DbTableTagDefMapper;
import top.tobehero.usermanage.db_manage.tab.mapper.DbTableTagRelationMapper;
import top.tobehero.usermanage.db_manage.tab.service.*;
import top.tobehero.usermanage.db_manage.tab.vo.EditTable;
import top.tobehero.usermanage.db_manage.tab.vo.EditTableColumn;

import javax.annotation.Resource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 数据源配置 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-23
 */
@Service
public class DbDatasourceServiceImpl extends ServiceImpl<DbDatasourceMapper, DbDatasource> implements IDbDatasourceService {
    @Autowired
    private DbTableInfoConvertorDelegate delegate;
    @Resource
    private IDbColDefAliasService iDbColDefAliasService;
    @Resource
    private IDbColDefService iDbColDefService;
    @Resource
    private IDbTableService iDbTableService;
    @Resource
    private IDbColumnService iDbColumnService;

    @Resource
    private DbTableTagRelationMapper dbTableTagRelationMapper;
    @Resource
    private DbTableTagDefMapper dbTableTagDefMapper;

    @Override
    public BaseRes parseDbTable(String dsName, String tableName) {
        Connection connection = null;
        List<TableInfo> tableInfoList = null;
        try{
            DbDatasource dbDatasource = this.lambdaQuery().eq(DbDatasource::getName, dsName).eq(DbDatasource::getFlag, "1").one();
            if(dbDatasource==null){
                return ResponseUtils.outErrorResp("-1","数据源不存在");
            }
            String jdbcUrl = dbDatasource.getJdbcUrl();
            String username = dbDatasource.getUsername();
            String password = dbDatasource.getPassword();
            connection = getJdbcConnection(username, password, jdbcUrl);
            DbTableInfo dbTableInfo = delegate.parseTableInfo(connection, null, null, tableName);
            if(dbTableInfo==null){
                return ResponseUtils.outErrorResp("-2","未查到表信息");
            }
            dbTableInfo.setDbDatasource(dbDatasource);
            EditTable editTable = this.toEditTable(dbTableInfo);
            return ResponseUtils.outSuccessResp(editTable);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            try {
                if(connection!=null){
                    boolean closed = connection.isClosed();
                    if(!closed){
                        connection.close();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ResponseUtils.outErrorResp("-2","未找到表信息");
    }

    private EditTable toEditTable(DbTableInfo dbTableInfo) {
        EditTable editTable = new EditTable();
        String tableName = dbTableInfo.getTableName();
        String remarks = dbTableInfo.getRemarks();
        DataBaseEnum dataBaseEnum = dbTableInfo.getDataBaseEnum();
        editTable.setTableName(remarks);
        String dbType = dataBaseEnum.getCode();
        editTable.setDbType(dbType);
        editTable.setLogicName(tableName);
        List<DbColInfo> columns = dbTableInfo.getColumns();
        List<String> pkList = dbTableInfo.getPkList();
        List<EditTableColumn> editColumns = new ArrayList<>();
        editTable.setColumns(editColumns);
        DbDatasource dbDatasource = dbTableInfo.getDbDatasource();
        DbTable dbTable = iDbTableService.lambdaQuery().eq(DbTable::getLogicName, tableName).eq(DbTable::getSysCode, dbDatasource.getSysCode()).eq(DbTable::getFlag, "1").one();
        Integer dbTableId = null;
        if(dbTable!=null){
            dbTableId = dbTable.getId();
            editTable.setTabId(dbTableId);
            String oldTableRemark = dbTable.getRemark();
            if(StringUtils.isNotBlank(oldTableRemark)){
                editTable.setRemark(oldTableRemark);
            }
            String oldTableName = dbTable.getName();
            editTable.setTableName(oldTableName);
            List<DbTableTagRelation> dbTableTagRelations = dbTableTagRelationMapper.selectList(new LambdaQueryWrapper<DbTableTagRelation>().select(DbTableTagRelation::getTagId).eq(DbTableTagRelation::getTabId, dbTableId));
            ArrayList<Integer> tagIds = new ArrayList<>();
            if(dbTableTagRelations!=null&&dbTableTagRelations.size()>0){
                dbTableTagRelations.stream().forEach((i)->{
                    tagIds.add(i.getTagId());
                });
                List<DbTableTagDef> dbTableTagDefs = dbTableTagDefMapper.selectList(new LambdaQueryWrapper<DbTableTagDef>().in(DbTableTagDef::getId,tagIds));
                if(dbTableTagDefs!=null){
                    List<String> tags = new ArrayList<>();
                    for (DbTableTagDef dbTableTagDef:dbTableTagDefs) {
                        String tagName = dbTableTagDef.getTagName();
                        tags.add(tagName);
                    }
                    editTable.setTags(tags);
                }
            }
        }
        if(columns!=null){
            for(DbColInfo dbColInfo:columns){
                EditTableColumn editTableColumn = new EditTableColumn();
                String columnName = dbColInfo.getColumnName();
                String colRemarks = dbColInfo.getRemarks();
                //长度
                int precision = dbColInfo.getPrecision();
                //精度
                int scale = dbColInfo.getScale();
                String columnTypeName = dbColInfo.getColumnTypeName();
                editTableColumn.setColName(colRemarks);
                editTableColumn.setLogicName(columnName);
                //实际数据库类型转换为系统类型名称
                DbColDef dbColDef = this.getDbColDefByRealColTypeName(columnTypeName,dbType);
                if(dbColDef!=null){
                    editTableColumn.setTypeId(dbColDef.getId());
                    String needSize = dbColDef.getNeedSize();
                    if("1".equals(needSize)){
                        editTableColumn.setTypeSize(precision);
                        //针对INFORMIX当precision==0时，长度使用displaySize
                        if(dbTableInfo.getDataBaseEnum()==DataBaseEnum.INFORMIX){
                            if(precision==0){
                                editTableColumn.setTypeSize(dbColInfo.getColumnDisplaySize());
                            }
                        }
                    }
                    String needDecimal = dbColDef.getNeedDecimal();
                    if("1".equals(needDecimal)){
                        editTableColumn.setTypeDecimal(scale);
                    }
                    editTableColumn.setTypeName(dbColDef.getName());
                }else{
                    editTableColumn.setUnknownTypeName(columnTypeName);
                }
                //判断是否自增
                boolean autoIncrement = dbColInfo.isAutoIncrement();
                if(autoIncrement){
                    editTableColumn.setIsAutoIncre("1");
                }
                //判断是否可以为空
                int nullable = dbColInfo.getNullable();
                if(nullable==0){
                    editTableColumn.setIsNotNull("1");
                }
                //是否为主键相关字段
                if(pkList!=null){
                    for(String pkName:pkList){
                        if(pkName.equalsIgnoreCase(columnName)){
                            editTableColumn.setIsPk("1");
                            editTableColumn.setIsNotNull("1");
                            break;
                        }
                    }
                }
                //查询是否已有维护表ID与字段ID
                if(dbTableId!=null){
                    DbColumn dbColumn = iDbColumnService.lambdaQuery().eq(DbColumn::getLogicName, columnName).eq(DbColumn::getTabId, dbTableId).one();
                    if(dbColumn!=null){
                        Integer dbColumnId = dbColumn.getId();
                        editTableColumn.setDbColumnId(dbColumnId);
                        editTableColumn.setIsEnum(dbColumn.getIsEnum());
                        editTableColumn.setDefaultVal(dbColumn.getDefaultVal());
                        String oldColName = dbColumn.getName();
                        //如果字段名称名如已维护过则使用自定内容，否则使用建表语句字段描述
                        if(StringUtils.isNotBlank(oldColName)){
                            editTableColumn.setColName(oldColName);
                        }
                        editTableColumn.setRemark(dbColumn.getRemark());
                    }
                }
                editColumns.add(editTableColumn);
            }
        }
        editTable.setDbType(dataBaseEnum.getCode());
        editTable.setSysCode(dbDatasource.getSysCode());
        return editTable;
    }

    private DbColDef getDbColDefByRealColTypeName(String columnTypeName,String dbType) {
        String columnTypeNameUpper = columnTypeName.toUpperCase();
        DbColDef dbColDef = iDbColDefService.lambdaQuery().eq(DbColDef::getDbType, dbType).eq(DbColDef::getName, columnTypeNameUpper).one();
        if(dbColDef!=null){
            return dbColDef;
        }
        DbColDefAlias dbColDefAlias = iDbColDefAliasService.lambdaQuery().eq(DbColDefAlias::getColTypeName, columnTypeNameUpper).eq(DbColDefAlias::getDbType, dbType).one();
        if(dbColDefAlias!=null) {
            String aliasName = dbColDefAlias.getAliasName();
            return iDbColDefService.lambdaQuery().eq(DbColDef::getDbType, dbType).eq(DbColDef::getName, aliasName).one();
        }
        return null;
    }

    @Resource
    private IDbDatasourceService iDbDatasourceService;
    @Override
    public BaseRes loadDsNames(String sysCode) {
        List<DbDatasource> list = iDbDatasourceService.lambdaQuery().eq(DbDatasource::getFlag, "1").eq(DbDatasource::getSysCode,sysCode).list();
        ArrayList<JSONObject> data = new ArrayList<>();
        list.forEach((item)->{
            Integer id = item.getId();
            String name = item.getName();
            JSONObject json = new JSONObject();
            json.put("label",name);
            json.put("value",name);
            data.add(json);
        });
        return ResponseUtils.outSuccessResp(data);
    }


    public static void main(String[] args) throws Exception {
        String jdbcUrl = "jdbc:postgresql://192.168.100.101:5432/testdb";
        String username = "postgres";
        String password = "postgres";
        parseTable(username,password,jdbcUrl);

    }
    /**
     * 解析指定表结构
     * @param username
     * @param password
     * @param jdbcUrl
     * @param tablesName
     * @return
     */
    private static void parseTable(String username,String password,String jdbcUrl,String... tablesName)throws Exception{
        Connection connection = getJdbcConnection(username,password,jdbcUrl);
//        PostgreSqlDbTableInfoConvertor convertor = new PostgreSqlDbTableInfoConvertor();
//        List<String> tables = convertor.getTables(connection, null, null);
//        for(String tableName:tables){
//            DbTableInfo tableInfo = convertor.parseTableInfo(connection, null, null, tableName);
//            System.out.println(JSON.toJSONString(tableInfo));
//        }
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet typeInfoRs = metaData.getTypeInfo();
        ResultSetMetaData typeInfoRsMd = typeInfoRs.getMetaData();
        int columnCount = typeInfoRsMd.getColumnCount();
        List<String> columnNames = new ArrayList<>();
        for(int i=1;i<=columnCount;i++){
            columnNames.add(typeInfoRsMd.getColumnName(i));
        }
        List<Object> rows = new ArrayList<>();
        while(typeInfoRs.next()){
            HashMap<String,Object> row = new HashMap<String,Object>();
            for(String colName:columnNames){
                row.put(colName,typeInfoRs.getObject(colName));
            }
            rows.add(row);
        }
        System.out.println(JSON.toJSONString(rows));
    }

    private static Connection getJdbcConnection(String username, String password, String jdbcUrl) throws Exception{
        DataBaseEnum dataBaseEnum = getDataBaseEnumByJdbcUrl(jdbcUrl);
        Assert.notNull(dataBaseEnum,"为获取到数据库类型");
        String driverClass = dataBaseEnum.getDriverClass();
        Class.forName(driverClass);
        return DriverManager.getConnection(jdbcUrl, username, password);
    }


    private static DataBaseEnum getDataBaseEnumByJdbcUrl(String jdbcUrl){
        if(jdbcUrl.startsWith("jdbc:postgresql://")){
            return DataBaseEnum.POSTGRESQL;
        }else if(jdbcUrl.startsWith("jdbc:mysql://")){
            return DataBaseEnum.MYSQL;
        }else if(jdbcUrl.startsWith("jdbc:informix")){
            return DataBaseEnum.INFORMIX;
        }else{
            return null;
        }
    }
}
