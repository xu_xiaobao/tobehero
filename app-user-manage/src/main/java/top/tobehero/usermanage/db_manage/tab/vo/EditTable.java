package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;

import java.util.List;
@Data
public class EditTable {
    private Integer tabId;
    private String logicName;
    private String tableName;
    private String dbType;
    private String sysCode;
    private String remark;

    private List<String> tags;

    public String getLogicName() {
        if(logicName==null){
            return null;
        }
        return logicName.toLowerCase();
    }

    public void setLogicName(String logicName) {
        if(logicName!=null){
            logicName = logicName.toLowerCase();
        }
        this.logicName = logicName;
    }

    private List<EditTableColumn> columns;


}
