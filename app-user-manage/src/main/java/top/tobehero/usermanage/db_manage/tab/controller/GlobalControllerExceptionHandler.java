package top.tobehero.usermanage.db_manage.tab.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.exception.BusinessException;
import top.tobehero.framework.common.utils.ResponseUtils;


@ControllerAdvice
public class GlobalControllerExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BaseRes customException(Exception e) {
        e.printStackTrace();
        return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public BaseRes businessException(BusinessException e) {
        e.printStackTrace();
        String errcode = e.getErrcode();
        String errmsg = e.getErrmsg();
        Object data = e.getData();
        return ResponseUtils.outErrorResp(errcode,errmsg,data);
    }
}
