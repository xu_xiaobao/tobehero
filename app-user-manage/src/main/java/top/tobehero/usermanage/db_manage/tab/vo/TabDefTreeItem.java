package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;
import top.tobehero.usermanage.db_manage.tab.entity.DbTable;

@Data
public class TabDefTreeItem extends BaseTreeItem<DbTable,Void>{
    public TabDefTreeItem(){
        super("tableDef");
    }
}
