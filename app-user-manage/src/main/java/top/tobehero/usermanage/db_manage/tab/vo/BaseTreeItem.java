package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;

import java.util.List;

/**
 *
 * @param <D>
 *     原始数据类型
 * @param <C>
 *     子节点数据类型
 */
@Data
public class BaseTreeItem<D,C> {
    private String type;
    private String id;
    private String label;
    private D data;
    private List<C> children;

    public BaseTreeItem(String type){
        this.type = type;
    }
}
