package top.tobehero.usermanage.db_manage.tab.service;

import top.tobehero.usermanage.db_manage.tab.entity.DbColDefAlias;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 数据库字段别名表 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-29
 */
public interface IDbColDefAliasService extends IService<DbColDefAlias> {

}
