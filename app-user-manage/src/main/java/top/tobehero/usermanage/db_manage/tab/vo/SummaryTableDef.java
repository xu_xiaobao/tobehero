package top.tobehero.usermanage.db_manage.tab.vo;

import lombok.Data;

import java.util.List;

@Data
public class SummaryTableDef {
    private Integer tableId;
    private String tableName;
    /**
     * 逻辑名称
     */
    private String logicName;
    private List<SummaryTabColDef> columns;
}
