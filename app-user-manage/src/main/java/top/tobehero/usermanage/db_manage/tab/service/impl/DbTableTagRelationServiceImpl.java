package top.tobehero.usermanage.db_manage.tab.service.impl;

import org.springframework.stereotype.Service;
import top.tobehero.usermanage.core.service.impl.BaseServiceImpl;
import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagRelation;
import top.tobehero.usermanage.db_manage.tab.mapper.DbTableTagRelationMapper;
import top.tobehero.usermanage.db_manage.tab.service.IDbTableTagRelationService;

/**
 * <p>
 * 表标签 服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-21
 */
@Service
public class DbTableTagRelationServiceImpl extends BaseServiceImpl<DbTableTagRelationMapper, DbTableTagRelation> implements IDbTableTagRelationService {

}
