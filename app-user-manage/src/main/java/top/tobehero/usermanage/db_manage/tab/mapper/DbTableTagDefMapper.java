package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagDef;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-21
 */
public interface DbTableTagDefMapper extends BaseMapper<DbTableTagDef> {

}
