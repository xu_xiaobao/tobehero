package top.tobehero.usermanage.db_manage.tab.service.impl;

import org.springframework.stereotype.Service;
import top.tobehero.usermanage.core.service.impl.BaseServiceImpl;
import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagDef;
import top.tobehero.usermanage.db_manage.tab.mapper.DbTableTagDefMapper;
import top.tobehero.usermanage.db_manage.tab.service.IDbTableTagDefService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-21
 */
@Service
public class DbTableTagDefServiceImpl extends BaseServiceImpl<DbTableTagDefMapper, DbTableTagDef> implements IDbTableTagDefService {

}
