package top.tobehero.usermanage.db_manage.convertor;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import top.tobehero.usermanage.db_manage.enums.DataBaseEnum;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class DbTableInfoConvertorDelegate implements IDbTableInfoConvertor{
    private List<IDbTableInfoConvertor> convertors;
    public DbTableInfoConvertorDelegate(){
        this.convertors = new ArrayList<>();
        this.convertors.add(new PostgreSqlDbTableInfoConvertor());
        this.convertors.add(new InformixDbTableInfoConvertor());
        this.convertors.add(new MySqlDbTableInfoConvertor());
    }
    /**
     * 解析表信息
     * @param connection
     * @param catalog
     * @param schema
     * @param tableName
     * @return
     */
    @Override
    public DbTableInfo parseTableInfo(Connection connection, String catalog, String schema, String tableName) throws Exception{
        IDbTableInfoConvertor convertor = getConvertor(connection);
        return convertor.parseTableInfo(connection,catalog,schema,tableName);
    }

    /**
     *  获取数据库库集合
     * @param connection
     * @return
     * @throws SQLException
     */
    @Override
    public List<String> getTables(Connection connection, String catalog, String schema) throws SQLException{
        IDbTableInfoConvertor convertor = getConvertor(connection);
        return convertor.getTables(connection,catalog,schema);
    }

    private IDbTableInfoConvertor getConvertor(Connection connection) throws SQLException {
        DataBaseEnum dataBaseEnum = getDataBaseEnumByConnection(connection);
        Assert.notNull(dataBaseEnum,"数据库类型未匹配");
        for(IDbTableInfoConvertor convertor:convertors){
            if(convertor.isMatch(dataBaseEnum)){
                return convertor;
            }
        }
        Assert.notNull(null,"转换器未找到");
        return null;
    }
    private DataBaseEnum getDataBaseEnumByConnection(Connection connection) throws SQLException {
        String databaseProductName = connection.getMetaData().getDatabaseProductName().toUpperCase();
       if(databaseProductName.contains("POSTGRESQL")){
           return DataBaseEnum.POSTGRESQL;
       }else if(databaseProductName.contains("INFORMIX")){
           return DataBaseEnum.INFORMIX;
       }else if(databaseProductName.contains("MYSQL")){
           return DataBaseEnum.MYSQL;
       }
       return null;
    }
}
