package top.tobehero.usermanage.db_manage.tab.mapper;

import top.tobehero.usermanage.db_manage.tab.entity.DbTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表名 Mapper 接口
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface DbTableMapper extends BaseMapper<DbTable> {

}
