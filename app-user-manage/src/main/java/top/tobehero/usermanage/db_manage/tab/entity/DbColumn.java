package top.tobehero.usermanage.db_manage.tab.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 字段表
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type= IdType.AUTO)
    private Integer id;
    private Integer tabId;
    /**
     * 字段名称
     */
    private String name;

    /**
     * 字段逻辑名称
     */
    private String logicName;

    /**
     * 字段类型ID
     */
    private Integer typeId;
    /**
     * 字段长度
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer typeSize;
    /**
     * 小数点长度
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer typeDecimal;

    /**
     * 是否主键
     */
    private String isPk;

    /**
     * 是否非空
     */
    private String isNotNull;

    /**
     * 是否自增
     */
    private String isAutoIncre;

    /**
     * 默认值
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String defaultVal;

    /**
     * 备注
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String remark;

    /**
     * 有效标识
     */
    private String flag;

    /**
     * 展示顺序
     */
    private Integer displayNo;

    /**
     * 是否有枚举值
     */
    private String isEnum;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private LocalDateTime updatedTime;


}
