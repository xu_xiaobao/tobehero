package top.tobehero.usermanage.db_manage.convertor;

import lombok.Data;
import top.tobehero.usermanage.db_manage.enums.DataBaseEnum;
import top.tobehero.usermanage.db_manage.tab.entity.DbDatasource;

import java.util.List;

@Data
public class DbTableInfo {
    private DataBaseEnum dataBaseEnum;
    private DbDatasource dbDatasource;
    private String tableName;
    /**表注释**/
    private String remarks;
    List<DbColInfo> columns;
    /**是否有主键**/
    private boolean hasPk;
    /**主键字段列表**/
    private List<String> pkList;
    /**主键名称**/
    private String pkName;
}
