package top.tobehero.usermanage.db_manage.tab.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.usermanage.core.service.IBaseService;
import top.tobehero.usermanage.db_manage.tab.entity.DbTable;
import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagDef;
import top.tobehero.usermanage.db_manage.tab.vo.EditColEnums;
import top.tobehero.usermanage.db_manage.tab.vo.EditTable;

import java.util.List;

/**
 * <p>
 * 表名 服务类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
public interface IDbTableService extends IBaseService<DbTable> {
    BaseRes save(EditTable tab);

    EditTable findEditTableById(Integer id);

    Page<DbTable> findPage(long current, long size, DbTable data);

    BaseRes update(EditTable tab);

    List<DbTableTagDef> findTags(String sysCode, String tagName);

    EditColEnums qryEnumsByColId(Integer colId);

    void saveColEnums(EditColEnums colEnums);

    /**
     * 获取数据表名称树形结构
     * @return
     */
    BaseRes getTableDefTree();

    /**
     * 获取表信息概要信息
     * @param tableId
     * @return
     */
    BaseRes getTableSummaryInfo(Integer tableId);
}
