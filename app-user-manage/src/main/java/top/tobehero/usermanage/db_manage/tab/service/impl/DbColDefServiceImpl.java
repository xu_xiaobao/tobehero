package top.tobehero.usermanage.db_manage.tab.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import top.tobehero.usermanage.core.service.impl.BaseServiceImpl;
import top.tobehero.usermanage.db_manage.tab.entity.DbColDef;
import top.tobehero.usermanage.db_manage.tab.mapper.DbColDefMapper;
import top.tobehero.usermanage.db_manage.tab.service.IDbColDefService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xuxiaobao
 * @since 2020-11-08
 */
@Service
public class DbColDefServiceImpl extends BaseServiceImpl<DbColDefMapper, DbColDef> implements IDbColDefService {

    @Override
    public Page<DbColDef> findPage(long current, long size, DbColDef data) {
        String name = data.getName();
        String dbType = data.getDbType();
        LambdaQueryWrapper<DbColDef> queryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(name)){
            queryWrapper.like(DbColDef::getName,"%"+name+"%");
        }
        if(StringUtils.isNotBlank(dbType)){
            queryWrapper.like(DbColDef::getDbType,dbType);
        }
        queryWrapper.orderByAsc(DbColDef::getDbType,DbColDef::getName);
        return this.page(new Page<>(current,size),queryWrapper);
    }
}
