package top.tobehero.usermanage.db_manage.tab.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.db_manage.tab.entity.DbColDef;
import top.tobehero.usermanage.db_manage.tab.service.IDbColDefService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 数据库字段定义
 */
@RestController
@RequestMapping("/dbColDef")
@Slf4j
public class DbColDefController {
    @Resource
    private IDbColDefService iDbColDefService;

    @RequestMapping("/save")
    public BaseRes save(@RequestBody DbColDef dbColDef){
        try{
            Integer count = iDbColDefService.lambdaQuery().eq(DbColDef::getDbType, dbColDef.getDbType()).eq(DbColDef::getName, dbColDef.getName()).count();
            if(count>0){
                return ResponseUtils.outErrorResp("-1","数据库字段类型已存在");
            }
            iDbColDefService.save(dbColDef);
        }catch (Exception ex){
            log.error("[添加数据库字段定义]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp(dbColDef);
    }

    @RequestMapping("/findById")
    public BaseRes findById(@RequestBody DbColDef dbColDef){
        try{
            DbColDef data = iDbColDefService.lambdaQuery().eq(DbColDef::getId, dbColDef.getId()).one();
            if(data==null){
                return ResponseUtils.outErrorResp("目标数据不存在");
            }
            return ResponseUtils.outSuccessResp(data);
        }catch (Exception ex){
            log.error("[添加数据库字段定义]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
    }

    @RequestMapping("/update")
    public BaseRes update(@RequestBody DbColDef dbColDef){
        try{
            iDbColDefService.updateById(dbColDef);
        }catch (Exception ex){
            log.error("[更新数据库字段定义]-[发生错误]",ex);
            return ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNKNOWN_ERROR);
        }
        return ResponseUtils.outSuccessResp();
    }
    @RequestMapping("/page")
    public BaseRes<Page<DbColDef>> page(@RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "10") long size, @RequestBody DbColDef data){
        Page<DbColDef> page = iDbColDefService.findPage(current, size, data);
        return ResponseUtils.outSuccessResp(page);
    }

    @RequestMapping("/loadColTypesByDbType")
    public BaseRes<List<JSONObject>> loadColTypesByDbType(String dbType){

        List<JSONObject> list = iDbColDefService.lambdaQuery().eq(DbColDef::getDbType, dbType).orderByAsc(DbColDef::getName).list().stream().map((dbColDef -> {
            String name = dbColDef.getName();
            Integer id = dbColDef.getId();
            JSONObject item = new JSONObject();
            item.put("label", name);
            item.put("value", id);
            return item;
        })).collect(Collectors.toList());
        return ResponseUtils.outSuccessResp(list);
    }

}
