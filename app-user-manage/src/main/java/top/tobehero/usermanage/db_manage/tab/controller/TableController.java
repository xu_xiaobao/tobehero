package top.tobehero.usermanage.db_manage.tab.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.utils.ResponseUtils;
import top.tobehero.usermanage.db_manage.tab.entity.DbEnumDic;
import top.tobehero.usermanage.db_manage.tab.entity.DbTable;
import top.tobehero.usermanage.db_manage.tab.entity.DbTableTagDef;
import top.tobehero.usermanage.db_manage.tab.service.IDbDatasourceService;
import top.tobehero.usermanage.db_manage.tab.service.IDbTableService;
import top.tobehero.usermanage.db_manage.tab.vo.EditColEnums;
import top.tobehero.usermanage.db_manage.tab.vo.EditTable;

import javax.annotation.Resource;
import java.util.*;

@RestController
@RequestMapping("/tableDef")
public class TableController {
    @Resource
    private IDbTableService iDbTableService;
    @Resource
    private IDbDatasourceService iDbDatasourceService;
    @RequestMapping("/save")
    public BaseRes addTable(@RequestBody EditTable tab){
        return iDbTableService.save(tab);
    }
    @RequestMapping("/findById")
    public BaseRes findById(Integer id){
        EditTable  data = iDbTableService.findEditTableById(id);
        if(data==null){
            return ResponseUtils.outErrorResp("-1","未查到表定义");
        }
        return ResponseUtils.outSuccessResp(data);
    }

    @RequestMapping("/page")
    public BaseRes<Page<DbTable>> page(@RequestParam(defaultValue = "1") long current, @RequestParam(defaultValue = "10") long size, @RequestBody DbTable data){
        Page<DbTable> page = iDbTableService.findPage(current, size, data);
        return ResponseUtils.outSuccessResp(page);
    }

    @RequestMapping("/update")
    public BaseRes update(@RequestBody EditTable tab){
        return iDbTableService.update(tab);
    }
    @RequestMapping("/findTags")
    public BaseRes findTags(String sysCode,String tagName){
        List<DbTableTagDef> tags = iDbTableService.findTags(sysCode,tagName);
        List<Map<String,String>> options = new ArrayList<>();
        Optional.ofNullable(tags).ifPresent((list)->{
           list.stream().forEach((tag) -> {
               HashMap<String,String> item = new HashMap<>();
               String tagName1 = tag.getTagName();
               item.put("label", tagName1);
               item.put("value", tagName1);
               options.add(item);
           });
        });
        return ResponseUtils.outSuccessResp(options);
    }
    @RequestMapping("/qryEnumsByColId")
    public BaseRes qryEnumsByColId(Integer colId){
        EditColEnums editColEnums = iDbTableService.qryEnumsByColId(colId);
        return ResponseUtils.outSuccessResp(editColEnums);
    }

    @RequestMapping("/saveColEnums")
    public BaseRes saveColEnums(@RequestBody EditColEnums colEnums){
        List<DbEnumDic> enums = colEnums.getEnums();
        Integer colId = colEnums.getColId();
        if(enums!=null){
            enums.forEach((i)->{
                i.setColId(colId);
            });
            iDbTableService.saveColEnums(colEnums);
            EditColEnums editColEnums = iDbTableService.qryEnumsByColId(colId);
            return ResponseUtils.outSuccessResp(editColEnums);
        }else{
            return ResponseUtils.outErrorResp("-1","枚举列表不能为null");
        }
    }

    @RequestMapping("/parseDbTable")
    public BaseRes parseDbTable(String dsName,String tableName){
        return this.iDbDatasourceService.parseDbTable(dsName,tableName);
    }

    @RequestMapping("/loadDsNames")
    public BaseRes loadDsNames(String sysCode){
        return this.iDbDatasourceService.loadDsNames(sysCode);
    }

    @RequestMapping("/getTableDefTree")
    public BaseRes getTableDefTree(){
        return this.iDbTableService.getTableDefTree();
    }
    @RequestMapping("/getTableSummaryInfo")
    public BaseRes getTableSummaryInfo(Integer tableId){
        return this.iDbTableService.getTableSummaryInfo(tableId);
    }
}
