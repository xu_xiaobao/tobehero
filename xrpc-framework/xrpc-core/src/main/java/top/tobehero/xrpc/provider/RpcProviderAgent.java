package top.tobehero.xrpc.provider;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.XRequestParam;
import top.tobehero.xrpc.api.XResponse;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.api.meta.RpcMethodMeta;
import top.tobehero.xrpc.api.meta.RpcParamMeta;
import top.tobehero.xrpc.api.meta.RpcServiceMeta;
import top.tobehero.xrpc.exception.XRpcException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

@Data
public class RpcProviderAgent extends AbstractRpcProviderAgent{
    @Override
    public boolean isMatch(XRequest request) {
        String serviceName = request.getServiceName();
        int version = request.getVersion();
        RpcServiceMeta serviceMeta = this.getServiceMeta();
        if(serviceMeta.getVersion()==version&&serviceName.equals(serviceMeta.getServiceName())){
            return true;
        }
        return false;
    }

    @Override
    public Object doInvoke(XRequest request) throws XRpcException, InvocationTargetException, IllegalAccessException {
        String methodName = request.getMethodName();
        List<XRequestParam> reqParams = request.getReqParams();
        RpcServiceMeta serviceMeta = this.getServiceMeta();
        Object springBean = this.getSpringBean();
        List<RpcMethodMeta> methodMetaList = serviceMeta.getMethodMetaList();
        RpcMethodMeta rpcMethodMeta = null;
        for (int i = 0; i < methodMetaList.size(); i++) {
            RpcMethodMeta methodMeta = methodMetaList.get(i);
            String metaMethodName = methodMeta.getMethodName();
            List<RpcParamMeta> paramListMeta = methodMeta.getParamListMeta();
            if(methodName.equals(metaMethodName)){
                //无参
                if((reqParams==null||reqParams.size()==0)&&(paramListMeta==null||paramListMeta.size()==0)){
                    rpcMethodMeta = methodMeta;
                    break;
                }else if((reqParams!=null&&reqParams.size()>0)&&(paramListMeta!=null&&paramListMeta.size()>0)&&paramListMeta.size()==reqParams.size()){
                    boolean isMatch = true;
                    for (int j = 0; j < reqParams.size(); j++) {
                        XRequestParam requestParam = reqParams.get(j);
                        String type = requestParam.getType();
                        RpcParamMeta rpcParamMeta = paramListMeta.get(j);
                        if(!rpcParamMeta.getType().equals(type)){
                            isMatch = false;
                            break;
                        }
                    }
                    if(isMatch){
                        rpcMethodMeta = methodMeta;
                        break;
                    }
                }
            }
        }
        if(rpcMethodMeta==null){
            return XResponse.of(RpcBaseResType.NOT_FIND_SERVICE,request.getReqId(),null);
        }
        Method method = rpcMethodMeta.getMethod();
        Object[] args = null;
        if(reqParams!=null&&reqParams.size()>=0){
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            args = new Object[reqParams.size()];
            for (int i = 0; i <reqParams.size(); i++) {
                XRequestParam reqParam = reqParams.get(i);
                Type parameterType = genericParameterTypes[i];
                String val = reqParam.getVal();
                if(parameterType instanceof ParameterizedType){
                    ParameterizedType parameterType1 = (ParameterizedType) parameterType;
                    args[i] = JSON.parseObject(val, parameterType1);
                }else{
                    args[i] = JSON.parseObject(val, parameterType);
                }

            }
        }
        Object resultObj = null;
        resultObj = method.invoke(springBean,args);
        return resultObj;
    }

}
