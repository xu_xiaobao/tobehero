package top.tobehero.xrpc.provider;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.XResponse;
import top.tobehero.xrpc.api.server.AbstractXRpcServer;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.provider.interceptor.IRpcProviderAgentInterceptor;

import java.util.*;

/**
 * 本应用内XRpc服务代理人，一个应用内只有一个。
 * 负责管理本地服务与调度中心通信一个维持本应用内对外服务健康状态
 */
@Data
public class XRpcProviderProxyer implements InitializingBean, ApplicationContextAware, ApplicationListener<ApplicationEvent> {
    private ApplicationContext ctx;
    @Autowired
    private AbstractXRpcServer xRpcServer;
    /**
     * 本应用内所有服务提供方代理
     */
    private List<AbstractRpcProviderAgent> providerAgents = new ArrayList<>();
    private List<IRpcProviderAgentInterceptor> interceptors;
    @Override
    public void afterPropertiesSet() throws Exception {
        //得到所有agent
        Map<String, RpcProviderAgent> map = this.getCtx().getBeansOfType(RpcProviderAgent.class);
        if(map!=null){
            for(Map.Entry<String, RpcProviderAgent> entry: map.entrySet()){
                RpcProviderAgent value = entry.getValue();
                providerAgents.add(value);
            }
        }
        interceptors = new ArrayList<>();
        Map<String, IRpcProviderAgentInterceptor> interceptorMap = ctx.getBeansOfType(IRpcProviderAgentInterceptor.class);
        if(interceptorMap!=null&&!interceptorMap.isEmpty()){
            for (Map.Entry<String, IRpcProviderAgentInterceptor> entry:interceptorMap.entrySet()) {
                IRpcProviderAgentInterceptor value = entry.getValue();
                interceptors.add(value);
            }
        }
        Collections.sort(interceptors,new Comparator<IRpcProviderAgentInterceptor>() {
            @Override
            public int compare(IRpcProviderAgentInterceptor o1, IRpcProviderAgentInterceptor o2) {
                return o1.getOrder()-o2.getOrder();
            }
        });
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("--->"+event);
        if(event instanceof ServletWebServerInitializedEvent){
            xRpcServer.registerAppMeta();
        }
    }

    /**
     * 调用内部服务
     * @param request
     * @return
     */
    public XResponse invokeService(XRequest request) throws Exception {
        AbstractRpcProviderAgent targetAgent = null;
        for (int i = 0; i < providerAgents.size(); i++) {
            AbstractRpcProviderAgent agent = providerAgents.get(i);
            boolean match = agent.isMatch(request);
            if(match){
                targetAgent = agent;
                break;
            }
        }
        if(targetAgent==null){
            return XResponse.of(RpcBaseResType.NOT_FIND_SERVICE,request.getReqId(),null);
        }
        Object resultObj = null;
        //执行前置拦截器
        int interceptorIndex = 0;
        XResponse response = new XResponse();
        for (;interceptorIndex < interceptors.size(); interceptorIndex++) {
            IRpcProviderAgentInterceptor interceptor = interceptors.get(interceptorIndex);
            try {
                if(!interceptor.preHandle(request, response, targetAgent)){
                    triggerAfterCompletions(request, targetAgent, interceptorIndex, response);
                    break;
                }
            } catch (Exception e) {
                return response;
            }
        }
        //服务调用前置拦截器
        resultObj = targetAgent.doInvoke(request);
        //调用后置拦截
        doPostHandle(request, targetAgent, response);
        triggerAfterCompletions(request, targetAgent, interceptorIndex, response);
        return XResponse.of(RpcBaseResType.OK,request.getReqId(), JSON.toJSONString(resultObj));
    }

    private void doPostHandle(XRequest request, AbstractRpcProviderAgent targetAgent, XResponse response) throws Exception {
        for (int i = interceptors.size()-1;interceptors.size()>0&& i >=0 ; i++) {
            interceptors.get(i).postHandle(request,response,targetAgent);
        }
    }

    private void triggerAfterCompletions(XRequest request, AbstractRpcProviderAgent targetAgent, int interceptorIndex, XResponse response) throws Exception {
        if(interceptors.size()==0){
            return;
        }
        for(int i=interceptorIndex;interceptors.size()>0 && i>=0;i--){
            IRpcProviderAgentInterceptor interceptor1 = interceptors.get(i);
            interceptor1.afterCompletion(request,response,targetAgent,null);
        }
    }
}
