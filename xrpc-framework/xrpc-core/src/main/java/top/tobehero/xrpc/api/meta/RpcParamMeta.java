package top.tobehero.xrpc.api.meta;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(exclude = {"methodMeta"})
@ToString(exclude = {"methodMeta"})
public class RpcParamMeta {
    @JSONField(serialize = false,deserialize = false)
    private RpcMethodMeta methodMeta;
    private String type;
    @JSONField(serialize = false,deserialize = false)
    private Class<?> clazz;
}
