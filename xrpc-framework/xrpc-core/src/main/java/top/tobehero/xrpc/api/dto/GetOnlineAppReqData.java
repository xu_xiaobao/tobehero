package top.tobehero.xrpc.api.dto;

import lombok.Data;
import top.tobehero.xrpc.api.meta.RpcServiceMeta;

import java.util.List;

/**
 * 获取在线APP元信息
 */
@Data
public class GetOnlineAppReqData {
    private String namespace;
    private String appName;
    private List<RpcServiceMeta> serviceMetaList;
}
