package top.tobehero.xrpc.api.dto;

import lombok.Data;

import java.util.Date;

/**
 * 注册响应
 */
@Data
public class RegisterAppResData {
    private int expire;
    private Date expireAt;
}
