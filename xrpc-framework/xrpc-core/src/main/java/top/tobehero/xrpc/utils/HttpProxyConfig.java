package top.tobehero.xrpc.utils;

import lombok.Data;

@Data
public class HttpProxyConfig {
    private boolean enableProxy;
    private String hostName;
    private int port;
}
