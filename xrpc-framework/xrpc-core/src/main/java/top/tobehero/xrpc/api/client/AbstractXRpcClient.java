package top.tobehero.xrpc.api.client;

import lombok.Data;
import top.tobehero.xrpc.api.meta.AbstractXRpcMataLoader;
import top.tobehero.xrpc.api.RpcAppItem;
import top.tobehero.xrpc.api.RpcServiceMethodItem;
import top.tobehero.xrpc.api.XRpcClientApi;
import top.tobehero.xrpc.api.dto.GetOnlineAppReqData;
import top.tobehero.xrpc.api.dto.GetOnlineAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.api.meta.RpcMethodMeta;
import top.tobehero.xrpc.api.meta.RpcParamMeta;
import top.tobehero.xrpc.api.meta.RpcServiceMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

@Data
//@Slf4j
public abstract class AbstractXRpcClient implements XRpcClientApi {
    private int heartbeat = 5000;
    private AbstractXRpcMataLoader xRpcMataLoader;
    private List<RpcServiceMethodItem> rpcServiceMethodList;
    private Timer heartTimer;
    public AbstractXRpcClient(AbstractXRpcMataLoader xRpcMataLoader){
        this.xRpcMataLoader = xRpcMataLoader;
    }
    @Override
    public void getOnlineAppMeta() {
        List<GetOnlineAppReqData> appMetaList = xRpcMataLoader.getConsumerAppMeta();
        if(appMetaList!=null){
            RpcBaseRes<List<GetOnlineAppResData>> rpcBaseRes = null;
            try{
                rpcBaseRes = this.doGetOnlineAppMeta(appMetaList);
                if(RpcBaseResType.OK.name().equals(rpcBaseRes.getResCode())){
                    this.getOnlineAppMetaSuccessCallBack(rpcBaseRes);
                }
            }catch (Exception ex){
                this.getOnlineAppMetaFailCallBack(rpcBaseRes,ex);
            }
        }
    }

    /**
     * 获取在线应用数据源数列表
     * @param appMetaList
     * @return
     */
    protected abstract RpcBaseRes<List<GetOnlineAppResData>> doGetOnlineAppMeta(List<GetOnlineAppReqData> appMetaList) throws Exception;

    /**
     *  注册失败回调
     * @param res
     * @param ex
     */
    protected void getOnlineAppMetaFailCallBack(RpcBaseRes<List<GetOnlineAppResData>> res, Exception ex){
       try{
           //无线重试
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.getOnlineAppMeta();
       }catch (Exception e){

       }
    }
    /**
     * 注册成功回调
     * @param res
     */
    protected void getOnlineAppMetaSuccessCallBack(RpcBaseRes<List<GetOnlineAppResData>> res){
        transferRpcServiceList(res.getData());
        if(heartTimer==null){
            heartTimer = new Timer();
        }
        AppMetaListTask appMetaListTask = new AppMetaListTask();
        appMetaListTask.setXRpcClient(this);
        heartTimer.schedule(appMetaListTask,heartbeat);
    }

    protected void transferRpcServiceList(List<GetOnlineAppResData> list){
        List<RpcServiceMethodItem> newList = new ArrayList<>();
        if(list!=null){
            for (int i = 0; i < list.size(); i++) {
                GetOnlineAppResData item = list.get(i);
                String namespace = item.getNamespace();
                String appName = item.getAppName();
                String instanceId = item.getInstanceId();
                String endpoint = item.getEndpoint();

                List<RpcServiceMeta> serviceMetaList = item.getServiceMetaList();
                if(serviceMetaList!=null){
                    for (int j = 0; j < serviceMetaList.size(); j++) {
                        RpcServiceMeta serviceMeta = serviceMetaList.get(j);
                        int version = serviceMeta.getVersion();
                        String serviceName = serviceMeta.getServiceName();
                        List<RpcMethodMeta> methodMetaList = serviceMeta.getMethodMetaList();
                        if(methodMetaList!=null){
                            for (int k = 0; k < methodMetaList.size(); k++) {
                                RpcMethodMeta methodMeta = methodMetaList.get(k);
                                String methodName = methodMeta.getMethodName();
                                List<RpcParamMeta> paramListMeta = methodMeta.getParamListMeta();
                                List<String> paramTypes = null;
                                if(paramListMeta!=null){
                                    paramTypes = new ArrayList<>();
                                    for (RpcParamMeta rpcParamMeta:paramListMeta) {
                                        paramTypes.add(rpcParamMeta.getType());
                                    }
                                }
                                //找到已存在的item
                                RpcServiceMethodItem serviceItem = null;
                                for (RpcServiceMethodItem existsItem:newList) {
                                    if(namespace.equals(existsItem.getNamespace())
                                    &&appName.equals(existsItem.getAppName())
                                    &&serviceName.equals(existsItem.getServiceName())
                                    &&version==existsItem.getVersion()
                                    &&methodName.equals(existsItem.getMethodMame())
                                    ){
                                        List<String> existsParamTypes = existsItem.getParamTypes();
                                        if((paramTypes==null||paramTypes.size()==0)&&(existsParamTypes==null||existsParamTypes.size()==0)){
                                            serviceItem = existsItem;
                                            break;
                                        }else if(paramTypes!=null&&paramTypes.size()>0&&existsParamTypes.size()==paramTypes.size()){
                                            boolean isMatch=true;
                                            for (int l = 0; l < paramTypes.size(); l++) {
                                                String paramType = paramTypes.get(l);
                                                String existParamType = existsParamTypes.get(i);
                                                if(!paramType.equals(existParamType)){
                                                    isMatch = false;
                                                    break;
                                                }
                                            }
                                            if(isMatch){
                                                serviceItem = existsItem;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if(serviceItem==null){
                                    serviceItem = new RpcServiceMethodItem();
                                    serviceItem.setNamespace(namespace);
                                    serviceItem.setAppName(appName);
                                    serviceItem.setServiceName(serviceName);
                                    serviceItem.setVersion(version);
                                    serviceItem.setMethodMame(methodName);
                                    serviceItem.setParamTypes(paramTypes);
                                    List<RpcAppItem> appList = new ArrayList<>();
                                    serviceItem.setAppList(appList);
                                    newList.add(serviceItem);
                                }
                                RpcAppItem appItem = new RpcAppItem();
                                appItem.setAppName(appName);
                                appItem.setNamespace(namespace);
                                appItem.setInstanceId(instanceId);
                                appItem.setEndpoint(endpoint);
                                serviceItem.getAppList().add(appItem);
                            }
                        }
                    }
                }
            }
        }
        this.rpcServiceMethodList = newList;
    }
}
