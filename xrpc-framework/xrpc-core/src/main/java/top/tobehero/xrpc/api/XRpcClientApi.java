package top.tobehero.xrpc.api;

/**
 * 客户端API
 */
public interface XRpcClientApi extends XRpcApi {
    /**
     * 获取在线app元数据
     */
    void getOnlineAppMeta();
}
