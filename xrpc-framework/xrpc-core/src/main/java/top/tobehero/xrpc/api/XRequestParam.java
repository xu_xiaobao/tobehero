package top.tobehero.xrpc.api;

import lombok.Data;

@Data
public class XRequestParam {
    /**请求参数类型**/
    private String type;
    /**请求参数值**/
    private String val;
}
