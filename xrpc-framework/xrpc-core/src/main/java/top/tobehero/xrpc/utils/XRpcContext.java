package top.tobehero.xrpc.utils;

import top.tobehero.xrpc.api.XRequest;

import java.util.HashMap;
import java.util.Map;

public class XRpcContext {
    private XRequest request;
    private Map<String,Object> dataMap;
    private Map<String,String> extHeaders;
    public Map<String, Object> getDataMap() {
        return dataMap;
    }
    public XRequest getRequest() {
        return request;
    }
    public void setRequest(XRequest request) {
        this.request = request;
        if(request!=null) {
            Map<String, String> extendHeaders = request.getExtendHeaders();
            if(extendHeaders!=null) {
                this.extHeaders = extendHeaders;
            }
        }
    }
    /**
     * 	增加自定义上下文数据
     * @param key
     * @param data
     */
    public void putData(String key, Object data) {
        if(dataMap!=null) {
            dataMap.put(key, data);
        }else {
            dataMap = new HashMap<>();
            dataMap.put(key, data);
        }
    }
    /**
     * 获取自定义上下文数据
     * @param key
     * @return
     */
    public Object getData(String key) {
        if(dataMap!=null) {
            return dataMap.get(key);
        }
        return null;
    }
    /**
     * 	获取本次请求额外的请求头Map
     * @return
     */
    public Map<String,String> getExternalHeaders(){
        return this.extHeaders;
    }

    /**
     * 	获取本次请求额外的请求头Map中指定key的value
     * @return
     */
    public String getExternalHeaderValue(String key){
        if(this.extHeaders!=null) {
            return this.extHeaders.get(key);
        }
        return null;
    }
}
