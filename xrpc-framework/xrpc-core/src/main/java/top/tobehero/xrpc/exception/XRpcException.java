package top.tobehero.xrpc.exception;

import top.tobehero.xrpc.api.dto.RpcBaseResType;

public class XRpcException extends Exception {
    private String code;
    private String msg;

    public XRpcException(String code, String msg) {
        this(null,code,msg);
    }
    public XRpcException(Throwable cause, String code, String msg) {
        super(cause);
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public static XRpcException of(RpcBaseResType e, Throwable ex){
        return new XRpcException(ex,e.name().toUpperCase(),e.getMsg());
    }
    public static XRpcException of(RpcBaseResType e){
        return of(e,null);
    }
}
