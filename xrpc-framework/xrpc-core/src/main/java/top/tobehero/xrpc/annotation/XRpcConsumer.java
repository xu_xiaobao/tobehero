package top.tobehero.xrpc.annotation;

import java.lang.annotation.*;

/**
 * RPC客户端代理注解，可用于接口类
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface XRpcConsumer {
    /**
     * 应用名称
     * @return
     */
    String appName();

    /**
     * 远程service name
     * @return
     */
    String value() default "";

    /**
     * 服务版本号
     * @return
     */
    int version() default 1;

    /**
     * 名称空间
     * @return
     */
    String namespace() default "default";
}
