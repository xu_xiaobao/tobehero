package top.tobehero.xrpc.provider.interceptor;

import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.XResponse;
import top.tobehero.xrpc.provider.AbstractRpcProviderAgent;

public class RpcProviderAgentInterceptorAdaptor implements IRpcProviderAgentInterceptor {
    @Override
    public boolean preHandle(XRequest request, XResponse response, AbstractRpcProviderAgent agent) throws Exception {
        return false;
    }

    @Override
    public void postHandle(XRequest request, XResponse response, AbstractRpcProviderAgent agent) throws Exception {

    }

    @Override
    public void afterCompletion(XRequest request, XResponse response, AbstractRpcProviderAgent agent, Exception ex) throws Exception {

    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}
