package top.tobehero.xrpc.utils;

public class XRpcContextUtils {
    /**每次线程执行时上下文变量**/
    private static final InheritableThreadLocal<XRpcContext> context = new InheritableThreadLocal<>();
    public static void clearContext() {
        context.remove();
    }
    public static void saveCurrentContext(XRpcContext ctx) {
        context.set(ctx);
    }
    /**
     * 	获取当前线程下的上下文对象
     * @return
     */
    public static XRpcContext getCurrentContext() {
        return context.get();
    }
}
