package top.tobehero.xrpc.api.dto;

import lombok.Data;
import top.tobehero.xrpc.api.meta.RpcServiceMeta;

import java.util.List;

/**
 * 获取在线APP元信息
 */
@Data
public class GetOnlineAppResData {
    private String namespace;
    private String appName;
    private String instanceId;
    private String endpoint;
    private List<RpcServiceMeta> serviceMetaList;
}
