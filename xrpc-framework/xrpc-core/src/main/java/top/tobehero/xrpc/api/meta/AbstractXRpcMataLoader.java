package top.tobehero.xrpc.api.meta;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;
import top.tobehero.xrpc.api.XRpcMetaApi;
import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.dto.GetOnlineAppReqData;
import top.tobehero.xrpc.consumer.RpcConsumerAgent;
import top.tobehero.xrpc.provider.RpcProviderAgent;
import top.tobehero.xrpc.utils.AopTargetUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractXRpcMataLoader implements XRpcMetaApi, ApplicationContextAware {
    public static final String XRPC_REGISTRY_CONFIG_PREFFIX="xrpc.registry";
    private ApplicationContext ctx;
    private RpcAppMeta appMeta;
    private RegistryConfig registry;
    /**消费者需要的服务端元数据**/
    private List<GetOnlineAppReqData> getOnlineAppReqDataList;
    @Autowired
    private Environment env;

    /**
     * 解析AppMeta
     * @return
     */
    public abstract RpcAppMeta resolveRpcAppMeta();

    @Override
    public RpcAppMeta getProviderAppMeta() {
        if(this.appMeta!=null){
            return this.appMeta;
        }
        appMeta = this.resolveRpcAppMeta();
        String appName = appMeta.getAppName();
        String endpoint = appMeta.getEndpoint();
        Assert.notNull(appName,"XRPC应用名称不能为空");
        Assert.notNull(endpoint,"XRPC服务暴露地址不能为空");
        this.getRegistryConfig();
        appMeta.setRegistry(registry);
        String address = registry.getAddress();
        String namespace = registry.getNamespace();
        Assert.notNull(address,"XRPC注册中心地址不能为空");
        Assert.notNull(namespace,"XRPC注册名称空间不能为空");
        appMeta.setRegistry(this.registry);
        this.resolveRpcServiceMetaList();
        return appMeta;
    }

    @Override
    public List<GetOnlineAppReqData> getConsumerAppMeta() {
        if(getOnlineAppReqDataList!=null){
            return getOnlineAppReqDataList;
        }
        this.getRegistryConfig();
        //加载ServiceMeta
        List<RpcConsumerAgent> serviceList = new ArrayList<>();
        Map<String, RpcConsumerAgent> agentMap = this.ctx.getBeansOfType(RpcConsumerAgent.class);
        if(agentMap!=null){
            for (RpcConsumerAgent agent:agentMap.values()) {
                serviceList.add(agent);
            }
            getOnlineAppReqDataList = new ArrayList<>();
            for (RpcConsumerAgent item:serviceList) {
                try {
                    RpcConsumerAgent agent = (RpcConsumerAgent) AopTargetUtils.getTarget(item);
                    //查询是否已存在appName
                    GetOnlineAppReqData data = null;
                    for (int i = 0; i < getOnlineAppReqDataList.size(); i++) {
                        GetOnlineAppReqData existsAgent = getOnlineAppReqDataList.get(i);
                        String appName = existsAgent.getAppName();
                        String namespace = existsAgent.getNamespace();
                        if(agent.getNamespace().equals(namespace)&&agent.getAppName().equals(appName)){
                            data = existsAgent;
                            break;
                        }
                    }
                    if(data==null){
                        data = new GetOnlineAppReqData();
                        data.setAppName(agent.getAppName());
                        data.setNamespace(agent.getNamespace());
                        getOnlineAppReqDataList.add(data);
                    }
                    List<RpcServiceMeta> serviceMetaList = data.getServiceMetaList();
                    if(serviceMetaList==null){
                        serviceMetaList = new ArrayList<>();
                        data.setServiceMetaList(serviceMetaList);
                    }
                    RpcServiceMeta serviceMeta = new RpcServiceMeta();
                    serviceMeta.setServiceName(agent.getServiceName());
                    serviceMeta.setVersion(agent.getVersion());
                    serviceMetaList.add(serviceMeta);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return getOnlineAppReqDataList;
    }
    /**
     * 解析Rpc服务元数据列表
     */
    private void resolveRpcServiceMetaList() {
        //加载ServiceMeta
        List<RpcProviderAgent> serviceList = new ArrayList<>();
        Map<String, RpcProviderAgent> agentMap = this.ctx.getBeansOfType(RpcProviderAgent.class);
        if(agentMap!=null){
            for (RpcProviderAgent agent:agentMap.values()) {
                serviceList.add(agent);
            }
        }
        List<RpcServiceMeta> serviceMetaList = getServiceMetaList(serviceList);
        for (RpcServiceMeta serviceMeta:serviceMetaList) {
            serviceMeta.setAppMeta(appMeta);
        }
        appMeta.setServiceMetaList(serviceMetaList);
    }

    @Override
    public RegistryConfig getRegistryConfig() {
        if(registry!=null){
            return registry;
        }
        registry = new RegistryConfig();
        String address = env.getProperty(XRPC_REGISTRY_CONFIG_PREFFIX + ".address");
        String namespace = env.getProperty(XRPC_REGISTRY_CONFIG_PREFFIX + ".namespace");
        registry.setAddress(address);
        if(org.apache.commons.lang3.StringUtils.isNotBlank(namespace)){
            registry.setNamespace(namespace);
        }
        return registry;
    }
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
    /**
     * 获取serviceMeta
     * @param serviceList
     * @return
     */
    private List<RpcServiceMeta> getServiceMetaList(List<RpcProviderAgent> serviceList) {
        List<RpcServiceMeta> list = new ArrayList<>();
        for (RpcProviderAgent agent:serviceList) {
            RpcServiceMeta serviceMeta = agent.analysisServiceMeta();
            list.add(serviceMeta);
        }
        return list;
    }

}
