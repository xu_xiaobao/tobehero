package top.tobehero.xrpc.api.dto;

import lombok.Data;

@Data
public class RpcBaseRes<T> {
    private String resCode;
    private String resMsg;
    private T data;

    public static <T> RpcBaseRes<T> of(String resCode, String resMsg, T data) {
        RpcBaseRes<T> resData = new RpcBaseRes<>();
        resData.setResCode(resCode);
        resData.setResMsg(resMsg);
        resData.setData(data);
        return resData;
    }

    public static <T> RpcBaseRes<T> of(String resCode, String resMsg) {
        return of(resCode, resMsg, null);
    }

    public static <T> RpcBaseRes<T> of(RpcBaseResType type, T data) {
        return of(type.name(), type.getMsg(), data);
    }

    public static RpcBaseRes of(RpcBaseResType type) {
        return of(type, null);
    }
}
