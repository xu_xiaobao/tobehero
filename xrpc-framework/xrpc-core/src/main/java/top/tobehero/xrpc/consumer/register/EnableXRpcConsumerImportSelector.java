package top.tobehero.xrpc.consumer.register;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ArrayList;
import java.util.List;

public class EnableXRpcConsumerImportSelector implements ImportSelector {
    public static final String BOOT_XRPC_CONSUMER_AUTO_CONFIGURATION = "top.tobehero.xrpc.boot.XRpcConsumerAutoConfiguration";

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        List<String> importClasses = new ArrayList<>();
        importClasses.add(EnableXRpcConsumerImportBeanDefinitionRegistrar.class.getName());
        Class<?> aClass = null;
        try {
            aClass = Class.forName(BOOT_XRPC_CONSUMER_AUTO_CONFIGURATION);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(aClass!=null){
            importClasses.add(BOOT_XRPC_CONSUMER_AUTO_CONFIGURATION);
        }
        return importClasses.toArray(new String[importClasses.size()]);
    }
}
