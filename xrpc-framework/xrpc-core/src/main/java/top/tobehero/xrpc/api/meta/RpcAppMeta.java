package top.tobehero.xrpc.api.meta;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import top.tobehero.xrpc.api.config.RegistryConfig;

import java.util.Date;
import java.util.List;

/**
 * 描述RPC应用元数据
 */
@Data
@EqualsAndHashCode(exclude = {"serviceMetaList"})
@ToString(exclude = {"serviceMetaList"})
public class RpcAppMeta {
    /**
     * 应用名称
     */
    private String appName;
    /***
     * 实例ID
     */
    private String instanceId;
    /**
     * 注册服务暴露地址
     */
    private String endpoint;
    /**
     * 注册中心配置
     */
    private RegistryConfig registry;

    /**
     * 服务元数据列表
     */
    private List<RpcServiceMeta> serviceMetaList;

    /**
     * 过期时间
     */
    private Date expireAt;

    /**
     * 过期秒数
     */
    private int expire = 30;
}
