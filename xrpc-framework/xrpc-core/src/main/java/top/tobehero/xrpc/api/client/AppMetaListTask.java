package top.tobehero.xrpc.api.client;

import lombok.Data;

import java.util.TimerTask;

@Data
public class AppMetaListTask extends TimerTask {
    private AbstractXRpcClient xRpcClient;
    private boolean isRunning;

    @Override
    public void run() {
        if(this.isRunning){
            return ;
        }
        this.isRunning = true;
        try{
            xRpcClient.getOnlineAppMeta();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            this.isRunning=false;
        }
    }
}
