package top.tobehero.xrpc.consumer.register;

import org.apache.commons.lang3.StringUtils;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import top.tobehero.xrpc.annotation.XRpcConsumer;
import top.tobehero.xrpc.consumer.RpcConsumerAgent;
import top.tobehero.xrpc.consumer.XRpcConsumserMethodInterceptor;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * 扫描classpath下的XRpcProvider注解，并注册
 */
public class XRpcConsumerPathBeanDefinitionScanner extends ClassPathBeanDefinitionScanner{
    private String xrpcConsumerMethodInterceptorSpringBeanName = XRpcConsumerPathBeanDefinitionScanner.class.getName()+"::xrpcConsumerMethodInterceptor";

    private Class<?>[] consumerClasses;
    private Class<? extends Annotation> handleAnnotation= XRpcConsumer.class;
    private List<String> consumerSpringBeanNames = new ArrayList<>();
    public XRpcConsumerPathBeanDefinitionScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters,Class<?>[] consumerClasses) {
        super(registry, useDefaultFilters);
        this.consumerClasses = consumerClasses;
    }
    protected void registerFilters(){
        addIncludeFilter(new AnnotationTypeFilter(this.handleAnnotation));
    }

    @Override
    protected boolean isCandidateComponent(MetadataReader metadataReader) throws IOException {
        AnnotationMetadata metadata = metadataReader.getAnnotationMetadata();
        if(consumerClasses!=null&&consumerClasses.length>0){
            for(Class<?> clazz:consumerClasses){
                String className = metadata.getClassName();
                String name = clazz.getName();
                if(name.equals(className)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        return true;
    }

    @Override
    protected void registerBeanDefinition(BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry) {
        if(!registry.containsBeanDefinition(xrpcConsumerMethodInterceptorSpringBeanName)){
            //注册代理拦截器
            BeanDefinition interceptorBd= new RootBeanDefinition();
            interceptorBd.setBeanClassName(XRpcConsumserMethodInterceptor.class.getName());
            MutablePropertyValues interceptorPv = new MutablePropertyValues();
            ((RootBeanDefinition) interceptorBd).setPropertyValues(interceptorPv);
            registry.registerBeanDefinition(xrpcConsumerMethodInterceptorSpringBeanName,interceptorBd);
        }

        BeanDefinition beanDefinition = definitionHolder.getBeanDefinition();
        String beanClassName = beanDefinition.getBeanClassName();
        Class<?> clazz = null;
        try {
            clazz = Class.forName(beanClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        RootBeanDefinition targetBd = new RootBeanDefinition();
        targetBd.setBeanClass(RpcConsumerAgent.class);
        XRpcConsumer xRpcConsumer = clazz.getAnnotation(XRpcConsumer.class);
        String appName = xRpcConsumer.appName();
        String namespace = xRpcConsumer.namespace();
        int version = xRpcConsumer.version();
        String serviceName = xRpcConsumer.value();
        if(StringUtils.isBlank(serviceName)){
            String simpleName = clazz.getSimpleName();
            if(simpleName.startsWith("I")){
                simpleName = simpleName.substring(1);
            }
            serviceName = simpleName.substring(0,1).toLowerCase()+simpleName.substring(1);
        }

        MutablePropertyValues targetProps = new MutablePropertyValues();
        targetProps.addPropertyValue(new PropertyValue("appName", appName));
        targetProps.addPropertyValue(new PropertyValue("serviceName", serviceName));
        targetProps.addPropertyValue(new PropertyValue("version", version));
        targetProps.addPropertyValue(new PropertyValue("namespace", namespace));
        targetProps.addPropertyValue(new PropertyValue("clazz", clazz));
        targetBd.setPropertyValues(targetProps);

        RootBeanDefinition proxyBd = new RootBeanDefinition();
        proxyBd.setBeanClass(ProxyFactoryBean.class);
        MutablePropertyValues proxyProps = new MutablePropertyValues();
        proxyProps.addPropertyValue(new PropertyValue("target", targetBd));
        proxyProps.addPropertyValue(new PropertyValue("interfaces", new Class<?>[]{clazz}));
        proxyProps.addPropertyValue(new PropertyValue("proxyTargetClass", true));
        proxyProps.addPropertyValue(new PropertyValue("interceptorNames", xrpcConsumerMethodInterceptorSpringBeanName));
        proxyBd.setPropertyValues(proxyProps);



        String beanName = definitionHolder.getBeanName();
        registry.registerBeanDefinition(beanName, proxyBd);
        consumerSpringBeanNames.add(beanName);
    }
}