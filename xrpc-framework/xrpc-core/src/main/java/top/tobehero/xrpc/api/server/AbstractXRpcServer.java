package top.tobehero.xrpc.api.server;

import lombok.Data;
import top.tobehero.xrpc.api.meta.AbstractXRpcMataLoader;
import top.tobehero.xrpc.api.XRpcServerApi;
import top.tobehero.xrpc.api.dto.RegisterAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.Timer;

@Data
public abstract class AbstractXRpcServer implements XRpcServerApi {
    private int heartbeat = 5000;
    /**是否APP启动阶段**/
    private boolean appStartup=true;
    private AbstractXRpcMataLoader xRpcMataLoader;
    public AbstractXRpcServer(AbstractXRpcMataLoader xRpcMataLoader){
        this.xRpcMataLoader = xRpcMataLoader;
    }
    @Override
    public void registerAppMeta() {
        RpcAppMeta appMeta = xRpcMataLoader.getProviderAppMeta();
        RpcBaseRes<RegisterAppResData> res = null;
        String resCode = null;
        try{
            res=doRegister(appMeta);
            if(res == null){
                this.registerFailCallBack(null,null);
                return;
            }
            resCode = res.getResCode();
            if(RpcBaseResType.OK.name().equals(resCode)){
                this.registerSuccessCallBack(res);
            }else{
                this.registerFailCallBack(res,null);
            }
        }catch (Exception ex){
            this.registerFailCallBack(res,ex);
        }
    }

    /**
     * 注册AppMeta
     * @param appMeta
     * @return
     */
    protected abstract RpcBaseRes<RegisterAppResData> doRegister(RpcAppMeta appMeta) throws Exception;

    /**
     *  注册失败回调
     * @param res
     * @param ex
     */
    protected void registerFailCallBack(RpcBaseRes<RegisterAppResData> res, Exception ex){
        //非APP启动阶段，无线重试注册
        if(!appStartup){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.registerAppMeta();
        }else{
            System.out.println("注册应用失败");
            throw new RuntimeException("注册应用失败");
        }
    }
    /**
     * 注册成功回调
     * @param res
     */
    protected void registerSuccessCallBack(RpcBaseRes<RegisterAppResData> res){
        if(this.appStartup){
            appStartup = false;
        }
        Timer heartTimer = new Timer();
        RpcAppMeta appMeta = this.getXRpcMataLoader().getProviderAppMeta();
        HeartBeatTimerTask heartBeatTimerTask = new HeartBeatTimerTask();
        heartBeatTimerTask.setTimer(heartTimer);
        heartBeatTimerTask.setRpcAppMeta(appMeta);
        heartBeatTimerTask.setRunning(false);
        heartBeatTimerTask.setXRpcServer(this);
        heartTimer.schedule(heartBeatTimerTask,heartbeat,heartbeat);
    }
}
