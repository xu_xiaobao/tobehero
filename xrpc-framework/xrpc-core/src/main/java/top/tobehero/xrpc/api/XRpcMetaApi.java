package top.tobehero.xrpc.api;

import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.dto.GetOnlineAppReqData;
import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.List;

public interface XRpcMetaApi {

    RegistryConfig getRegistryConfig();
    /**
     * 获取作为生产者的app元数据
     * @return
     */
    RpcAppMeta getProviderAppMeta();

    /**
     * 获取作为消费者的app元数据列表
     * @return
     */
    List<GetOnlineAppReqData> getConsumerAppMeta();
}
