package top.tobehero.xrpc.annotation;

import org.springframework.context.annotation.Import;
import top.tobehero.xrpc.provider.register.EnableXRpcProviderImportSelector;

import java.lang.annotation.*;

/**
 * 开启XRPC服务注册发布
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(EnableXRpcProviderImportSelector.class)
public @interface EnableXRpcProvider {
    /**
     * 扫描基础包，默认全部扫描
     * @return
     */
    String[] basePackages() default {""};

    /**
     * 应用ID
     * @return
     */
    String appId();

    /**
     * 终端服务地址
     * @return
     */
    String[] endpoint() default {"/xrpc"};
}
