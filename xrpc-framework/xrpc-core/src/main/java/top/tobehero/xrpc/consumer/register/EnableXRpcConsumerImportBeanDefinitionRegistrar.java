package top.tobehero.xrpc.consumer.register;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import top.tobehero.xrpc.annotation.EnableXRpcConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EnableXRpcConsumerImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar, ResourceLoaderAware, BeanFactoryAware {
    private ResourceLoader resourceLoader;
    private BeanFactory beanFactory;

    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata,
                                        BeanDefinitionRegistry registry) {
        //扫描注解
        Map<String, Object> annotationAttributes = annotationMetadata
                .getAnnotationAttributes(EnableXRpcConsumer.class.getName());
        Class<?>[] basePackages = (Class<?>[]) annotationAttributes.get("basePackages");
        Class<?>[] consumers = (Class<?>[]) annotationAttributes.get("consumers");
        XRpcConsumerPathBeanDefinitionScanner scanner =
                new XRpcConsumerPathBeanDefinitionScanner(registry,false,consumers);
        scanner.setResourceLoader(resourceLoader);
        scanner.registerFilters();
        if(consumers.length>0){
            //解析得到cosumer所在包，增加自定义过滤器，过滤出所有指定Consumer
            List<String> packages = new ArrayList<>();
            for (Class clazz:consumers) {
                String clazzName = clazz.getName();
                String packageName = clazzName.substring(0, clazzName.lastIndexOf('.'));
                packages.add(packageName);
            }
            String[] basePackages1 = packages.toArray(new String[packages.size()]);
            scanner.scan(basePackages1);
        }else{
            List<String> packages = new ArrayList<>();
            if(basePackages!=null){
                for (Class<?> clazz:basePackages) {
                    String clazzName = clazz.getName();
                    String packageName = clazzName.substring(0, clazzName.lastIndexOf('.'));
                    packages.add(packageName);
                }
            }
            scanner.scan(packages.toArray(new String[packages.size()]));
        }
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory=beanFactory;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader=resourceLoader;
    }
}
