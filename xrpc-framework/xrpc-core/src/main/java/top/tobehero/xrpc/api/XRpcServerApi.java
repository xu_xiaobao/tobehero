package top.tobehero.xrpc.api;

import top.tobehero.xrpc.api.dto.RegisterAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcPing;

/**
 * 服务端API
 */
public interface XRpcServerApi extends XRpcApi {
    /**
     * 注册RPC应用元数据
     * @return
     */
    void registerAppMeta();

    /**
     * 心跳
     *
     * @param ping
     * @return
     */
    RpcBaseRes<RegisterAppResData> heartbeat(RpcPing ping) throws Exception;
}
