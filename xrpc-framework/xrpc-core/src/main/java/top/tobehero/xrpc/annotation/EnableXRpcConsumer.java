package top.tobehero.xrpc.annotation;

import org.springframework.context.annotation.Import;
import top.tobehero.xrpc.consumer.register.EnableXRpcConsumerImportSelector;

import java.lang.annotation.*;

/**
 * 开启XRPC客户端
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(EnableXRpcConsumerImportSelector.class)
public @interface EnableXRpcConsumer {
    /**
     * 扫描基础包数组
     * @return
     */
    Class<?>[] basePackageClasses() default {};

    /**
     * 指定@XRpcConsumer相关接口类，如果不为空，则包扫描失效
     * @return
     */
    Class<?>[] consumers() default {};

}
