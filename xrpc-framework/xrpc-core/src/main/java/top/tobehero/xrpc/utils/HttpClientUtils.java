package top.tobehero.xrpc.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.util.Assert;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class HttpClientUtils {
    /***
     * 定义一个全局不使用http代理的对象
     */
    private static final HttpProxyConfig DEFAULT_NO_HTTP_PROXY = new HttpProxyConfig();
    private final static ThreadLocal<Map<String,Object>> LOCAL_MAP = new ThreadLocal<>();
    private final static String DEFAULT_ENCODE="UTF-8";
    static{
        DEFAULT_NO_HTTP_PROXY.setEnableProxy(false);//不使用代理
    }
    private static HttpClient client = null;
    static {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(128);
        cm.setDefaultMaxPerRoute(128);
        client = HttpClients.custom().setConnectionManager(cm).build();
    }

    /**
     * 不使用代理post表单，返回响应报文
     * @param url 地址
     * @param map 参数
     */
    public static String doPost(String url, Map<String, String> map) {
        return doPost(url, map, DEFAULT_NO_HTTP_PROXY);
    }
    /**
     * 以非multipart方式post表单数据，返回响应报文（可使用代理）
     * @param url
     * 			地址
     * @param map
     * 			post参数
     * @param proxyConfig
     * 			http代理配置，null或者proxyConfig.enableProxy==false不使用代理
     */
    public static String doPost(String url, Map<String, String> map,HttpProxyConfig proxyConfig) {
        return doPost(url,map,proxyConfig,false);
    }
    /**
     * post表单数据，返回响应报文（可使用代理）
     * @param url
     * 			地址
     * @param map
     * 			post参数
     * @param proxyConfig
     * 			http代理配置，null或者proxyConfig.enableProxy==false不使用代理
     * @param isMultiPart
     * 				是否使用multipart方式传输
     */
    public static String doPost(String url, Map<String, String> map,HttpProxyConfig proxyConfig,boolean isMultiPart) {
        String responseStr = null;
        CloseableHttpResponse response = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httpPost = new HttpPost(url);
            if(isMultiPart){
                Charset utf_8 = Charset.forName("UTF-8");
                MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,null,utf_8);
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    String key = entry.getKey();
                    String val = entry.getValue();
                    entity.addPart(key,new StringBody(val, utf_8));
                }
                httpPost.setEntity(entity);
            }else{
                List<NameValuePair> parameters = new ArrayList<NameValuePair>();
                Set<String> set = map.keySet();
                String key = "";
                if(set != null){
                    Iterator<String> it = set.iterator();
                    while(it.hasNext()){
                        key = it.next();
                        parameters.add(new BasicNameValuePair(key,map.get(key)));
                    }
                }
                httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            }
            //配置代理
            if(proxyConfig!=null&&proxyConfig.isEnableProxy()){
                HttpHost proxyHost=new HttpHost(proxyConfig.getHostName(), proxyConfig.getPort());
                httpPost.setConfig(RequestConfig.custom().setProxy(proxyHost).build());
            }
            response = httpclient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            Assert.isTrue(statusCode==200,"响应状态异常，响应码【"+statusCode+"】");
            HttpEntity entity = response.getEntity();
            List<String> oneLines=IOUtils.readLines(new BufferedReader(new InputStreamReader(entity.getContent(),"UTF-8")));
            StringBuffer sb=new StringBuffer();
            for (String oneLine : oneLines) {
                sb.append(oneLine);
            }
            responseStr=sb.toString();
        } catch (Exception e) {
            log.info("请求地址:"+url+",请求参数："+ JSONObject.toJSONString(map)+"，请求发生错误：",e);
            throw new RuntimeException(e);
        } finally {
            log.info("请求地址:{},请求参数：{},返回报文:{}",new Object[]{url,JSONObject.toJSONString(map),responseStr});
            if (null != response) {
                try {
                    response.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if(null != httpclient ){
                try{
                    httpclient.close();
                }catch(Exception ex){
                    throw new RuntimeException(ex);
                }

            }
        }
        return responseStr;
    }

    /**
     * post数据指定请求头，不使用代理
     * @param url
     * @param headers
     * @param bodyStr
     * @return
     * @throws SocketException
     * @throws Exception
     */
    public static String doPost(String url,Map<String,String> headers, String bodyStr) throws SocketException,HttpException,NoHttpResponseException,Exception {
        return doPost(url,bodyStr,headers,DEFAULT_NO_HTTP_PROXY);
    }
    /**
     *
     * @param url
     * 				请求地址
     * @param bodyStr
     * 				发送报文
     * @param proxyConfig
     * 				http代理配置，null或者proxyConfig.enableProxy==false不使用代理
     * @return
     * @throws SocketException
     * 				连接重置，地址不通
     * @throws Exception
     */
    public static String doPost(String url, String bodyStr,HttpProxyConfig proxyConfig) throws SocketException,HttpException,NoHttpResponseException,Exception {
        return doPost(url, bodyStr, null, proxyConfig);
    }
    /**
     * post数据指定请求头
     * @param url
     * @param bodyStr
     * @param reqHeaders
     * @param proxyConfig
     * @return
     * @throws SocketException
     * @throws Exception
     */
    public static String doPost(String url, String bodyStr,Map<String,String> reqHeaders,HttpProxyConfig proxyConfig) throws SocketException,HttpException,NoHttpResponseException,Exception {
        return doPost(url, bodyStr, reqHeaders, proxyConfig, DEFAULT_ENCODE, DEFAULT_ENCODE);
    }
    /**
     * post数据指定请求头
     * @param url
     * @param bodyStr
     * @param reqHeaders
     * @param proxyConfig
     * @param reqEncode
     * @param resEncode
     * @return
     * @throws SocketException
     * @throws Exception
     */
    public static String doPost(String url, String bodyStr,Map<String,String> reqHeaders,HttpProxyConfig proxyConfig,String reqEncode,String resEncode) throws SocketException,HttpException,NoHttpResponseException,Exception {
        String responseStr = null;
        CloseableHttpResponse response = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httpPost = new HttpPost( url );
            httpPost.setEntity( new StringEntity(bodyStr ,reqEncode));
            if(reqHeaders!=null){
                Set<Map.Entry<String,String>> entrySet = reqHeaders.entrySet();
                for (Map.Entry<String, String> entry : entrySet) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    httpPost.addHeader(key, value);
                }
            }
            //配置代理
            if(proxyConfig!=null&&proxyConfig.isEnableProxy()){
                HttpHost proxyHost=new HttpHost(proxyConfig.getHostName(), proxyConfig.getPort());
                httpPost.setConfig(RequestConfig.custom().setProxy(proxyHost).build());
            }
            response = httpclient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();;
            if(statusCode!=200){
                throw new HttpException("[请求发生错误，非正常响应状态]-[请求地址:"+url+"]-[响应状态："+statusCode+"]");
            }
            HttpEntity entity = response.getEntity();
            List<String> oneLines=IOUtils.readLines(new BufferedReader(new InputStreamReader(entity.getContent(),resEncode)));
            StringBuffer sb=new StringBuffer();
            for (String oneLine : oneLines) {
                sb.append(oneLine);
            }
            responseStr=sb.toString();
        } catch (SocketException|NoHttpResponseException|HttpException e){
            throw e;
        } catch (Exception e) {
            log.info("请求地址:"+url+",请求报文："+bodyStr+"，请求发生错误：",e);
            throw new RuntimeException(e);
        } finally {
            log.info("请求地址:{},请求报文：{},返回报文:{}",new Object[]{url,bodyStr,responseStr});
            if (null != response) {
                try {
                    response.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if(null != httpclient ){
                try{
                    httpclient.close();
                }catch(Exception ex){
                    throw new RuntimeException(ex);
                }

            }
        }
        return responseStr;
    }
    /**
     * @param url         地址
     * @param plain
     * @param signature
     * @return
     */
    @SuppressWarnings("finally")
    public static String httpURLConectionGET(String url, String plain,String signature) {
        log.info("开始访问!");
        String encoding="UTF-8";
        StringBuilder sb = new StringBuilder();
        StringBuffer bufferUrl = new StringBuffer(url);
        bufferUrl.append("?Plain="+plain);
        bufferUrl.append("&Signature="+signature);
        try {
            URL url1 = new URL(bufferUrl.toString());// 把字符串转换为URL请求地址
            HttpURLConnection connection = (HttpURLConnection) url1.openConnection();// 打开连接
            connection.connect();// 连接会话
            // 获取输入流
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(),encoding));
            String line;
            while ((line = br.readLine()) != null) {// 循环读取流
                sb.append(line);
            }
            br.close();// 关闭流
            connection.disconnect();// 断开连接
            log.info("info==>>Xml解析完成"+sb.toString());
        } catch (Exception e) {
            String st = ExceptionUtils.getStackTrace(e);
            log.error("访问下载地址出错："+st);
        }finally {
            return sb.toString();
        }
    }

    /**
     * 可绕过ssl验证，支持http,https,连接超时时间设置
     * @param url
     * @param nvp
     * @param mimeType
     * @param charset
     * @param connTimeout
     * @param readTimeout
     * @return
     * @throws ConnectTimeoutException
     * @throws SocketTimeoutException
     * @throws Exception
     */
    public static String post(String url, List<NameValuePair> nvp, String mimeType,
                              String charset, Integer connTimeout, Integer readTimeout)
            throws ConnectTimeoutException, SocketTimeoutException, Exception {
        HttpClient client = null;
        HttpPost post = new HttpPost(url);
        String result = "";
        try {
            post.setEntity(new UrlEncodedFormEntity(nvp,charset));
            // 设置参数
            RequestConfig.Builder customReqConf = RequestConfig.custom();
            if (connTimeout != null && !connTimeout.equals("")) {
                customReqConf.setConnectTimeout(connTimeout);
            }
            if (readTimeout != null && !readTimeout.equals("")) {
                customReqConf.setSocketTimeout(readTimeout);
            }
            post.setConfig(customReqConf.build());

            HttpResponse res;
            if (url.startsWith("https")) {
                // 执行 Https 请求.
                client = createSSLInsecureClient();
                res = client.execute(post);
            } else {
                // 执行 Http 请求.
                client = HttpClientUtils.client;
                res = client.execute(post);
            }
            result = IOUtils.toString(res.getEntity().getContent(), charset);
        } finally {
            post.releaseConnection();
            if (url.startsWith("https") && client != null
                    && client instanceof CloseableHttpClient) {
                ((CloseableHttpClient) client).close();
            }
        }
        return result;
    }

    private static CloseableHttpClient createSSLInsecureClient()
            throws GeneralSecurityException {
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(
                    null, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(X509Certificate[] chain,
                                                 String authType) throws CertificateException {
                            return true;
                        }
                    }).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslContext, new X509HostnameVerifier() {

                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }

                @Override
                public void verify(String host, SSLSocket ssl)
                        throws IOException {
                }

                @Override
                public void verify(String host, X509Certificate cert)
                        throws SSLException {
                }

                @Override
                public void verify(String host, String[] cns,
                                   String[] subjectAlts) throws SSLException {
                }

            });
            return HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } catch (GeneralSecurityException e) {
            throw e;
        }
    }
    /**
     * 可绕过ssl验证，支持http,https,连接超时时间设置
     * @param url
     * @param body
     * @param charset
     * @param connTimeout
     * @param readTimeout
     * @return
     * @throws ConnectTimeoutException
     * @throws SocketTimeoutException
     * @throws Exception
     */
    public static String post(String url,String body, String charset, Integer connTimeout, Integer readTimeout,HttpProxyConfig proxyConfig)
            throws ConnectTimeoutException, SocketTimeoutException, Exception {
        HttpClient client = null;
        HttpPost post = new HttpPost(url);
        String result = "";
        try {
            StringEntity entity = new StringEntity(body,charset);
            entity.setContentType("application/json;charset=UTF-8");
            post.setEntity(entity);
            // 设置参数
            RequestConfig.Builder customReqConf = RequestConfig.custom();
            if (connTimeout != null && !connTimeout.equals("")) {
                customReqConf.setConnectTimeout(connTimeout);
            }
            if (readTimeout != null && !readTimeout.equals("")) {
                customReqConf.setSocketTimeout(readTimeout);
            }
            //配置代理
            if(proxyConfig!=null&&proxyConfig.isEnableProxy()){
                HttpHost proxyHost=new HttpHost(proxyConfig.getHostName(), proxyConfig.getPort());
                customReqConf.setProxy(proxyHost);
            }
            post.setConfig(customReqConf.build());
            HttpResponse res;
            if (url.startsWith("https")) {
                // 执行 Https 请求.
                client = createSSLInsecureClient();
                res = client.execute(post);
            } else {
                // 执行 Http 请求.
                client = HttpClientUtils.client;
                res = client.execute(post);
            }
            result = IOUtils.toString(res.getEntity().getContent(), charset);
        } finally {
            post.releaseConnection();
            if (url.startsWith("https") && client != null
                    && client instanceof CloseableHttpClient) {
                ((CloseableHttpClient) client).close();
            }
        }
        return result;
    }
    /**
     * post数据指定请求头,支持重试
     * @param url
     * @param bodyStr
     * @param reqHeaders
     * @throws SocketException
     * @throws Exception
     */
    public static String doPostCanRetry(String url, String bodyStr,Map<String,String> reqHeaders,String reqEncode,String resEncode) throws SocketException,HttpException,NoHttpResponseException,Exception {
        return doPostCanRetry(url, bodyStr, reqHeaders, DEFAULT_NO_HTTP_PROXY,reqEncode,resEncode);
    }
    /**
     * post数据指定请求头,支持重试
     * @param url
     * @param bodyStr
     * @param reqHeaders
     * @param proxyConfig
     * @return
     * @throws SocketException
     * @throws Exception
     */
    public static String doPostCanRetry(String url, String bodyStr,Map<String,String> reqHeaders,HttpProxyConfig proxyConfig,String reqEncode,String resEncode) throws SocketException,HttpException,NoHttpResponseException,Exception {
        String response = null;
        AtomicInteger timer = null;
        String localMapKey = url;
        Map<String, Object> map = LOCAL_MAP.get();
        int maxRetryTimes = 5;
        if(map==null){
            map = new HashMap<String, Object>();
            LOCAL_MAP.set(map);
        }
        timer = (AtomicInteger) map.get(localMapKey);
        if(timer==null){
            timer = new AtomicInteger(0);
            map.put(localMapKey,timer);
        }
        try{
            response = doPost(url, bodyStr, reqHeaders, proxyConfig,reqEncode,resEncode);
            timer.set(0);
        }catch(Exception ex){
            if(timer.get()<maxRetryTimes){
                timer.getAndIncrement();
                response = doPost(url, bodyStr, reqHeaders, proxyConfig);
            }else{
                timer.set(0);
                throw ex;
            }
        }
        return response;
    }
    /**
     * post表单提交
     * @param url
     * @param reqHeaders
     * @param params
     * @param proxyConfig
     * @param reqEncode
     * @return
     * @throws SocketException
     * @throws Exception
     */
    public static CloseableHttpResponse doPostFormData(String url,Map<String,String> reqHeaders,Map<String,String> params,HttpProxyConfig proxyConfig,String reqEncode) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httpPost = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,null,Charset.forName(reqEncode));
            if(params!=null){
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    String key = entry.getKey();
                    String val = entry.getValue();
                    Charset reqCharset = Charset.forName(reqEncode);
                    multipartEntity.addPart(key,new StringBody(val, reqCharset));
                }
            }
            httpPost.setEntity(multipartEntity);
            if(reqHeaders!=null){
                Set<Map.Entry<String,String>> entrySet = reqHeaders.entrySet();
                for (Map.Entry<String, String> entry : entrySet) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    httpPost.addHeader(key, value);
                }
            }
            //配置代理
            if(proxyConfig!=null&&proxyConfig.isEnableProxy()){
                HttpHost proxyHost=new HttpHost(proxyConfig.getHostName(), proxyConfig.getPort());
                httpPost.setConfig(RequestConfig.custom().setProxy(proxyHost).build());
            }
            return httpclient.execute(httpPost);
        } catch (Exception e) {
            log.info("[请求地址:"+url+"]-[请求参数："+ JSON.toJSONString(params)+"]-[请求发生错误]",e);
            throw new RuntimeException(e);
        }
    }
    /**
     * post multipart/form-data 表单提交
     * @param url
     * @param bodyParts
     * @param reqEncode
     * @param resEncode
     * @param proxyConfig
     * @return
     * @throws Exception
     */
    public static String doPostMultipart(String url,List<FormBodyPart> bodyParts,String reqEncode,String resEncode,HttpProxyConfig proxyConfig) throws Exception{
        String responseStr = null;
        CloseableHttpResponse response = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httpPost = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,null,Charset.forName(reqEncode));
            httpPost.setEntity(multipartEntity);
            for (int i = 0; i < bodyParts.size(); i++) {
                multipartEntity.addPart(bodyParts.get(i));
            }
            //配置代理
            if(proxyConfig!=null&&proxyConfig.isEnableProxy()){
                HttpHost proxyHost=new HttpHost(proxyConfig.getHostName(), proxyConfig.getPort());
                httpPost.setConfig(RequestConfig.custom().setProxy(proxyHost).build());
            }
            response = httpclient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();;
            if(statusCode!=200){
                throw new HttpException("[请求发生错误，非正常响应状态]-[请求地址:"+url+"]-[响应状态："+statusCode+"]");
            }
            HttpEntity entity = response.getEntity();
            List<String> oneLines=IOUtils.readLines(new BufferedReader(new InputStreamReader(entity.getContent(),resEncode)));
            StringBuffer sb=new StringBuffer();
            for (String oneLine : oneLines) {
                sb.append(oneLine);
            }
            responseStr=sb.toString();
        } catch (SocketException|NoHttpResponseException|HttpException e){
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            log.info("请求地址:{},请求报文：{},返回报文:{}",new Object[]{url,responseStr});
            if (null != response) {
                try {
                    response.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if(null != httpclient ){
                try{
                    httpclient.close();
                }catch(Exception ex){
                    throw new RuntimeException(ex);
                }

            }
        }
        return responseStr;
    }
}
