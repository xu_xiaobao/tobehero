package top.tobehero.xrpc.api.client;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import top.tobehero.xrpc.api.meta.AbstractXRpcMataLoader;
import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.dto.GetOnlineAppReqData;
import top.tobehero.xrpc.api.dto.GetOnlineAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.utils.HttpClientUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XRpcHttpClient extends AbstractXRpcClient {
    public XRpcHttpClient(AbstractXRpcMataLoader xRpcMataLoader){
        super(xRpcMataLoader);
    }
    @Override
    protected RpcBaseRes<List<GetOnlineAppResData>> doGetOnlineAppMeta(List<GetOnlineAppReqData> appMetaList) throws Exception {
        RegistryConfig registry = getXRpcMataLoader().getRegistryConfig();
        String url = registry.getAddress()+"/onlineAppMeta";
        String bodyStr = JSON.toJSONString(appMetaList);
        Map<String,String> reqHeaders = new HashMap<>();
        reqHeaders.put("content-type","application/json");
        String resBody = HttpClientUtils.doPostCanRetry(url, bodyStr, reqHeaders, "UTF-8", "UTF-8");
        return JSON.parseObject(resBody,new TypeReference<RpcBaseRes<List<GetOnlineAppResData>>>(){});
    }
}
