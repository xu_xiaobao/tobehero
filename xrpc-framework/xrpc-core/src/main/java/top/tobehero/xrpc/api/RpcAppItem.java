package top.tobehero.xrpc.api;

import lombok.Data;

@Data
public class RpcAppItem {
    private String namespace;
    private String appName;
    private String instanceId;
    private String endpoint;
}
