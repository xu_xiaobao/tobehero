package top.tobehero.xrpc.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import top.tobehero.xrpc.api.*;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.exception.XRpcException;
import top.tobehero.xrpc.utils.HttpClientUtils;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode(exclude = {"proxyer"})
@Slf4j
public class RpcConsumerAgent implements IClientAgent{
    @JSONField(serialize = false,deserialize=false)
    @Autowired
    private XRpcConsumerProxyer proxyer;
    private String namespace;
    /***
     * 应用名称
     */
    private String appName;
    private String serviceName;
    private int version;
    private Class<?> clazz;
    /**决策应用实例随机**/
    private SecureRandom determinateAppRandom = new SecureRandom();

    /**
     * 暂定协议为：
     * 目标服务地址：注册中心服务暴露地址/名称空间/应用名称/服务名称/方法版本号（可选，默认为1）/方法名称
     * 例如：http://0.0.0.0:8088/xprc/helloService/v1/hello
     * 请求数据格式：未加密前基于json
     * @param method
     * @param arguments
     * @return
     * @throws XRpcException
     */
    @Override
    public Object doInvoke(Method method, Object[] arguments) throws XRpcException {
        //决策服务调用权限：每个客户端会保存一份自己的远程服务访问列表，可以找到即为对目标服务有调用权限
        //决策调用目标地址：一个远程appName可能有多个实例提供服务，需要决策出一个本次调用目标
        String methodName = method.getName();
        Class<?>[] parameterTypes = method.getParameterTypes();
        List<String> paramTypes = null;
        if (parameterTypes!=null){
            paramTypes = new ArrayList<>();
            for (Class<?> type:parameterTypes) {
                paramTypes.add(type.getName());
            }
        }
        List<RpcAppItem> rpcAppItems = this.determinateAppItemList(namespace, appName, serviceName, version, methodName, paramTypes);
        if(rpcAppItems==null||rpcAppItems.size()==0){
            throw XRpcException.of(RpcBaseResType.NO_AVALIABLE_SERVICE);
        }
        //随机决策一个服务实例
        RpcAppItem appItem = rpcAppItems.get(determinateAppRandom.nextInt(rpcAppItems.size()));
        String endpoint = appItem.getEndpoint();
        String requestUrl = endpoint+"/"+serviceName+"/v"+version+"/"+methodName;
        System.out.println("决策请求地址-->"+appItem.getEndpoint());
        //封装请求数据：在这里可以考虑进行额外的数据封装，加密、增加认证信息等
        XRequest xRequest = packageXRequest(methodName,paramTypes,arguments);
        //增加额外头信息填充，允许自定义配置
        //远程调用，解析响应数据
        HashMap<String, String> reqHeaders = new HashMap<>();
        reqHeaders.put("content-type","application/json");
        String reqBody = JSON.toJSONString(xRequest);
        XResponse xResponse = null;
        try {
            String responseString = HttpClientUtils.doPost(requestUrl,reqHeaders, reqBody);
            Assert.isTrue(StringUtils.isNotBlank(responseString),"响应不能为空");
            xResponse = JSON.parseObject(responseString, XResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw XRpcException.of(RpcBaseResType.CALL_ERROR,e);
        }
        String resCode = xResponse.getResCode();
        if(RpcBaseResType.OK.name().equals(resCode)){
            String resBody = xResponse.getResBody();
            Type returnType = method.getGenericReturnType();
            if(returnType instanceof ParameterizedType){
                return JSON.parseObject(resBody,(ParameterizedType)returnType);
            }else{
                return JSON.parseObject(resBody, returnType);
            }
        }else{
            //解析错误状态，抛出异常
            RpcBaseResType[] values = RpcBaseResType.values();
            for (RpcBaseResType item:values) {
                if(item.name().equals(resCode)){
                    throw XRpcException.of(item);
                }
            }
            throw XRpcException.of(RpcBaseResType.UNKNOWN_ERROR);
        }
    }

    /**
     * 创建请求信息
     * @param paramTypes
     * @param arguments
     * @return
     */
    private XRequest packageXRequest(String methodName,List<String> paramTypes, Object[] arguments) {
        XRequest request = new XRequest();
        request.setNamespace(namespace);
        request.setAppName(appName);
        request.setServiceName(serviceName);
        request.setVersion(version);
        request.setMethodName(methodName);
        request.setReqId(UUID.randomUUID().toString());
        List<XRequestParam> reqParams = null;
        if(paramTypes!=null&&paramTypes.size()>0){
            reqParams = new ArrayList<>();
            for (int i = 0; i < arguments.length; i++) {
                String paramType = paramTypes.get(i);
                Object val = arguments[i];
                XRequestParam requestParam = new XRequestParam();
                requestParam.setType(paramType);
                requestParam.setVal(JSON.toJSONString(val));
                reqParams.add(requestParam);
            }
        }
        request.setReqParams(reqParams);
        return request;
    }

    /**
     * 决策远程调用地址
     * @param namespace
     * @param appName
     * @param serviceName
     * @param version
     * @param methodName
     * @param param
     * @return
     */
    public List<RpcAppItem> determinateAppItemList(String namespace, String appName, String serviceName, int version, String methodName, List<String>param){
        List<RpcServiceMethodItem> rpcServiceMethodList = this.proxyer.getXRpcClient().getRpcServiceMethodList();
        if(rpcServiceMethodList==null){
            return null;
        }
        List<RpcAppItem> appItems = null;
        for (int i = 0; i < rpcServiceMethodList.size(); i++) {
            RpcServiceMethodItem rpcServiceMethod = rpcServiceMethodList.get(i);
            if(namespace.equals(rpcServiceMethod.getNamespace())
                && appName.equalsIgnoreCase(rpcServiceMethod.getAppName())
                && serviceName.equalsIgnoreCase(rpcServiceMethod.getServiceName())
                && version == rpcServiceMethod.getVersion()
                && methodName.equalsIgnoreCase(rpcServiceMethod.getMethodMame())
            ){
                if(param!=null){
                    List<String> paramTypes = rpcServiceMethod.getParamTypes();
                    if(paramTypes!=null&&paramTypes.size()==param.size()){
                        boolean isMatch = true;
                        for (int j = 0; j < param.size(); j++) {
                            String tagetType = param.get(j);
                            String paramType = paramTypes.get(j);
                            if(!tagetType.equals(paramType)){
                                isMatch = false;
                                break;
                            }
                        }
                        if(isMatch){
                            appItems = rpcServiceMethod.getAppList();
                            break;
                        }
                    }
                }else if(rpcServiceMethod.getParamTypes()==null){
                    appItems = rpcServiceMethod.getAppList();
                    break;
                }
            }
        }
        return appItems;
    }
}
