package top.tobehero.xrpc.annotation;

import java.lang.annotation.*;

/**
 * RPC服务提供方，可用于接口类
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface XRpcProvider {
    /**
     * ioc bean name
     * @return
     */
    String contextId() default "";

    /**
     * 服务名称
     * @return
     */
    String value() default "";

    /**
     * 接口版本
     * @return
     */
    int version() default 1;
}
