package top.tobehero.xrpc.provider.interceptor.impl;

import org.springframework.core.Ordered;
import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.XResponse;
import top.tobehero.xrpc.provider.AbstractRpcProviderAgent;
import top.tobehero.xrpc.provider.interceptor.RpcProviderAgentInterceptorAdaptor;
import top.tobehero.xrpc.utils.XRpcContext;
import top.tobehero.xrpc.utils.XRpcContextUtils;

/**
 * 请求上下文拦截器
 * 将请求的额外信息放置于上下文内
 */
public class RequestContextAgentInterceptor extends RpcProviderAgentInterceptorAdaptor {
    @Override
    public boolean preHandle(XRequest request, XResponse response, AbstractRpcProviderAgent agent) throws Exception {
        XRpcContext context = new XRpcContext();
        context.setRequest(request);
        XRpcContextUtils.saveCurrentContext(context);
        return true;
    }

    @Override
    public void postHandle(XRequest request, XResponse response, AbstractRpcProviderAgent agent) throws Exception {
        XRpcContextUtils.clearContext();
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
