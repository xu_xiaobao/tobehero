package top.tobehero.xrpc.api.dto;

public enum RpcBaseResType {
    OK("成功"),
    UNKNOWN_ERROR("未知错误"),
    CALL_ERROR("调用错误"),
    NO_AVALIABLE_SERVICE("无可用服务"),
    NOT_FIND_SERVICE("目标服务不存在"),
    PARAMS_ERROR("参数错误"),
    REQUEST_TIMEOUT("请求超时"),
    UNAUTHETICATED("未认证的客户端"),
    UNAUTHORIZED("未授权的服务"),
    ;
    private String msg;

    RpcBaseResType(String resMsg) {
        this.msg = resMsg;
    }

    public String getMsg() {
        return msg;
    }
}
