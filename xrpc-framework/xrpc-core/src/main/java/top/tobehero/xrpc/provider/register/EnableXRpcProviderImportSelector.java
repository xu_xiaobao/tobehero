package top.tobehero.xrpc.provider.register;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ArrayList;
import java.util.List;

/**
 * 自动适配当前环境，选择自动装配注册器
 * 目前支持：Spring Boot以及传统web应用自动装配
 *
 */
public class EnableXRpcProviderImportSelector implements ImportSelector {
    public static final String BOOT_XRPC_PROVIDER_AUTO_CONFIGURATION = "top.tobehero.xrpc.boot.XRpcProviderAutoConfiguration";

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        List<String> importClasses = new ArrayList<>();
        importClasses.add(EnableXRpcProviderImportBeanDefinitionRegistrar.class.getName());
        Class<?> aClass = null;
        try {
            aClass = Class.forName(BOOT_XRPC_PROVIDER_AUTO_CONFIGURATION);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(aClass!=null){
            importClasses.add(BOOT_XRPC_PROVIDER_AUTO_CONFIGURATION);
        }
        return importClasses.toArray(new String[importClasses.size()]);
    }
}
