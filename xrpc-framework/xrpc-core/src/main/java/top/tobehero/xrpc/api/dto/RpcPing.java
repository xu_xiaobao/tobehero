package top.tobehero.xrpc.api.dto;

import lombok.Data;

@Data
public class RpcPing {
    private String namespace;
    private String instanceId;
    private String appName;
}
