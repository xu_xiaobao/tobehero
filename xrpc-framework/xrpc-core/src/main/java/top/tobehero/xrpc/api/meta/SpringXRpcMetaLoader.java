package top.tobehero.xrpc.api.meta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.UUID;

public class SpringXRpcMetaLoader extends AbstractXRpcMataLoader {
    @Autowired
    private Environment env;
    @Override
    public RpcAppMeta resolveRpcAppMeta() {
        String appName = env.getProperty("xrpc.appName");
        String endpoint = env.getProperty("xrpc.endpoint");
        RpcAppMeta appMeta = new RpcAppMeta();
        appMeta.setAppName(appName);
        appMeta.setEndpoint(endpoint);
        appMeta.setInstanceId(UUID.randomUUID().toString());
        return appMeta;
    }
}
