package top.tobehero.xrpc.provider.interceptor;

import org.springframework.core.Ordered;
import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.XResponse;
import top.tobehero.xrpc.exception.XRpcException;
import top.tobehero.xrpc.provider.AbstractRpcProviderAgent;

/**
 * 请求拦截器
 * 注意顺序为：preHandle-->afterCompletion-->agent实际处理-->postHandle
 */
public interface IRpcProviderAgentInterceptor extends Ordered {
    /**
     * 前置处理请求
     * @throws XRpcException
     * @return 返回true,进行执行下一个拦截器
     */
    boolean preHandle(XRequest request, XResponse response, AbstractRpcProviderAgent agent) throws Exception;

    /**
     * 后置处理请求
     * @param request
     * @param response
     * @throws XRpcException
     */
    void postHandle(XRequest request,XResponse response,AbstractRpcProviderAgent agent) throws Exception;

    /**
     * preHandle处理完成后调用
     * @param request
     * @param response
     * @param agent
     * @param ex
     * @throws Exception
     */
    void afterCompletion(XRequest request, XResponse response,AbstractRpcProviderAgent agent, Exception ex) throws Exception;
}
