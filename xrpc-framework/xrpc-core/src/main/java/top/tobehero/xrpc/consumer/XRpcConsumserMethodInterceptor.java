package top.tobehero.xrpc.consumer;

import lombok.Data;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.Assert;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.exception.XRpcException;

import java.lang.reflect.Method;

@Data
public class XRpcConsumserMethodInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object aThis = invocation.getThis();
        Assert.isTrue(aThis instanceof RpcConsumerAgent,"目标对象类型异常，必须为"+RpcConsumerAgent.class.getSimpleName());
        Method method = invocation.getMethod();
        Object[] arguments = invocation.getArguments();
        RpcConsumerAgent agent = (RpcConsumerAgent) aThis;
        Object result = null;
        try {
            result = agent.doInvoke(method,arguments);
        }catch (Exception ex){
            if(ex instanceof XRpcException){
                throw ex;
            }else{
                throw XRpcException.of(RpcBaseResType.UNKNOWN_ERROR,ex);
            }
        }
        return result;
    }
}
