package top.tobehero.xrpc.api.meta;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.lang.reflect.Method;
import java.util.List;

@Data
@EqualsAndHashCode(exclude = {"serviceMeta"})
@ToString(exclude = {"serviceMeta"})
public class RpcMethodMeta {
    @JSONField(serialize = false,deserialize = false)
    private RpcServiceMeta serviceMeta;
    /**
     * 服务方法名称
     */
    private String methodName;
    /**
     * 方法参数元数据列表
     */
    private List<RpcParamMeta> paramListMeta;

    @JSONField(serialize = false,deserialize = false)
    private Method method;
}
