package top.tobehero.xrpc.api.config;

import lombok.Data;

/**
 * 注册中心属性配置
 */
@Data
public class RegistryConfig {
    /**
     * 注册中心地址
     */
    private String address;

    /**
     * 应用所在名称空间，不同名称空间内服务相互隔离
     */
    private String namespace="default";

    /**
     * 是否优先注册IP
     */
    private boolean perfectIpAddress;
}
