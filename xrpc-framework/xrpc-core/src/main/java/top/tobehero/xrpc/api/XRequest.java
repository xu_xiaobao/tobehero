package top.tobehero.xrpc.api;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class XRequest {
    /**请求ID**/
    private String reqId;
    private String namespace;
    private String appName;
    private String serviceName;
    private int version;
    private String methodName;
    /***请求参数列表**/
    private List<XRequestParam> reqParams;
    /**额外的请求头信息**/
    private Map<String,String> extendHeaders;

}
