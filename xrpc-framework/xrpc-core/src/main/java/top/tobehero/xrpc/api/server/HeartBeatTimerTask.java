package top.tobehero.xrpc.api.server;

import lombok.Data;
import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.dto.RegisterAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.api.dto.RpcPing;
import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 心跳任务
 */
@Data
public class HeartBeatTimerTask extends TimerTask {
    private RpcAppMeta rpcAppMeta;
    private Timer timer;
    private boolean isRunning;
    private AbstractXRpcServer xRpcServer;
    @Override
    public void run() {
        if(this.isRunning){
            return ;
        }
        this.isRunning = true;
        boolean reRegister = false;
        RegistryConfig registry = rpcAppMeta.getRegistry();
        String namespace = registry.getNamespace();
        String appName = rpcAppMeta.getAppName();
        String instanceId = rpcAppMeta.getInstanceId();
        RpcPing ping = new RpcPing();
        ping.setNamespace(namespace);
        ping.setAppName(appName);
        ping.setInstanceId(instanceId);
        RpcBaseRes<RegisterAppResData> rpcBaseRes = null;
        try {
            rpcBaseRes = xRpcServer.heartbeat(ping);
            String resCode = rpcBaseRes.getResCode();
            if(RpcBaseResType.OK.name().equals(resCode)){
                this.isRunning = false;
            }else{
                reRegister = true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            reRegister = true;
        }
        if(reRegister){
            //元数据失效，需重新注册
            xRpcServer.registerAppMeta();
        }
    }
}
