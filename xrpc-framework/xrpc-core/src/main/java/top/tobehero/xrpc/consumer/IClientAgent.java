package top.tobehero.xrpc.consumer;

import top.tobehero.xrpc.exception.XRpcException;

import java.lang.reflect.Method;

/**
 * 客户端代理器
 */
public interface IClientAgent {
    /**
     * 调用远程服务方法，返回响应结果
     * @param method
     * @param arguments
     * @return
     */
    Object doInvoke(Method method,Object[] arguments) throws XRpcException;
}
