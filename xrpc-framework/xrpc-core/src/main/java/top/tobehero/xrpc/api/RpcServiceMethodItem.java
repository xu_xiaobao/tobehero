package top.tobehero.xrpc.api;

import lombok.Data;

import java.util.List;

@Data
public class RpcServiceMethodItem {
    private String namespace;
    private String appName;
    private String serviceName;
    private int version;
    private String methodMame;
    /**参数类型数组**/
    private List<String> paramTypes;
    /***在线服务列表*/
    private List<RpcAppItem> appList;
}
