package top.tobehero.xrpc.provider;


import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.XResponse;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.exception.XRpcException;

/**
 * xrpc服务暴露Controller
 */
@Controller
@RequestMapping("/xrpc")
public class XRpcController {
    @Autowired
    private XRpcProviderProxyer proxyer;
    @ResponseBody
    @RequestMapping(value="/{serviceName}/v{version}/{methodName}",method = {RequestMethod.POST,RequestMethod.GET},consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public String endpoint(@PathVariable String serviceName, @PathVariable int version, @PathVariable String methodName, @RequestBody XRequest request){
        XResponse response = null;
        try {
            response = proxyer.invokeService(request);
        }catch (XRpcException ex0){
            String code = ex0.getCode();
            String msg = ex0.getMsg();
            return JSON.toJSONString(XResponse.of(code,msg,request.getReqId(),null));
        }catch (Exception ex){
            return JSON.toJSONString(XResponse.of(RpcBaseResType.UNKNOWN_ERROR,request.getReqId(),null));
        }
        return JSON.toJSONString(response);
    }

    @ResponseBody
    @RequestMapping(value="/hello",method = {RequestMethod.GET})
    public String hello(){
        return "OK";
    }

}
