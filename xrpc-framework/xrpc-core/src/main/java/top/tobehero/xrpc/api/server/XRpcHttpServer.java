package top.tobehero.xrpc.api.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import top.tobehero.xrpc.api.meta.AbstractXRpcMataLoader;
import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.dto.RegisterAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcPing;
import top.tobehero.xrpc.api.meta.RpcAppMeta;
import top.tobehero.xrpc.utils.HttpClientUtils;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class XRpcHttpServer extends AbstractXRpcServer {
    public XRpcHttpServer(AbstractXRpcMataLoader loader){
        super(loader);
    }

    @Override
    protected RpcBaseRes<RegisterAppResData> doRegister(RpcAppMeta appMeta) throws Exception{
        RegistryConfig registry = appMeta.getRegistry();
        String url = registry.getAddress()+"/register";
        String bodyStr = JSON.toJSONString(appMeta);
        Map<String,String> reqHeaders = new HashMap<>();
        reqHeaders.put("content-type","application/json");
        String resBody = HttpClientUtils.doPostCanRetry(url, bodyStr, reqHeaders, "UTF-8", "UTF-8");
        return JSON.parseObject(resBody,new TypeReference<RpcBaseRes<RegisterAppResData>>(){});
    }

    public RpcBaseRes<RegisterAppResData> heartbeat(RpcPing ping)throws Exception {
        RpcAppMeta appMeta = this.getXRpcMataLoader().getProviderAppMeta();
        RegistryConfig registry = appMeta.getRegistry();
        String url = registry.getAddress()+"/ping";
        String bodyStr = JSON.toJSONString(ping);
        Map<String,String> reqHeaders = new HashMap<>();
        reqHeaders.put("content-type","application/json");
        String resBody = HttpClientUtils.doPostCanRetry(url, bodyStr, reqHeaders, "UTF-8", "UTF-8");
        return JSON.parseObject(resBody, RpcBaseRes.class);
    }
}
