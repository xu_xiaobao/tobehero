package top.tobehero.xrpc.provider.register;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import top.tobehero.xrpc.annotation.XRpcProvider;
import top.tobehero.xrpc.provider.RpcProviderAgent;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * 扫描classpath下的XRpcProvider注解，并注册
 */
public class XRpcProviderPathBeanDefinitionScanner extends ClassPathBeanDefinitionScanner{
    /**本应用内XRpc代理Bean**/
    private String xrpcProxyerBeanName = "xRrcProviderProxyer";
    private Class<? extends Annotation> handleAnnotation= XRpcProvider.class;
    private List<String> xrpcServiceNames = new ArrayList<>();
    public XRpcProviderPathBeanDefinitionScanner(BeanDefinitionRegistry registry,boolean useDefaultFilters) {
        super(registry, useDefaultFilters);
    }
    protected void registerFilters(){
        addIncludeFilter(new AnnotationTypeFilter(this.handleAnnotation));
    }
    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        AnnotationMetadata metadata = beanDefinition.getMetadata();
        return metadata.hasAnnotation(this.handleAnnotation.getName());
    }

    @Override
    protected void registerBeanDefinition(BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry) {
        //注册Service原始对象
        BeanDefinition serviceBeanBd = definitionHolder.getBeanDefinition();
        String beanClassName = serviceBeanBd.getBeanClassName();
        Class<?> beanClass = null;
        try {
            beanClass = Class.forName(beanClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("beanClassName 未找到");
        }
        XRpcProvider rpcProvider = beanClass.getAnnotation(XRpcProvider.class);
        String serviceBeanName = rpcProvider.contextId();
        if(StringUtils.isBlank(serviceBeanName)){
            serviceBeanName = beanClassName.substring(beanClassName.lastIndexOf('.')+1);
            serviceBeanName = serviceBeanName.substring(0,1).toLowerCase()+serviceBeanName.substring(1);
        }
        String xrpcServiceName = rpcProvider.value();
        if(StringUtils.isBlank(xrpcServiceName)){
            xrpcServiceName = serviceBeanName;
        }
        int xrpcVersion = rpcProvider.version();
        String existsRpcServiceNameKey = xrpcServiceName + "_" + xrpcVersion;
        if(xrpcServiceNames.contains(existsRpcServiceNameKey)){
            throw new RuntimeException("已经存在一个XRpc服务名称和版本【服务:"+xrpcServiceName+"，版本:"+xrpcVersion+"】,是否需要注入多个版本？");
        }
        registry.registerBeanDefinition(serviceBeanName, serviceBeanBd);
        //注册远程调用Service代理对象
        GenericBeanDefinition agentBd = new GenericBeanDefinition();
        agentBd.setBeanClass(RpcProviderAgent.class);
        MutablePropertyValues propertyValues = agentBd.getPropertyValues();
        propertyValues.add("springBeanName",serviceBeanName);
        propertyValues.add("springBean",serviceBeanBd);
        propertyValues.add("xrpcServiceName", xrpcServiceName);
        registry.registerBeanDefinition("RpcProviderAgent::"+serviceBeanName, agentBd);
        xrpcServiceNames.add(existsRpcServiceNameKey);
    }
}