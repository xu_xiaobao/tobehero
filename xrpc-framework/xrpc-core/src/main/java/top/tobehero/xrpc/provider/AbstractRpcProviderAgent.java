package top.tobehero.xrpc.provider;

import org.springframework.aop.support.AopUtils;
import top.tobehero.xrpc.annotation.XRpcProvider;
import top.tobehero.xrpc.api.XRequest;
import top.tobehero.xrpc.api.meta.RpcMethodMeta;
import top.tobehero.xrpc.api.meta.RpcParamMeta;
import top.tobehero.xrpc.api.meta.RpcServiceMeta;
import top.tobehero.xrpc.exception.XRpcException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务方代理
 */
public abstract class AbstractRpcProviderAgent {
    private String springBeanName;
    private Object springBean;
    private String xrpcServiceName;
    private RpcServiceMeta serviceMeta;
    /**
     * 匹配服务代理对象
     * @param request
     * @return
     */
    public abstract boolean isMatch(XRequest request);

    /**
     * 调用内部服务
     * @param request
     * @return
     */
    public abstract Object doInvoke(XRequest request) throws XRpcException, InvocationTargetException, IllegalAccessException;

    /**
     * 解析自身代理服务元数据
     * @return
     */
    public RpcServiceMeta analysisServiceMeta() {
        if(serviceMeta==null){
            serviceMeta = new RpcServiceMeta();
            serviceMeta.setServiceName(xrpcServiceName);
            XRpcProvider xRpcProvider = springBean.getClass().getAnnotation(XRpcProvider.class);
            int version = 1;
            if(xRpcProvider!=null){
                version = xRpcProvider.version();
            }
            serviceMeta.setVersion(version);
            List<RpcMethodMeta> methodMetaList = analysisServiceMethodMetaList();
            serviceMeta.setMethodMetaList(methodMetaList);
        }
        return serviceMeta;
    }

    private List<RpcMethodMeta> analysisServiceMethodMetaList() {
        Class<?> implClass = this.springBean.getClass();
        boolean aopProxy = AopUtils.isAopProxy(this.springBean);
        if(aopProxy){
            implClass = AopUtils.getTargetClass(springBean);
        }
        Type[] genericInterfaces = implClass.getGenericInterfaces();
        if(genericInterfaces.length==0){
            throw new RuntimeException(implClass.getName()+"【RpcProvider必须实现至少一个接口】");
        }
        List<RpcMethodMeta> list = new ArrayList<>();
        for(Type interfType:genericInterfaces){
            Class<?> interfClass = (Class<?>) interfType;
            System.out.println(interfClass);
            Method[] declaredMethods = interfClass.getDeclaredMethods();
            for(Method method:declaredMethods){
                RpcMethodMeta methodMeta = analysisMethodMeta(method);
                methodMeta.setServiceMeta(serviceMeta);
                list.add(methodMeta);
            }
        }
        return list;
    }
    /**
     * 解析方法元数据
     * @param method
     * @return
     */
    public RpcMethodMeta analysisMethodMeta(Method method) {
        RpcMethodMeta meta = new RpcMethodMeta();
        meta.setMethodName(method.getName());
        meta.setMethod(method);
        List<RpcParamMeta> paramList = getParamMetaList(method.getParameterTypes());
        meta.setParamListMeta(paramList);
        for(RpcParamMeta paramMeta:paramList){
            paramMeta.setMethodMeta(meta);
        }
        return meta;
    }

    private List<RpcParamMeta> getParamMetaList(Class<?>[] parameterTypes) {
        List<RpcParamMeta> list = new ArrayList<>();
        for(Class<?> paramType:parameterTypes){
            RpcParamMeta meta = new RpcParamMeta();
            String name = paramType.getName();
            Class<?> clazz = paramType;
            meta.setType(name);
            meta.setClazz(clazz);
            list.add(meta);
        }
        return list;
    }

    public String getSpringBeanName() {
        return springBeanName;
    }

    public void setSpringBeanName(String springBeanName) {
        this.springBeanName = springBeanName;
    }

    public Object getSpringBean() {
        return springBean;
    }

    public void setSpringBean(Object springBean) {
        this.springBean = springBean;
    }

    public String getXrpcServiceName() {
        return xrpcServiceName;
    }

    public void setXrpcServiceName(String xrpcServiceName) {
        this.xrpcServiceName = xrpcServiceName;
    }

    public RpcServiceMeta getServiceMeta() {
        return serviceMeta;
    }

    public void setServiceMeta(RpcServiceMeta serviceMeta) {
        this.serviceMeta = serviceMeta;
    }
}
