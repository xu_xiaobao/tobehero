package top.tobehero.xrpc.api.meta;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@EqualsAndHashCode(exclude = {"appMeta"})
@ToString(exclude = {"appMeta"})
public class RpcServiceMeta {
    @JSONField(serialize = false,deserialize = false)
    private RpcAppMeta appMeta;
    /**
     * 服务名称
     */
    private String serviceName;
    /**
     * 服务方法元数据
     */
    private List<RpcMethodMeta> methodMetaList;
    /**
     * 服务版本号
     */
    private int version;
}
