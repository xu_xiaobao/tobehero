package top.tobehero.xrpc.consumer;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import top.tobehero.xrpc.api.client.AbstractXRpcClient;

@Slf4j
@Data
public class XRpcConsumerProxyer implements ApplicationContextAware, ApplicationListener<ApplicationEvent> {
    private ApplicationContext ctx;
    @Autowired
    private AbstractXRpcClient xRpcClient;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(event instanceof ContextRefreshedEvent){
            xRpcClient.getOnlineAppMeta();
        }
    }
}
