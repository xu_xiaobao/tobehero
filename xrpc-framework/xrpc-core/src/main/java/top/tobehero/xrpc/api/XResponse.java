package top.tobehero.xrpc.api;

import lombok.Data;
import top.tobehero.xrpc.api.dto.RpcBaseResType;

/***
 * Xrpc远程方法调用响应
 */
@Data
public class XResponse {
    private String resCode;
    private String resMsg;
    private String resId;
    private String resBody;

    public static XResponse of(String code,String msg,String resId,String resBody){
        XResponse response = new XResponse();
        response.setResId(resId);
        response.setResBody(resBody);
        response.setResCode(code);
        response.setResMsg(msg);
        return response;
    }

    public static XResponse of(RpcBaseResType type,String resId,String resBody){
        return of(type.name(),type.getMsg(),resId,resBody);
    }
    public static XResponse of(RpcBaseResType type,String resId){
        return of(type,resId,null);
    }
}
