--
CREATE TABLE xrpc_app(
    id serial NOT NULL,
    namespace VARCHAR(1024),
    app_name VARCHAR(64),
    app_id VARCHAR(64),
    app_secret VARCHAR(128),
    flag VARCHAR(1) DEFAULT 1,
    REVISION INT,
    CREATED_BY VARCHAR(32),
    CREATED_TIME DATE,
    UPDATED_BY VARCHAR(32),
    UPDATED_TIME DATE,
    PRIMARY KEY (id)
);

COMMENT ON TABLE xrpc_app IS '应用配置';
COMMENT ON COLUMN xrpc_app.id IS 'id';
COMMENT ON COLUMN xrpc_app.namespace IS '名称空间';
COMMENT ON COLUMN xrpc_app.app_name IS '应用名称';
COMMENT ON COLUMN xrpc_app.app_id IS '应用ID';
COMMENT ON COLUMN xrpc_app.app_secret IS '应用secret';
COMMENT ON COLUMN xrpc_app.flag IS '有效标识';
COMMENT ON COLUMN xrpc_app.REVISION IS '乐观锁';
COMMENT ON COLUMN xrpc_app.CREATED_BY IS '创建人';
COMMENT ON COLUMN xrpc_app.CREATED_TIME IS '创建时间';
COMMENT ON COLUMN xrpc_app.UPDATED_BY IS '更新人';
COMMENT ON COLUMN xrpc_app.UPDATED_TIME IS '更新时间';