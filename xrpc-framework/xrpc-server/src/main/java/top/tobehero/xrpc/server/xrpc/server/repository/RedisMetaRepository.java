package top.tobehero.xrpc.server.xrpc.server.repository;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis管理元数据Dao
 * redis key设计：
 * 应用key: 前缀+namespace+appName+instanceId 设置过期时间
 */
@Data
@Repository
public class RedisMetaRepository implements MetaRepository {
    public static final String APP_KEY_PREFFIX = "XRPC_APP::";
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public void save(RpcAppMeta appMeta) {
        RegistryConfig registry = appMeta.getRegistry();
        String namespace = registry.getNamespace();
        String instanceId = appMeta.getInstanceId();
        String appName = appMeta.getAppName();
        int expire = appMeta.getExpire();
        String appNameKey = getAppNameKey(namespace, appName, instanceId);
        BoundValueOperations<String, String> appKeyOps = redisTemplate.boundValueOps(appNameKey);
        appKeyOps.set(JSON.toJSONString(appMeta));
        appKeyOps.expire(expire, TimeUnit.SECONDS);
    }

    private String getAppNameKey(String namespace,String appName,String instanceId){
        return APP_KEY_PREFFIX+namespace+"::"+appName+"::"+instanceId;
    }
    @Override
    public List<RpcAppMeta> getAppMetaList(String namespace, String appName) {
        Set<String> keys = redisTemplate.keys(APP_KEY_PREFFIX + namespace + "::" + appName + "::" + "*");
        if(keys==null){
            return new ArrayList<>();
        }
        List<RpcAppMeta> list = new ArrayList<>();
        for(String key : keys){
            String value = redisTemplate.boundValueOps(key).get();
            if(value!=null){
                RpcAppMeta appMeta = JSON.parseObject(value, RpcAppMeta.class);
                list.add(appMeta);
            }
        }
        return list;
    }

    @Override
    public RpcAppMeta getAppMeta(String namespace, String appName, String instanceId) {
        String val = redisTemplate.boundValueOps(getAppNameKey(namespace, appName, instanceId)).get();
        if(StringUtils.isNotBlank(val)){
            return JSON.parseObject(val,RpcAppMeta.class);
        }
        return null;
    }

    @Override
    public void ttlAppMeta(RpcAppMeta appMeta) {
        String namespace = appMeta.getRegistry().getNamespace();
        String appName = appMeta.getAppName();
        String instanceId = appMeta.getInstanceId();
        int expire = appMeta.getExpire();
        BoundValueOperations<String, String> keyOps = redisTemplate.boundValueOps(getAppNameKey(namespace, appName, instanceId));
        keyOps.setIfAbsent(JSON.toJSONString(appMeta));
        keyOps.expire(expire,TimeUnit.SECONDS);
    }
}
