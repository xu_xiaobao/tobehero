package top.tobehero.xrpc.server.xrpc.server.repository;

import lombok.Data;
import top.tobehero.xrpc.api.config.RegistryConfig;
import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.*;

/**
 * 内存元数据仓库
 */
//@Repository
@Data
public class MemeryMetaRepository implements MetaRepository {
    private Map<String, List<RpcAppMeta>> appMap = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void save(RpcAppMeta appMeta) {
        RegistryConfig registry = appMeta.getRegistry();
        String namespace = registry.getNamespace();
        String appName = appMeta.getAppName();
        List<RpcAppMeta> appMetaList = this.getAppMetaList(namespace,appName);
        if (appMetaList == null) {
            appMetaList = new ArrayList<>();
            appMap.put(appName, appMetaList);
        }
        String instanceId = null;
        if(instanceId==null){
            instanceId = UUID.randomUUID().toString();
            appMeta.setInstanceId(instanceId);
        }
        RpcAppMeta existAppMeta = this.getAppMeta(namespace,appName,instanceId);
        //新应用元数据，需保存
        if (existAppMeta == null) {
            existAppMeta = appMeta;
            appMetaList.add(existAppMeta);
        }
        //延长存活时间
        this.ttlAppMeta(appMeta);
    }

    @Override
    public List<RpcAppMeta> getAppMetaList(String namespace, String appName) {
        List<RpcAppMeta> appMetaList = appMap.get(namespace+"::"+appName);
        return appMetaList;
    }

    @Override
    public RpcAppMeta getAppMeta(String namespace, String appName, String instanceId) {
        List<RpcAppMeta> appMetaList = this.getAppMetaList(namespace, appName);
        for(RpcAppMeta meta:appMetaList){
            String instanceId1 = meta.getInstanceId();
            if(instanceId1.equals(instanceId)){
                return meta;
            }
        }
        return null;
    }

    @Override
    public void ttlAppMeta(RpcAppMeta appMeta) {
        //每次上线过期时间延长X秒
        int delaySeconds = 30;
        Calendar nowCal = Calendar.getInstance();
        nowCal.add(Calendar.SECOND, delaySeconds);
        Date expireAt = nowCal.getTime();
        appMeta.setExpireAt(expireAt);
        appMeta.setExpire(delaySeconds);
    }
}
