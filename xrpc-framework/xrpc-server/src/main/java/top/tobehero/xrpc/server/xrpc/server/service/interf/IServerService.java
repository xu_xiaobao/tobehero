package top.tobehero.xrpc.server.xrpc.server.service.interf;

import top.tobehero.xrpc.api.dto.*;
import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.List;

public interface IServerService{
    /**
     * 注册应用实例
     * @param appMeta
     * @return
     */
    RpcBaseRes<RegisterAppResData> registerAppMeta(RpcAppMeta appMeta);

    /**
     * 接收实例心跳
     * @param ping
     * @return
     */
    RpcBaseRes<RegisterAppResData> heartbeat(RpcPing ping);

    /**
     * 获取在线服务列表
     * @param appMetaList
     * @return
     */
    RpcBaseRes<List<GetOnlineAppResData>> onlineAppMeta(List<GetOnlineAppReqData> appMetaList);
}
