package top.tobehero.xrpc.server.xrpc.server.repository;

import top.tobehero.xrpc.api.meta.RpcAppMeta;

import java.util.List;

/**
 * 元数据仓库
 */
public interface MetaRepository {
    /**
     * 添加应用元数据
     *
     * @param appMeta
     * @return
     */
    void save(RpcAppMeta appMeta);

    /**
     * 根据appId查询应用元数据列表
     * @param namespace
     *          名称空间
     * @param appName
     *          应用名称
     * @return
     */
    List<RpcAppMeta> getAppMetaList(String namespace, String appName);

    /**
     * 得到AppMeta
     * @param namespace
     * @param appName
     * @param instanceId
     * @return
     */
    RpcAppMeta getAppMeta(String namespace,String appName,String instanceId);
    /**
     * 延期元数据存活时间
     */
    void ttlAppMeta(RpcAppMeta appMeta);
}
