package top.tobehero.xrpc.server.xrpc.server.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.xrpc.api.dto.GetOnlineAppReqData;
import top.tobehero.xrpc.api.dto.GetOnlineAppResData;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcPing;
import top.tobehero.xrpc.api.meta.RpcAppMeta;
import top.tobehero.xrpc.server.xrpc.server.service.interf.IServerService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/")
public class ServerController{
    @Resource
    private IServerService iServerService;

    private Map<String, AtomicInteger> countMap = new HashMap<>();
    @PostMapping(value = "/register", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public RpcBaseRes registerAppMeta(@RequestBody RpcAppMeta appMeta) {
        return iServerService.registerAppMeta(appMeta);
    }

    @PostMapping(value = "/ping", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public RpcBaseRes heartbeat(@RequestBody RpcPing ping) {
//        String instanceId = ping.getInstanceId();
//        AtomicInteger atomicInteger = countMap.get(instanceId);
//        if(atomicInteger==null){
//            atomicInteger = new AtomicInteger();
//            countMap.put(instanceId,atomicInteger);
//        }
//        int i = atomicInteger.incrementAndGet();
//        if(i>10){
//            countMap.put(instanceId,null);
//            return RpcBaseRes.of("-2","元数据未找到");
//        }
        return iServerService.heartbeat(ping);
    }

    @RequestMapping(value = "/onlineAppMeta", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public RpcBaseRes<List<GetOnlineAppResData>> onlineAppMeta(@RequestBody List<GetOnlineAppReqData> appMetaList){
       return iServerService.onlineAppMeta(appMetaList);
    }

}
