package top.tobehero.xrpc.server.xrpc.server.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import top.tobehero.xrpc.api.dto.*;
import top.tobehero.xrpc.api.meta.RpcAppMeta;
import top.tobehero.xrpc.api.meta.RpcServiceMeta;
import top.tobehero.xrpc.server.xrpc.server.repository.MetaRepository;
import top.tobehero.xrpc.server.xrpc.server.service.interf.IServerService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ServerService implements IServerService {
    @Resource
    private MetaRepository metaRepository;

    @Override
    public RpcBaseRes<RegisterAppResData> registerAppMeta(RpcAppMeta appMeta) {
        metaRepository.save(appMeta);
        int expire = appMeta.getExpire();
        Date expireAt = appMeta.getExpireAt();
        RegisterAppResData data = new RegisterAppResData();
        data.setExpire(expire);
        data.setExpireAt(expireAt);
        return RpcBaseRes.of(RpcBaseResType.OK, data);
    }

    @Override
    public RpcBaseRes<RegisterAppResData> heartbeat(RpcPing ping) {
        String instanceId = ping.getInstanceId();
        String namespace = ping.getNamespace();
        String appName = ping.getAppName();
        if (StringUtils.isBlank(instanceId)) {
            return RpcBaseRes.of("-1", "instanceId不能为空");
        }
        if (StringUtils.isBlank(appName)) {
            return RpcBaseRes.of("-1", "appName不能为空");
        }
        RpcAppMeta appMeta = metaRepository.getAppMeta(namespace,appName,instanceId);
        if (appMeta == null) {
            return RpcBaseRes.of("-2", "未查询到应用元数据");
        }
        //延期元数据存活时间
        metaRepository.ttlAppMeta(appMeta);
        return RpcBaseRes.of(RpcBaseResType.OK);
    }

    @Override
    public RpcBaseRes<List<GetOnlineAppResData>> onlineAppMeta(List<GetOnlineAppReqData> appMetaList) {
        List<GetOnlineAppResData> resList = new ArrayList<>();
        for (GetOnlineAppReqData reqData:appMetaList) {
            String appName = reqData.getAppName();
            String namespace = reqData.getNamespace();
            List<RpcAppMeta> list = metaRepository.getAppMetaList(namespace,appName);
            if(list!=null){
                for (RpcAppMeta remoteAppMeta:list) {
                    GetOnlineAppResData resData = new GetOnlineAppResData();
                    resData.setNamespace(namespace);
                    resData.setAppName(appName);
                    String instanceId = remoteAppMeta.getInstanceId();
                    String endpoint = remoteAppMeta.getEndpoint();
                    resData.setInstanceId(instanceId);
                    resData.setEndpoint(endpoint);
                    List<RpcServiceMeta> serviceMetaList = remoteAppMeta.getServiceMetaList();
                    List<RpcServiceMeta> resServiceMetaList = new ArrayList<>();
                    resData.setServiceMetaList(resServiceMetaList);
                    for(RpcServiceMeta needMeta:reqData.getServiceMetaList()){
                        String targetServiceName = needMeta.getServiceName();
                        int targetServiceVersion = needMeta.getVersion();
                        for (int i = 0; i < serviceMetaList.size(); i++) {
                            RpcServiceMeta existsMeta = serviceMetaList.get(i);
                            if(existsMeta.getServiceName().equals(targetServiceName)&&existsMeta.getVersion()==targetServiceVersion){
                                resServiceMetaList.add(existsMeta);
                                break;
                            }
                        }
                    }
                    resList.add(resData);
                }
            }
        }
        return RpcBaseRes.of(RpcBaseResType.OK,resList);
    }
}
