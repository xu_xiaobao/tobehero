package top.tobehero.xrpc.xrpc.demo.provider.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import top.tobehero.xrpc.annotation.XRpcProvider;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.api.dto.RpcBaseResType;
import top.tobehero.xrpc.xrpc.demo.provider.repository.HelloRepository;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.HelloReq;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.HelloRes;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloService;

import java.util.ArrayList;
import java.util.List;

@XRpcProvider()
public class HelloService implements IHelloService {
    @Autowired
    private HelloRepository helloRepository;
    @Override
    public String sayHello(String name) {
        System.out.println("helloRepository:"+helloRepository);
        return "您好，"+name+"。";
    }

    @Override
    public String sayHello2(String name) {
        return "您好，"+name+"。我是Hello2";
    }

    @Override
    public RpcBaseRes sayHello3(String name, Integer version) {
        String msg = "您好，" + name + "。version:"+version;
        return RpcBaseRes.of(RpcBaseResType.OK,msg);
    }

    @Override
    public HelloRes sayHello(HelloReq req) {
        HelloRes helloRes = new HelloRes();
        helloRes.setName(req.getName());
        helloRes.setAge(req.getAge());
        return helloRes;
    }

    @Override
    public List<HelloRes> sayHelloList() {
        List<HelloRes> res = new ArrayList<>();
        HelloRes e1 = new HelloRes();
        e1.setName("张三");
        e1.setAge(18);
        res.add(e1);

        HelloRes e2 = new HelloRes();
        e2.setName("李四");
        e2.setAge(18);
        res.add(e2);
        return res;
    }

    @Override
    public List<HelloRes> sayHelloList(List<HelloReq> reqs) {
        List<HelloRes> list = new ArrayList<>();
        for (int i = reqs.size()-1;i>=0;i--) {
            HelloReq req = reqs.get(i);
            HelloRes res= new HelloRes();
            res.setName(req.getName());
            res.setAge(req.getAge());
            list.add(res);
        }
        return list;
    }
}
