package top.tobehero.xrpc.xrpc.demo.provider.service.impl;

import top.tobehero.xrpc.annotation.XRpcProvider;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloAService;

@XRpcProvider(value = "helloAService",version = 2)
public class HelloAServiceV2 implements IHelloAService {
    @Override
    public String sayHello(String name) {
        return "hello "+name+",I am V2";
    }

    @Override
    public String helloNoArgs() {
        return "Hi! Anonymous!!!";
    }
}
