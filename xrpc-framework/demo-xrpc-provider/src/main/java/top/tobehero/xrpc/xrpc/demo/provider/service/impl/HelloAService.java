package top.tobehero.xrpc.xrpc.demo.provider.service.impl;

import top.tobehero.xrpc.annotation.XRpcProvider;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloAService;

@XRpcProvider
public class HelloAService implements IHelloAService{
    @Override
    public String sayHello(String name) {
        return "您好，"+name+"。";
    }

    @Override
    public String helloNoArgs() {
        return null;
    }
}
