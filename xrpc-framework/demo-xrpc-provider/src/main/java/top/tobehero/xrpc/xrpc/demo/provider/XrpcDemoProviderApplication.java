package top.tobehero.xrpc.xrpc.demo.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import top.tobehero.xrpc.annotation.EnableXRpcProvider;

@SpringBootApplication
@EnableXRpcProvider(
        appId="xrpc-demo-provider",
        basePackages = "top.tobehero.xrpc.xrpc.demo.provider")
public class XrpcDemoProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(XrpcDemoProviderApplication.class, args);
    }


}
