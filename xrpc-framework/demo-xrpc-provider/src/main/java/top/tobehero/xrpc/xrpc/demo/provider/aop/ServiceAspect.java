package top.tobehero.xrpc.xrpc.demo.provider.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceAspect {
    /**
     * 定义切入点，切入点为com.example.demo.aop.AopController中的所有函数
     *通过@Pointcut注解声明频繁使用的切点表达式
     */
    @Pointcut("execution(public * top.tobehero.xrpc.xrpc.demo.provider.service.impl.*Service.*(..)))")
    public void ServiceAspect(){

    }

    /**
     * @description  在连接点执行之前执行的通知
     */
    @Before("ServiceAspect()")
    public void doBeforeService(){
        System.out.println("service 运行前执行！");
    }
}
