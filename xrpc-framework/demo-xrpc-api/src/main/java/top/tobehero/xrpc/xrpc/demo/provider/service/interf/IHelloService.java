package top.tobehero.xrpc.xrpc.demo.provider.service.interf;

import top.tobehero.xrpc.annotation.XRpcConsumer;
import top.tobehero.xrpc.api.dto.RpcBaseRes;

import java.util.List;

/**
 * 提供远程服务Demo
 */
@XRpcConsumer(appName = "demo-xrpc-provider")
public interface IHelloService {
    /**
     * 向name打招呼
     * @param name
     * @return
     */
    String sayHello(String name);

    /**
     * 向name打招呼
     * @param name
     * @return
     */
    String sayHello2(String name);

    /**
     * 向name打招呼
     * @param name
     * @return
     */
    RpcBaseRes sayHello3(String name, Integer version);

    HelloRes sayHello(HelloReq req);

    List<HelloRes> sayHelloList();

    List<HelloRes> sayHelloList(List<HelloReq> reqs);

}
