package top.tobehero.xrpc.xrpc.demo.provider.service.interf;

import lombok.Data;

@Data
public class HelloRes {
    private String name;
    private Integer age;
}
