package top.tobehero.xrpc.xrpc.demo.provider.service.interf;

import top.tobehero.xrpc.annotation.XRpcConsumer;

/**
 * 提供远程服务Demo
 */
@XRpcConsumer(appName = "demo-xrpc-provider",version = 2)
public interface IHelloAService {
    /**
     * 向name打招呼
     * @param name
     * @return
     */
    String sayHello(String name);

    String helloNoArgs();
}
