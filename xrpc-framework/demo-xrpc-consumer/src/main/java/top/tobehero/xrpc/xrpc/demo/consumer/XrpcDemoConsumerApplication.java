package top.tobehero.xrpc.xrpc.demo.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.tobehero.xrpc.annotation.EnableXRpcConsumer;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloAService;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloService;

@SpringBootApplication
@EnableXRpcConsumer(consumers = {IHelloService.class, IHelloAService.class})
public class XrpcDemoConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(XrpcDemoConsumerApplication.class, args);
    }
    @Configuration
    public class WebAppConfigurer implements WebMvcConfigurer {

        @Override
        public void addInterceptors(InterceptorRegistry registry) {
            // 可添加多个
            registry.addInterceptor(new TestRequestInterceptor()).addPathPatterns("/**");
        }
    }


}
