package top.tobehero.xrpc.xrpc.demo.consumer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobehero.xrpc.api.dto.RpcBaseRes;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.HelloReq;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.HelloRes;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloAService;
import top.tobehero.xrpc.xrpc.demo.provider.service.interf.IHelloService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class HelloController {
    @Resource
    private IHelloService helloService;
    @Resource
    private IHelloAService iHelloAService;
    @RequestMapping("/helloNoArgs")
    public String helloNoArgs(){
        return iHelloAService.helloNoArgs();
    }
    @RequestMapping("/hello")
    public RpcBaseRes hello(String name){
        return helloService.sayHello3(name,1000);
    }

    @RequestMapping("/hello2")
    public HelloRes hello2(String name, Integer age){
        HelloReq req = new HelloReq();
        req.setName(name);
        req.setAge(age);
        return helloService.sayHello(req);
    }
    @RequestMapping("/hello3")
    public List<HelloRes> hello3(){
        return helloService.sayHelloList();
    }

    @RequestMapping("/hello4")
    public List<HelloRes> hello4(){
        List<HelloReq> reqList = new ArrayList<>();
        HelloReq e1 = new HelloReq();
        e1.setName("张三");
        e1.setAge(18);
        reqList.add(e1);

        HelloReq e2 = new HelloReq();
        e2.setName("李四");
        e2.setAge(18);
        reqList.add(e2);
        List<HelloRes> res = helloService.sayHelloList(reqList);
        return res;
    }


}
