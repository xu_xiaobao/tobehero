package top.tobehero.xrpc.boot;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.tobehero.xrpc.api.meta.AbstractXRpcMataLoader;
import top.tobehero.xrpc.api.client.AbstractXRpcClient;
import top.tobehero.xrpc.api.client.XRpcHttpClient;
import top.tobehero.xrpc.consumer.XRpcConsumerProxyer;
import top.tobehero.xrpc.consumer.XRpcConsumserMethodInterceptor;

/**
 * 使用于SpringBoot的自动配置
 */
@Configuration
public class XRpcConsumerAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(AbstractXRpcMataLoader.class)
    public AbstractXRpcMataLoader xRpcMetaLoader(){
        return new SpringBootXRpcMetaLoader();
    }

    @Bean
    public AbstractXRpcClient xRpcClient(){
        AbstractXRpcClient xRpcClientx = new XRpcHttpClient(xRpcMetaLoader());
        return  xRpcClientx;
    }

    @Bean
    public XRpcConsumerProxyer xRpcConsumerProxyer(){
        return new XRpcConsumerProxyer();
    }

    @Bean
    public XRpcConsumserMethodInterceptor xRpcConsumserMethodInterceptor(){
        return new XRpcConsumserMethodInterceptor();
    }
}
