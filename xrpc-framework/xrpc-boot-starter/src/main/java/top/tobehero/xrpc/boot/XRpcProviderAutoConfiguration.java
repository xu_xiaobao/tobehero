package top.tobehero.xrpc.boot;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.tobehero.xrpc.api.meta.AbstractXRpcMataLoader;
import top.tobehero.xrpc.api.server.AbstractXRpcServer;
import top.tobehero.xrpc.api.server.XRpcHttpServer;
import top.tobehero.xrpc.provider.XRpcController;
import top.tobehero.xrpc.provider.XRpcProviderProxyer;

/**
 * 使用于SpringBoot的自动配置
 */
@Configuration
public class XRpcProviderAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(AbstractXRpcMataLoader.class)
    public AbstractXRpcMataLoader xRpcMetaLoader(){
        return new SpringBootXRpcMetaLoader();
    }

    @Bean
    public AbstractXRpcServer xRpcServer(){
        XRpcHttpServer xRpcServer = new XRpcHttpServer(xRpcMetaLoader());
        return  xRpcServer;
    }

    @Bean
    public XRpcProviderProxyer xRpcProviderProxyer(){
        return new XRpcProviderProxyer();
    }

    @Bean
    public XRpcController xrpcController(){
        return new XRpcController();
    }
}
