package top.tobehero.xrpc.boot;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import top.tobehero.xrpc.api.meta.RpcAppMeta;
import top.tobehero.xrpc.api.meta.SpringXRpcMetaLoader;

public class SpringBootXRpcMetaLoader extends SpringXRpcMetaLoader {
    @Value("${server.port:8080}")
    private int port;
    @Value("${server.address:''}")
    private String address;
    @Value("${server.servlet.context-path:'/'}")
    private String contextPath;
    @Value("${server.ssl.enabled:false}")
    private boolean sslEnabled;
    @Value("${spring.application.name}")
    private String appName;

    @Override
    public RpcAppMeta resolveRpcAppMeta() {
        RpcAppMeta rpcAppMeta = super.resolveRpcAppMeta();
        String endpoint = rpcAppMeta.getEndpoint();
        String appName = rpcAppMeta.getAppName();
        if(StringUtils.isBlank(appName)){
            rpcAppMeta.setAppName(this.appName);
        }
        if(StringUtils.isBlank(endpoint)){
            endpoint = this.resolveRpcEndpoint();
            rpcAppMeta.setEndpoint(endpoint);
        }
        return rpcAppMeta;
    }
    /**
     * 解析当前服务注册服务地址
     * @return
     */
    private String resolveRpcEndpoint() {
        String endpoint = (this.sslEnabled?"https":"http")+"://"+this.address +":"+this.port+this.contextPath+"/xrpc";
        return endpoint;
    }
}
