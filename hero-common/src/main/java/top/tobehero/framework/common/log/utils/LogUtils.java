package top.tobehero.framework.common.log.utils;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

public class LogUtils {
    private static final NoArgGenerator uuids = Generators.randomBasedGenerator();
    public static class Constants{
        /**日志跟踪ID**/
        public static final String MDC_TRACE_ID = "trace_id";
        public static final String HTTP_HEADER_TRACE_ID = "x-trace-id";

    }
    public static String generateTraceId(){
        return uuids.generate().toString();
    }
}
