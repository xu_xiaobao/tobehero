package top.tobehero.framework.common.exception;

import lombok.Getter;
import top.tobehero.framework.common.utils.ResponseUtils;

@Getter
public class BusinessException extends RuntimeException {
    private String errcode;
    private String errmsg;
    private Exception exception;
    private Object data;

    public BusinessException(String errcode, String errmsg, Exception exception) {
        super(errmsg, exception);
        this.errcode = errcode;
        this.errmsg = errmsg;
        this.exception = exception;
    }

    public BusinessException(String errcode, String errmsg) {
        this(errcode, errmsg, null);
    }

    public BusinessException(String errmsg) {
        this(ResponseUtils.ResponseEnum.UNKNOWN_ERROR.getErrcode(), errmsg);
    }

    public static BusinessException of(String errcode, String errmsg, Exception exception, Object data) {
        BusinessException businessException = new BusinessException(errcode, errmsg, exception);
        businessException.data = data;
        return businessException;
    }

    public static BusinessException of(String errcode, String errmsg, Object data) {
        return of(errcode, errmsg, null, data);
    }

    public static BusinessException of(Exception exception, Object data) {
        ResponseUtils.ResponseEnum unknownError = ResponseUtils.ResponseEnum.UNKNOWN_ERROR;
        return of(unknownError.getErrcode(), unknownError.getErrmsg(), exception, data);
    }

    public static BusinessException of(Object data) {
        return of(null, data);
    }

}
