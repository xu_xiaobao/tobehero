package top.tobehero.framework.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseRes<D> {
    private String errcode;
    private String errmsg;
    private D data;

}
