package top.tobehero.framework.common.log.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import top.tobehero.framework.common.log.utils.LogUtils;

import static top.tobehero.framework.common.log.utils.LogUtils.Constants.HTTP_HEADER_TRACE_ID;

@Component
public class TraceLogTnterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        if (!template.headers().containsKey(HTTP_HEADER_TRACE_ID)) {
            String traceId = MDC.get(LogUtils.Constants.MDC_TRACE_ID);
            if(StringUtils.isBlank(traceId)){
                traceId = LogUtils.generateTraceId();
            }
            template.header(HTTP_HEADER_TRACE_ID, traceId);
        }
    }
}
