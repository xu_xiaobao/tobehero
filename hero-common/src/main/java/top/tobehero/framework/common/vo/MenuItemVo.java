package top.tobehero.framework.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class MenuItemVo {
    private String parentCode;
    private String menuCode;
    private String menuName;
    private String icon;
    private Integer displayNo;
    private String path;
    private List<MenuItemVo> children;
}
