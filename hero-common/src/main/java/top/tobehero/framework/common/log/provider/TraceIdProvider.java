package top.tobehero.framework.common.log.provider;

import ch.qos.logback.classic.spi.ILoggingEvent;
import com.fasterxml.jackson.core.JsonGenerator;
import net.logstash.logback.composite.JsonWritingUtils;
import net.logstash.logback.composite.loggingevent.UuidProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import top.tobehero.framework.common.log.utils.LogUtils;

import java.io.IOException;

public class TraceIdProvider extends UuidProvider {
    private static final String FIELD_NAME = LogUtils.Constants.MDC_TRACE_ID;

    public TraceIdProvider(){
        setFieldName(FIELD_NAME);
    }

    @Override
    public void writeTo(JsonGenerator generator, ILoggingEvent iLoggingEvent) throws IOException {
        String oldTraceId = MDC.get(FIELD_NAME);
        String newTraceId = null;
        if(StringUtils.isBlank(oldTraceId)){
            newTraceId = LogUtils.generateTraceId();
            MDC.put(FIELD_NAME,newTraceId);
        }else{
            newTraceId = oldTraceId;
        }
        JsonWritingUtils.writeStringField(generator, getFieldName(), newTraceId);
    }
}
