package top.tobehero.framework.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HeroVo {
//    introduction: 'I am a super administrator',
//    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
//    name: 'Super Admin'
    private String name;
    private String avatar;
    private String introduction;
}
