package top.tobehero.framework.common.utils;

import top.tobehero.framework.common.base.BaseRes;

public class ResponseUtils {
    public static enum ResponseEnum{
//        50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
        SUCCESS("SUCCESS","成功"),
        INVALID_TOKEN("INVALID_TOKEN","无效token"),
        UNAUTHETICATED("UNAUTHETICATED","未认证"),
        UNAUTHORIZED("UNAUTHORIZED","未授权"),
        UNKNOWN_ERROR("UNKNOWN_ERROR","系统异常");

        private String errcode;
        private String errmsg;
        ResponseEnum(String errcode,String errmsg){
            this.errcode = errcode;
            this.errmsg = errmsg;
        }

        public String getErrcode() {
            return errcode;
        }

        public void setErrcode(String errcode) {
            this.errcode = errcode;
        }

        public String getErrmsg() {
            return errmsg;
        }

        public void setErrmsg(String errmsg) {
            this.errmsg = errmsg;
        }

    }

    public static <D> BaseRes<D> outEnumResp(ResponseEnum respEnum,D data){
        BaseRes<D> res = new BaseRes<>();
        res.setErrcode(respEnum.errcode);
        res.setErrmsg(respEnum.errmsg);
        res.setData(data);
        return res;
    }
    public static <D> BaseRes<D> outEnumResp(ResponseEnum respEnum){
        return outEnumResp(respEnum,null);
    }
    public static <D> BaseRes<D> outSuccessResp(D data){
       return outEnumResp(ResponseEnum.SUCCESS,data);
    }
    public static BaseRes outSuccessResp(){
        return outSuccessResp(null);
    }

    public static <D> BaseRes<D> outErrorResp(D data){
        return outEnumResp(ResponseEnum.UNKNOWN_ERROR,data);
    }
    public static BaseRes outErrorResp(){
        return outErrorResp(null);
    }
    public static <D> BaseRes<D> outErrorResp(String errcode,String errmsg,D data){
        BaseRes<D> res = new BaseRes<>();
        res.setErrcode(errcode);
        res.setErrmsg(errmsg);
        res.setData(data);
        return res;
    }
    public static <D> BaseRes<D> outErrorResp(String errcode,String errmsg){
        return outErrorResp(errcode,errmsg,null);
    }
}
