package top.tobehero.framework.common.log.filter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;
import top.tobehero.framework.common.log.utils.LogUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static top.tobehero.framework.common.log.utils.LogUtils.Constants.HTTP_HEADER_TRACE_ID;

public class TraceLogFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String traceId = request.getHeader(HTTP_HEADER_TRACE_ID);
        if(StringUtils.isBlank(traceId)){
             traceId = LogUtils.generateTraceId();
        }
        MDC.put(LogUtils.Constants.MDC_TRACE_ID,traceId);
        filterChain.doFilter(request,response);
    }
}
