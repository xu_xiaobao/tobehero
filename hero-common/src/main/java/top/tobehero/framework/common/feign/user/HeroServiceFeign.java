package top.tobehero.framework.common.feign.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import top.tobehero.framework.common.base.BaseRes;
import top.tobehero.framework.common.vo.HeroVo;
import top.tobehero.framework.common.vo.MenuItemVo;

import java.util.List;
import java.util.Map;

/**
 * token服务Feign
 */
@FeignClient("hero-user-manage")
@RequestMapping("/hero")
@ResponseBody
public interface HeroServiceFeign {

    /**
     * 获取token值
     * @return
     */
    @PostMapping("/getToken")
    String getToken();

    /**
     * 登录
     * @return
     */
    @PostMapping(value="/online",consumes = {MediaType.APPLICATION_JSON_VALUE})
    BaseRes online(@RequestBody  Map<String, String> params);

    /**
     * 获取用户信息
     * @return
     */
    @PostMapping(value="/info")
    BaseRes<HeroVo> info(@RequestParam String token);

    /**
     * 获取菜单信息
     * @return
     */
    @PostMapping(value="/menuInfo")
    BaseRes<List<MenuItemVo>> menuInfo(@RequestParam  String sysCode);
    
    /**
     * 英雄下线
     * @return
     */
    @PostMapping(value="/offline")
    BaseRes offline();
    /**
     * 验证token是否认证
     * @return
     */
    @PostMapping("/isAuthed")
    boolean isAuthed(@RequestParam String token);

    /**
     * 验证token是否有资源操作权限
     * @param token
     *          令牌
     * @param sysName
     *          系统名称
     * @param path
     *          访问资源地址
     * @return
     */
    @PostMapping("/isAuthorized")
    boolean isAuthorized(@RequestParam String token,@RequestParam String sysName, @RequestParam String path);

}
