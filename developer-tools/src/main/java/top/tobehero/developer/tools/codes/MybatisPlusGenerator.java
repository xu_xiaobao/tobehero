package top.tobehero.developer.tools.codes;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.ConstVal;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.builder.Entity;
import com.baomidou.mybatisplus.generator.config.builder.Mapper;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import static com.baomidou.mybatisplus.generator.config.rules.DateType.ONLY_DATE;

/**
 * 代码生成器
 */
public class MybatisPlusGenerator {
    private static final String[] tableNames = {"demo_form"};
    private static final String formatMapperFileName = "%sMapper";
    private static final String formatXmlFileName = "%sMapper";
    private static final String formatEntityFileName = "%s";
    private static final String jdbcUrl = "jdbc:mysql://192.168.100.101:3306/hero";
    private static final String username = "root";
    private static final String password = "root123";
    private static final String authorName = "xuxiaobao";
    /**
     * 顶级包名
     */
    private static final String packageName = "top.tobehero.usermanage";
    /**
     * 子模块名称
     */
    private static final String moduleName = "";
    /**
     * 指定输出目录
     */
    private static final String outputPath = "D:\\codeGen";
//    private static final String xmlOutputPath = outputPath + File.separator + "xml";


    public static void main(String[] args) {
        FastAutoGenerator.create(jdbcUrl, username, password)
                .globalConfig(builder -> {
                    builder.author(authorName) // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .dateType(ONLY_DATE) // 只使用 java.util.date 代替
                            .fileOverride() // 覆盖已生成文件
                            .outputDir(outputPath)// 指定输出目录
                    ;

                })
                .packageConfig(builder -> {
                    builder.parent(packageName) // 设置父包名
                            .moduleName(moduleName) // 设置父包模块名
                    // 设置mapperXml生成路径
//                            .pathInfo(Collections.singletonMap(OutputFile.xml, xmlOutputPath))
                    ;
                })
                .strategyConfig(builder -> {
                    StrategyConfig.Builder strategyConfigBuilder = builder
                            // 设置需要生成的表名
//                            .addTablePrefix("t_", "c_")// 设置过滤表前缀
                            .addInclude(tableNames);
                    configEntityBuilder(strategyConfigBuilder.entityBuilder());
                    configMapperBuilder(strategyConfigBuilder.mapperBuilder());
                    ;
                })
                .templateConfig((builder -> {
                    final String TEMPLATE_PREFIX = "/mybatis-plus/generator";
                    builder
                            .controller(TEMPLATE_PREFIX + ConstVal.TEMPLATE_CONTROLLER)
                            .service(TEMPLATE_PREFIX + ConstVal.TEMPLATE_SERVICE)
                            .serviceImpl(TEMPLATE_PREFIX + ConstVal.TEMPLATE_SERVICE_IMPL)
                            .mapper(TEMPLATE_PREFIX + ConstVal.TEMPLATE_MAPPER)
                            .xml(TEMPLATE_PREFIX + ConstVal.TEMPLATE_XML)
                            .entity(TEMPLATE_PREFIX + ConstVal.TEMPLATE_ENTITY_JAVA)
                            .disable(TemplateType.CONTROLLER, TemplateType.SERVICE)

                    ;
                }))
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

    private static void configMapperBuilder(Mapper.Builder mapperBuilder) {
        mapperBuilder
                .fileOverride()
                .formatMapperFileName(formatMapperFileName)
                .formatXmlFileName(formatXmlFileName)
        ;
    }

    private static void configEntityBuilder(Entity.Builder entityBuilder) {
        entityBuilder.fileOverride()
                .enableLombok()
                .formatFileName(formatEntityFileName);
    }


}
