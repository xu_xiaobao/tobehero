package top.tobehero.framework.log.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadTest02 implements Runnable{
    private static Logger log = LoggerFactory.getLogger(ThreadTest02.class);
    @Override
    public void run() {
        for(int i=0;i<10;i++){
            log.info("【2】测试输出日志-->"+i);
        }
    }
}
