package top.tobehero.framework.log;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.tobehero.framework.log.threads.ThreadTest01;
import top.tobehero.framework.log.threads.ThreadTest02;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LogbackStudyTests {
    private static final Logger logger = LoggerFactory.getLogger(LogbackStudyTests.class);
    @Test
    public void testHelloWorld() throws IOException {
        logger.info("logback hello world 1");
        logger.info("logback hello world 2");
        System.out.println("输出完毕");
        System.in.read();
    }
    private ExecutorService executors = Executors.newFixedThreadPool(2);
    @Test
    public void testMultiThreadLog() throws Exception{
        Thread thread1 = new Thread(new ThreadTest01());
        thread1.start();
        Thread thread2 = new Thread(new ThreadTest02());
        thread2.start();
        System.in.read();
    }
    @Test
    public void testExceptionPrint() throws Exception{
        try{
            int i = 1/0;
        }catch (Exception ex){
            logger.error("[发生错误]",ex);
        }
        System.in.read();
    }

}
