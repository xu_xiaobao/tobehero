package top.tobehero.framework.log.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadTest01 implements Runnable{
    private static Logger log = LoggerFactory.getLogger(ThreadTest01.class);
    @Override
    public void run() {
        for(int i=0;i<10;i++){
            log.info("【1】测试输出日志-->"+i);
        }
    }
}
