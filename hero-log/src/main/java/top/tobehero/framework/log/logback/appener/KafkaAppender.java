package top.tobehero.framework.log.logback.appener;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.MDC;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Properties;

/**
 * 日志输出到kafka
 *
 */

public class KafkaAppender extends RollingFileAppender<ILoggingEvent> {
    private KafkaProducer<String,String> kafkaProducer;
    private String topicName="app-log";
    private String bootStrapServers = "192.168.100.101:9093";
    private String acks = "all";
    private int batchSize=16384;
    private long lingerMs=5;
    public KafkaAppender(){
        Properties props = new Properties();
        props.put("bootstrap.servers", bootStrapServers);
        props.put("acks", acks);
        props.put("batch.size", batchSize);
        props.put("linger.ms", lingerMs);
        props.put("retries", 0);
        props.put("request.timeout.ms", 15000);
        props.put("max.block.ms", 10000);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProducer = new KafkaProducer<String, String>(props);
        System.out.println("测试数据发送------------------------");
        kafkaProducer.send(new ProducerRecord<String, String>(topicName, "test-opening", "test connection..."), (metadata,exception)->{
            if(exception!=null){
                exception.printStackTrace();
                System.exit(1);
            }
        });
        MDC.put("ip","windows.hostname");
    }
    protected void subAppend(ILoggingEvent event) {
        //优先推送kafka
        try{
            this.appendKafka(event);
        }catch (Exception ex){
            addError("推送kafka失败",ex);
            System.err.println(ex);
            super.subAppend(event);
        }
    }
    private void appendKafka(ILoggingEvent event) throws Exception {
        byte[] encode = this.encoder.encode(event);
        String msg = new String(encode, "UTF-8");
        System.out.println("推送kafka:"+msg);
        LocalDateTime beginSend = LocalDateTime.now();
        kafkaProducer.send(new ProducerRecord<String, String>(topicName, "test-slf4j", msg),(metadata,exception)->{
            if(exception==null){
                LocalDateTime endSend = LocalDateTime.now();
                long useTime = endSend.toInstant(ZoneOffset.UTC).toEpochMilli() - beginSend.toInstant(ZoneOffset.UTC).toEpochMilli();
                System.out.println("useTime:"+useTime);
            }
        });
    }
}
