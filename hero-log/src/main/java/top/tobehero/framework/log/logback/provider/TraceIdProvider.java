package top.tobehero.framework.log.logback.provider;

import ch.qos.logback.classic.spi.ILoggingEvent;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;
import net.logstash.logback.composite.JsonWritingUtils;
import net.logstash.logback.composite.loggingevent.UuidProvider;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import java.io.IOException;

/***
 * 日志追踪ID
 */
public class TraceIdProvider extends UuidProvider {
    private static final String FIELD_NAME = "trace_id";
    private NoArgGenerator uuids = Generators.randomBasedGenerator();
    public TraceIdProvider(){
        setFieldName(FIELD_NAME);
    }

    @Override
    public void writeTo(JsonGenerator generator, ILoggingEvent iLoggingEvent) throws IOException {
        String oldTraceId = MDC.get(FIELD_NAME);
        String newTraceId = null;
        if(StringUtils.isBlank(oldTraceId)){
            newTraceId = uuids.generate().toString();
            MDC.put(FIELD_NAME,newTraceId);
        }else{
            newTraceId = oldTraceId;
        }
        JsonWritingUtils.writeStringField(generator, getFieldName(), newTraceId);
    }
}
