package top.tobehero.threads;

import lombok.Getter;

import java.util.concurrent.TimeUnit;

public class ProductorConsumerDemo {
    public static class ShareData{
        @Getter
        private volatile int store;
        private int maxStore;
        ShareData(int max){
            this.maxStore= max;
        }
        public synchronized void product(){
            while(store==maxStore){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
//            try {
//                TimeUnit.MILLISECONDS.sleep(200);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            store++;
            this.notifyAll();
        }
        public synchronized void consume(){
            while(store==0){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            store --;
            this.notifyAll();
        }
    }

    public static void main(String[] args) {
        ShareData shareData = new ShareData(10);
        for (int i = 0; i < 2; i++) {
            new Thread(()->{
                while(true){
                    shareData.product();
                    System.out.println(Thread.currentThread().getName()+"\t生产-->"+"\t"+shareData.getStore());
                }
            }).start();
        }

        for (int i = 0; i < 3; i++) {
            new Thread(()->{
                while(true){
                    shareData.consume();
                    System.out.println(Thread.currentThread().getName()+"\t消费-->"+"\t"+shareData.getStore());
                }
            }).start();
        }
    }

}
