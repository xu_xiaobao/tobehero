package top.tobehero.threads;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class MyCache{
    private volatile Map<String,Object> map = new HashMap<>();
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    public void put(String key,Object val){
        lock.writeLock().lock();
        try{
            System.out.println(Thread.currentThread().getName()+" put data begin \t"+key);
            map.put(key,val);
            System.out.println(Thread.currentThread().getName()+" put data end \t"+key);
            TimeUnit.MICROSECONDS.sleep(100);
        }catch (Exception ex){

        }finally {
            lock.writeLock().unlock();
        }
    }
    public Object get(String key){
        lock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName()+" get data\t"+key);
            return map.get(key);
        }catch (Exception ex){
            return null;
        }finally {
            lock.readLock().unlock();
        }
    }

}
public class ReadWriteLockTest {
    public static void main(String[] args) throws InterruptedException {
        MyCache myCache = new MyCache();

        for (int i = 0; i < 1; i++) {
            new Thread(()->{
                myCache.get("1");
            },"读_"+i).start();
        }
        TimeUnit.SECONDS.sleep(1);
        for (int i = 0; i < 1; i++) {
            int finalI = i;
            new Thread(()->{
                myCache.put(finalI+"","");
            },"写_"+i).start();
        }

    }

}
