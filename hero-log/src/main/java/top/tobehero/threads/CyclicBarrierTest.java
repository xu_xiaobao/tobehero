package top.tobehero.threads;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierTest {
    public static void main(String[] args) {
        int parties = 4;
        BarrierCallBack barrierAction = new BarrierCallBack();
        CyclicBarrier barrier = new CyclicBarrier(parties, barrierAction);
        barrierAction.setCyclicBarrier(barrier);
        int n=2;
        while(n-->0){
            for (int i = 0; i < parties-1; i++) {
                int finalI = i;
                int finalN = n;
                new Thread(()->{
                    System.out.println(finalN +"-->"+finalI);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(finalN +"-->结束");
                    try {
                        barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
            System.out.println("主线程等待");
            try {
                barrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
            System.out.println("主线程等待...结束");

        }

    }

    private static class BarrierCallBack implements Runnable {
        private CyclicBarrier cyclicBarrier;
        private int count;
        public CyclicBarrier getCyclicBarrier() {
            return cyclicBarrier;
        }

        public void setCyclicBarrier(CyclicBarrier cyclicBarrier) {
            this.cyclicBarrier = cyclicBarrier;
        }

        @Override
        public void run() {
            System.out.println("第"+(++count)+"轮结束");
        }
    }
}
