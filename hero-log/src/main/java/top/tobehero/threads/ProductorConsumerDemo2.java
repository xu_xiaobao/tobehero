package top.tobehero.threads;

import java.util.concurrent.ArrayBlockingQueue;

public class ProductorConsumerDemo2 {
    public static class ShareData{
        private ArrayBlockingQueue<Integer> queue;
        ShareData(int max){
            this.queue = new ArrayBlockingQueue<>(max);
        }
        public void product(Integer i){
            try {
                queue.put(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        public Integer consume(){
            Integer res = null;
            while(res==null){
                try {
                    res = queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return res;
        }
    }

    public static void main(String[] args) {
        ShareData shareData = new ShareData(10);
        for (int i = 0; i < 2; i++) {
            int finalI = i;
            new Thread(()->{
                while(true){
                    shareData.product(finalI);
                    System.out.println(Thread.currentThread().getName()+"\t生产-->"+"\t"+finalI);
                }
            }).start();
        }

        for (int i = 0; i < 3; i++) {
            new Thread(()->{
                while(true){
                    Integer consume = shareData.consume();
                    System.out.println(Thread.currentThread().getName()+"\t消费-->"+"\t"+consume);
                }
            }).start();
        }
    }

}
