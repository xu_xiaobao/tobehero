package top.tobehero.threads;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {
    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(0);
        for (int i = 0; i < 9; i++) {
            int finalI = i;
            new Thread(()->{
                System.out.println(finalI +"-->"+finalI);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(finalI +"-->结束");
                latch.countDown();
            }).start();
        }
        latch.countDown();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("主线程结束");
    }
}
