package top.tobehero.mianshi;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class GetWinners {
    private static class Person{
        private String id;
        private int factor;//倍率

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getFactor() {
            return factor;
        }

        public void setFactor(int factor) {
            this.factor = factor;
        }
    }

    public static String[] getWinners(Person[] persons,int number){
        List<String> res = new ArrayList<>();
        if(number>persons.length){
            for (Person p:persons) {
                res.add(p.getId());
            }
        }
        //计算倍率总值，这里实际应该考虑适应BigDecimal
        int total = 0;
        for (Person p:persons) {
            total += p.getFactor();
        }
        SecureRandom secureRandom = new SecureRandom();
        for (int i = 0; i < number; i++) {
            int target = secureRandom.nextInt(total)+1;
            int begin = 1;
            int end = 0;
            Person winner = null;
            for (int j = 0; j < persons.length; j++) {
                Person person = persons[j];
                end = begin + person.getFactor();
                if(target>=begin&&target<end){
                    winner = person;
                    break;
                }
                begin = end +1;
            }
            res.add(winner.getId());
        }
        return res.toArray(new String[res.size()]);
    }
}
