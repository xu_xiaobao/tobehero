package top.tobehero.study.singleton;

/**
 * 饥汉单例模式
 */

public class HungryMan {
    private static HungryMan instance = new HungryMan();
    private HungryMan(){}
    public static HungryMan getInstance(){
        return instance;
    }
}
