package top.tobehero.study.singleton;

public class LazyMan {
    private static volatile LazyMan instance = null;
    private LazyMan(){}
    public static LazyMan getInstance(){
        //第一层检查，volatile保证了可见性，如果实例已生成，则直接返回
        if(instance == null){
            //准备创建实例对象前，加锁，此时有可能发生两个线程都准备创建实例，所以创建前需要double-check
            synchronized(LazyMan.class){
                //第二层检查，防止多线程调用时，实例创建前虽然加锁但是由于可能A线程刚刚创建完实例，B线程得到锁也准备实例，所以需要再次检查实例是否为null
                if(instance==null){
                    instance = new LazyMan();
                }
            }
        }
        return instance;
    }
    public static synchronized LazyMan getInstance1(){
        if (instance==null){
            instance= new LazyMan();
        }
        return instance;
    }
}
