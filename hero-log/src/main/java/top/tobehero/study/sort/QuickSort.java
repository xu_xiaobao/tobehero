package top.tobehero.study.sort;

import java.security.SecureRandom;
import java.util.Arrays;

public class QuickSort {

    public void sort(int[] arr){
        sort1(arr,0,arr.length-1);
    }
//    private void sort(int[] arr,int start,int end){
//        if(start>=end) return;
//        int base = start + (end - start)/2;
//        int i = start;
//        int j = end;
//        int baseVal = arr[base];
//        while(i<j){
//            while((i<j)&&(arr[j]>baseVal)){
//                j--;
//            }
//            while((i<j)&&(arr[i]<baseVal)){
//                i++;
//            }
//            if((arr[i]==arr[j])&&(i<j)){
//                i++;
//            }else{
//                int tmp = arr[i];
//                arr[i] = arr[j];
//                arr[j] = tmp;
//            }
//        }
//        sort(arr,start,i-1);
//        sort(arr,i+1,end);
//    }
    private void sort1(int[] arr,int start,int end){
        if(start>=end) return;
        int i = start;
        int j = end;
        int middle = start + (end - start) / 2;
        int tmp = arr[middle];
        arr[middle] = arr[i];
        arr[i] = tmp;

        int baseVal = arr[i];
        while(i<j){
            //从右侧找到第一个比对比值小的索引位，将其放置
            while((i<j)&&(arr[j]>baseVal)){
                j--;
            }
            if(i<j) arr[i++] = arr[j];
            while((i<j)&&(arr[i]<baseVal)){
                i++;
            }
            if(i<j) arr[j--] = arr[i];
        }
        arr[i] = baseVal;
        sort1(arr,start,i-1);
        sort1(arr,i+1,end);
    }

    public static void main(String[] args) {
        int length = 1000;
        SecureRandom secureRandom = new SecureRandom();
        QuickSort quickSort = new QuickSort();
        for (int i = 0; i < 100000; i++) {
            int[] arr = new int[length];
            for (int j = 0; j < length; j++) {
                arr[j] = secureRandom.nextInt(length*10);
            }
            System.out.println("第"+(i+1)+"次排序");
            System.out.println("排序前："+Arrays.toString(arr));
            quickSort.sort(arr);
//            SortTest.quickSort(arr);
            System.out.println("排序后："+Arrays.toString(arr));
            Arrays.sort(arr);
            System.out.println("排序后："+Arrays.toString(arr));
            System.out.println("----------------------------------");
        }
    }
}
