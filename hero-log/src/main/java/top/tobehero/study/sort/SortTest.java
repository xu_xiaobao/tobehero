package top.tobehero.study.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class SortTest {
    /***
     * 冒泡排序
     * @param arr
     */
    public static void bubbleSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++) {//循环length-1次
            for (int j = 0; j < arr.length - 1 -i; j++) {
                if(arr[j]>arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }            
        }
    }

    /**
     * 简单选择排序
     * @param arr
     */
    public static void simpleSelect(int[] arr){
        for (int i = 0; i < arr.length-1; i++) {
            int maxIndex = 0;
            for (int j = 0; j < arr.length-i ; j++) {
                if(arr[j]>arr[maxIndex]){
                    maxIndex = j;
                }
            }
            //将最大值放入右侧
            int swapIndex = arr.length-1-i;
            int temp = arr[swapIndex];
            arr[swapIndex] = arr[maxIndex];
            arr[maxIndex] = temp;
        }
    }

    /**
     * 快速排序
     * @param arr
     */
    public static void quickSort(int[] arr){
        quickSort(arr,0,arr.length-1);
    }
    public static void quickSort(int[] arr,int left,int right){
        if(left>=right)return;
        //随机选择一个值作为比较值
        int splitIndex = left + (right - left) / 2;
        int base = arr[splitIndex];
        int l = left;
        int r = right;
        while (l<r){
            //从右侧开始扫描，如果l<r且值大于基准值，则r--
            while((l<r)&&(arr[r]>base)){
                r--;
            }
            //从左侧开始扫描，如果l<r且值小于基准值，则l++
            while((l<r)&&(arr[l]<base)){
                l++;
            }
            //如果两侧扫描后l=r，说明本次的数组拆分的索引位置为l+1,l+1的位置上符合左侧小，左侧大
            if(arr[l]==arr[r]&&(l<r)){
                splitIndex = l;
            }else{//否则互换l<->r
                int tmp = arr[l];
                arr[l] = arr[r];
                arr[r] = tmp;
            }
        }
        quickSort(arr,left,splitIndex-1);
        quickSort(arr,splitIndex+1,right);
    }

    /**
     * 直接插入排序
     * @param arr
     */
    public static void insertSort(int[] arr){
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j <i ; j++) {
                if(arr[j]>arr[i]){
                    int tmp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = tmp;
                }
            }
        }
    }

    @Test
    public void testSort(){
        int [] arr= new int[]{5,4,3,2,1,10,9,8,7,6,11,12,13,14};
//        int [] arr= new int[]{1,3,2};
        insertSort(arr);
        System.out.println(Arrays.toString(arr));
    }

}
