package top.tobehero.study;

import java.util.Stack;

public class 对称判断 {

    public static boolean isSymmetric(String str){
//        char[] chars = str.toCharArray();
//        Stack<Character> stack = new Stack<>();
//        for (int i = 0; i < chars.length; i++) {
//            char c = chars[i];
//            if(')'==c||']'==c||'}'==c){
//                if(stack.empty()){
//                    return false;
//                }if(')'==c){ if(stack.peek() =='(') stack.pop();
//                }else if(']'==c){if(stack.peek() =='[') stack.pop();
//                }else if('}'==c){if(stack.peek() =='{') stack.pop();
//                }
//            }else{
//                stack.push(c);
//            }
//        }
//        return stack.size()==0;

        char[] chars = str.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(stack.empty()){
                stack.push(c);
                continue;
            }
            if(')'==c && stack.peek() =='(') stack.pop();
            else if(']'==c && stack.peek() =='[') stack.pop();
            else if('}'==c && stack.peek() =='{') stack.pop();
            else stack.push(c);
        }
        return stack.empty();

    }

    public static void main(String[] args) {
        String str = "[";
        System.out.println(str+"\t"+isSymmetric(str));
        str = "[]";
        System.out.println(str+"\t"+isSymmetric(str));
        str = "[{}()]";
        System.out.println(str+"\t"+isSymmetric(str));
        str = "[{}()[]]";
        System.out.println(str+"\t"+isSymmetric(str));
        str = "[{()}]";
        System.out.println(str+"\t"+isSymmetric(str));
        str = "[{()}])";
        System.out.println(str+"\t"+isSymmetric(str));
        str = "[{()}](";
        System.out.println(str+"\t"+isSymmetric(str));
    }
}
