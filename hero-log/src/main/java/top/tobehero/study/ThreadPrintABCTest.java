package top.tobehero.study;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadPrintABCTest {
    private static volatile Integer state= 0;
    private static ReentrantLock lock = new ReentrantLock();
    private static Condition conditionA = lock.newCondition();
    private static Condition conditionB = lock.newCondition();
    private static Condition conditionC = lock.newCondition();


    static class PrintLetterThread extends Thread{
        private int num ;
        private String letter;
        private Condition self;
        private Condition next;
        private int repeatTimes = 10;
        public PrintLetterThread(int num,String letter,Condition self,Condition next){
            this.letter = letter;
            this.num = num;
            this.self = self;
            this.next = next;
        }
        @Override
        public void run() {
            lock.lock();
            try{
                while (--repeatTimes>=0){
                    while(state%3!=num){
                        self.await();
                    }
                    System.out.println(letter);
                    state++;
                    next.signal();
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
    }


    public static void main(String[] args) {
        new PrintLetterThread(0,"A",conditionA,conditionB).start();
        new PrintLetterThread(1,"B",conditionB,conditionC).start();
        new PrintLetterThread(2,"C",conditionC,conditionA).start();
    }
}
