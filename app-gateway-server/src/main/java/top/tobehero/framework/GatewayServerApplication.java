package top.tobehero.framework;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import top.tobehero.framework.common.feign.user.HeroServiceFeign;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(clients={HeroServiceFeign.class})
@EnableConfigurationProperties()
public class GatewayServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayServerApplication.class, args);
    }

    @Configuration
    public static class FeignExtConfiguration{
        @Bean
        @ConditionalOnMissingBean
        public HttpMessageConverters httpMessageConverters(){
            List<HttpMessageConverter<?>> httpMessageConverterList = new ArrayList<>();
            FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
            httpMessageConverterList.add(fastJsonHttpMessageConverter);
            HttpMessageConverters httpMessageConverters = new HttpMessageConverters(httpMessageConverterList);
            return httpMessageConverters;
        }
    }
}
