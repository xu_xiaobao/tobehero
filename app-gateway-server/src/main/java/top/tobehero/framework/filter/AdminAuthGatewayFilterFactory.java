package top.tobehero.framework.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import top.tobehero.framework.common.constants.AuthConstants;
import top.tobehero.framework.common.feign.user.HeroServiceFeign;
import top.tobehero.framework.common.utils.ResponseUtils;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class AdminAuthGatewayFilterFactory extends AbstractGatewayFilterFactory<AdminAuthGatewayFilterFactory.Config> {
    private static final String KEY_NEED_AURH = "needAuth";
    public AdminAuthGatewayFilterFactory() {
        super(AdminAuthGatewayFilterFactory.Config.class);
    }

    @Resource
    private HeroServiceFeign heroServiceFeign;
    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList(KEY_NEED_AURH);
    }
    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();
            boolean needAuth = config.isNeedAuth()&&!"/hero/online".equals(request.getPath().value());
            if(!needAuth){
                return chain.filter(exchange);
            }
            String auth_token = getValueFromRequest(request, AuthConstants.AUTH_TOKEM_KEY);
            if(StringUtils.isBlank(auth_token)){
                JSONObject resJson = new JSONObject();
                String errmsg = resJson.getString("errmsg");
                byte[] bits = JSON.toJSONString(ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNAUTHETICATED)).getBytes(StandardCharsets.UTF_8);
                DataBuffer buffer = response.bufferFactory().wrap(bits);
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                HttpHeaders responseHeaders = response.getHeaders();
                responseHeaders.add("Content-Type", "application/json;charset=UTF-8");
                return response.writeWith(Mono.just(buffer));
            }
            Boolean isAuth = true;
            try {
                isAuth = CompletableFuture.<Boolean>supplyAsync(()-> heroServiceFeign.isAuthed(auth_token)).get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(!isAuth){
                JSONObject resJson = new JSONObject();
                String errmsg = resJson.getString("errmsg");
                byte[] bits = JSON.toJSONString(ResponseUtils.outEnumResp(ResponseUtils.ResponseEnum.UNAUTHETICATED)).getBytes(StandardCharsets.UTF_8);
                DataBuffer buffer = response.bufferFactory().wrap(bits);
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                HttpHeaders responseHeaders = response.getHeaders();
                responseHeaders.add("Content-Type", "application/json;charset=UTF-8");
                return response.writeWith(Mono.just(buffer));
            }

            ServerHttpRequest.Builder reqBuilder = request.mutate()
//                    .header("userId", userId)
//                    .header("tokenId", tokenId)
//                    .header("appName",appName)
//                    .header("channel",channel)
                    ;
            ServerHttpRequest newRequest = reqBuilder.build();

            ServerHttpResponseDecorator decoratedResponse = new ServerHttpResponseDecorator(response) {
                @Override
                public HttpHeaders getHeaders() {
                    HttpHeaders headers = super.getHeaders();
                    return headers;
                }
            };
            ServerWebExchange newExchange = exchange.mutate().request(newRequest).response(decoratedResponse).build();
            return chain.filter(newExchange);
        };
    }


    private String getValueFromRequest(ServerHttpRequest request,String key){
        MultiValueMap<String, HttpCookie> cookies = request.getCookies();
        if(cookies!=null){
            HttpCookie httpCookie = cookies.getFirst(key);
            if(httpCookie!=null){
                String cookieVal = httpCookie.getValue();
                if (StringUtils.isNotBlank(cookieVal)){
                    return cookieVal;
                }
            }
        }
        HttpHeaders headers = request.getHeaders();
        if(headers!=null){
            String headerVal = headers.getFirst(key);
            if (StringUtils.isNotBlank(headerVal)){
                return headerVal;
            }
        }
        MultiValueMap<String, String> queryParams = request.getQueryParams();
        if(queryParams!=null){
            String paramVal = queryParams.getFirst(key);
            if (StringUtils.isNotBlank(paramVal)){
                return paramVal;
            }
        }
        return null;
    }


    public static class Config{
        /**是否校验token需要认证**/
        private boolean needAuth;

        public boolean isNeedAuth() {
            return needAuth;
        }

        public void setNeedAuth(boolean needAuth) {
            this.needAuth = needAuth;
        }
    }
}
