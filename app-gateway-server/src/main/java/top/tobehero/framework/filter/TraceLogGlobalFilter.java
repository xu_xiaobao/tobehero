package top.tobehero.framework.filter;

import org.slf4j.MDC;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import top.tobehero.framework.common.log.utils.LogUtils;

import static top.tobehero.framework.common.log.utils.LogUtils.Constants.HTTP_HEADER_TRACE_ID;

@Component
public class TraceLogGlobalFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        HttpHeaders headers = request.getHeaders();
        if(!headers.containsKey(HTTP_HEADER_TRACE_ID)){
            String traceId = LogUtils.generateTraceId();
            MDC.put(HTTP_HEADER_TRACE_ID,traceId);

            ServerHttpRequest.Builder reqBuilder = request.mutate()
                    .header(HTTP_HEADER_TRACE_ID, traceId)
                    ;
            ServerHttpRequest newRequest = reqBuilder.build();
            exchange = exchange.mutate().request(newRequest).build();
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE;
    }
}
