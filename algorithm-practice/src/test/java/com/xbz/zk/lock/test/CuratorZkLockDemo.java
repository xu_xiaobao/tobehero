package com.xbz.zk.lock.test;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;

import java.util.concurrent.TimeUnit;

public class CuratorZkLockDemo {
    public static void main(String[] args) {
        CuratorFramework client = CuratorZkLockUtils.getClient();
        for (int i = 0; i < 1; i++) {
            new Thread(()->{
                    String path = "/locks";
                    InterProcessMutex interProcessMutex = new InterProcessMutex(client, path);
                    try{
                        interProcessMutex.acquire();
                        System.out.println(Thread.currentThread().getName()+"\t处理业务逻辑");
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println(Thread.currentThread().getName()+"\t结束");
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }finally {
                        System.out.println(Thread.currentThread().getName()+"\t释放锁");
                        System.out.println();
                        System.out.println();
                        try {
                            interProcessMutex.acquire();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
            },"thread-"+i).start();
        }

    }
}
