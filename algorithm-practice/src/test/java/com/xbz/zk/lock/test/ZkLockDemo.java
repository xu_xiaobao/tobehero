package com.xbz.zk.lock.test;

import java.util.concurrent.TimeUnit;

public class ZkLockDemo {
    public static void main(String[] args) {
        ZooKeeperDistributedLock zkLock = new ZooKeeperDistributedLock("001");
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                zkLock.acquireDistributedLock();
                try{
                    System.out.println(Thread.currentThread().getName()+"\t处理业务逻辑");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+"\t结束");
                }catch (Exception ex){
                    ex.printStackTrace();
                }finally {
                    System.out.println(Thread.currentThread().getName()+"\t释放锁");
                    System.out.println();
                    System.out.println();
                    zkLock.unlock();
                }
            },"thread-"+i).start();
        }

    }
}
