package com.xbz.zk.lock.test;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class CuratorZkLockUtils {
    private static final String connectString = "127.0.0.1:2183";
    private static CuratorFramework client;
    static{
        // 重试策略，初始化每次重试之间需要等待的时间，基准等待时间为1秒。
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);

        // 使用默认的会话时间（60秒）和连接超时时间（15秒）来创建 Zookeeper 客户端
        client = CuratorFrameworkFactory.builder().
                connectString(connectString).
                connectionTimeoutMs(15 * 1000).
                sessionTimeoutMs(60 * 100).
                retryPolicy(retryPolicy).
                build();
        // 启动客户端
        client.start();
    }
    public static CuratorFramework getClient(){
        return client;
    }
}
