package top.tobehero.thread;

import java.util.concurrent.TimeUnit;

public class WaitNotify {
    public static  final String lock = "lock";
    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            doWork();
        },"workA").start();
        new Thread(()->{
            doWork();
        },"workB").start();
        TimeUnit.SECONDS.sleep(1);
        synchronized (lock){
            lock.notifyAll();//将等待池中的所有线程移动的锁池，参与锁竞争
//            lock.notify();//将等待池中的一个线程移动到锁池，参与锁竞争
        }
    }

    private static void doWork() {
        synchronized (lock){
            System.out.println(Thread.currentThread().getName()+"...wait");
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"...起来工作");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"...起来结束");
        }
    }

}
