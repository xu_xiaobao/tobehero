package top.tobehero.thread;

/**
 * a执行完毕后执行b
 * b执行完毕后执行c
 */
public class 顺序执行多线程任务 {
    public static volatile int state = 0;
    public static final String lock = "lock";
    public static int maxTimes = 10;
    public static void main(String[] args) {
        Thread threadC = new Thread(() -> {
            synchronized (lock){
                int times = maxTimes;
                for (int i = 0; i < times; i++) {
                    while(state != 2){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    state ++;
                    state = state%3;
                    System.out.println("Thread...C");
                    lock.notifyAll();
                }
            }
        });
        Thread threadB = new Thread(() -> {
            synchronized (lock) {
                int times = maxTimes;
                for (int i = 0; i < times; i++) {
                    while (state != 1) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    state++;
                    state = state % 3;
                    System.out.println("Thread...B");
                    lock.notifyAll();
                }
            }
        });
        Thread threadA = new Thread(() -> {
            synchronized (lock) {
                int times = maxTimes;
                for (int i = 0; i < times; i++) {
                    while (state != 0) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    state++;
                    state = state % 3;
                    System.out.println("Thread...A");
                    lock.notifyAll();
                }
            }
        });
        threadA.start();
        threadB.start();
        threadC.start();
    }
}
