package top.tobehero.work;

public abstract class AbstractRpcComponent<OI,II,IO> {

    /**
     * 使用内部服务输入调用内部服务，返回指定类型的响应
     * @param ii
     * @param outClass
     * @return
     */
    public abstract <O extends IO> RpcResponseWrapper<O> invokeService(II ii, Class<O> outClass);

    /**
     * 使用外部服务输出调用内部舒服，返回指定类型的响应
     * @param oi
     * @param outClass
     * @return
     */
    public <O extends IO> RpcResponseWrapper<O> execute(OI oi, Class<O> outClass){
        II ii = o2IConvert(oi);
        return this.invokeService(ii,outClass);
    }

    /**
     * 外部输出转内部输入
     * @param oi
     * @return
     */
    protected abstract II o2IConvert(OI oi);

    protected abstract boolean isOk(IO oi);

    protected abstract Object okConvertor(IO io);

    protected abstract Object errConvertor(IO io);
}
