package top.tobehero.work;

import lombok.Data;

@Data
public class RpcResponseWrapper<T>{
    private T response;
    private AbstractRpcComponent comp;

    public <OO> OO get(IConvertor<T, OO> okConvertor, IConvertor<T, OO> errConvertor){
        if(comp.isOk()){
            return okConvertor.convert(this.response);
        }else{
            return errConvertor.convert(this.response);
        }
    }

    public Object get(IConvertor<T,Object> okConvertor){
        return  this.get(okConvertor, (in)->comp.errConvertor(in));
    }

    public <OO> OO get(Class<OO> oClass){
        return this.get((in)->(OO)comp.okConvertor(in), (in)->(OO)comp.errConvertor(in));
    }

    public <OO> OO get(){
        return this.get((in)->(OO)comp.okConvertor(in), (in)->(OO)comp.errConvertor(in));
    }

}
