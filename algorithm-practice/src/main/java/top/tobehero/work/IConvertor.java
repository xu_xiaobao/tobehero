package top.tobehero.work;

@FunctionalInterface
public interface IConvertor<I,O> {
    O convert(I in);
}
