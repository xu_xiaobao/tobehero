package top.tobehero.work.ofc;

import top.tobehero.work.AbstractRpcComponent;
import top.tobehero.work.BaseRequest;
import top.tobehero.work.RpcResponseWrapper;
import top.tobehero.work.mp.model.MP20001Request;
import top.tobehero.work.mp.model.MP20001Response;
import top.tobehero.work.ofc.model.BaseOfcRequest;
import top.tobehero.work.ofc.model.BaseOfcResponse;
import top.tobehero.work.ofc.model.OFC315Response;

public class BaseOfcRpcComponent extends AbstractRpcComponent<BaseRequest, BaseOfcRequest, BaseOfcResponse> {
    @Override
    public <O extends BaseOfcResponse> RpcResponseWrapper<O> invokeService(BaseOfcRequest baseOfcRequest, Class<O> outClass) {
        return null;
    }

    @Override
    public BaseOfcRequest o2IConvert(BaseRequest baseRequest) {
        //固定特征的对象处理逻辑
        //返回一个内部请求的对象类型
        return new BaseOfcRequest();
    }

    @Override
    protected boolean isOk(BaseOfcResponse oi) {
        return false;
    }

    @Override
    protected Object okConvertor(BaseOfcResponse baseOfcResponse) {
        return null;
    }

    @Override
    protected Object errConvertor(BaseOfcResponse baseOfcResponse) {
        return null;
    }


    public static void main(String[] args) {
        BaseOfcRpcComponent ofcRpcComponent = new BaseOfcRpcComponent();
        MP20001Request oi = new MP20001Request();
        RpcResponseWrapper<OFC315Response> responseWrapper = ofcRpcComponent.execute(oi, OFC315Response.class);
        MP20001Response response = responseWrapper.get(MP20001Response.class);
        Object obj = responseWrapper.get((in) -> {
            Object o = new Object();
            return o;
        });
    }

}
