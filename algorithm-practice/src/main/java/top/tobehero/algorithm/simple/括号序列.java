package top.tobehero.algorithm.simple;

import java.util.Stack;

/**
 * 题目描述
 * 给出一个仅包含字符'(',')','{','}','['和']',的字符串，判断给出的字符串是否是合法的括号序列
 * 括号必须以正确的顺序关闭，"()"和"()[]{}"都是合法的括号序列，但"(]"和"([)]"不合法。
 */
public class 括号序列 {
    /**
     *
     * @param s string字符串
     * @return bool布尔型
     */
    public boolean isValid (String s) {
        // write code here
        Stack<Character> stack = new Stack<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(stack.size()==0){
                stack.push(c);
                continue;
            }
            if(c == ')'&&stack.peek()=='(')stack.pop();
            else if(c == ']'&&stack.peek()=='[')stack.pop();
            else if(c == '}'&&stack.peek()=='{')stack.pop();
            else stack.push(c);
        }
        return stack.empty();
    }

    public static void main(String[] args) {
        System.out.println(new 括号序列().isValid("()"));
    }

}
