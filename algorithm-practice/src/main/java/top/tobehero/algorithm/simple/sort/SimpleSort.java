package top.tobehero.algorithm.simple.sort;

import java.security.SecureRandom;
import java.util.Arrays;

/***
 * 简单排序
 */
public class SimpleSort {
    public static void sort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++) {
            int maxIndex = 0;
            for (int j = 1; j < arr.length - i; j++) {
                if(arr[maxIndex]<arr[j]){
                    maxIndex = j;
                }
            }
            int swapIndex = arr.length - 1 - i;
            int tmp = arr[swapIndex];
            arr[swapIndex] = arr[maxIndex];
            arr[maxIndex] = tmp;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 10, 9, 8, 7, 6, 12, 11, 13, 14, 15};
        System.out.println("排序前："+Arrays.toString(arr));
        for (int i = 0; i < 100; i++) {
            SecureRandom random = new SecureRandom();
            int a = random.nextInt(arr.length);
            int b = random.nextInt(arr.length);
            int tmp = arr[a];
            arr[a] = arr[b];
            arr[b] = tmp;
        }
        System.out.println("随机前："+Arrays.toString(arr));
        sort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }
}
