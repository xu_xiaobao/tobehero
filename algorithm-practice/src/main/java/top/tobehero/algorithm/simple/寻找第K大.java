package top.tobehero.algorithm.simple;

/**
 * 题目描述
 * 有一个整数数组，请你根据快速排序的思路，找出数组中第K大的数。
 *
 * 给定一个整数数组a,同时给定它的大小n和要找的K(K在1到n之间)，请返回第K大的数，保证答案存在。
 */
public class 寻找第K大 {
    public int findKth(int[] a, int n, int K) {
        // write code here
       return findKth(a,0,a.length-1,K-1);
    }

    private int findKth(int[] arr, int start, int end, int k) {
        if(start>end) return -1;
        int i = start;
        int j = end;
        int baseVal = arr[i+(j-i)/2];
        while(i<j){
            while (i<j&&arr[j]<baseVal){
                j--;
            }
            while (i<j&&arr[i]>baseVal){
                i++;
            }
            if(i<j&&arr[i]==arr[j]){
                i++;
            }else{
                int tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }
        }
        if(i==k){
            return arr[k];
        }else if(k<i){
            return findKth(arr,start,i-1,k);
        }else {
            return findKth(arr,i+1,end,k);
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 2, 2};
        for (int i = 1; i <= arr.length; i++) {
            int kth = new 寻找第K大().findKth(arr, arr.length, i);
            System.out.println("第"+(i)+"大："+kth);
        }
    }

}
