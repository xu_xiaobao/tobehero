package top.tobehero.algorithm.simple.sort;

import java.util.Arrays;

/**
 * 快速排序
 * (1)首先设定一个分界值，通过该分界值将数组分成左右两部分。
 * (2)将大于或等于分界值的数据集中到数组右边，小于分界值的数据集中到数组的左边。此时，左边部分中各元素都小于或等于分界值，而右边部分中各元素都大于或等于分界值。
 * (3)然后，左边和右边的数据可以独立排序。对于左侧的数组数据，又可以取一个分界值，将该部分数据分成左右两部分，同样在左边放置较小值，右边放置较大值。右侧的数组数据也可以做类似处理。
 * (4)重复上述过程，可以看出，这是一个递归定义。通过递归将左侧部分排好序后，再递归排好右侧部分的顺序。当左、右两个部分各数据排序完成后，整个数组的排序也就完成了。
 * 平均时间复杂度：nO(logN)，最差的时间复杂度O(N^2)
 *
 */
public class QuickSort {

    public void sort(int[] arr){
        sort(arr,0,arr.length-1);

    }

    private void sort(int[] arr,int start,int end){
        if(start>=end){
            return;
        }
        int i = start;
        int j = end;
        int baseVal = arr[i+(j-i)/2];
        while(i<j){
            while(i<j&&arr[j]>baseVal){
                j--;
            }
            while (i<j&&arr[i]<baseVal){
                i++;
            }
            if(i<j&&arr[i]==arr[j]){
                i++;
            }else{
                int tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }
        }
//        arr[i] = baseVal;
        sort(arr,start,i-1);
        sort(arr,i+1,end);
    }

    public static void main(String[] args) {
        int[] arr = new int[]{10,9,8,7,6,1,2,3,4,5,15,14,13,12,11};
//        arr = new int[] {1, 3, 5, 2, 2};
        System.out.println("排序前："+Arrays.toString(arr));
        new QuickSort().sort(arr);
        System.out.println("排序后："+Arrays.toString(arr));
    }
}
