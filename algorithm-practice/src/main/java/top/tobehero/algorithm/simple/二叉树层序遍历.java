package top.tobehero.algorithm.simple;

import java.util.ArrayList;

public class 二叉树层序遍历 {
    static class TreeNode {
        int val = 0;
        TreeNode left = null;
        TreeNode right = null;
    }

    public ArrayList<ArrayList<Integer>> levelOrder (TreeNode root) {
        ArrayList<ArrayList<Integer>> resList = new ArrayList<>();
        levelOrder(root, resList,0);
        return resList;
    }

    private void levelOrder(TreeNode root, ArrayList<ArrayList<Integer>> resList,int level) {
        if(root==null){
            return;
        }
        ArrayList<Integer> list = null;
        if(resList.size()==level){
            list = new ArrayList<>();
            resList.add(list);
        }
        list = resList.get(level);
        list.add(root.val);
        levelOrder(root.left, resList,level+1);
        levelOrder(root.right, resList,level+1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode();
        root.val = 3;
        TreeNode level_1_left = new TreeNode();
        TreeNode level_1_right = new TreeNode();
        root.left = level_1_left;
        root.right = level_1_right;
        level_1_left.val = 9;
        level_1_right.val = 20;
        TreeNode level_2_left = new TreeNode();
        TreeNode level_2_right = new TreeNode();
        level_1_right.left = level_2_left;
        level_1_right.right = level_2_right;
        level_2_left.val = 15;
        level_2_right.val = 7;


        ArrayList<ArrayList<Integer>> arrayLists = new 二叉树层序遍历().levelOrder(root);
        for (int i = 0; i < arrayLists.size(); i++) {
            ArrayList<Integer> integers = arrayLists.get(i);
            System.out.println("level:"+i+",numbers:"+ integers);
        }

    }
}
