package top.tobehero.algorithm.simple;

/**
 * 给定一个数组arr，返回子数组的最大累加和
 * 例如，arr = [1, -2, 3, 5, -2, 6, -1]，所有子数组中，[3, 5, -2, 6]可以累加出最大的和12，所以返回12.
 * 题目保证没有全为负数的数据
 * [要求]
 * 时间复杂度为O(n)O(n)，空间复杂度为O(1)
 * 参考解题思路：https://blog.csdn.net/m0_46204129/article/details/105870299
 */
public class 子数组的最大累加和问题 {
    public int maxsumofSubarray (int[] arr) {
        // write code here
        //延伸得到子数组
        int start = 0;
        int end = 0;

        int maxSum = 0;
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if(sum<0){
                sum = 0;
                start = i+1;
            }
            if(sum>maxSum){
                maxSum = sum;
                end = i;
            }
        }
        System.out.println("子数组位置："+start+"-"+end);
        return maxSum;
    }

    public static void main(String[] args) {
        int[] arr = {1, -2, 3, 5, -2, 6, -1};
        new 子数组的最大累加和问题().maxsumofSubarray(arr);
        System.out.println();
    }
}
