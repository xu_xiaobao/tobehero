package top.tobehero.algorithm.simple;

public class 判断链表是否有环 {
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }
    public static boolean isCycle(ListNode head){
        ListNode quick = head;
        ListNode slow = head;
        do{
            slow = slow.next;
            if(quick.next!=null){
                quick = quick.next.next;
            }else{
                quick = null;
            }
            if(slow == quick){
                return true;
            }
        }while(quick!=null&&slow!=null);
        return false;
    }

    public static void main(String[] args) {
        ListNode listNode0 = new ListNode(0);
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        listNode0.next = listNode1;
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;

        listNode4.next=listNode0;
        System.out.println(isCycle(listNode0));
    }
}
