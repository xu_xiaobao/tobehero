package top.tobehero.algorithm.simple;

/**
 *题目：给定一个 32 位有符号整数，将整数中的数字进行反转。
 * 示例 1:
 * 输入: 123
 * 输出: 321
 *
 * 示例 2:
 * 输入: -123
 * 输出: -321
 *
 * 示例 3:
 * 输入: 120
 * 输出: 21
 *
 * 假设我们的环境只能存储 32 位有符号整数，其数值范围是 [−2^31, 2^31 − 1]。根据这个假设，如果反转后的整数溢出，则返回 0。
 */
public class  数字翻转{
    public static int reverseNumber(int x){
        int rev = 0;
        while(x!=0){
            int pop = x%10;
            //防止整型上限溢出 rev*10+pop>MAX_VALUE
//            if(rev>Integer.MAX_VALUE/10||rev==Integer.MAX_VALUE/10&&pop>7){
//                return 0;
//            }
            //防止整型下限溢出 rev*10+pop<MIN_VALUE
//            if(rev<Integer.MIN_VALUE/10||rev==Integer.MIN_VALUE/10&&pop<-8){
//                return 0;
//            }
            //上述应该等价于下面
            if(rev>(Integer.MAX_VALUE-pop)/10||rev<(Integer.MIN_VALUE-pop)/10){
                return 0;
            }
            rev = rev * 10 + pop;
            x = x/10;
        }
        return rev;
    }

    public static void main(String[] args) {
        int x = Integer.MIN_VALUE;
        int y = reverseNumber(x);
        System.out.println("原值："+x+",翻转后："+y);
    }
}
