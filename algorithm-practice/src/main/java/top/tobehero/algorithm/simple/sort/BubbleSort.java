package top.tobehero.algorithm.simple.sort;

import java.util.Arrays;

/***
 * 冒泡排序
 */
public class BubbleSort {
    public static void sort(int[] arr){
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if(arr[j+1]<arr[j]){
                    int tmp = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 10, 9, 8, 7, 6, 12, 11, 13, 14, 15};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
