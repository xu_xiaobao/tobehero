package top.tobehero.algorithm.simple;

import java.util.ArrayList;

/**
 * 给定一个数组，找出其中最小的K个数。例如数组元素是4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4。如果K>数组的长度，那么返回一个空的数组
 */
public class 最小的K个数 {
    public ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
        ArrayList<Integer> resList = new ArrayList<>();
        if(input.length<k){
            return resList;
        }
        int maxIndex;
        for (int i = 0; i < input.length; i++) {
            int now = input[i];
            if(resList.size()<k){
                resList.add(now);
            }else if (resList.size()>0){
                maxIndex = 0;
                for (int j = 1; j < resList.size(); j++) {
                    if(resList.get(maxIndex)<resList.get(j)){
                        maxIndex = j;
                    }
                }
                Integer max = resList.get(maxIndex);
                if(now<max){
                    resList.set(maxIndex,now);
                }
            }
        }
        if(resList.size()>1){
            resList.sort((o1, o2) -> o1-o2);
        }
        return resList;
    }

    public static void main(String[] args) {
        int[] arr = {4,5,1,6,2,7,3,8};
        ArrayList<Integer> resList = new 最小的K个数().GetLeastNumbers_Solution(arr, 0);
        System.out.println(resList);
    }
}
