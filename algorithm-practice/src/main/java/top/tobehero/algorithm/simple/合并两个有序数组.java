package top.tobehero.algorithm.simple;

/**
 *题目描述
 * 给出两个有序的整数数组A和B，请将数组B合并到数组A中，变成一个有序的数组
 * 注意：
 * 可以假设A数组有足够的空间存放B数组的元素，A和B中初始的元素数目分别为m和 n
 */
public class 合并两个有序数组 {

    public void merge(int A[], int m, int B[], int n) {
//        if(n ==0){
//            return;
//        }
//        int aIndex = 0;
//        int aVal;
//        int bVal;
//        while(aIndex<m){
//            aVal = A[aIndex];
//            bVal = B[0];
//            //a数组值 小于 b数组值，满足升序，不交换，a索引++
//            if(aVal <= bVal){
//                aIndex++;
//            }else{ //b<a，交换数组值
//                B[0] = aVal;
//                A[aIndex] = bVal;
//                aIndex++;
//                //B数组需要一次冒泡排序
//                for (int i = 0; i < n-1; i++) {
//                    if(B[i]>B[i+1]){
//                        int tmp = B[i];
//                        B[i] = B[i+1];
//                        B[i+1] = tmp;
//                    }
//                }
//            }
//        }
//        for (int i = 0; i < n; i++) {
//            A[m+i] = B[i];
//        }

        /**
         * 两个数组，从右向左，谁大先放谁
         */
        int i = m -1;
        int j = n -1;
        int index = m + n - 1;
        while(i>=0 && j>=0 ){
            A[index--] = A[i]>B[j]?A[i--]:B[j--];
        }
        while (j >= 0){
            A[index -- ] = B[j--];
        }
    }
}
