package top.tobehero.algorithm.simple;

import java.util.Arrays;

public class 斐波那契数列 {
    public static int fibonacci(int n){
        int pre = 1;
        int next = 1;
        while(--n>0){
            int tmp = next;
            next = pre + next;
            pre = tmp;
        }
        return next;
    }

    public static void main(String[] args) {
        int length = 100;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = fibonacci(i);
        }
        System.out.println(Arrays.toString(arr));
    }
}
