package top.tobehero.algorithm.difficulty;

/**
 * 题目描述
 * 将给出的链表中的节点每k 个一组翻转，返回翻转后的链表
 * 如果链表中的节点数不是k 的倍数，将最后剩下的节点保持原样
 * 你不能更改节点中的值，只能更改节点本身。
 * 要求空间复杂度 O(1)
 * 例如：
 * 给定的链表是 1→2→3→4→5
 * 对于 k=2, 你应该返回 2→1→4→3→5
 * 对于 k=3, 你应该返回 3→2→1→4→5
 *
 * 结题思路：
 * 假设我们定义一个新的都节点
 * 通过分析。我们不难发现数据的插入规律
 * 【基准元素】-->【基-1】-->【基-2】。。。
 * 每组K个元素，都是插入到【基】与【基-1】的位置，每个周期之后变换【基】元素
 *
 * 假设我们的链表为 1   2   3   4   5   6   7   8
 * k=3
 * 我们创建一个元素[]作为我们的基准元素
 * 第一组
 * []   1
 * []   2   1
 * []   3   2   1
 * 不难看出此时的规律就是[]    []-1 的中位置进行插入元素
 * 第一组遍历完成后，我们记录[]变为1
 * 第二组
 * 1    4
 * 1    5   4
 * 1    6   5   4
 * 剩余节点不足，不需要遍历
 * 最终为[] 3  2   1   6   5   4   7   8
 *
 */
public class  链表中每K个节点一组翻转{
     class ListNode {
        int val;
        ListNode next = null;
     }
    /**
     *
     * @param head ListNode类
     * @param k int整型
     * @return ListNode类
     */
    public ListNode reverseKGroup (ListNode head, int k) {
        // write code here
        //判断需要进行几次翻转
        ListNode h = head;
        int length = 0;
        while(h!=null){
            h = h.next;
            length ++;
        }
        h = head;
        ListNode one = new ListNode();
        ListNode tail = null;
        ListNode nHead = one;
        for (int i = 0; i < length / k; i++) {
            for (int j = 0; j < k; j++) {
                if(j==0) tail = h;
                ListNode next = h.next;
                ListNode oneNext = one.next;
                one.next = h;
                h.next = oneNext;
                h = next;
            }
            one = tail;
        }
        if(tail !=null) tail.next = h;
        else return head;
        return nHead.next;
    }

}
