package top.tobehero.algorithm.basic;


public class MainDemo {
    public static class ListNode{
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }
    public static ListNode mergeList(ListNode list1,ListNode list2){
        if(list1==null){
            return list2;
        }
        if(list2 == null){
            return list1;
        }
        ListNode head = new ListNode(0);
        ListNode h = head;
        while(list1!=null&&list2!=null){
            if(list1.val<list2.val){
                h.next = list1;
                list1 = list1.next;
                h = h.next;
            }else{
                h.next = list2;
                list2 = list2.next;
                h = h.next;
            }
        }
        h.next = list1==null?list2:list1;
        return head.next;
    }
    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode1_1 = new ListNode(2);
        ListNode listNode1_2 = new ListNode(4);
        ListNode listNode1_3 = new ListNode(6);
        listNode1.next = listNode1_1;
        listNode1_1.next = listNode1_2;
        listNode1_2.next = listNode1_3;


        ListNode listNode2 = new ListNode(1);
        ListNode listNode2_1 = new ListNode(3);
        ListNode listNode2_2 = new ListNode(5);
        listNode2.next = listNode2_1;
        listNode2_1.next = listNode2_2;

        ListNode listNode = mergeList(listNode1, listNode2);
        System.out.println(listNode);
    }
}
