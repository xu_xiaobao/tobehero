package top.tobehero.algorithm;

public class 合并有序链表 {
     static class ListNode {
       int val;
       ListNode next = null;
     }
    /**
     *
     * @param l1 ListNode类
     * @param l2 ListNode类
     * @return ListNode类
     */
    public ListNode mergeTwoLists (ListNode l1, ListNode l2) {
//        // write code here
//        if(l1==null){
//            return l2;
//        }
//        if(l2==null){
//            return l1;
//        }
//        ListNode head = l1;
//        ListNode scanHead;
//        ListNode l1Tail = null;
//        while(l2!=null){
//            ListNode next = l2.next;
//            if(l2.val<=head.val){
//                l2.next = head;
//                head = l2;
//            }else{
//                scanHead = head;
//                while(scanHead!=null){
//                    ListNode l1Next = scanHead.next;
//                    if(l1Next==null){
//                        l1Tail = scanHead;
//                    }else if(scanHead.val<l2.val && l2.val <= l1Next.val){
//                        break;
//                    }
//                    scanHead = l1Next;
//                }
//                if(scanHead!=null){
//                    ListNode l1Next = scanHead.next;
//                    scanHead.next = l2;
//                    l2.next = l1Next;
//                }else{
//                    l2.next = null;
//                    l1Tail.next = l2;
//                }
//            }
//            l2 = next;
//        }
//        return head;
        ListNode tmp = new ListNode();
        ListNode curr = tmp;
        while(l1!=null&&l2!=null){
            if(l1.val<l2.val){
                curr.next = l1;
                l1 = l1.next;
            }else{
                curr.next = l2;
                l2 = l2.next;
            }
            curr = curr.next;
        }
        curr.next = l1==null?l2:l1;
        return tmp.next;
    }
}
