package top.tobehero.algorithm;

/**
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法（先后次序不同算不同的结果）。
 *  当n=1,f(n)=1;
 *  当n=2,f(2)=2;
 *  当n=3时：最后一跳有两种可能：跳一个台阶和跳两个台阶
 *  最后跳一个台阶则前面n-1个台阶的可能性时f(n-1)
 *  最后跳两个台阶，则前面n-2个台阶可能性时f(n-2)
 *  也就是说f(n)=f(n-1)+f(n-2),n>=3
 *
 *
 */
public class 跳台阶 {

    public int JumpFloor(int target) {
        if(target<=0){
            throw  new IllegalArgumentException("台阶必须为正整数");
        }
        if(target==1||target==2){
            return target;
        }
        int two = 2;//跳两个台阶的可能性
        int one = 1;//跳一个台阶的可能性
        int sum = 0 ;
        for (int i = 3; i <= target ; i++) {
            sum = one + two;
            one = two;
            two = sum;
        }
        return sum;
    }
}
