package top.tobehero.algorithm.middle;

import java.util.HashSet;

/**
 * 题目描述
 * 给定一个数组arr，返回arr的最长无的重复子串的长度(无重复指的是所有数字都不相同)。
 */
public class 找到字符串的最长无重复字符子串 {
    /**
     *
     * @param arr int整型一维数组 the array
     * @return int整型
     */
    public int maxLength (int[] arr) {
        // write code here
        int max = 0;
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            set.clear();
            for (int j = i; j < arr.length; j++) {
                boolean add = set.add(arr[j]);
                if(add){
                    if(set.size()>max){
                        max = set.size();
                    }
                }else{
                    break;
                }
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] arr = {2,2,3,4,3};
        System.out.println(new 找到字符串的最长无重复字符子串().maxLength(arr));
    }
}
