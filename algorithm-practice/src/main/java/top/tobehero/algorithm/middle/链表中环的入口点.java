package top.tobehero.algorithm.middle;

/**
 * 题目描述
 * 对于一个给定的链表，返回环的入口节点，如果没有环，返回null
 * 拓展：
 * 你能给出不利用额外空间的解法么？
 *
 * 结题思路
 *
 */
public class 链表中环的入口点 {
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    /**
     * 检测链表是否有环
     * @param head
     * @return
     */
    public ListNode detectCycle(ListNode head) {
//        ListNode h = new ListNode(0);
//        while(head!=null){
//            ListNode next = head.next;
//            ListNode nH = h.next;
//            boolean isContains = false;
//            while(nH!=null){
//                if(nH == head){
//                    isContains = true;
//                    break;
//                }
//                nH = nH.next;
//            }
//            if(isContains){
//                return head;
//            }
//            head.next = h.next;
//            h.next = head;
//            head = next;
//        }
        return null;
    }
}
