package top.tobehero.algorithm.middle;

import java.util.LinkedHashMap;
import java.util.Map;

public class LRUCache<K,V> {
    private int capacity;
    private LinkedHashMap<K,V> map;
    public LRUCache(int capacity){
        this.map = new LinkedHashMap<K,V>(capacity,0.75f,true){
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return this.size()>capacity;
            }
        };
    }
    public void put(K k,V v){
        this.map.put(k,v);
    }
    public V get(K k){
       return this.map.get(k);
    }

    @Override
    public String toString() {
        return this.map.toString();
    }

    public static void main(String[] args) {
        LRUCache<String, String> map = new LRUCache<String, String>(10);
        for (int i = 0; i < 11; i++) {
            map.put(i+"",i+"");
        }
        map.put("0","0");
        System.out.println(map);
    }

}
