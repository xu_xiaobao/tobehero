package top.tobehero.algorithm.middle;

/**
 * 题目描述
 * 给定一个链表，删除链表的倒数第 nn 个节点并返回链表的头指针
 * 例如，
 * 给出的链表为: 1→2→3→4→5, n=2.
 * 删除了链表的倒数第 n 个节点之后,链表变为1→2→3→5.
 *
 * 备注：
 * 题目保证 nn 一定是有效的
 * 请给出请给出时间复杂度为O(n) 的算法
 *
 * 考点：双指针遍历 确定移除的位置
 */
public class 删除链表倒数第N个节点 {
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }
    /**
     *
     * @param head ListNode类
     * @param n int整型
     * @return ListNode类
     */
    /**
     *
     * @param head ListNode类
     * @param n int整型
     * @return ListNode类
     */
    public ListNode removeNthFromEnd (ListNode head, int n) {
        // write code here
        if(head==null || n<1){
            return null;
        }
        ListNode fast=head;
        ListNode slow=head;

        for(int i=0;i<n;i++){
            fast=fast.next;
        }

        //如果n的值等于链表的长度,直接返回去掉头结点的链表
        if(fast==null){
            return head.next;
        }

        //走到倒数的第n+1个结点
        while(fast.next!=null){
            fast=fast.next;
            slow=slow.next;
        }
        //跳过倒数的第n个结点
        slow.next=slow.next.next;

        return head;
    }
}
