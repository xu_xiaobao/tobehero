package top.tobehero.algorithm.middle;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 三个线程分别打印a,l,i
 * 打印次数由a线程决定
 * 解题思路：
 *  定义一个全局currentState标识当前应该打印哪个字母
 *  一个lock+3个条件
 *  当currentState不属于当前线程时，就等待self.await，属于当前线程时输出字母，并设置currentState指向下一个线程next.signal
 */
public class 多线程打印 {
    private static volatile int currentState = 0;
    private static ReentrantLock lock = new ReentrantLock();
    private static Condition conditionA = lock.newCondition();
    private static Condition conditionB = lock.newCondition();
    private static Condition conditionC = lock.newCondition();
    public static class PrintLetter implements Runnable{
        private String letter ;
        private int state;
        private Condition self;
        private Condition next;
        private int repeat;

        public PrintLetter(String letter,int state, Condition self,Condition next,int repeat){
            this.letter = letter;
            this.state = state;
            this.self = self;
            this.next = next;
            this.repeat = repeat;
        }


        @Override
        public void run() {
            while(repeat-->0){
                lock.lock();
                try{
                    while(currentState!=state){
                        try {
                            self.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(letter);
                    currentState = ++currentState%3;
                    next.signal();
                }catch (Exception ex){
                    ex.printStackTrace();
                }finally {
                    lock.unlock();
                }
            }
        }
    }

    public static void main(String[] args) {
        int repeat = 0;
        Scanner scanner = new Scanner(System.in);
        String next = null;
        System.out.println("请输入一个正整数");
        while(repeat<=0){
            try{
                next = scanner.nextLine();
                repeat = Integer.valueOf(next);
                assert repeat>0;
            }catch (Exception ex){
                System.out.println("请输入一个合法的正整数！");
            }
        }
        new Thread(new PrintLetter("a",0,conditionA,conditionB,repeat)).start();
        new Thread(new PrintLetter("l",1,conditionB,conditionC,repeat)).start();
        new Thread(new PrintLetter("i",2,conditionC,conditionA,repeat)).start();
    }
}
