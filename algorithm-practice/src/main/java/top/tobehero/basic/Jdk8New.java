package top.tobehero.basic;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *  java 8 的新特性有哪些？
 *  1. 接口可以有默认实现
 *  2. Lambda 表达式和函数式接口
 *  3. Stream API：用函数式编程方式在集合类上进行复杂操作的工具，配合Lambda表达式可以方便的对集合进行处理。
 *  4. 方法引用
 *  5. 日期时间API
 */

public class Jdk8New {
    public static interface Jdk8Interface{
        default void helloJdk8Interface(String a) {
            System.out.println("hello jdk8 interface 默认实现");
        }
    }
    public static class Jdk8InterfaceImpl implements Jdk8Interface{
        @Override
        public void helloJdk8Interface(String a) {
            System.out.println("hello jdk8 interface 实际实现");
        }
    }
    public static void staticPrint(String word){
        System.out.println("staticPrint:"+word);
    }
    public void print(String word){
        System.out.println(word);
    }

    public static void main(String[] args) {
        //接口可以有默认实现
        new Jdk8InterfaceImpl().helloJdk8Interface("123");
        //支持lambda表达式和匿名函数
        new Thread(()->{
            System.out.println("hello lambda");
        }).start();
        //支持stream api 用函数式编程方式在集合类上进行复杂操作的工具，配合Lambda表达式可以方便的对集合进行处理。
        ArrayList<String> list = new ArrayList<>();
        list.add("1");
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        Stream<String> stream = list.stream();
        stream.forEach((i)->{
            System.out.println(i);
        });
        List<String> res = list.stream().filter((x) -> x == "5").collect(Collectors.toList());
        System.out.println(res);
        //方法引用：方法引用提供了非常有用的语法，可以直接引用已有Java类或对象（实例）的方法或构造器。与lambda联合使用，方法引用可以使语言的构造更紧凑简洁，减少冗余代码。
        list.stream().forEach(Jdk8New::staticPrint);//静态方法
        Jdk8New jdk8New = new Jdk8New();
        list.stream().forEach(jdk8New::print);//成员方法
        Supplier<List<String>> supplier1= () -> new  ArrayList<String>();
        supplier1= ArrayList::new;
    }

}
